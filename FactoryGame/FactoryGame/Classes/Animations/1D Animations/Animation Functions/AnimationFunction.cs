﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    abstract class AnimationFunction
    {
        // -------- Public Methods --------
        public abstract float GetOutputFraction(float inputFraction);
    }
}
