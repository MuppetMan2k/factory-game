﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class LinearAnimationFunction : AnimationFunction
    {
        // -------- Public Methods --------
        public override float GetOutputFraction(float inputFraction)
        {
            return inputFraction;
        }
    }
}
