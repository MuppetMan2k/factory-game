﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class SCurveAnimationFunction : AnimationFunction
    {
        // -------- Public Methods --------
        public override float GetOutputFraction(float inputFraction)
        {
            return (0.5f * (1 - Mathf.Cos(Mathf.Pi * inputFraction)));
        }
    }
}
