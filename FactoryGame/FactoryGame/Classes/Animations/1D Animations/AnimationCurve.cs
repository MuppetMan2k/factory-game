﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class AnimationCurve
    {
        // -------- Private Fields --------
        private AnimationFunction function;
        private float timer;
        // Parameters
        private float timeToRun;
        private bool useClockTime;
        private float outputMultiplier;
        private float runSpeedMultiplier;

        // -------- Constructors --------
        public AnimationCurve(AnimationFunction inFunction, float inTimeToRun, bool inUseClockTime, float inOutputMultiplier = 1f, float inRunSpeedMultiplier = 1f)
        {
            function = inFunction;
            timeToRun = inTimeToRun;
            useClockTime = inUseClockTime;
            outputMultiplier = inOutputMultiplier;
            runSpeedMultiplier = inRunSpeedMultiplier;

            timer = 0f;
        }

        // -------- Public Methods --------
        public void Update()
        {
            if (timer == timeToRun)
                return;

            timer += (useClockTime ? GameSpeedManager.ElapsedGameTime : GameTimeManager.ElapsedGameTime) * runSpeedMultiplier;

            if (timer > timeToRun)
                timer = timeToRun;
        }

        public void Reset()
        {
            timer = 0f;
        }
        public void Reset(float inTimeToRun)
        {
            timer = 0f;
            timeToRun = inTimeToRun;
        }

        public float GetOutput()
        {
            if (function == null)
                return 0f;

            if (timer == timeToRun)
                return 1f;

            float inputFraction = timer / timeToRun;
            return function.GetOutputFraction(inputFraction) * outputMultiplier;
        }
    }
}
