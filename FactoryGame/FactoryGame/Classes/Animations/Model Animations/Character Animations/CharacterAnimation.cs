﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class CharacterAnimation
    {
        // -------- Properties --------
        public List<float> TimeStamps { get; private set; }
        public List<CharacterPosition> Positions { get; private set;}
        public bool UseGameSpeed { get; set; }
        public float Duration
        {
            get
            {
                if (TimeStamps == null || TimeStamps.Count == 0)
                    return 0f;

                return TimeStamps.Last();
            }
        }

        // -------- Private Fields --------
        private float timer;

        // -------- Constructors --------
        public CharacterAnimation()
        {
            TimeStamps = new List<float>();
            Positions = new List<CharacterPosition>();
            UseGameSpeed = true;
            timer = 0f;
        }

        // -------- Public Methods --------
        public void Update()
        {
            if (Duration == 0f)
                return;

            timer += (UseGameSpeed ? GameSpeedManager.ElapsedGameTime : GameTimeManager.ElapsedGameTime);

            if (timer >= Duration)
                timer -= Duration;

            if (timer < 0f)
                timer = 0f;
        }

        public CharacterAnimation Clone()
        {
            return (CharacterAnimation)MemberwiseClone();
        }

        // Add position methods
        public void AddPosition(float timeStamp, CharacterPosition position)
        {
            TimeStamps.Add(timeStamp);
            Positions.Add(position);
        }
        public void AddPosition_BendOver(float timeStamp, float degrees)
        {
            if (!TimeStampExists(timeStamp))
                CreateNewPosition(timeStamp);

            GetCharacterPosition(timeStamp).BendOver = degrees;
        }
        public void AddPosition_TurnHead(float timeStamp, float degrees)
        {
            if (!TimeStampExists(timeStamp))
                CreateNewPosition(timeStamp);

            GetCharacterPosition(timeStamp).TurnHead = degrees;
        }
        public void AddPosition_LeftShoulder(float timeStamp, float degrees)
        {
            if (!TimeStampExists(timeStamp))
                CreateNewPosition(timeStamp);

            GetCharacterPosition(timeStamp).LeftShoulder = degrees;
        }
        public void AddPosition_LeftWrist(float timeStamp, float degrees)
        {
            if (!TimeStampExists(timeStamp))
                CreateNewPosition(timeStamp);

            GetCharacterPosition(timeStamp).LeftWrist = degrees;
        }
        public void AddPosition_RightShoulder(float timeStamp, float degrees)
        {
            if (!TimeStampExists(timeStamp))
                CreateNewPosition(timeStamp);

            GetCharacterPosition(timeStamp).RightShoulder = degrees;
        }
        public void AddPosition_RightWrist(float timeStamp, float degrees)
        {
            if (!TimeStampExists(timeStamp))
                CreateNewPosition(timeStamp);

            GetCharacterPosition(timeStamp).RightWrist = degrees;
        }
        public void AddPosition_LeftKick(float timeStamp, float degrees)
        {
            if (!TimeStampExists(timeStamp))
                CreateNewPosition(timeStamp);

            GetCharacterPosition(timeStamp).LeftKick = degrees;
        }
        public void AddPosition_RightKick(float timeStamp, float degrees)
        {
            if (!TimeStampExists(timeStamp))
                CreateNewPosition(timeStamp);

            GetCharacterPosition(timeStamp).RightKick = degrees;
        }

        public CharacterPosition GetPosition()
        {
            if (TimeStamps.Count == 1)
                return Positions[0];

            for (int i = 0; i < TimeStamps.Count - 1; i++)
            {
                float thisTime = TimeStamps[i];
                float nextTime = TimeStamps[i + 1];

                if (timer >= thisTime && timer <= nextTime)
                {
                    float lerp = (timer - thisTime) / (nextTime - thisTime);

                    CharacterPosition thisPosition = Positions[i];
                    CharacterPosition nextPosition = Positions[i + 1];

                    return CharacterPosition.Lerp(thisPosition, nextPosition, lerp);
                }
            }

            return Positions.Last();
        }

        // -------- Private Methods ---------
        private CharacterPosition GetCharacterPosition(float time)
        {
            for (int i = 0; i < TimeStamps.Count; i++)
            {
                if (TimeStamps[i] == time)
                    return Positions[i];
            }

            return null;
        }

        private bool TimeStampExists(float time)
        {
            return TimeStamps.Contains(time);
        }
        private void CreateNewPosition(float time)
        {
            if (TimeStamps.Count == 0)
            {
                // Create first position
                TimeStamps.Add(time);
                Positions.Add(CharacterPosition.Zero);
                return;
            }

            if (time > Duration)
            {
                // Create hold-extrapolated position at end
                TimeStamps.Add(time);
                Positions.Add(Positions.Last().Clone());
                return;
            }

            if (time < TimeStamps[0])
            {
                // Create hold-extrapolated position at beginning
                TimeStamps.Insert(0, time);
                Positions.Insert(0, Positions[0].Clone());
            }

            // Create interpolated position
            for (int i = 0; i < TimeStamps.Count - 1; i++)
            {
                float thisTime = TimeStamps[i];
                float nextTime = TimeStamps[i + 1];

                if (time > thisTime && time < nextTime)
                {
                    float interp = (time - thisTime) / (nextTime / thisTime);

                    CharacterPosition thisPosition = Positions[i];
                    CharacterPosition nextPosition = Positions[i + 1];
                    CharacterPosition interpPosition = CharacterPosition.Lerp(thisPosition, nextPosition, interp);

                    TimeStamps.Insert(i + 1, time);
                    Positions.Insert(i + 1, interpPosition);
                }
            }
        }
    }
}
