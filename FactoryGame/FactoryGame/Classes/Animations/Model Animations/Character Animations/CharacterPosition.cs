﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class CharacterPosition
    {
        // -------- Properties --------
        public float BendOver { get; set; }
        public float LeftShoulder { get; set; }
        public float LeftWrist { get; set; }
        public float RightShoulder { get; set; }
        public float RightWrist { get; set; }
        public float LeftKick { get; set; }
        public float RightKick { get; set; }
        public float TurnHead { get; set; }

        // -------- Public Methods --------
        public static CharacterPosition Lerp(CharacterPosition pos1, CharacterPosition pos2, float interp)
        {
            var cp = new CharacterPosition();

            cp.BendOver = Mathf.Lerp(pos1.BendOver, pos2.BendOver, interp);
            cp.LeftShoulder = Mathf.Lerp(pos1.LeftShoulder, pos2.LeftShoulder, interp);
            cp.LeftWrist = Mathf.Lerp(pos1.LeftWrist, pos2.LeftWrist, interp);
            cp.RightShoulder = Mathf.Lerp(pos1.RightShoulder, pos2.RightShoulder, interp);
            cp.RightWrist = Mathf.Lerp(pos1.RightWrist, pos2.RightWrist, interp);
            cp.LeftKick = Mathf.Lerp(pos1.LeftKick, pos2.LeftKick, interp);
            cp.RightKick = Mathf.Lerp(pos1.RightKick, pos2.RightKick, interp);
            cp.TurnHead = Mathf.Lerp(pos1.TurnHead, pos2.TurnHead, interp);

            return cp;
        }

        public CharacterPosition Clone()
        {
            return (CharacterPosition)MemberwiseClone();
        }

        // -------- Static Instances --------
        public static CharacterPosition Zero
        {
            get
            {
                var cp = new CharacterPosition();

                cp.BendOver = 0f;
                cp.LeftShoulder = 0f;
                cp.LeftWrist = 0f;
                cp.RightShoulder = 0f;
                cp.RightWrist = 0f;
                cp.LeftKick = 0f;
                cp.RightKick = 0f;
                cp.TurnHead = 0f;

                return cp;
            }
        }
    }
}
