﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataTypes;

namespace FactoryGame
{
    static class ContentList
    {
        // -------- Private Fields --------
        private static ContentListItem[] textureContentList;
        private static ContentListItem[] spriteFontContentList;
        private static ContentListItem[] modelContentList;
        private static ContentListItem[] effectContentList;

        // -------- Public Methods --------
        public static void LoadContent()
        {
            textureContentList = ContentLoader.LoadGameContent<ContentListItem[]>("XML/Asset Lists/TextureContentList");
            spriteFontContentList = ContentLoader.LoadGameContent<ContentListItem[]>("XML/Asset Lists/SpriteFontContentList");
            modelContentList = ContentLoader.LoadGameContent<ContentListItem[]>("XML/Asset Lists/ModelContentList");
            effectContentList = ContentLoader.LoadGameContent<ContentListItem[]>("XML/Asset Lists/EffectContentList");
        }

        // Get list item methods
        public static ContentListItem GetTextureListItem(string id)
        {
            foreach (ContentListItem listItem in textureContentList)
                if (listItem.id == id)
                    return listItem;

            return null;
        }
        public static ContentListItem GetSpriteFontListItem(string id)
        {
            foreach (ContentListItem listItem in spriteFontContentList)
                if (listItem.id == id)
                    return listItem;

            return null;
        }
        public static ContentListItem GetModelListItem(string id)
        {
            foreach (ContentListItem listItem in modelContentList)
                if (listItem.id == id)
                    return listItem;

            return null;
        }
        public static ContentListItem GetEffectListItem(string id)
        {
            foreach (ContentListItem listItem in effectContentList)
                if (listItem.id == id)
                    return listItem;

            return null;
        }
    }
}
