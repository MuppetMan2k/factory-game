﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace FactoryGame
{
    static class ContentLoader
    {
        // -------- Private Fields --------
		private static ContentManager content;

		// -------- Public Methods --------
		public static void SetUpContentManager(ContentManager inContentManager)
		{
			content = inContentManager;
			content.RootDirectory = "Content";
		}

        // Load methods
        public static T LoadGameContent<T>(string contentPath)
        {
            try
            {
                return content.Load<T>(contentPath);
            }
            catch (ContentLoadException e)
            {
                Debug.LogError("Tried to load content: " + contentPath + ". But it caused exception: " + e.ToString());
                return default(T);
            }
        }

        public static string GetContentRootDirectory()
        {
            return content.RootDirectory;
        }
    }
}
