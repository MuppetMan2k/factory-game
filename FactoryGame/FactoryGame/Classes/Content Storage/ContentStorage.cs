﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using DataTypes;

namespace FactoryGame
{
    static class ContentStorage
    {
        // -------- Private Fields --------
        // Loaded content
        private static Dictionary<string, Texture2D> loadedTextures;
        private static Dictionary<string, SpriteFont> loadedSpriteFonts;
        private static Dictionary<string, GameModel> loadedModels;
        private static Dictionary<string, Effect> loadedEffects;
        // Default content
        private static Texture2D defaultTexture;
        private static SpriteFont defaultSpriteFont;
        private static GameModel defaultModel;
        // Initial content id's
        private static List<string> initialTextures;
        private static List<string> initialSpriteFonts;
        private static List<string> initialModels;
        private static List<string> initialEffects;

        // -------- Public Methods --------
        public static void Initialize()
        {
            SetInitialTextureIds();
            SetInitialSpriteFontIds();
            SetInitialModelIds();
            SetInitialEffectIds();

            SetUpLoadedContentDicts();
        }

        public static void LoadInitialContent()
        {
            foreach (string id in initialTextures)
                LoadTextureFromContent(id);
            initialTextures = null;

            foreach (string id in initialSpriteFonts)
                LoadSpriteFontFromContent(id);
            initialSpriteFonts = null;

            foreach (string id in initialEffects)
                LoadEffectFromContent(id);
            initialEffects = null;

            foreach (string id in initialModels)
                LoadModelFromContent(id);
            initialModels = null;

            LoadDefaultAssets();
        }

        // Get content methods
        public static Texture2D GetTexture(string id)
        {
            if (!loadedTextures.ContainsKey(id))
                LoadTextureFromContent(id);

            if (!loadedTextures.ContainsKey(id))
                return defaultTexture;

            return loadedTextures[id];
        }
        public static SpriteFont GetSpriteFont(string id)
        {
            if (!loadedSpriteFonts.ContainsKey(id))
                LoadSpriteFontFromContent(id);

            if (!loadedSpriteFonts.ContainsKey(id))
                return defaultSpriteFont;

            return loadedSpriteFonts[id];
        }
        public static GameModel GetModel(string id)
        {
            if (!loadedModels.ContainsKey(id))
                LoadModelFromContent(id);

            if (!loadedModels.ContainsKey(id))
                return defaultModel;

            return loadedModels[id];
        }
        public static Effect GetEffect(string id)
        {
            if (!loadedEffects.ContainsKey(id))
                LoadEffectFromContent(id);

            if (!loadedEffects.ContainsKey(id))
                return null;

            return loadedEffects[id];
        }

        // -------- Private Methods --------
        private static void LoadTextureFromContent(string id)
        {
            if (loadedTextures.ContainsKey(id))
                return;

            ContentListItem contentItem = ContentList.GetTextureListItem(id);

            if (contentItem == null)
                return;

            Texture2D texture = ContentLoader.LoadGameContent<Texture2D>(contentItem.contentPath);

            if (texture == null)
                return;

            loadedTextures.Add(id, texture);
        }
        private static void LoadSpriteFontFromContent(string id)
        {
            if (loadedSpriteFonts.ContainsKey(id))
                return;

            ContentListItem contentItem = ContentList.GetSpriteFontListItem(id);

            if (contentItem == null)
                return;

            SpriteFont spriteFont = ContentLoader.LoadGameContent<SpriteFont>(contentItem.contentPath);
            loadedSpriteFonts.Add(id, spriteFont);
        }
        private static void LoadModelFromContent(string id)
        {
            if (loadedModels.ContainsKey(id))
                return;

            ContentListItem contentItem = ContentList.GetModelListItem(id);

            if (contentItem == null)
                return;

            Model model = ContentLoader.LoadGameContent<Model>(contentItem.contentPath);
            GameModel gameModel = GameModelManager.CreateGameModel(model);
            loadedModels.Add(id, gameModel);
        }
        private static void LoadEffectFromContent(string id)
        {
            if (loadedEffects.ContainsKey(id))
                return;

            ContentListItem contentItem = ContentList.GetEffectListItem(id);

            if (contentItem == null)
                return;

            Effect effect = ContentLoader.LoadGameContent<Effect>(contentItem.contentPath);
            loadedEffects.Add(id, effect);
        }

        private static void SetUpLoadedContentDicts()
        {
            loadedTextures = new Dictionary<string, Texture2D>();
            loadedSpriteFonts = new Dictionary<string, SpriteFont>();
            loadedModels = new Dictionary<string, GameModel>();
            loadedEffects = new Dictionary<string, Effect>();
        }

        private static void LoadDefaultAssets()
        {
            // Default texture
            defaultTexture = ContentLoader.LoadGameContent<Texture2D>("Assets/Default Assets/MagentaPixel");

            // Default sprite font
            defaultSpriteFont = ContentLoader.LoadGameContent<SpriteFont>("Assets/Default Assets/Arial12");

            // Default model
            defaultModel = GameModelManager.CreateGameModel(ContentLoader.LoadGameContent<Model>("Assets/Default Assets/MagentaCube"));
        }

        // Set initial content lists
        private static void SetInitialTextureIds()
        {
            initialTextures = new List<string>()
            {
                "pointer-cursor",
                "white-pixel",
                // TODO: Add initial texture id's here.
            };
        }
        private static void SetInitialSpriteFontIds()
        {
            initialSpriteFonts = new List<string>()
            {
                "arial-12",
                // TODO: Add initial sprite font id's here.
            };
        }
        private static void SetInitialModelIds()
        {
            initialModels = new List<string>()
            {
                "office-wall",
                "grass-tile",
                "paved-tile",
                "vinyl-tile",
                
                "brick-wall",
                "test-object-1",
                // TODO: Add initial model id's here.
            };
        }
        private static void SetInitialEffectIds()
        {
            initialEffects = new List<string>()
            {
                "simple-color",
                "simple-texture",
                "shaded-color",
                "shaded-texture",
                // TODO: Add initial effect id's here.
            };
        }
    }
}
