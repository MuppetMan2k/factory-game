﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    class CharacterGameModel
    {
        // -------- Properties --------
        public GameModel Body { get; set; }
        public GameModel Head { get; set; }
        public GameModel HeadPiece { get; set; }

        // -------- Constructors --------
        public CharacterGameModel(CharacterStyle inStyle)
        {
            Body = ContentStorage.GetModel(inStyle.BodyModelId);
            Head = ContentStorage.GetModel(inStyle.HeadModelId);
            if (!string.IsNullOrEmpty(inStyle.HeadPieceModelId))
                HeadPiece = ContentStorage.GetModel(inStyle.HeadPieceModelId);
        }
    }
}
