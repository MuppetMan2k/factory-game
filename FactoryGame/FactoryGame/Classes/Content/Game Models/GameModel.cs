﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FactoryGame
{
    class GameModel
    {
        // -------- Properties --------
        public Model Model { get; set; }
        public List<Triangle> ModelTriangles { get; set; }

        // -------- Private Fields --------
        private Matrix[] boneTransforms;

        // -------- Public Methods --------
        public void SetBoneTransforms(Matrix[] inBoneTransforms)
        {
            boneTransforms = inBoneTransforms;
        }

        public Matrix[] GetBoneTransforms()
        {
            return boneTransforms;
        }
    }
}
