﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    struct Triangle
    {
        // -------- Public Fields --------
        public Vector3 Vertex1;
        public Vector3 Vertex2;
        public Vector3 Vertex3;
        public string MeshName;
        public int boneIndex;

        // -------- Constructors --------
        public Triangle(Vector3 inVertex1, Vector3 inVertex2, Vector3 inVertex3, string inMeshName, int inBoneIndex)
        {
            Vertex1 = inVertex1;
            Vertex2 = inVertex2;
            Vertex3 = inVertex3;
            MeshName = inMeshName;
            boneIndex = inBoneIndex;
        }

        // -------- Public Methods --------
        public Triangle Transform(Matrix transform)
        {
            var triangle = new Triangle();

            triangle.Vertex1 = Vector3.Transform(Vertex1, transform);
            triangle.Vertex2 = Vector3.Transform(Vertex2, transform);
            triangle.Vertex3 = Vector3.Transform(Vertex3, transform);

            triangle.MeshName = MeshName;

            return triangle;
        }
    }
}
