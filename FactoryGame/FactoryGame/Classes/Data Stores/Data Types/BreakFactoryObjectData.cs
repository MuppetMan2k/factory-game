﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    struct BreakFactoryObjectData
    {
        // -------- Public Fields --------
        public string Id;
        public string ModelId;
        public bool[,] PassableMask;
        public bool[,] PlacableMask;
        public StaffNeedChange NeedChange;
        public float InteractionTime;
        public EnvironmentalFactors Environment;
    }
}
