﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    // -------- Enumerations --------
    public enum eElementLevel
    {
        RAW_MATERIAL,
        COMPONENT,
        PRODUCT
    }

    struct ElementData
    {
        // -------- Public Fields --------
        public string Id;
        public string IconTextureId;
        public string NameLocId;
        public int BaseValue;
        public eElementLevel Level;

        // -------- Constructors --------
        public ElementData(string inId, string inIconTextureId, string inNameLocId, eElementLevel inLevel, int inBaseValue = -1)
        {
            Id = inId;
            IconTextureId = inIconTextureId;
            NameLocId = inNameLocId;
            Level = inLevel;
            BaseValue = inBaseValue;
        }

        // -------- Static Instances --------
        public static ElementData Null
        {
            get
            {
                return new ElementData(null, null, null, eElementLevel.RAW_MATERIAL);
            }
        }
    }
}
