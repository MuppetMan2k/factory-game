﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    struct MachineData
    {
        // -------- Public Fields --------
        public string Id;
        public string ModelId;
        public string NameLocId;
        public int InStoreSize;
        public int OutStoreSize;
        public bool[,] PassableMask;
        public bool[,] PlacableMask;
        public eStatLevel Speed;
        public eStatLevel Noise;
        public List<ProcessData> Processes;
        public List<InteractionData> Interactions;
        public EnvironmentalFactors Environment;

        // -------- Public Methods --------
        public void SetProcesses(List<string> processIds)
        {
            Processes = new List<ProcessData>();
            foreach (string id in processIds)
                Processes.Add(AlmanacManager.GetProcess(id));
        }

        public void AddInteractionData(TileCoordinate inTileRelCoord, Vector2 inRelPosition, eSquareRotation inRelRotation, string inAnimation, eInteractionType inType)
        {
            if (Interactions == null)
                Interactions = new List<InteractionData>();

            InteractionData interaction = new InteractionData();
            interaction.TileRelCoord = inTileRelCoord;
            interaction.RelPosition = inRelPosition;
            interaction.RelRotation = inRelRotation;
            interaction.Animation = inAnimation;
            interaction.Type = inType;

            Interactions.Add(interaction);
        }

        // -------- Static Instances --------
        public static MachineData Null
        {
            get
            {
                return new MachineData();
            }
        }
    }
}
