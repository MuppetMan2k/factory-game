﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    struct ProcessData
    {
        // -------- Properties --------
        public string Id;
        public List<ElementData> ElementsIn;
        public ElementData ElementOut;
        public SkillData AssociatedSkill;
        public float BaseTime;

        // -------- Constructors --------
        public ProcessData(string inId, string inSkillId, float inBaseTime, string inElementOutId, string inElementInId)
        {
            Id = inId;
            AssociatedSkill = AlmanacManager.GetSkill(inSkillId);
            BaseTime = inBaseTime;
            ElementOut = AlmanacManager.GetElement(inElementOutId);
            ElementsIn = new List<ElementData>();
            ElementsIn.Add(AlmanacManager.GetElement(inElementInId));
        }
        public ProcessData(string inId, string inSkillId, float inBaseTime, string inElementOutId, string inElementIn1Id, string inElementIn2Id)
        {
            Id = inId;
            AssociatedSkill = AlmanacManager.GetSkill(inSkillId);
            BaseTime = inBaseTime;
            ElementOut = AlmanacManager.GetElement(inElementOutId);
            ElementsIn = new List<ElementData>();
            ElementsIn.Add(AlmanacManager.GetElement(inElementIn1Id));
            ElementsIn.Add(AlmanacManager.GetElement(inElementIn2Id));
        }
        public ProcessData(string inId, string inSkillId, float inBaseTime, string inElementOutId, string inElementIn1Id, string inElementIn2Id, string inElementIn3Id)
        {
            Id = inId;
            AssociatedSkill = AlmanacManager.GetSkill(inSkillId);
            BaseTime = inBaseTime;
            ElementOut = AlmanacManager.GetElement(inElementOutId);
            ElementsIn = new List<ElementData>();
            ElementsIn.Add(AlmanacManager.GetElement(inElementIn1Id));
            ElementsIn.Add(AlmanacManager.GetElement(inElementIn2Id));
            ElementsIn.Add(AlmanacManager.GetElement(inElementIn3Id));
        }

        // -------- Static Instances --------
        public static ProcessData Null
        {
            get
            {
                return new ProcessData(null, null, 0f, null, null);
            }
        }
    }
}
