﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    struct SkillData
    {
        // -------- Properties --------
        public string Id;
        public string IconTextureId;
        public string NameLocId;

        // -------- Constructors --------
        public SkillData(string inId, string inIconTextureId, string inNameLocId)
        {
            Id = inId;
            IconTextureId = inIconTextureId;
            NameLocId = inNameLocId;
        }

        // -------- Static Instances --------
        public static SkillData Null
        {
            get
            {
                return new SkillData(null, null, null);
            }
        }
    }
}
