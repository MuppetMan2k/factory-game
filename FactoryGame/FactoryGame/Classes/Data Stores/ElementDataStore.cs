﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    static class ElementDataStore
    {
        // -------- Public Methods --------
        public static Dictionary<string, ElementData> GetElementTypes()
        {
            var d = new Dictionary<string, ElementData>();

            // Create element types
            NewElementType(d, "test-element-1", "icon-element-test-1", "text_element_test_1", eElementLevel.RAW_MATERIAL, 1);
            NewElementType(d, "test-element-2", "icon-element-test-2", "text_element_test_2", eElementLevel.COMPONENT);
            // Add new element types here...

            return d;
        }

        // -------- Private Methods ---------
        private static void NewElementType(Dictionary<string, ElementData> d, string inId, string inIconTextureId, string inNameLocId, eElementLevel inLevel, int inBaseValue = -1)
        {
            d.Add(inId, new ElementData(inId, inIconTextureId, inNameLocId, inLevel, inBaseValue));
        }
    }
}
