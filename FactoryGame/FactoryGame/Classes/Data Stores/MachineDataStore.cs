﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    static class MachineDataStore
    {
        // -------- Public Methods --------
        public static Dictionary<string, MachineData> GetMachines()
        {
            var d = new Dictionary<string, MachineData>();

            // Create machines
            MachineData md;

            md = CreateMachineData("test-machine", "test-machine", "text_machine_name_test_machine", MachineManager.DefaultInStoreSize, MachineManager.DefaultOutStoreSize);
            md.PlacableMask = new bool[,] { { false, false } };
            md.PassableMask = new bool[,] { { false, false } };
            md.SetProcesses(new List<string>() { "test-process" });
            md.AddInteractionData(new TileCoordinate(1, 0), new Vector2(1, 1), eSquareRotation.ROTATION_180, "idle", eInteractionType.OPERATE);
            md.AddInteractionData(new TileCoordinate(1, 0), new Vector2(1, 1), eSquareRotation.ROTATION_180, "idle", eInteractionType.FIX);
            md.AddInteractionData(new TileCoordinate(0, -1), new Vector2(0, 0.5f), eSquareRotation.ROTATION_90, "idle", eInteractionType.PUT_DOWN);
            md.AddInteractionData(new TileCoordinate(0, 2), new Vector2(2, 0.5f), eSquareRotation.ROTATION_270, "idle", eInteractionType.PICK_UP);
            md.Noise = eStatLevel.LEVEL_1;
            md.Speed = eStatLevel.LEVEL_1;
            md.Environment = null;
            d.Add(md.Id, md);

            // Create new machines here...

            return d;
        }

        // -------- Private Methods ---------
        private static MachineData CreateMachineData(string id, string modelId, string nameLocId, int inStoreSize, int outStoreSize)
        {
            MachineData md = new MachineData();
            md.Id = id;
            md.ModelId = modelId;
            md.NameLocId = nameLocId;
            md.InStoreSize = inStoreSize;
            md.OutStoreSize = outStoreSize;

            return md;
        }
    }
}
