﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    static class ProcessDataStore
    {
        // -------- Public Methods --------
        public static Dictionary<string, ProcessData> GetProcesses()
        {
            var d = new Dictionary<string, ProcessData>();

            // Create processes
            NewProcess(d, "test-process", "test-skill", 10f, "test-element-2", "test-element-1");
            // Add new processes here...

            return d;
        }

        // -------- Private Methods ---------
        private static void NewProcess(Dictionary<string, ProcessData> d, string inId, string inSkillId, float inBaseTime, string inElementOutId, string inElementInId)
        {
            d.Add(inId, new ProcessData(inId, inSkillId, inBaseTime, inElementOutId, inElementInId));
        }
        private static void NewProcess(Dictionary<string, ProcessData> d, string inId, string inSkillId,  float inBaseTime, string inElementOutId, string inElementIn1Id, string inElementIn2Id)
        {
            d.Add(inId, new ProcessData(inId, inSkillId, inBaseTime, inElementOutId, inElementIn1Id, inElementIn2Id));
        }
        private static void NewProcess(Dictionary<string, ProcessData> d, string inId, string inSkillId,  float inBaseTime, string inElementOutId, string inElementIn1Id, string inElementIn2Id, string inElementIn3Id)
        {
            d.Add(inId, new ProcessData(inId, inSkillId, inBaseTime, inElementOutId, inElementIn1Id, inElementIn2Id, inElementIn3Id));
        }
    }
}
