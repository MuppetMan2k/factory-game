﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    static class SkillDataStore
    {
        // -------- Public Methods --------
        public static Dictionary<string, SkillData> GetSkills()
        {
            var d = new Dictionary<string, SkillData>();

            // Create skills
            NewSkill(d, "test-skill", "icon-skill-test", "text_skill_test");
            // Add new skills here...

            return d;
        }

        // -------- Private Methods ---------
        private static void NewSkill(Dictionary<string, SkillData> d, string inId, string inIconTextureId, string inNameLocId)
        {
            d.Add(inId, new SkillData(inId, inIconTextureId, inNameLocId));
        }
    }
}
