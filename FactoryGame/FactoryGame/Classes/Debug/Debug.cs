﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace FactoryGame
{
    static class Debug
    {
        // -------- Properties --------
        public static bool DrawDebugOutput { get; private set; }

        // -------- Private Fields --------
        private static List<DebugMessage> debugMessages;
        // Parameters
        private const int NUM_DEBUG_MESSAGES_TO_DRAW = 10;
        private const int MAX_NUM_DEBUG_MESSAGES = 100;
        private const int TEXT_HEIGHT = 14;
        private const int PIXELS_BEWTWEEN_DEBUG_MESSAGES = 5;
        private const float DEBUG_ALPHA = 0.5f;
        private const bool OPEN_DEBUG_ON_WARNING = true;

        // -------- Public Methods --------
        public static void Initialize()
        {
            DrawDebugOutput = false;
            debugMessages = new List<DebugMessage>();
        }

        public static void Update()
        {
            if (InputManager.KeyIsDown(Keys.Home))
                DrawDebugOutput = !DrawDebugOutput;
        }

        public static void SBDraw()
        {
            if (!DrawDebugOutput)
                return;

            // Draw window
            Color windowColor = new Color(new Vector4(0f, 0f, 0f, DEBUG_ALPHA));
            GraphicsManager.GetSpriteBatch().Draw(ContentStorage.GetTexture("white-pixel"), new Rectangle(0, 0, GraphicsManager.ScreenWidth, GetDebugWindowHeight()), windowColor);

            // Draw messages
            int startIndex = Mathf.Clamp(debugMessages.Count - NUM_DEBUG_MESSAGES_TO_DRAW, 0, MAX_NUM_DEBUG_MESSAGES);
            int endIndex = Mathf.Clamp(startIndex + NUM_DEBUG_MESSAGES_TO_DRAW - 1, 0, debugMessages.Count - 1);
            int i = 0;
            for (int m = startIndex; m <= endIndex; m++)
            {
                DebugMessage message = debugMessages[m];
                Vector2 position = new Vector2(PIXELS_BEWTWEEN_DEBUG_MESSAGES, (i + 1) * PIXELS_BEWTWEEN_DEBUG_MESSAGES + i * TEXT_HEIGHT);
                GraphicsManager.GetSpriteBatch().DrawString(ContentStorage.GetSpriteFont("arial-12"), message.Message, position, GetMessageColor(message.Type));

                i++;
            }
        }

        public static void Log(Object message)
        {
            CreateDebugMessage(eDebugMessageType.INFO, message.ToString());
        }
        public static void LogWarning(Object message)
        {
            CreateDebugMessage(eDebugMessageType.WARNING, message.ToString());

            if (OPEN_DEBUG_ON_WARNING)
                DrawDebugOutput = true;
        }
        public static void LogError(Object message)
        {
            CreateDebugMessage(eDebugMessageType.ERROR, message.ToString());

            if (OPEN_DEBUG_ON_WARNING)
                DrawDebugOutput = true;
        }

        // -------- Private Methods --------
        private static void CreateDebugMessage(eDebugMessageType type, string message)
        {
            DebugMessage newDebugMessage = new DebugMessage();
            newDebugMessage.Type = type;
            newDebugMessage.Message = message;

            debugMessages.Add(newDebugMessage);

            CleanDebugMessages();
        }

        private static void CleanDebugMessages()
        {
            if (debugMessages.Count > MAX_NUM_DEBUG_MESSAGES)
                debugMessages.RemoveRange(0, debugMessages.Count - NUM_DEBUG_MESSAGES_TO_DRAW);
        }

        private static int GetDebugWindowHeight()
        {
            return NUM_DEBUG_MESSAGES_TO_DRAW * TEXT_HEIGHT + (NUM_DEBUG_MESSAGES_TO_DRAW + 1) * PIXELS_BEWTWEEN_DEBUG_MESSAGES;
        }

        private static Color GetMessageColor(eDebugMessageType type)
        {
            Color color = new Color();

            switch (type)
            {
                case eDebugMessageType.INFO:
                    color = Color.White;
                    break;
                case eDebugMessageType.WARNING:
                    color = Color.Yellow;
                    break;
                case eDebugMessageType.ERROR:
                    color = Color.Red;
                    break;
            }

            //Vector4 colorVec4 = color.ToVector4();
            //colorVec4.W = debugAlpha;
            //color = new Color(colorVec4);

            return color;
        }
    }
}
