﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    // -------- Enumerations --------
    public enum eDebugMessageType
    {
        INFO,
        WARNING,
        ERROR
    }

    class DebugMessage
    {
        // -------- Properties --------
        public eDebugMessageType Type { get; set; }
        public string Message { get; set; }
    }
}
