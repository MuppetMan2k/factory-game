﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class IdleInstruction : Instruction
    {
        // -------- Properties --------
        public float IdleTime { get; set; }

        // -------- Constructors --------
        public IdleInstruction(Task inParentTask, float inIdleTime)
            : base(inParentTask)
        {
            IdleTime = inIdleTime;
        }
    }
}
