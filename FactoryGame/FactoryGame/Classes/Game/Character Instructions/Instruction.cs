﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    abstract class Instruction
    {
        // -------- Properties --------
        public Task ParentTask { get; set; }

        // -------- Constructors --------
        public Instruction(Task inParentTask)
        {
            ParentTask = inParentTask;
        }
    }
}
