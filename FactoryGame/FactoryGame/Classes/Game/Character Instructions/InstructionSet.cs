﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    // -------- Enumerations --------
    public enum eInstructionSetType
    {
        CLEAN,
        FIX,
        LOAD,
        OPERATE,
        TAKE_BREAK,
        IDLE,
        ENTER_FACTORY,
        EXIT_FACTORY
    }

    class InstructionSet
    {
        // -------- Properties --------
        public List<Instruction> Instructions { get; set; }
        public List<TileCoordinate> PathTiles { get; set; }
        public eInstructionSetType Type { get; set; }
        public Task ParentTask { get; set; }

        // -------- Constructors --------
        public InstructionSet()
        {
            Instructions = new List<Instruction>();
            PathTiles = new List<TileCoordinate>();
        }

        // -------- Public Methods --------
        public void Combine(InstructionSet nextInstructionSet)
        {
            Instructions.AddRange(nextInstructionSet.Instructions);
        }

        public void AddInstruction(Instruction instructionToAdd)
        {
            Instructions.Add(instructionToAdd);
        }

        public void OnFinishInstruction()
        {
            Instructions.RemoveAt(0);
        }

        public bool HasInstructionsToComplete()
        {
            if (Instructions == null)
                return false;

            if (Instructions.Count == 0)
                return false;

            return true;
        }
    }
}
