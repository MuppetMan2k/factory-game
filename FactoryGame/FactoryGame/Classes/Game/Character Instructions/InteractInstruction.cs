﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class InteractInstruction : Instruction
    {
        // -------- Properties --------
        public InteractableElement InteractableElement { get; set; }

        // -------- Constructors --------
        public InteractInstruction(Task inParentTask, InteractableElement inInteractableElement)
            : base(inParentTask)
        {
            InteractableElement = inInteractableElement;
        }
    }
}
