﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class MoveInstruction : Instruction
    {
        // -------- Properties --------
        public eSquareCompassDirection Direction { get; set; }

        // -------- Constructors --------
        public MoveInstruction(Task inParentTask, eSquareCompassDirection inDirection)
            : base(inParentTask)
        {
            Direction = inDirection;
        }
    }
}
