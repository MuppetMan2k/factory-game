﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class TeleportInstruction : Instruction
    {
        // -------- Properties --------
        public TileCoordinate Destination { get; private set; }

        // -------- Constructors --------
        public TeleportInstruction(Task inParentTask, TileCoordinate inDestination)
            : base(inParentTask)
        {
            Destination = inDestination;
        }
    }
}
