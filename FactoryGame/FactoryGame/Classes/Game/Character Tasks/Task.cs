﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    // -------- Enumerations --------
    public enum eTaskType
    {
        CLEAN,
        FIX,
        IDLE,
        LOAD,
        OPERATE,
        TAKE_BREAK,
        ENTER_FACTORY,
        EXIT_FACTORY
    }

    abstract class Task
    {
        // -------- Properties --------
        public eTaskType Type { get; protected set; }

        // -------- Public Methods --------
        public abstract InstructionSet GetInstructionSetForTask(Staff parent, out eTaskPriority priority);

        public virtual void OnTaskFinished()
        {
        }

        public virtual bool ShouldFindNewTaskAfterFinish()
        {
            return true;
        }
    }
}
