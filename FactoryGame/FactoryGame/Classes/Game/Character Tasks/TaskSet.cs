﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class TaskSet
    {
        // -------- Properties --------
        public Staff Parent { get; set; }
        public List<Task> Tasks { get; set; }
        public Task CurrentTask { get; set; }

        // -------- Constructors --------
        public TaskSet(Staff inParent)
        {
            Parent = inParent;
            Tasks = new List<Task>();
        }

        // -------- Public Methods --------
        public InstructionSet GetInstructionSet()
        {
            Task chosenTask;
            InstructionSet instructionSet = TaskManager.GetInstructionSetForTasks(Parent, out chosenTask);
            return instructionSet;
        }

        public List<ActiveTask> GetActiveTasks()
        {
            List<ActiveTask> activeTasks = new List<ActiveTask>();

            foreach (Task t in Tasks)
            {
                ActiveTask at = t as ActiveTask;
                if (at != null)
                    activeTasks.Add(at);
            }

            return activeTasks;
        }
        public List<PassiveTask> GetPassiveTasks()
        {
            List<PassiveTask> passiveTasks = new List<PassiveTask>();

            foreach (Task t in Tasks)
            {
                PassiveTask pt = t as PassiveTask;
                if (pt != null)
                    passiveTasks.Add(pt);
            }

            return passiveTasks;
        }

        public int GetActiveTaskCount()
        {
            return GetActiveTasks().Count;
        }
        public int GetPassiveTaskCount()
        {
            return GetPassiveTasks().Count;
        }
    }
}
