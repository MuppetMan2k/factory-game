﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class CleanTask : PassiveTask
    {
        // -------- Constructors --------
        public CleanTask()
        {
            Type = eTaskType.CLEAN;
        }

        // -------- Public Methods --------
        public override InstructionSet GetInstructionSetForTask(Staff parent, out eTaskPriority priority)
        {
            List<Trash> trash = TrashManager.GetAllTrash();

            if (trash.Count == 0)
            {
                priority = eTaskPriority.ZERO_PRIORITY;
                return null;
            }

            List<Path> possiblePaths = new List<Path>();
            List<Trash> possibleTrash = new List<Trash>();

            foreach (Trash t in trash)
            {
                if (!t.Interactability.GetInteractableElement_CleanUp().IsAvailable(parent))
                    continue;

                Path path;
                if (PathFindingManager.TryFindPath(parent.CharacterCoordinate.GetApproxTileCoordinate(), t.MiniTileCoordinate.TileCoordinate, out path))
                {
                    possiblePaths.Add(path);
                    possibleTrash.Add(t);
                }
            }

            if (possiblePaths.Count == 0)
            {
                priority = eTaskPriority.ZERO_PRIORITY;
                return null;
            }

            Path chosenPath = possiblePaths[0];
            int chosenPathLength = chosenPath.Tiles.Count;
            Trash chosenTrash = possibleTrash[0];

            for (int i = 1; i < possiblePaths.Count; i++)
            {
                if (possiblePaths[i].Tiles.Count < chosenPathLength)
                {
                    chosenPath = possiblePaths[i];
                    chosenPathLength = possiblePaths[i].Tiles.Count;
                    chosenTrash = possibleTrash[i];
                }
            }

            InstructionSet instructions = InstructionManager.CreateInstructionsFromPath(chosenPath, this, eInstructionSetType.CLEAN);
            instructions.AddInstruction(new InteractInstruction(this, chosenTrash.Interactability.GetInteractableElement_CleanUp()));
            instructions.ParentTask = this;

            priority = eTaskPriority.MEDIUM_PRIORITY_JOB;
            return instructions;
        }
    }
}
