﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class EnterFactoryTask : PassiveTask
    {
        // -------- Constructors --------
        public EnterFactoryTask()
        {
            Type = eTaskType.ENTER_FACTORY;
        }

        // -------- Public Methods --------
        public override InstructionSet GetInstructionSetForTask(Staff parent, out eTaskPriority priority)
        {
            priority = eTaskPriority.MAX_PRIORITY_SYSTEM;

            InstructionSet instructions = new InstructionSet();
            instructions.Type = eInstructionSetType.ENTER_FACTORY;
            for (int i = 0; i < FloorManager.FactoryExitPathLength; i++)
            {
                MoveInstruction move = new MoveInstruction(this, FloorManager.FactoryEntryDirection);
                instructions.AddInstruction(move);
            }
            instructions.ParentTask = this;

            return instructions;
        }
    }
}
