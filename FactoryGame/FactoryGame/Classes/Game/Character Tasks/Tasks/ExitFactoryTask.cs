﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class ExitFactoryTask : PassiveTask
    {
        // -------- Private Fields --------
        private Staff parentStaff;

        // -------- Constructors --------
        public ExitFactoryTask()
        {
            Type = eTaskType.EXIT_FACTORY;
        }

        // -------- Public Methods --------
        public override InstructionSet GetInstructionSetForTask(Staff parent, out eTaskPriority priority)
        {
            parentStaff = parent;

            InstructionSet instructions;
            priority = eTaskPriority.MAX_PRIORITY_SYSTEM;

            Path pathToDoorTile;
            bool canReachDoorTile = PathFindingManager.TryFindPath(parent.CharacterCoordinate.GetApproxTileCoordinate(), FloorManager.FactoryDoorTile, out pathToDoorTile);

            if (canReachDoorTile)
            {
                // Walk to door tile
                instructions = InstructionManager.CreateInstructionsFromPath(pathToDoorTile, this, eInstructionSetType.EXIT_FACTORY);
            }
            else
            {
                // Teleport to door tile
                instructions = new InstructionSet();
                instructions.Type = eInstructionSetType.EXIT_FACTORY;
                instructions.AddInstruction(new TeleportInstruction(this, FloorManager.FactoryDoorTile));
            }

            // Walk out of factory
            InstructionSet walkOutInstructions = new InstructionSet();
            walkOutInstructions.Type = eInstructionSetType.EXIT_FACTORY;
            for (int i = 0; i < FloorManager.FactoryExitPathLength; i++)
            {
                MoveInstruction move = new MoveInstruction(this, FloorManager.FactoryExitDirection);
                walkOutInstructions.AddInstruction(move);
            }

            instructions.Combine(walkOutInstructions);
            instructions.ParentTask = this;
            return instructions;
        }

        public override void OnTaskFinished()
        {
            base.OnTaskFinished();
            CharacterManager.RemoveCharacter(parentStaff);
            GameManager.OnStaffExitFactory(parentStaff);
        }

        public override bool ShouldFindNewTaskAfterFinish()
        {
            return false;
        }
    }
}
