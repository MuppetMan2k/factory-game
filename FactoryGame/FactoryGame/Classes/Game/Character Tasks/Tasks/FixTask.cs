﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class FixTask : PassiveTask
    {
        // -------- Constructors --------
        public FixTask()
        {
            Type = eTaskType.FIX;
        }

        // -------- Public Methods --------
        public override InstructionSet GetInstructionSetForTask(Staff parent, out eTaskPriority priority)
        {
            List<Machine> machines = FactoryObjectManager.GetAllMachines();

            if (machines.Count == 0)
            {
                priority = eTaskPriority.ZERO_PRIORITY;
                return null;
            }

            List<Path> possiblePaths = new List<Path>();
            List<Machine> possibleMachines = new List<Machine>();

            foreach (Machine m in machines)
            {
                if (!m.Broken)
                    continue;

                Path path;
                TileCoordinate fixCoord = m.Interactability.GetInteractableElement_Fix().GetInteractionTileCoordinate();
                if (PathFindingManager.TryFindPath(parent.CharacterCoordinate.GetApproxTileCoordinate(), fixCoord, out path))
                {
                    possiblePaths.Add(path);
                    possibleMachines.Add(m);
                }
            }

            if (possiblePaths.Count == 0)
            {
                priority = eTaskPriority.ZERO_PRIORITY;
                return null;
            }

            Path chosenPath = possiblePaths[0];
            int chosenPathLength = chosenPath.Tiles.Count;
            Machine chosenMachine = possibleMachines[0];

            for (int i = 1; i < possiblePaths.Count; i++)
            {
                if (possiblePaths[i].Tiles.Count < chosenPathLength)
                {
                    chosenPath = possiblePaths[i];
                    chosenPathLength = possiblePaths[i].Tiles.Count;
                    chosenMachine = possibleMachines[i];
                }
            }

            InstructionSet instructions = InstructionManager.CreateInstructionsFromPath(chosenPath, this, eInstructionSetType.FIX);
            instructions.AddInstruction(new InteractInstruction(this, chosenMachine.Interactability.GetInteractableElement_Fix()));
            instructions.ParentTask = this;

            priority = eTaskPriority.HIGH_PRIORITY_JOB;
            return instructions;
        }
    }
}
