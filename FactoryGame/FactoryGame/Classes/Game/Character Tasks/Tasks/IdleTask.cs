﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class IdleTask : PassiveTask
    {
        // -------- Private Fields --------
        // Constant params
        private const int MAX_NUM_IDLE_COORD_ATTEMPTS = 3;

        // -------- Constructors --------
        public IdleTask()
        {
            Type = eTaskType.IDLE;
        }

        // -------- Public Methods --------
        public override InstructionSet GetInstructionSetForTask(Staff parent, out eTaskPriority priority)
        {
            InstructionSet instructions = null;

            int attempt = 1;
            bool foundIdleCoord = false;
            while (!foundIdleCoord && attempt <= MAX_NUM_IDLE_COORD_ATTEMPTS)
            {
                TileCoordinate idleCoord = TileManager.GetRandomPurchasedTileCoord();

                instructions = CreateIdleInstructionSet(idleCoord, parent);

                foundIdleCoord = (instructions != null);
                attempt++;
            }

            // Couldn't find idle target, so perform stationary idle
            if (!foundIdleCoord)
                instructions = CreateStationaryIdleInstructionSet(parent);

            instructions.ParentTask = this;

            priority = eTaskPriority.ZERO_PRIORITY;
            return instructions;
        }

        // -------- Private Methods --------
        private InstructionSet CreateIdleInstructionSet(TileCoordinate coord, Character character)
        {
            // Square to idle in must be placable
            if (!PassPlaceMapManager.GetPlacableFloorMap()[coord.Row, coord.Col])
                return null;

            Path path;
            if (PathFindingManager.TryFindPath(character.CharacterCoordinate.GetApproxTileCoordinate(), coord, out path))
            {
                InstructionSet instructions = InstructionManager.CreateInstructionsFromPath(path, CharacterManager.GetCharacterIdleTask(), eInstructionSetType.IDLE);
                IdleInstruction idle = new IdleInstruction(CharacterManager.GetCharacterIdleTask(), InstructionManager.GetIdleInstructionTime());
                instructions.AddInstruction(idle);

                return instructions;
            }
            else
                return null;
        }
        private InstructionSet CreateStationaryIdleInstructionSet(Character character)
        {
            InstructionSet instructions = new InstructionSet();
            instructions.PathTiles.Clear();
            instructions.Type = eInstructionSetType.IDLE;
            IdleInstruction idle = new IdleInstruction(CharacterManager.GetCharacterIdleTask(), InstructionManager.GetIdleInstructionTime());
            instructions.AddInstruction(idle);

            return instructions;
        }
    }
}
