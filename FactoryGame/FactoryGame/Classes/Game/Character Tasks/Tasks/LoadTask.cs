﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class LoadTask : ActiveTask
    {
        // -------- Properties --------
        public InteractableElement PickUpInteractability { get; set; }
        public InteractableElement PutDownInteractability { get; set; }

        // -------- Constructors --------
        public LoadTask()
        {
            Type = eTaskType.LOAD;
        }

        // -------- Public Methods --------
        public override InstructionSet GetInstructionSetForTask(Staff parent, out eTaskPriority priority)
        {
            if (!PickUpInteractability.IsAvailable(parent))
            {
                priority = eTaskPriority.ZERO_PRIORITY;
                return null;
            }

            if (!PutDownInteractability.IsAvailable(parent))
            {
                priority = eTaskPriority.ZERO_PRIORITY;
                return null;
            }

            if (GetPickUpElementStore().IsEmpty())
            {
                priority = eTaskPriority.ZERO_PRIORITY;
                return null;
            }

            if (GetPutDownElementStore().IsFull())
            {
                priority = eTaskPriority.ZERO_PRIORITY;
                return null;
            }

            if (ElementManager.GetElementIndexToLoad(this) == -1)
            {
                priority = eTaskPriority.ZERO_PRIORITY;
                return null;
            }

            Path path1;
            Path path2;
            bool canCompletePath1 = PathFindingManager.TryFindPath(parent.CharacterCoordinate.GetApproxTileCoordinate(), PickUpInteractability.GetInteractionTileCoordinate(), out path1);
            bool canCompletePath2 = PathFindingManager.TryFindPath(PickUpInteractability.GetInteractionTileCoordinate(), PutDownInteractability.GetInteractionTileCoordinate(), out path2);

            if (canCompletePath1 && canCompletePath2)
            {
                InstructionSet instructions = InstructionManager.CreateInstructionsFromPath(path1, this, eInstructionSetType.LOAD);
                instructions.AddInstruction(new InteractInstruction(this, PickUpInteractability));
                instructions.Combine(InstructionManager.CreateInstructionsFromPath(path2, this, eInstructionSetType.LOAD));
                instructions.AddInstruction(new InteractInstruction(this, PutDownInteractability));
                instructions.ParentTask = this;

                priority = GetTaskPriority(parent);
                return instructions;
            }

            priority = eTaskPriority.ZERO_PRIORITY;
            return null;
        }

        public ElementStore GetPickUpElementStore()
        {
            PickUpInteraction pickUp = PickUpInteractability.Interaction as PickUpInteraction;
            return pickUp.ParentElementStore;
        }
        public ElementStore GetPutDownElementStore()
        {
            PutDownInteraction putDown = PutDownInteractability.Interaction as PutDownInteraction;
            return putDown.ParentElementStore;
        }

        // -------- Private Methods ---------
        private eTaskPriority GetTaskPriority(Staff parent)
        {
            if (parent.Type == eStaffType.LOADER)
            {
                if (GetPickUpElementStore().IsFull() || GetPutDownElementStore().IsEmpty())
                    return eTaskPriority.HIGH_PRIORITY_JOB;

                return eTaskPriority.MEDIUM_PRIORITY_JOB;
            }
            else
            {
                if (GetPickUpElementStore().IsFull() || GetPutDownElementStore().IsEmpty())
                    return eTaskPriority.MEDIUM_PRIORITY_JOB;

                return eTaskPriority.LOW_PRIORITY_JOB;
            }
        }
    }
}
