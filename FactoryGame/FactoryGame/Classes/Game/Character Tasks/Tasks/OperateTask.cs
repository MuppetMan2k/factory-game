﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class OperateTask : ActiveTask
    {
        // -------- Properties --------
        public InteractableElement OperateInteractability { get; set; }
        public ProcessData OperationProcess { get; set; }

        // -------- Constructors --------
        public OperateTask()
        {
            Type = eTaskType.OPERATE;
        }

        // -------- Public Methods --------
        public override InstructionSet GetInstructionSetForTask(Staff parent, out eTaskPriority priority)
        {
            if (!OperateInteractability.IsAvailable(parent))
            {
                priority = eTaskPriority.ZERO_PRIORITY;
                return null;
            }

            Machine machineParent = OperateInteractability.Parent as Machine;
            if (machineParent != null)
            {
                if (machineParent.Broken)
                {
                priority = eTaskPriority.ZERO_PRIORITY;
                return null;
                }

                if (!machineParent.OutElementStore.ContainsSpaceForProcess(OperationProcess) || !machineParent.InElementStore.ContainsElementsForProcess(OperationProcess))
                {
                    priority = eTaskPriority.ZERO_PRIORITY;
                    return null;
                }
            }

            Path path;
            if (PathFindingManager.TryFindPath(parent.CharacterCoordinate.GetApproxTileCoordinate(), OperateInteractability.GetInteractionTileCoordinate(), out path))
            {
                InstructionSet instructions = InstructionManager.CreateInstructionsFromPath(path, this, eInstructionSetType.OPERATE);
                instructions.AddInstruction(new InteractInstruction(this, OperateInteractability));
                instructions.ParentTask = this;

                priority = GetTaskPriority();
                return instructions;
            }

            priority = eTaskPriority.ZERO_PRIORITY;
            return null;
        }

        // -------- Private Methods ---------
        private eTaskPriority GetTaskPriority()
        {
            Machine machineParent = OperateInteractability.Parent as Machine;
            if (machineParent != null)
            {
                if (machineParent.OutElementStore.IsEmpty() || machineParent.InElementStore.IsFull())
                    return eTaskPriority.HIGH_PRIORITY_JOB;
            }

            return eTaskPriority.MEDIUM_PRIORITY_JOB;
        }
    }
}
