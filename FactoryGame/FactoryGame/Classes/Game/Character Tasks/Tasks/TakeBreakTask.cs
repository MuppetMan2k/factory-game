﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class TakeBreakTask : BackgroundTask
    {
        // -------- Constructors --------
        public TakeBreakTask()
        {
            Type = eTaskType.TAKE_BREAK;
        }

        // -------- Public Methods --------
        public override InstructionSet GetInstructionSetForTask(Staff parent, out eTaskPriority priority)
        {
            List<BreakFactoryObject> breakFOs = FactoryObjectManager.GetAllBreakFactoryObjects();

            if (breakFOs.Count == 0)
            {
                priority = eTaskPriority.ZERO_PRIORITY;
                return null;
            }

            List<Path> possiblePaths = new List<Path>();
            List<BreakFactoryObject> possibleBreakFOs = new List<BreakFactoryObject>();

            foreach (BreakFactoryObject bfo in breakFOs)
            {
                if (!bfo.Interactability.GetInteractableElement_TakeBreak().IsAvailable(parent))
                    continue;

                Path path;
                TileCoordinate bfoCoord = bfo.Interactability.GetInteractableElement_TakeBreak().GetInteractionTileCoordinate();
                if (PathFindingManager.TryFindPath(parent.CharacterCoordinate.GetApproxTileCoordinate(), bfoCoord, out path))
                {
                    possiblePaths.Add(path);
                    possibleBreakFOs.Add(bfo);
                }
            }

            if (possiblePaths.Count == 0)
            {
                priority = eTaskPriority.ZERO_PRIORITY;
                return null;
            }

            TakeBreakInteraction breakInteraction = possibleBreakFOs[0].Interactability.GetInteractableElement_TakeBreak().Interaction as TakeBreakInteraction;

            Path chosenPath = possiblePaths[0];
            eTaskPriority chosenPriority = StaffNeedManager.GetMaxPriorityForBreak(parent.Needs, breakInteraction.NeedChange);
            BreakFactoryObject chosenBreakFO = possibleBreakFOs[0];

            for (int i = 1; i < possiblePaths.Count; i++)
            {
                breakInteraction = possibleBreakFOs[i].Interactability.GetInteractableElement_TakeBreak().Interaction as TakeBreakInteraction;
                eTaskPriority newPriority = StaffNeedManager.GetMaxPriorityForBreak(parent.Needs, breakInteraction.NeedChange);

                if (TaskManager.TaskShouldBePrioritizedOverCurrent(chosenPriority, newPriority))
                {
                    chosenPath = possiblePaths[i];
                    chosenPriority = newPriority;
                    chosenBreakFO = possibleBreakFOs[i];
                }
            }

            InstructionSet instructions = InstructionManager.CreateInstructionsFromPath(chosenPath, this, eInstructionSetType.TAKE_BREAK);
            instructions.AddInstruction(new InteractInstruction(this, chosenBreakFO.Interactability.GetInteractableElement_TakeBreak()));
            instructions.ParentTask = this;

            priority = chosenPriority;
            return instructions;
        }
    }
}
