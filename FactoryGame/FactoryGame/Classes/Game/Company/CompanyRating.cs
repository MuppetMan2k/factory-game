﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    struct CompanyRating
    {
        // -------- Public Fields --------
        public float QualityRating;
        public float ElementNumberRating;
        public float ReliabilityRating;
    }
}
