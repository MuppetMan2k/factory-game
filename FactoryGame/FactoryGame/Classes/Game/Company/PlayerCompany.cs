﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class PlayerCompany : Company
    {
        // -------- Properties --------
        public TraitSet Traits { get; set; }
        public SkillSet Skills { get; set; }
    }
}
