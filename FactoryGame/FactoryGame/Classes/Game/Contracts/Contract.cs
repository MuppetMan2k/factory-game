﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class Contract
    {
        // -------- Properties --------
        public ElementData Element { get; set; }
        public int ElementNumber { get; set; }
        public NonPlayerCompany AssociatedCompany { get; set; }
        public ElementQuality QualityLimit { get; set; }
    }
}
