﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    abstract class DataView
    {
        // -------- Public Methods --------
        public abstract void Activate();
        public abstract void Deactivate();

        public abstract bool ForcesColor();
        public virtual Vector3 GetForcedColor(GameEntity entity, Room room)
        {
            return new Vector3();
        }
    }
}
