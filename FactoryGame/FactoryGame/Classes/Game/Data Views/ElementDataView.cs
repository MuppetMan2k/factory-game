﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    class ElementDataView : DataView
    {
        // -------- Private Fields --------
        private static Vector3 emptyColor = new Vector3(0.0f, 1.0f, 0.0f);
        private static Vector3 fullColor = new Vector3(1.0f, 0.0f, 0.0f);

        // -------- Public Methods --------
        public override void Activate()
        {
            ElementCarryingUIManager.Activated = true;
            ElementCarryingUIManager.CreateAllElementCarryingUI();
            MouseOverSelectionFilterManager.SetSelectionFilter(MouseOverSelectionFilter.UIOnly);
        }
        public override void Deactivate()
        {
            ElementCarryingUIManager.Activated = false;
            ElementCarryingUIManager.DestroyAllElementCarryingUI();
            MouseOverSelectionFilterManager.SetSelectionFilter(MouseOverSelectionFilter.Default);
        }

        public override bool ForcesColor()
        {
            return true;
        }
        public override Vector3 GetForcedColor(GameEntity entity, Room room)
        {
            List<ElementCarrying> carryings = GameEntityUtilities.GetGameEntityElementCarryings(entity);

            if (carryings.Count > 0)
            {
                int totalSpaces = 0;
                int totalElements = 0;

                foreach (ElementCarrying c in carryings)
                {
                    if (!c.Limitless)
                    {
                        totalSpaces += c.NumElementHolders;
                        totalElements += c.GetNumberElements();
                    }
                }

                if (totalSpaces == 0)
                    return emptyColor;

                float fullnessRatio = (float)totalElements / (float)totalSpaces;
                return Vector3.Lerp(emptyColor, fullColor, fullnessRatio);
            }

            return new Vector3(1f, 1f, 1f);
        }
    }
}
