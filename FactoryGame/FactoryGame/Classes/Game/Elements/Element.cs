﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class Element
    {
        // -------- Properties --------
        public ElementData Data { get; set; }
        public ElementQuality Quality { get; set; }
        public bool QualityVisible { get; set; }

        // -------- Constructors --------
        public Element(ElementData inData, ElementQuality inQuality, bool inQualityVisible)
        {
            Data = inData;
            Quality = inQuality;
            QualityVisible = inQualityVisible;
        }

        // -------- Public Methods --------
        public string GetQualityString()
        {
            if (QualityVisible)
                return Quality.GetQualityString();

            return Localization.LocalizeString("text_element_quality_unknown");
        }

        public string GetWholeQualityString()
        {
            string qualityString = string.Format(Localization.LocalizeString("text_element_quality_title"), GetQualityString());
            return qualityString;
        }
    }
}
