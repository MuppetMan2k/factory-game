﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    struct ElementCarrying
    {
        // -------- Public Fields --------
        public string LocId;
        public int NumElementHolders;
        public List<ElementHolder> ElementHolders;
        public bool Limitless;

        // -------- Public Methods --------
        public int GetNumberElements()
        {
            int numElements = 0;
            foreach (ElementHolder h in ElementHolders)
                if (h.IsHoldingElement())
                    numElements++;

            return numElements;
        }

        public string GetNumberMoreElementsText()
        {
            if (!Limitless)
            {
                Debug.LogWarning("Tried to get number more elements text of a limited-size element carrying");
                return "";
            }

            int numberMoreElements = GetNumberElements() - NumElementHolders;

            if (numberMoreElements <= 0)
                return "";
            if (numberMoreElements == 1)
            {
                string moreElementsText = Localization.LocalizeString("text_elemcarry_more_element");
                moreElementsText = string.Format(moreElementsText, numberMoreElements.ToString());
                return moreElementsText;
            }
            else
            {
                string moreElementsText = Localization.LocalizeString("text_elemcarry_more_elements");
                moreElementsText = string.Format(moreElementsText, numberMoreElements.ToString());
                return moreElementsText;
            }
        }
    }
}
