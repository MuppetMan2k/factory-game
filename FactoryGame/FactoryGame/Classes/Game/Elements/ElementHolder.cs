﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class ElementHolder
    {
        // -------- Properties --------
        public Element Element { get; private set; }

        // -------- Public Methods --------
        public void GiveElement(Element newElement)
        {
            Element = newElement;
        }
        public Element TakeElement()
        {
            Element returnElement = Element;
            Element = null;
            return returnElement;
        }

        public bool IsHoldingElement()
        {
            return (Element != null);
        }
    }
}
