﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    // -------- Enumerations --------
    public enum eElementQualityLevel
    {
        LEVEL_F,
        LEVEL_E,
        LEVEL_D,
        LEVEL_C,
        LEVEL_B,
        LEVEL_A
    }

    struct ElementQuality
    {
        // -------- Properties --------
        public eElementQualityLevel Level
        {
            get
            {
                eElementQualityLevel[] qualityLevels = EnumUtilities.GetEnumValues<eElementQualityLevel>();
                int numLevels = qualityLevels.Length;
                float band = 1f / (float)numLevels;

                for (int i = 0; i < numLevels; i++)
                    if (Value <= (i + 1) * band)
                        return qualityLevels[i];

                return qualityLevels[0];
            }
        }

        // -------- Public Fields --------
        public float Value;

        // -------- Constructors --------
        public ElementQuality(float inValue)
        {
            Value = inValue;
        }

        // -------- Public Methods --------
        public string GetQualityString()
        {
            switch (Level)
            {
                case eElementQualityLevel.LEVEL_A:
                    return Localization.LocalizeString("text_element_quality_A");
                case eElementQualityLevel.LEVEL_B:
                    return Localization.LocalizeString("text_element_quality_B");
                case eElementQualityLevel.LEVEL_C:
                    return Localization.LocalizeString("text_element_quality_C");
                case eElementQualityLevel.LEVEL_D:
                    return Localization.LocalizeString("text_element_quality_D");
                case eElementQualityLevel.LEVEL_E:
                    return Localization.LocalizeString("text_element_quality_E");
                case eElementQualityLevel.LEVEL_F:
                    return Localization.LocalizeString("text_element_quality_F");
                default:
                    return "";
            }
        }
    }
}
