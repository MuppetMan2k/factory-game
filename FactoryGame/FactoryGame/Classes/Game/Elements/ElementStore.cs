﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class ElementStore
    {
        // -------- Properties --------
        public List<ElementHolder> ElementHolders { get; set; }

        // -------- Private Fields --------
        protected int storeSize;

        // -------- Constructors --------
        public ElementStore(int inSize)
        {
            storeSize = inSize;
            ElementHolders = new List<ElementHolder>();
            for (int i = 0; i < storeSize; i++)
                ElementHolders.Add(new ElementHolder());
        }

        // -------- Public Methods --------
        public bool IsEmpty()
        {
            return (NumberElements() == 0);
        }
        public virtual bool IsFull()
        {
            return (NumberElements() == storeSize);
        }

        public int NumberElements()
        {
            int number = 0;
            for (int i = 0; i < storeSize; i++)
                if (ElementHolders[i].IsHoldingElement())
                    number++;

            return number;
        }
        
        public virtual int GetVisibleElementHolderNumber()
        {
            return storeSize;
        }

        public virtual bool ContainsSpaceForProcess(ProcessData process)
        {
            return (NumberSpaces() >= 1);
        }

        public bool ContainsElementsForProcess(ProcessData process)
        {
            List<ElementData> storeElementTypes = new List<ElementData>();
            foreach (ElementHolder h in ElementHolders)
                if (h.IsHoldingElement())
                    storeElementTypes.Add(h.Element.Data);

            foreach (ElementData et in process.ElementsIn)
            {
                if (storeElementTypes.Remove(et))
                    continue;

                return false;
            }

            return true;
        }
        public List<Element> GetElementsForProcess(ProcessData process)
        {
            List<Element> elements = new List<Element>();
            List<int> indicesIncluded = new List<int>();

            foreach (ElementData d in process.ElementsIn)
            {
                for (int i = 0; i < storeSize; i++)
                {
                    if (indicesIncluded.Contains(i))
                        continue;

                    ElementHolder h = ElementHolders[i];

                    if (!h.IsHoldingElement())
                        continue;

                    if (h.Element.Data.Equals(d))
                    {
                        elements.Add(h.Element);
                        indicesIncluded.Add(i);
                        continue;
                    }
                }
            }

            return elements;
        }

        public Element TakeElement(ElementData data)
        {
            for (int i = 0; i < storeSize; i++)
            {
                ElementHolder h = ElementHolders[i];

                if (!h.IsHoldingElement())
                    continue;

                if (h.Element.Data.Equals(data))
                    return TakeElement(i);
            }

            return null;
        }
        public virtual Element TakeElement(int index)
        {
            Element element = ElementHolders[index].TakeElement();
            CascadeElementsDown(index);
            return element;
        }

        public virtual void AddElement(Element element)
        {
            for (int i = 0; i < storeSize; i++)
            {
                if (!ElementHolders[i].IsHoldingElement())
                {
                    ElementHolders[i].GiveElement(element);
                    return;
                }
            }

            Debug.LogWarning("Tried to add element to store but there is no space.");
        }

        public void ClearElements()
        {
            ElementHolders.Clear();
        }

        // -------- Private Methods --------
        private int NumberSpaces()
        {
            return (storeSize - NumberElements());
        }

        protected void CascadeElementsDown(int index)
        {
            for (int i = index; i < storeSize - 1; i++)
            {
                ElementHolder hThis = ElementHolders[i];
                ElementHolder hNext = ElementHolders[i + 1];

                if (!hThis.IsHoldingElement() && hNext.IsHoldingElement())
                    hThis.GiveElement(hNext.TakeElement());
            }
        }
    }
}
