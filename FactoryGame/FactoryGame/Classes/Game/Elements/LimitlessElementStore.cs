﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class LimitlessElementStore : ElementStore
    {
        // -------- Public Fields --------
        public const int MIN_NUM_ELEMENT_HOLDERS = 6;

        // -------- Constructors --------
        public LimitlessElementStore()
            : base(MIN_NUM_ELEMENT_HOLDERS)
        {
        }

        // -------- Public Methods --------
        public override bool ContainsSpaceForProcess(ProcessData process)
        {
            return true;
        }

        public override bool IsFull()
        {
            return false;
        }

        public override int GetVisibleElementHolderNumber()
        {
            return MIN_NUM_ELEMENT_HOLDERS;
        }

        public override void AddElement(Element element)
        {
            if (NumberElements() == storeSize)
            {
                // Need to increase store size by 1
                storeSize++;
                ElementHolders.Add(new ElementHolder());
            }

            base.AddElement(element);
        }

        // -------- Private Methods --------
        public override Element TakeElement(int index)
        {
            bool needToRemoveElementHolder = (NumberElements() > MIN_NUM_ELEMENT_HOLDERS);
            Element e = base.TakeElement(index);
            if (needToRemoveElementHolder)
            {
                int lastElementHolderIndex = ElementHolders.Count - 1;
                ElementHolders.RemoveAt(lastElementHolderIndex);
                storeSize--;
            }
            return e;
        }
    }
}
