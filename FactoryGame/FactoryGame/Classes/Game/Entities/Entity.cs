﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    abstract class Entity
    {
        // -------- Properties --------
        public bool MouseOver { get; protected set; }
        public bool DrawHighlight { get; protected set; }

        // -------- Public Methods --------
        public abstract void Initialize();
        public abstract void Denitialize();
        public abstract void Update();

        public virtual bool MouseIsOver(out float depth)
        {
            depth = 1f;
            return false;
        }
        public virtual void OnMouseOver()
        {
            MouseOver = true;
            UIManager.OnMouseOverEntity(this);
        }
        public virtual void OnMouseOut()
        {
            MouseOver = false;
            UIManager.OnMouseOutEntity(this);
        }
        public virtual void OnMouseClick()
        {
            UIManager.OnMouseClickEntity(this);
        }
        public virtual void Destroy()
        {
        }

        public abstract Point GetBubbleAnchorPoint();
    }
}
