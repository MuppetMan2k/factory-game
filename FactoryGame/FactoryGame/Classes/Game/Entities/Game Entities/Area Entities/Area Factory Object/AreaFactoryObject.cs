﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    abstract class AreaFactoryObject : AreaEntity
    {
        // -------- Properties --------
        public string ModelId { get; protected set; }
        public bool[,] PassableMask { get; protected set; }
        public bool[,] PlacableMask { get; protected set; }
        public EnvironmentalFactors Environment { get; set; }

        // -------- Constructors --------
        public AreaFactoryObject(string inModelId)
        {
            ModelId = inModelId;
        }

        // -------- Public Methods --------
        public override void Initialize()
        {
            Model = ContentStorage.GetModel(ModelId);
        }

        public override void Update()
        {
        }

        public override bool MouseIsOver(out float depth)
        {
            return ModelMouseOverManager.MouseIsOverAreaModel(this, out depth);
        }

        public override void Destroy()
        {
            FactoryObjectManager.RemoveAreaFactoryObject(this);
        }

        // -------- Private Methods ---------
        protected override void DrawModel()
        {
            ModelGraphicsManager.DrawAreaModel(this);
        }
    }
}
