﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class BreakFactoryObject : InteractableFactoryObject
    {
        // -------- Properties --------
        public BreakFactoryObjectData Data { get; set; }

        // -------- Constructors --------
        public BreakFactoryObject(BreakFactoryObjectData inData)
            : base(inData.ModelId)
        {
            PassableMask = inData.PassableMask;
            PlacableMask = inData.PlacableMask;

            Environment = inData.Environment;

            Interactability = new Interactability();

            InteractableElement element = new InteractableElement();
            element.Interaction = new TakeBreakInteraction(this, element, inData.NeedChange);
            element.Interaction.InteractionTime = inData.InteractionTime;
            Interactability.InteractableElements.Add(element);
        }
    }
}
