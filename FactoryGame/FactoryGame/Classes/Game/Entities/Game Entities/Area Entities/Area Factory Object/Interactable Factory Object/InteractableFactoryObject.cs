﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    abstract class InteractableFactoryObject : AreaFactoryObject
    {
        // -------- Properties --------
        public Interactability Interactability { get; set; }

        // -------- Constructors --------
        public InteractableFactoryObject(string inModelId)
            : base(inModelId)
        {
        }

        // -------- Public Methods --------
        public override void Update()
        {
            base.Update();

            Interactability.Update();
        }
    }
}
