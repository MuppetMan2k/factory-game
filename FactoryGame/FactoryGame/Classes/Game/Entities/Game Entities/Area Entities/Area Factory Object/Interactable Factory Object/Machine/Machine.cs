﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class Machine : InteractableFactoryObject
    {
        // -------- Properties --------
        public string NameLocId { get; set; }
        public eStatLevel Speed { get; set; }
        public eStatLevel Noise { get; set; }
        public ElementStore InElementStore { get; set; }
        public ElementStore OutElementStore { get; set; }
        public List<ProcessData> Processes { get; set; }
        public bool Broken { get; set; }

        // -------- Constructors --------
        public Machine(TileCoordinate inCoord, eSquareRotation inRotation, MachineData inData)
            : base(inData.ModelId)
        {
            CreateAreaCoord(inCoord, inRotation, inData.PlacableMask);
            NameLocId = inData.NameLocId;
            Speed = inData.Speed;
            Noise = inData.Noise;
            InElementStore = new ElementStore(inData.InStoreSize);
            OutElementStore = new ElementStore(inData.OutStoreSize);
            PassableMask = inData.PassableMask;
            PlacableMask = inData.PlacableMask;
            Processes = inData.Processes;
            CreateInteractability(inData.Interactions);
            Environment = inData.Environment;
        }

        // -------- Public Methods --------
        public void CarryOutProcess(ProcessData process, Staff staff)
        {
            List<Element> elementsIn = InElementStore.GetElementsForProcess(process);
            ElementQuality quality = ElementManager.GetCreatedElementQuality(elementsIn, process, staff);

            foreach (ElementData d in process.ElementsIn)
                InElementStore.TakeElement(d);

            OutElementStore.AddElement(new Element(process.ElementOut, quality, GetCreatedElementQualityVisible()));

            ElementManager.OnElementMoved();
        }

        public bool IsBeingFixed()
        {
            InteractableElement fixInteraction = Interactability.GetInteractableElement_Fix();
            if (fixInteraction != null && fixInteraction.InteractingCharacter != null)
                return true;

            return false;
        }
        public bool IsBeingOperated()
        {
            InteractableElement operateInteraction = Interactability.GetInteractableElement_Operate();
            if (operateInteraction != null && operateInteraction.InteractingCharacter != null)
                return true;

            return false;
        }
        public ProcessData GetCurrentProcess()
        {
            if (!IsBeingOperated())
                return ProcessData.Null;

            InteractableElement operateInteraction = Interactability.GetInteractableElement_Operate();
            OperateInteraction operation = operateInteraction.Interaction as OperateInteraction;
            if (operation != null)
                return operation.GetProcess();

            return ProcessData.Null;
        }

        public float GetProcessCompletion()
        {
            if (!IsBeingOperated())
                return 0f;

            InteractableElement operateInteraction = Interactability.GetInteractableElement_Operate();
            OperateInteraction operation = operateInteraction.Interaction as OperateInteraction;
            if (operation != null)
                return operation.GetCompletionPercentage();

            return 0f;
        }

        public string GetMachineName()
        {
            return Localization.LocalizeString(NameLocId);
        }

        // -------- Private Methods ---------
        protected void CreateAreaCoord(TileCoordinate coord, eSquareRotation rotation, bool[,] placableMask)
        {
            AreaCoordinate = new AreaCoordinate(coord, placableMask.GetLength(1), placableMask.GetLength(0), rotation);
        }
        protected void CreateInteractability(List<InteractionData> data)
        {
            Interactability = new Interactability();

            foreach (InteractionData i in data)
            {
                InteractableElement e = new InteractableElement();
                e.Parent = this;
                e.InteractionTileRelativeCoordinate = i.TileRelCoord;
                e.InteractionRelativePosition = i.RelPosition;
                e.InteractionRelativeRotation = i.RelRotation;
                e.InteractionAnimation = i.Animation;
                e.CreateInteraction(i.Type);
                if (i.Type == eInteractionType.PUT_DOWN)
                    e.SetInteractionPutDownElementStore(InElementStore);
                if (i.Type == eInteractionType.PICK_UP)
                    e.SetInteractionPickUpElementStore(OutElementStore);

                Interactability.InteractableElements.Add(e);
            }
        }

        protected virtual bool GetCreatedElementQualityVisible()
        {
            return false;
        }
    }
}
