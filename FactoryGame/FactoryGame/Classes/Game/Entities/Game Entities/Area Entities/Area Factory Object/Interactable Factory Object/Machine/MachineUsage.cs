﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    struct MachineUsage
    {
        // -------- Public Fields --------
        public ProcessData Process;
        public Staff Staff;
    }
}
