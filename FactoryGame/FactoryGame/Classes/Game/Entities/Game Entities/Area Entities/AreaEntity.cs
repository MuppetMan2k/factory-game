﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    abstract class AreaEntity : GameEntity
    {
        // -------- Properties --------
        public AreaCoordinate AreaCoordinate { get; set; }
    }
}
