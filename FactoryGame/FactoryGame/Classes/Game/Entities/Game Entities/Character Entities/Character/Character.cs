﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    abstract class Character : CharacterEntity
    {
        // -------- Properties --------
        public CharacterGameModel CharacterModel { get; protected set; }
        public CharacterName Name { get; protected set; }
        public CharacterStyle Style { get; protected set; }
        public InstructionSet InstructionSet { get; protected set; }

        // -------- Constructors --------
        public Character(CharacterName inName, CharacterStyle inStyle)
        {
            Name = inName;
            Style = inStyle;
        }

        // -------- Public Methods --------
        public override void Initialize()
        {
            SetUpCharacterModel();
        }

        public override void Denitialize()
        {
            base.Denitialize();
            CharacterModel = null;
        }

        public override void Update()
        {
            CharacterCoordinate.Update();
        }

        public override bool MouseIsOver(out float depth)
        {
            return ModelMouseOverManager.MouseIsOverCharacterModel(this, out depth);
        }

        public override void Destroy()
        {
            CharacterManager.RemoveCharacter(this);
        }

        public void SetCoordinate(CharacterCoordinate coord)
        {
            CharacterCoordinate = coord;
        }

        public void OnFinishInstruction()
        {
            InstructionSet.OnFinishInstruction();

            if (InstructionSet.HasInstructionsToComplete())
                RunNextInstruction();
            else
                OnFinishInstructionSet();
        }

        public virtual void GiveInstructions(InstructionSet instructionSet)
        {
            InstructionSet = instructionSet;
            RunNextInstruction();
        }

        // -------- Private Methods ---------
        protected override void DrawModel()
        {
            ModelGraphicsManager.DrawCharacterModel(this);
        }

        private void SetUpCharacterModel()
        {
            CharacterModel = new CharacterGameModel(Style);
        }

        protected abstract InstructionSet FindNextInstructionSet();

        private void RunNextInstruction()
        {
            RunInstruction(InstructionSet.Instructions[0]);
        }
        private void RunInstruction(Instruction instruction)
        {
            InteractInstruction interact = instruction as InteractInstruction;
            if (interact != null)
            {
                CharacterCoordinate.RunInstruction(instruction);
                interact.InteractableElement.StartInteraction(this, instruction);
            }

            CharacterCoordinate.RunInstruction(instruction);
        }

        private void OnFinishInstructionSet()
        {
            CharacterCoordinate.OnFinishInstructionSet();
            InstructionSet.ParentTask.OnTaskFinished();

            if (InstructionSet.ParentTask.ShouldFindNewTaskAfterFinish())
                GiveInstructions(FindNextInstructionSet());
        }
    }
}
