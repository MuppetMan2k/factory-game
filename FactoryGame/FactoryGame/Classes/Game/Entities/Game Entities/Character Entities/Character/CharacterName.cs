﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class CharacterName
    {
        // -------- Properties --------
        public string FirstName { get; set; }
        public string LastName { get; set; }

        // -------- Constructors --------
        public CharacterName(string inFirstName, string inLastName)
        {
            FirstName = inFirstName;
            LastName = inLastName;
        }

        // -------- Public Methods --------
        public string GetAbbreviatedName()
        {
            return FirstName + " " + LastName[0] + ".";
        }
    }
}
