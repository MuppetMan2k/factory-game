﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class CharacterStyle
    {
        // -------- Properties --------
        public string BodyModelId { get; set; }
        public string HeadModelId { get; set; }
        public string HeadPieceModelId { get; set; }

        // -------- Constructors --------
        public CharacterStyle(string inBodyId, string inHeadId, string inHeadPieceId)
        {
            BodyModelId = inBodyId;
            HeadModelId = inHeadId;
            HeadPieceModelId = inHeadPieceId;
        }
    }
}
