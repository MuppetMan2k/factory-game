﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class Skill
    {
        // -------- Properties --------
        public SkillData Data { get; set; }
        public int Level { get; set; }

        // -------- Public Methods --------
        public float ModifyCreatedElementQuality(float quality)
        {
            float percentQualityIncrease = Level * GameBalanceManager.PercentElementQualityIncreasePerSkillLevel;
            return quality * (1f + percentQualityIncrease / 100f);
        }

        public float ModifyMachineOperateSpeed(float speed)
        {
            float percentSpeedIncrease = Level * GameBalanceManager.PercentMachineOperateSpeedIncreasePerSKillLevel;
            return speed * (1f + percentSpeedIncrease / 100f);
        }

        public bool ShouldDisplay()
        {
            return (Level >= 1);
        }
    }
}
