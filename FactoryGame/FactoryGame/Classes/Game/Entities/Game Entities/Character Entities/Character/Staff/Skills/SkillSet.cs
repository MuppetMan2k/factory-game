﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class SkillSet
    {
        // -------- Properties --------
        public List<Skill> Skills { get; set; }

        // -------- Constructors --------
        public SkillSet()
        {
            Skills = new List<Skill>();
        }

        // -------- Public Methods --------
        public void Denitialize()
        {
            Skills = null;
        }

        public List<Skill> GetDisplayedSkills()
        {
            List<Skill> displaySkills = new List<Skill>();

            foreach (Skill s in Skills)
                if (s.ShouldDisplay())
                    displaySkills.Add(s);

            return displaySkills;
        }

        public int GetDisplayedSkillCount()
        {
            return GetDisplayedSkills().Count;
        }

        public float ModifyCreatedElementQuality(ProcessData process, float quality)
        {
            foreach (Skill s in Skills)
                if (process.AssociatedSkill.Equals(s.Data))
                    quality = s.ModifyCreatedElementQuality(quality);

            return quality;
        }
        public float ModifyMachineOperateSpeed(ProcessData process, float speed)
        {
            foreach (Skill s in Skills)
                if (process.AssociatedSkill.Equals(s.Data))
                    speed = s.ModifyMachineOperateSpeed(speed);

            return speed;
        }
    }
}
