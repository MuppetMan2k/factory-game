﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class Staff : Character
    {
        // -------- Properties --------
        public eStaffType Type { get; set; }
        public ElementHolder CarryElement { get; protected set; }
        public TaskSet Tasks { get; set; }
        public StaffNeeds Needs { get; set; }
        public StaffHours Hours { get; set; }
        public SkillSet Skills { get; set; }
        public TraitSet Traits { get; set; }
        public bool BeenToWorkToday { get; set; }

        // -------- Constructors --------
        public Staff(eStaffType inStaffType, CharacterName inName, CharacterStyle inStyle)
            : base(inName, inStyle)
        {
            Type = inStaffType;
            Tasks = TaskManager.GetDefaultTaskSet(this);
            Needs = new StaffNeeds(this);
            Hours = StaffHours.GetDefaultHours(this);
            Skills = new SkillSet();
            Traits = new TraitSet();
            BeenToWorkToday = false;
        }

        // -------- Public Methods --------
        public override void Initialize()
        {
            base.Initialize();
            CarryElement = new ElementHolder();
        }

        public override void Denitialize()
        {
            Skills.Denitialize();
            Traits.Denitialize();
            CarryElement = null;
            base.Denitialize();
        }

        public override void Update()
        {
            Needs.Update();
            
            base.Update();
        }

        public void OnNewDay()
        {
            Hours.OnNewDay();
            BeenToWorkToday = false;
        }

        public string GetNameTitle()
        {
            string nameTitle = Name.GetAbbreviatedName();
            nameTitle += " - ";
            nameTitle += Localization.LocalizeString(StaffManager.GetStaffTypeLocId(Type));

            return nameTitle;
        }

        public int GetDailyPay()
        {
            return (StaffManager.GetStaffHourlyPayRate(Type) * Hours.GetDailyHours());
        }
        public int GetNewDailyPay()
        {
            return (StaffManager.GetStaffHourlyPayRate(Type) * Hours.GetNewDailyHours());
        }
        public string GetDailyPayString()
        {
            string dailyPaystring = string.Format(Localization.LocalizeString("text_per_day"), MoneyManager.GetMoneyString(GetDailyPay()));
            return dailyPaystring;
        }
        public string GetNewDailyPayString()
        {
            string dailyPayString = string.Format(Localization.LocalizeString("text_per_day"), MoneyManager.GetMoneyString(GetNewDailyPay()));
            return dailyPayString;
        }

        public string GetTaskNumberString()
        {
            int taskNum = Tasks.GetActiveTaskCount();
            string taskString = taskNum == 1 ? Localization.LocalizeString("text_tasks_task") : Localization.LocalizeString("text_tasks_tasks");
            string taskNumString = string.Format(taskString, taskNum.ToString());

            return taskNumString;
        }

        public string GetCurrentTaskDescriptionString()
        {
            return TaskManager.GetCurrentTaskDescription(Tasks.CurrentTask);
        }

        public void PickUpElement(Element element)
        {
            CarryElement.GiveElement(element);
        }
        public Element PutDownElement()
        {
            return CarryElement.TakeElement();
        }

        public bool IsCarryingElement()
        {
            return (CarryElement.IsHoldingElement());
        }

        public string GetCarryElementString()
        {
            if (!IsCarryingElement())
                return Localization.LocalizeString("text_no_carry_element");

            string carryString = string.Format(Localization.LocalizeString("text_carry_element"), Localization.LocalizeString(CarryElement.Element.Data.NameLocId));
            return carryString;
        }

        public void CheckForArrival()
        {
            if (BeenToWorkToday)
                return;

            if (Hours.StartTimeToday > GameClockManager.GetClockTime())
                return;

            BeenToWorkToday = true;
            CharacterManager.OnStaffArriveForWork(this);
        }

        public override void GiveInstructions(InstructionSet instructionSet)
        {
            base.GiveInstructions(instructionSet);
            Tasks.CurrentTask = instructionSet.ParentTask;
        }

        // -------- Private Methods ---------
        protected override InstructionSet FindNextInstructionSet()
        {
            eTaskPriority priority;

            if (Hours.ShouldGoHome())
            {
                // Go home
                ExitFactoryTask exitTask = new ExitFactoryTask();
                return exitTask.GetInstructionSetForTask(this, out priority);
            }

            // Try doing actual task
            InstructionSet instructions = Tasks.GetInstructionSet();
            if (instructions != null)
                return instructions;

            // Idle
            IdleTask idleTask = new IdleTask();
            return idleTask.GetInstructionSetForTask(this, out priority);
        }
    }
}
