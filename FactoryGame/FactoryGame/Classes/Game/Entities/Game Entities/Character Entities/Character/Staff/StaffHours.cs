﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class StaffHours
    {
        // -------- Properties --------
        public int NewStartTime { get; private set; }
        public int NewFinishTime { get; private set; }
        public int StartTime { get; private set; }
        public int FinishTime { get; private set; }
        public bool NewTimeSet { get; private set; }
        public float StartTimeToday { get; private set; }

        // -------- Private Fields --------
        private Staff parent;

        // -------- Constructors --------
        public StaffHours(Staff inParent, int inStart, int inFinish)
        {
            parent = inParent;
            StartTime = inStart;
            NewStartTime = inStart;
            FinishTime = inFinish;
            NewFinishTime = inFinish;
            NewTimeSet = false;
        }

        // -------- Public Methods --------
        public int GetDailyHours()
        {
            return (FinishTime - StartTime);
        }
        public int GetNewDailyHours()
        {
            return (NewFinishTime - NewStartTime);
        }

        public void SetNewStartTime(int inNewStartTime)
        {
            NewStartTime = inNewStartTime;
            NewTimeSet = (NewStartTime != StartTime || NewFinishTime != FinishTime);
        }
        public void SetNewFinishTime(int inNewFinishTime)
        {
            NewFinishTime = inNewFinishTime;
            NewTimeSet = (NewStartTime != StartTime || NewFinishTime != FinishTime);
        }

        public void OnNewDay()
        {
            if (NewTimeSet)
                ApplyNewTimes();

            StartTimeToday = StaffManager.GetStaffStartTimeToday(parent, StartTime);
        }

        public string GetHoursApplyText()
        {
            if (NewTimeSet)
                return Localization.LocalizeString("text_hours_apply");

            return "";
        }

        public bool ShouldGoHome()
        {
            return (GameClockManager.GetClockTime() >= (float)FinishTime);
        }

        // -------- Private Methods --------
        private void ApplyNewTimes()
        {
            StartTime = NewStartTime;
            FinishTime = NewFinishTime;

            NewTimeSet = false;
        }

        // -------- Static Instances --------
        public static StaffHours GetDefaultHours(Staff staff)
        {
            return new StaffHours(staff, StaffManager.DefaultStartTime, StaffManager.DefaultFinishTime);
        }
    }
}
