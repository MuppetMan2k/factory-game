﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class StaffNeedChange
    {
        // -------- Properties --------
        public eChangeLevel HungerFactor { get; set; }
        public eChangeLevel BladderFactor { get; set; }
        public eChangeLevel EnergyFactor { get; set; }

        // -------- Constructors --------
        public StaffNeedChange(eChangeLevel inHunger, eChangeLevel inBladder, eChangeLevel inEnergy)
        {
            HungerFactor = inHunger;
            BladderFactor = inBladder;
            EnergyFactor = inEnergy;
        }
    }
}
