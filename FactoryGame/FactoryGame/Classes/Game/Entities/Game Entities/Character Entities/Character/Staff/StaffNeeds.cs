﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    // -------- Enumerations --------
    public enum eNeedLevel
    {
        LEVEL_1,
        LEVEL_2,
        LEVEL_3,
        LEVEL_4,
        LEVEL_5
    }

    class StaffNeeds
    {
        // -------- Private Fields --------
        private float hunger;
        private float bladder;
        private float energy;
        private float environment;
        private float happiness;
        private Staff parentStaff;

        // -------- Constructors --------
        public StaffNeeds(Staff inStaff)
        {
            parentStaff = inStaff;

            hunger = 1f;
            bladder = 1f;
            energy = 1f;
            environment = 1f;
            happiness = 1f;
        }

        // -------- Public Methods --------
        public void Update()
        {
            ApplyPassiveNeedChange();
            UpdateEnvironment();
            UpdateHappiness();
        }

        public void ApplyNeedChange(StaffNeedChange change)
        {
            float hungerChangeRate = GameBalanceManager.GetNeedChangeRate(change.HungerFactor);
            hungerChangeRate = ModifierManager.ModifyStaffHungerChangeRate(hungerChangeRate, parentStaff);
            ChangeHunger(hungerChangeRate * GameSpeedManager.ElapsedGameTime);

            float bladderChangeRate = GameBalanceManager.GetNeedChangeRate(change.BladderFactor);
            bladderChangeRate = ModifierManager.ModifyStaffBladderChangeRate(bladderChangeRate, parentStaff);
            ChangeBladder(bladderChangeRate * GameSpeedManager.ElapsedGameTime);

            float energyChangeRate = GameBalanceManager.GetNeedChangeRate(change.EnergyFactor);
            energyChangeRate = ModifierManager.ModifyStaffEnergyChangeRate(energyChangeRate, parentStaff);
            ChangeEnergy(energyChangeRate * GameSpeedManager.ElapsedGameTime);
        }

        // Need get methods
        public float GetHunger()
        {
            return hunger;
        }
        public float GetBladder()
        {
            return bladder;
        }
        public float GetEnergy()
        {
            return energy;
        }
        public float GetEnvironment()
        {
            return environment;
        }
        public float GetHappiness()
        {
            return happiness;
        }

        // -------- Private Methods --------
        private void ApplyPassiveNeedChange()
        {
            float energyChangeRate = GameBalanceManager.GetPassiveEnergyChangeRateForTask(parentStaff.Tasks.CurrentTask.Type);
            energyChangeRate = ModifierManager.ModifyStaffPassiveEnergyChangeRate(energyChangeRate, parentStaff);
            ChangeEnergy(energyChangeRate * GameSpeedManager.ElapsedGameTime);

            float hungerChangeRate = GameBalanceManager.PassiveHungerChangeRate;
            hungerChangeRate = ModifierManager.ModifyStaffPassiveHungerChangeRate(hungerChangeRate, parentStaff);
            ChangeHunger(hungerChangeRate * GameSpeedManager.ElapsedGameTime);

            float bladderChangeRate = GameBalanceManager.PassiveBladderChangeRate;
            bladderChangeRate = ModifierManager.ModifyStaffPassiveBladderChangeRate(bladderChangeRate, parentStaff);
            ChangeBladder(bladderChangeRate * GameSpeedManager.ElapsedGameTime);
        }

        private void ChangeHunger(float change)
        {
            hunger += change;
            hunger = Mathf.Clamp01(hunger);
        }
        private void ChangeBladder(float change)
        {
            bladder += change;
            bladder = Mathf.Clamp01(bladder);
        }
        private void ChangeEnergy(float change)
        {
            energy += change;
            energy = Mathf.Clamp01(energy);
        }
        private void SetHappiness(float val)
        {
            happiness = val;
            happiness = Mathf.Clamp01(happiness);
        }

        private void UpdateEnvironment()
        {
            // TODO
        }
        private void UpdateHappiness()
        {
            float newHappiness = Mathf.Lerp(GetHappiness(), GetTargetHappiness(), GameBalanceManager.HappinessLerpRate * GameSpeedManager.ElapsedGameTime);
            SetHappiness(newHappiness);
        }
        private float GetTargetHappiness()
        {
            float minFactor = Mathf.Min(GetHunger(), GetBladder(), GetEnergy(), GetEnvironment());
            float averageFactor = Mathf.Average(GetHunger(), GetBladder(), GetEnergy(), GetEnvironment());

            float baseHappiness = Mathf.Lerp(minFactor, averageFactor, GameBalanceManager.HappinessMinToAvLerpFactor);

            return ModifierManager.ModifyStaffTargetHappiness(baseHappiness, parentStaff);
        }
    }
}
