﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    abstract class Trait
    {
        // -------- Public Methods --------
        public virtual string GetName()
        {
            return Localization.LocalizeString(GetNameLocId());
        }

        public abstract string GetIconTextureId();

        public virtual float ModifyPassiveHungerChangeRate(float rate)
        {
            return rate;
        }
        public virtual float ModifyPassiveBladderChangeRate(float rate)
        {
            return rate;
        }
        public virtual float ModifyPassiveEnergyChangeRate(float rate)
        {
            return rate;
        }
        public virtual float ModifyHungerChangeRate(float rate)
        {
            return rate;
        }
        public virtual float ModifyBladderChangeRate(float rate)
        {
            return rate;
        }
        public virtual float ModifyEnergyChangeRate(float rate)
        {
            return rate;
        }
        public virtual float ModifyEnvironmentNeed(float environment)
        {
            return environment;
        }
        public virtual float ModifyTargetHappiness(float happiness)
        {
            return happiness;
        }
        public virtual float ModifyCreatedElementQuality(float quality)
        {
            return quality;
        }
        public virtual float ModifyMachineOperateSpeed(float speed)
        {
            return speed;
        }
        public virtual float ModifyElementPickUpSpeed(float speed)
        {
            return speed;
        }
        public virtual float ModifyElementPutDownSpeed(float speed)
        {
            return speed;
        }
        public virtual float ModifyEarlyArrivalWindow(float window)
        {
            return window;
        }
        public virtual float ModifyLateArrivalWindow(float window)
        {
            return window;
        }

        // -------- Private Methods --------
        protected abstract string GetNameLocId();
    }
}
