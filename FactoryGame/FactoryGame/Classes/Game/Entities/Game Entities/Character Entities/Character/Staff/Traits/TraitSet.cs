﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class TraitSet
    {
        // -------- Properties --------
        public List<Trait> Traits { get; set; }

        // -------- Constructors --------
        public TraitSet()
        {
            Traits = new List<Trait>();
        }

        // -------- Public Methods --------
        public void Denitialize()
        {
            Traits = null;
        }

        // Modification methods
        public float ModifyStaffPassiveHungerChangeRate(float rate)
        {
            foreach (Trait t in Traits)
                rate = t.ModifyPassiveHungerChangeRate(rate);

            return rate;
        }
        public float ModifyStaffPassiveBladderChangeRate(float rate)
        {
            foreach (Trait t in Traits)
                rate = t.ModifyPassiveBladderChangeRate(rate);

            return rate;
        }
        public float ModifyStaffPassiveEnergyChangeRate(float rate)
        {
            foreach (Trait t in Traits)
                rate = t.ModifyPassiveEnergyChangeRate(rate);

            return rate;
        }
        public float ModifyStaffHungerChangeRate(float rate)
        {
            foreach (Trait t in Traits)
                rate = t.ModifyHungerChangeRate(rate);

            return rate;
        }
        public float ModifyStaffBladderChangeRate(float rate)
        {
            foreach (Trait t in Traits)
                rate = t.ModifyBladderChangeRate(rate);

            return rate;
        }
        public float ModifyStaffEnergyChangeRate(float rate)
        {
            foreach (Trait t in Traits)
                rate = t.ModifyEnergyChangeRate(rate);

            return rate;
        }
        public float ModifyStaffEnvironmentNeed(float environment)
        {
            foreach (Trait t in Traits)
                environment = t.ModifyEnvironmentNeed(environment);

            return environment;
        }
        public float ModifyStaffTargetHappiness(float happiness)
        {
            foreach (Trait t in Traits)
                happiness = t.ModifyTargetHappiness(happiness);

            return happiness;
        }
        public float ModifyCreatedElementQuality(float quality)
        {
            foreach (Trait t in Traits)
                quality = t.ModifyCreatedElementQuality(quality);

            return quality;
        }
        public float ModifyMachineOperateSpeed(float speed)
        {
            foreach (Trait t in Traits)
                speed = t.ModifyMachineOperateSpeed(speed);

            return speed;
        }
        public float ModifyElementPickUpSpeed(float speed)
        {
            foreach (Trait t in Traits)
                speed = t.ModifyElementPickUpSpeed(speed);

            return speed;
        }
        public float ModifyElementPutDownSpeed(float speed)
        {
            foreach (Trait t in Traits)
                speed = t.ModifyElementPutDownSpeed(speed);

            return speed;
        }
        public float ModifyStaffEarlyArrivalWindow(float window)
        {
            foreach (Trait t in Traits)
                window = t.ModifyEarlyArrivalWindow(window);

            return window;
        }
        public float ModifyStaffLateArrivalWindow(float window)
        {
            foreach (Trait t in Traits)
                t.ModifyLateArrivalWindow(window);

            return window;
        }
    }
}
