﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    abstract class CharacterEntity : GameEntity
    {
        // -------- Properties --------
        public CharacterCoordinate CharacterCoordinate { get; set; }
    }
}
