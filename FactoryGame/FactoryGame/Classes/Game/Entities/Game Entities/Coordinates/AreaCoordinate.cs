﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    struct AreaCoordinate
    {
        // -------- Public Fields --------
        public TileCoordinate Coordinate;
        public int Width;
        public int Height;
        public eSquareRotation Rotation;
        public bool OnCeiling;

        // -------- Constructors --------
        public AreaCoordinate(TileCoordinate inCoordinate, int inWidth, int inHeight, eSquareRotation inRotation, bool inOnCeiling = false)
        {
            Coordinate = inCoordinate;
            Width = inWidth;
            Height = inHeight;
            Rotation = inRotation;
            OnCeiling = inOnCeiling;
        }
    }
}
