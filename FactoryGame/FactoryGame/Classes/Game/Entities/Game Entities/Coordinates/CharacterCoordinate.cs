﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    // -------- Enumerations --------
    public enum eCharacterCoordinateState
    {
        STATIONARY,
        TRANSITIONING,
        INTERACTING,
        IDLING
    }

    class CharacterCoordinate
    {
        // -------- Public Fields --------
        public Character Parent;
        public eCharacterCoordinateState State;
        public eSquareRotation Rotation;
        public TileCoordinate StationaryCoordinate;
        public TileCoordinate TransitionOrigin;
        public eSquareCompassDirection TransitionDirection;
        public float TransitionProgress;
        public InteractableElement Interaction;
        public CharacterAnimation Animation;
        public float IdleTimer;
        public float IdleTimeLimit;

        // -------- Constructors --------
        public CharacterCoordinate(Character inParent, TileCoordinate inStationaryCoord, eSquareRotation inRotation)
        {
            Parent = inParent;
            State = eCharacterCoordinateState.STATIONARY;
            Rotation = inRotation;
            StationaryCoordinate = inStationaryCoord;
            TransitionOrigin = inStationaryCoord;
            TransitionDirection = eSquareCompassDirection.NORTH;
            TransitionProgress = 0f;
            Interaction = null;
            Animation = CharacterAnimationStore.GetAnimation("idle");
        }

        // -------- Public Methods --------
        public void Update()
        {
            Animation.Update();

            if (State == eCharacterCoordinateState.TRANSITIONING)
                UpdateTransition();

            if (State == eCharacterCoordinateState.IDLING)
                UpdateIdle();
        }

        public TileCoordinate GetTransitionTargetTile()
        {
            return TileUtilities.GetOffsetTile(TransitionOrigin, TransitionDirection);
        }

        public TileCoordinate GetApproxTileCoordinate()
        {
            switch (State)
            {
                case eCharacterCoordinateState.STATIONARY:
                    return StationaryCoordinate;
                case eCharacterCoordinateState.TRANSITIONING:
                    if (TransitionProgress < 0.5f)
                        return TransitionOrigin;
                    else
                        return GetTransitionTargetTile();
                case eCharacterCoordinateState.INTERACTING:
                    return Interaction.GetInteractionTileCoordinate();
                case eCharacterCoordinateState.IDLING:
                    return StationaryCoordinate;
                default:
                    return new TileCoordinate();
            }
        }

        public Vector2 GetTransitioningTileSpacePosition()
        {
            if (State != eCharacterCoordinateState.TRANSITIONING)
                return new Vector2();

            Vector2 origin = SpaceUtilities.ConvertTileCoordToTs(TransitionOrigin);
            Vector2 target = SpaceUtilities.ConvertTileCoordToTs(GetTransitionTargetTile());

            return Vector2.Lerp(origin, target, TransitionProgress);
        }

        public Vector2 GetCentralTileSpacePosition()
        {
            if (State == eCharacterCoordinateState.TRANSITIONING)
            {
                Vector2 transTsPos = GetTransitioningTileSpacePosition();
                return TileUtilities.GetCentralTsPosForTile(transTsPos);
            }

            if (State == eCharacterCoordinateState.INTERACTING)
                return GetInteractingCentralTsPos();

            Vector2 tsPos = SpaceUtilities.ConvertTileCoordToTs(GetApproxTileCoordinate());
            return TileUtilities.GetCentralTsPosForTile(tsPos);
        }

        public void RunInstruction(Instruction instruction)
        {
            MoveInstruction move = instruction as MoveInstruction;
            if (move != null)
            {
                RunMoveInstruction(move);
                return;
            }

            TeleportInstruction teleport = instruction as TeleportInstruction;
            if (teleport != null)
            {
                RunTeleportInstruction(teleport);
                return;
            }

            InteractInstruction interact = instruction as InteractInstruction;
            if (interact != null)
            {
                RunInteractInstruction(interact);
                return;
            }

            IdleInstruction idle = instruction as IdleInstruction;
            if (idle != null)
            {
                RunIdleInstruction(idle);
                return;
            }

            Debug.LogWarning("Tried to run instruction in character coordinate that cannot be run: " + instruction.ToString());
        }

        public void OnFinishInstructionSet()
        {
        }

        public List<TileCoordinate> GetOccupiedTiles()
        {
            var tiles = new List<TileCoordinate>();

            switch (State)
            {
                case eCharacterCoordinateState.STATIONARY:
                    tiles.Add(StationaryCoordinate);
                    break;
                case eCharacterCoordinateState.TRANSITIONING:
                    tiles.Add(TransitionOrigin);
                    tiles.Add(GetTransitionTargetTile());
                    break;
                case eCharacterCoordinateState.INTERACTING:
                    break;
                case eCharacterCoordinateState.IDLING:
                    tiles.Add(StationaryCoordinate);
                    break;
            }

            if (Parent.InstructionSet != null && Parent.InstructionSet.Type == eInstructionSetType.IDLE)
                if (Parent.InstructionSet.PathTiles.Count > 0)
                    tiles.Add(Parent.InstructionSet.PathTiles.Last());

            return tiles;
        }

        public void SetStationaryCoordinate(TileCoordinate newCoord, eSquareRotation newRotation)
        {
            State = eCharacterCoordinateState.STATIONARY;
            StationaryCoordinate = newCoord;
            Rotation = newRotation;
        }

        // -------- Private Methods ---------
        private void UpdateTransition()
        {
            TransitionProgress += GameSpeedManager.ElapsedGameTime * CharacterManager.DefaultWalkSpeed;

            if (TransitionProgress >= 1f)
            {
                TransitionProgress = 1f;
                Parent.OnFinishInstruction();
            }
        }

        private void UpdateIdle()
        {
            IdleTimer += GameSpeedManager.ElapsedGameTime;

            if (IdleTimer >= IdleTimeLimit)
                Parent.OnFinishInstruction();
        }

        // Instruction methods
        private void RunMoveInstruction(MoveInstruction instruction)
        {
            ChangeState(eCharacterCoordinateState.TRANSITIONING);
            TransitionDirection = instruction.Direction;
            Rotation = CompassUtilities.ConvertSquareDirectionToRotation(TransitionDirection);

            FloorManager.OnCharacterStepOnTile(Parent, TransitionOrigin, TransitionDirection);
        }
        private void RunTeleportInstruction(TeleportInstruction instruction)
        {
            SetStationaryCoordinate(instruction.Destination, Rotation);
        }
        private void RunInteractInstruction(InteractInstruction instruction)
        {
            Interaction = instruction.InteractableElement;
            ChangeState(eCharacterCoordinateState.INTERACTING);
        }
        private void RunIdleInstruction(IdleInstruction instruction)
        {
            IdleTimeLimit = instruction.IdleTime;
            IdleTimer = 0f;
            ChangeState(eCharacterCoordinateState.IDLING);
        }

        private void ChangeState(eCharacterCoordinateState newState)
        {
            switch (newState)
            {
                case eCharacterCoordinateState.STATIONARY:
                    if (State == eCharacterCoordinateState.STATIONARY)
                        return;

                    if (State == eCharacterCoordinateState.TRANSITIONING)
                    {
                        StationaryCoordinate = GetTransitionTargetTile();
                    }

                    if (State == eCharacterCoordinateState.INTERACTING)
                    {
                        StationaryCoordinate = Interaction.GetInteractionTileCoordinate();
                    }

                    string stationaryAnimationId = CharacterAnimationManager.GetStationaryCharacterAnimationId(Parent);
                    Animation = CharacterAnimationStore.GetAnimation(stationaryAnimationId);
                    break;
                case eCharacterCoordinateState.TRANSITIONING:
                    TransitionProgress = 0f;

                    if (State == eCharacterCoordinateState.TRANSITIONING)
                    {
                        TransitionOrigin = GetTransitionTargetTile();
                        return;
                    }

                    if (State == eCharacterCoordinateState.STATIONARY || State == eCharacterCoordinateState.IDLING)
                        TransitionOrigin = StationaryCoordinate;

                    if (State == eCharacterCoordinateState.INTERACTING)
                        TransitionOrigin = Interaction.GetInteractionTileCoordinate();

                    string movingAnimationId = CharacterAnimationManager.GetMovingCharacterAnimationId(Parent);
                    Animation = CharacterAnimationStore.GetAnimation(movingAnimationId);
                    break;
                case eCharacterCoordinateState.INTERACTING:
                    if (State == eCharacterCoordinateState.INTERACTING)
                        return;

                    Animation = CharacterAnimationStore.GetAnimation(Interaction.InteractionAnimation);
                    break;
                case eCharacterCoordinateState.IDLING:
                    if (State == eCharacterCoordinateState.IDLING)
                        return;

                    if (State == eCharacterCoordinateState.TRANSITIONING)
                    {
                        StationaryCoordinate = GetTransitionTargetTile();
                    }

                    if (State == eCharacterCoordinateState.INTERACTING)
                    {
                        StationaryCoordinate = Interaction.GetInteractionTileCoordinate();
                    }

                    string idlingAnimationId = CharacterAnimationManager.GetIdlingCharacterAnimationId(Parent);
                    Animation = CharacterAnimationStore.GetAnimation(idlingAnimationId);
                    break;
                default:
                    break;
            }

            State = newState;
        }

        private Vector2 GetInteractingCentralTsPos()
        {
            TileCoordinate entityCoord = GameEntityUtilities.GetGameEntityTileCoordinate(Interaction.Parent);
            eSquareRotation entityRotation = GameEntityUtilities.GetGameEntityRotation(Interaction.Parent);
            Vector2 entityTsPos = TileUtilities.GetTileCornerTsPosition(entityCoord, entityRotation);
            Vector2 charTsPos = TileUtilities.AddRotatedTsPositionToTsOrigin(entityTsPos, Interaction.InteractionRelativePosition, entityRotation);

            return charTsPos;
        }
    }
}
