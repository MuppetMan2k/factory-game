﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    struct MiniTileCoordinate
    {
        // -------- Public Fields --------
        public TileCoordinate TileCoordinate;
        public Vector2 Position;

        // -------- Constructors --------
        public MiniTileCoordinate(TileCoordinate inTileCoordinate, Vector2 inPosition)
        {
            TileCoordinate = inTileCoordinate;
            Position = inPosition;
        }
    }
}
