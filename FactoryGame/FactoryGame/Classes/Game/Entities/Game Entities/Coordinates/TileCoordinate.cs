﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    struct TileCoordinate
    {
        // -------- Public Fields --------
        public int Row;
        public int Col;

        // -------- Constructors --------
        public TileCoordinate(int inRow, int inCol)
        {
            Row = inRow;
            Col = inCol;
        }
    }
}
