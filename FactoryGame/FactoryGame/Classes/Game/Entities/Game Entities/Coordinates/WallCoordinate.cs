﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    // -------- Enumerations --------
    public enum eWallDirection
    {
        NORTH,
        WEST
    }

    struct WallCoordinate
    {
        // -------- Public Fields --------
        public TileCoordinate Coordinate;
        public eWallDirection Direction;

        // -------- Public Methods --------
        public WallCoordinate(TileCoordinate inCoordinate, eWallDirection inDirection)
        {
            Coordinate = inCoordinate;
            Direction = inDirection;
        }
        public WallCoordinate(int inRow, int inCol, eWallDirection inDirection)
        {
            Coordinate = new TileCoordinate(inRow, inCol);
            Direction = inDirection;
        }
    }
}
