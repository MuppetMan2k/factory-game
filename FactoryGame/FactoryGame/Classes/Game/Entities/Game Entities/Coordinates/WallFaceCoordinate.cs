﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    // -------- Enumerations --------
    public enum eWallFace
    {
        INSIDE,
        OUTSIDE
    }

    struct WallFaceCoordinate
    {
        // -------- Public Fields --------
        public WallCoordinate WallCoordinate;
        public eWallFace Face;

        // -------- Constructors --------
        public WallFaceCoordinate(WallCoordinate inCoord, eWallFace inFace)
        {
            WallCoordinate = inCoord;
            Face = inFace;
        }
    }
}
