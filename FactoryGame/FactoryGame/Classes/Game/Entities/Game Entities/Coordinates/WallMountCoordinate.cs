﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    // -------- Enumerations --------
    public enum eWallMountLocation
    {
        INSIDE,
        OUTSIDE,
        THROUGH
    }

    struct WallMountCoordinate
    {
        // -------- Public Fields --------
        public WallCoordinate WallCoordinate;
        public eWallMountLocation Location;

        // -------- Constructors --------
        public WallMountCoordinate(WallCoordinate inCoordinate, eWallMountLocation inLocation)
        {
            WallCoordinate = inCoordinate;
            Location = inLocation;
        }
    }
}
