﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    abstract class GameEntity : Entity
    {
        // -------- Properties --------
        public GameModel Model { get; protected set; }

        // -------- Public Methods --------
        public override void Denitialize()
        {
            Model = null;
        }

        public virtual void Draw()
        {
            if (!EffectTransparencyManager.ShouldDrawTransparent(this))
                DrawModel();
        }

        public virtual void DrawTransparent()
        {
            if (EffectTransparencyManager.ShouldDrawTransparent(this))
                DrawModel();
        }

        public override void OnMouseOver()
        {
            DrawHighlight = true;
            CursorManager.SetCursorMode(eCursorMode.SELECT);
            base.OnMouseOver();
        }
        public override void OnMouseOut()
        {
            DrawHighlight = false;
            CursorManager.SetCursorMode(eCursorMode.POINTER);
            base.OnMouseOut();
        }

        public override Point GetBubbleAnchorPoint()
        {
            Vector3 gsVec = GameEntityUtilities.GetGameEntityBubbleAnchorPointGS(this);
            Point point = SpaceUtilities.ConvertGsToPsPoint(gsVec);

            return point;
        }

        // -------- Private Methods ---------
        protected abstract void DrawModel();
    }
}
