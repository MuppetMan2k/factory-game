﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    abstract class MiniTileEntity : GameEntity
    {
        // -------- Properties --------
        public MiniTileCoordinate MiniTileCoordinate { get; set; }

        // -------- Private Methods ---------
        protected override void DrawModel()
        {
            ModelGraphicsManager.DrawMiniTileModel(this);
        }
    }
}
