﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    class Trash : MiniTileEntity
    {
        // -------- Properties --------
        public Interactability Interactability { get; set; }

        // -------- Private Fields --------
        private string modelId;

        // -------- Constructors --------
        public Trash(TileCoordinate inCoordinate)
        {
            MiniTileCoordinate = new MiniTileCoordinate(inCoordinate, GetRandomPosition());
            modelId = TrashManager.GetRandomTrashModelId();
            SetUpInteractability();
        }

        // -------- Public Methods --------
        public override void Initialize()
        {
            Model = ContentStorage.GetModel(modelId);
        }

        public override void Update()
        {
            Interactability.Update();
        }

        public override bool MouseIsOver(out float depth)
        {
            return ModelMouseOverManager.MouseIsOverMiniTileModel(this, out depth);
        }

        public override void Destroy()
        {
            TrashManager.RemoveTrash(this);
        }

        // -------- Private Methods ---------
        private void SetUpInteractability()
        {
            Interactability = new Interactability();

            InteractableElement element = new InteractableElement();
            element.Interaction = new CleanUpInteraction(this, element);
            element.Interaction.InteractionTime = TrashManager.TrashPickupTime;
            element.InteractionAnimation = "clean";
            element.InteractionRelativePosition = new Vector2(0.5f, 0.5f);
            element.InteractionRelativeRotation = eSquareRotation.ROTATION_0;
            element.InteractionTileRelativeCoordinate = new TileCoordinate();
            element.Parent = this;

            Interactability.InteractableElements.Add(element);
        }

        protected override void DrawModel()
        {
            ModelGraphicsManager.DrawMiniTileModel(this);
        }

        private Vector2 GetRandomPosition()
        {
            Vector2 position = new Vector2();
            position.X = RandomManager.RandomNumber(0.5f - TrashManager.TrashPositionSpread, 0.5f + TrashManager.TrashPositionSpread);
            position.Y = RandomManager.RandomNumber(0.5f - TrashManager.TrashPositionSpread, 0.5f + TrashManager.TrashPositionSpread);
            
            return position;
        }
    }
}
