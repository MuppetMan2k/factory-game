﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    abstract class TileEntity : GameEntity
    {
        // -------- Properties --------
        public TileCoordinate TileCoordinate { get; set; }
    }
}
