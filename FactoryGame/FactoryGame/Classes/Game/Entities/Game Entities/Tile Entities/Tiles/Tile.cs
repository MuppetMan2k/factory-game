﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class Tile : TileEntity
    {
        // -------- Properties --------
        public TileStyle Style { get; set; }
        public bool InFactory { get; set; }
        public bool Purchased { get; set; }

        // -------- Constructors --------
        public Tile(TileCoordinate inCoord, TileStyle inStyle, bool inInFactory, bool inPurchased)
        {
            TileCoordinate = inCoord;
            Style = inStyle;
            InFactory = inInFactory;
            Purchased = inPurchased;
        }

        // -------- Public Methods --------
        public override void Initialize()
        {
            Model = ContentStorage.GetModel(Style.ModelId);
        }

        public override void Update()
        {
        }

        public override bool MouseIsOver(out float depth)
        {
            return ModelMouseOverManager.MouseIsOverTileModel(this, out depth);
        }

        public override void Destroy()
        {
            TileManager.RemoveTile(this);
        }

        // -------- Private Methods ---------
        protected override void DrawModel()
        {
            ModelGraphicsManager.DrawTileModel(this);
        }
    }
}
