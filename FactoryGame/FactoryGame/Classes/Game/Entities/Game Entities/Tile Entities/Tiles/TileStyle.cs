﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class TileStyle
    {
        // -------- Properties --------
        public string ModelId { get; set; }

        // -------- Static Instances --------
        // DEBUG
        public static TileStyle Grass
        {
            get
            {
                var style = new TileStyle();
                style.ModelId = "grass-tile";

                return style;
            }
        }
        public static TileStyle Paved
        {
            get
            {
                var style = new TileStyle();
                style.ModelId = "paved-tile";

                return style;
            }
        }
        public static TileStyle Vinyl
        {
            get
            {
                var style = new TileStyle();
                style.ModelId = "vinyl-tile";

                return style;
            }
        }
    }
}
