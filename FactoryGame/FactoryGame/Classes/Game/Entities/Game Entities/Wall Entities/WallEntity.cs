﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    abstract class WallEntity : GameEntity
    {
        // -------- Properties --------
        public WallCoordinate WallCoordinate { get; set; }
    }
}
