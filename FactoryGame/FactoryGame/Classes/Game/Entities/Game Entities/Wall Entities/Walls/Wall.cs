﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    // -------- Enumerations --------
    public enum eWallType
    {
        EXTERIOR,
        INTERIOR,
        PURCHASABLE_ZONE
    }

    class Wall : WallEntity
    {
        // -------- Properties --------
        public eWallType Type { get; set; }
        public WallStyle InnerStyle { get; set; }
        public WallStyle OuterStyle { get; set; }
        // Models
        public GameModel InnerModel { get; protected set; }
        public GameModel OuterModel { get; protected set; }

        // -------- Constructors --------
        public Wall(WallCoordinate inCoord, eWallType inType, WallStyle inInnerWallStyle, WallStyle inOuterWallStyle)
        {
            WallCoordinate = inCoord;
            Type = inType;
            InnerStyle = inInnerWallStyle;
            OuterStyle = inOuterWallStyle;
        }

        // -------- Public Methods --------
        public override void Initialize()
        {
            InnerModel = ContentStorage.GetModel(InnerStyle.ModelId);
            OuterModel = ContentStorage.GetModel(OuterStyle.ModelId);
        }

        public override void Denitialize()
        {
            base.Denitialize();
            InnerModel = null;
            OuterModel = null;
        }

        public override void Update()
        {
        }

        public override bool MouseIsOver(out float depth)
        {
            return ModelMouseOverManager.MouseIsOverWallModel(this, out depth);
        }

        public override void Destroy()
        {
            WallManager.RemoveWall(this);
        }

        // -------- Private Methods ---------
        protected override void DrawModel()
        {
            ModelGraphicsManager.DrawWallModels(this);
        }

        // -------- Static Methods --------
        public static Wall CreatePurchasableZoneWall(WallCoordinate coord)
        {
            return new Wall(coord, eWallType.PURCHASABLE_ZONE, WallStyle.Office, WallStyle.Office);
        }
    }
}
