﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class WallStyle
    {
        // -------- Properties --------
        public string ModelId { get; set; }

        // -------- Static Instances --------
        // DEBUG
        public static WallStyle Brick
        {
            get
            {
                var style = new WallStyle();
                style.ModelId = "brick-wall";

                return style;
            }
        }
        public static WallStyle Office
        {
            get
            {
                var style = new WallStyle();
                style.ModelId = "office-wall";

                return style;
            }
        }
    }
}
