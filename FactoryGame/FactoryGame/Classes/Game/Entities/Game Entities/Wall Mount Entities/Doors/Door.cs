﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    abstract class Door : WallMountEntity
    {
        // -------- Constructors --------
        public Door(WallCoordinate inCoord)
        {
            WallMountCoordinate = new WallMountCoordinate(inCoord, eWallMountLocation.THROUGH);
        }

        // -------- Public Methods --------
        public override void Destroy()
        {
            DoorManager.RemoveDoor(this);
        }

        public abstract void OpenDoor(eHingeOpenDirection direction);
        public abstract void CloseDoor();
    }
}
