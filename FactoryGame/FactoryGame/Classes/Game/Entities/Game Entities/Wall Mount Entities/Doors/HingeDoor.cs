﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    // -------- Enumerations --------
    public enum eHingeOpenDirection
    {
        INSIDE,
        OUTSIDE
    }

    class HingeDoor : Door
    {
        // -------- Properties --------
        public float Rotation { get; private set; }
        // Models
        public GameModel InnerModel { get; protected set; }
        public GameModel OuterModel { get; protected set; }

        // -------- Private Fields --------
        private AnimationCurve rotationAnimation;
        private float startRotation;
        private float targetRotation;
        private float closeTimer;
        // Parameters
        private const string DOOR_MODEL_ID = "door";
        private const float TIME_TO_ROTATE = 0.25f;
        private const float CLOSE_WAIT_TIME = 2f;

        // -------- Constructors --------
        public HingeDoor(WallCoordinate inCoord)
            : base(inCoord)
        {
            Rotation = 0f;
            startRotation = 0f;
            targetRotation = 0f;
            closeTimer = CLOSE_WAIT_TIME;
            rotationAnimation = new AnimationCurve(new SCurveAnimationFunction(), TIME_TO_ROTATE, true);
        }

        // -------- Public Methods --------
        public override void Initialize()
        {
            InnerModel = ContentStorage.GetModel(DOOR_MODEL_ID + "-inside");
            OuterModel = ContentStorage.GetModel(DOOR_MODEL_ID + "-outside");
        }

        public override void Denitialize()
        {
            base.Denitialize();
            InnerModel = null;
            OuterModel = null;
        }

        public override void Update()
        {
            UpdateRotation();
            UpdateCloseTimer();
        }

        public override bool MouseIsOver(out float depth)
        {
            return ModelMouseOverManager.MouseIsOverHingeDoorModel(this, out depth);
        }

        public override void OpenDoor(eHingeOpenDirection direction)
        {
            targetRotation = direction == eHingeOpenDirection.INSIDE ? 90f : -90f;
            startRotation = Rotation;
            rotationAnimation.Reset();
            closeTimer = 0f;
        }
        public override void CloseDoor()
        {
            targetRotation = 0f;
            startRotation = Rotation;
            rotationAnimation.Reset();
        }

        // -------- Private Methods ---------
        protected override void DrawModel()
        {
            ModelGraphicsManager.DrawHingeDoorModels(this);
        }

        private void UpdateRotation()
        {
            rotationAnimation.Update();
            Rotation = Mathf.Lerp(startRotation, targetRotation, rotationAnimation.GetOutput());
        }

        private void UpdateCloseTimer()
        {
            if (closeTimer == CLOSE_WAIT_TIME)
                return;

            closeTimer += GameSpeedManager.ElapsedGameTime;

            if (closeTimer >= CLOSE_WAIT_TIME)
            {
                CloseDoor();
                closeTimer = CLOSE_WAIT_TIME;
            }
        }
    }
}
