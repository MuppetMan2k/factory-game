﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class EnvironmentalWallMountFactoryObject : WallMountFactoryObject
    {
        // -------- Properties --------
        public EnvironmentalFactors EnvironmentalFactors { get; private set; }

        // -------- Constructors --------
        public EnvironmentalWallMountFactoryObject(WallMountCoordinate inCoord, string inModelId, EnvironmentalFactors factors)
            : base(inCoord, inModelId)
        {
            EnvironmentalFactors = factors;
        }
    }
}
