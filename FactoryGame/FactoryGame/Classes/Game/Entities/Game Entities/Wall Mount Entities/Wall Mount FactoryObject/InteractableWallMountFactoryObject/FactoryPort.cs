﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    class FactoryPort : InteractableWallMountFactoryObject
    {
        // -------- Properties --------
        public LimitlessElementStore ElementStore { get; set; }
        public bool IsInPort { get; private set; }

        // -------- Constructors --------
        public FactoryPort(WallMountCoordinate inCoord, bool inIsInPort)
            : base(inCoord, inIsInPort ? "factory-in-port" : "factory-out-port")
        {
            IsInPort = inIsInPort;
        }

        // -------- Public Methods --------
        public override void Initialize()
        {
            SetUpElementStore();
            SetUpInteractability();
            base.Initialize();
        }

        // -------- Private Methods --------
        private void SetUpInteractability()
        {
            Interactability = new Interactability();
            InteractableElement ie = new InteractableElement();
            ie.Parent = this;
            if (IsInPort)
            {
                ie.Interaction = new PickUpInteraction(this, ie);
                ie.SetInteractionPickUpElementStore(ElementStore);
            }
            else
            {
                ie.Interaction = new PutDownInteraction(this, ie);
                ie.SetInteractionPutDownElementStore(ElementStore);
            }
            ie.InteractionAnimation = "idle";
            ie.InteractionRelativePosition = new Vector2(0.5f, 0f);
            ie.InteractionRelativeRotation = eSquareRotation.ROTATION_0;
            ie.InteractionTileRelativeCoordinate = new TileCoordinate(0, 0);

            Interactability.InteractableElements.Add(ie);
        }

        private void SetUpElementStore()
        {
            int storeSize = IsInPort ? FactoryPortManager.InStoreSize : FactoryPortManager.OutStoreSize;
            ElementStore = new LimitlessElementStore();
        }
    }
}
