﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class InteractableWallMountFactoryObject : WallMountFactoryObject
    {
        // -------- Properties --------
        public Interactability Interactability { get; set; }

        // -------- Constructors --------
        public InteractableWallMountFactoryObject(WallMountCoordinate inCoord, string inModelId)
            : base(inCoord, inModelId)
        {
        }

        // -------- Public Methods --------
        public override void Update()
        {
            Interactability.Update();
            base.Update();
        }
    }
}
