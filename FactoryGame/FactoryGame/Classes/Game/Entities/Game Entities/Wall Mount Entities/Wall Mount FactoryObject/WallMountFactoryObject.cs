﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class WallMountFactoryObject : WallMountEntity
    {
        // -------- Private Fields --------
        private string modelId;

        // -------- Constructors --------
        public WallMountFactoryObject(WallMountCoordinate inCoord, string inModelId)
        {
            WallMountCoordinate = inCoord;
            modelId = inModelId;
        }

        // -------- Public Methods --------
        public override void Initialize()
        {
            Model = ContentStorage.GetModel(modelId);
        }

        public override void Update()
        {
        }

        public override bool MouseIsOver(out float depth)
        {
            return ModelMouseOverManager.MouseIsOverWallMountedModel(this, out depth);
        }

        public override void Destroy()
        {
            FactoryObjectManager.RemoveWallMountFactoryObject(this);
        }

        // -------- Private Methods ---------
        protected override void DrawModel()
        {
            ModelGraphicsManager.DrawWallMountedModel(this);
        }
    }
}
