﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    abstract class WallMountEntity : GameEntity
    {
        // -------- Properties --------
        public WallMountCoordinate WallMountCoordinate { get; set; }
    }
}
