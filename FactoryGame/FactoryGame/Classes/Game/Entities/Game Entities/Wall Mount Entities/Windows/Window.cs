﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class Window : WallMountEntity
    {
        // -------- Private Fields --------
        private const string windowModelId = "window-frame-half";

        // -------- Constructors --------
        public Window(WallCoordinate inCoord)
        {
            WallMountCoordinate = new WallMountCoordinate(inCoord, eWallMountLocation.THROUGH);
        }

        // -------- Public Methods --------
        public override void Initialize()
        {
            Model = ContentStorage.GetModel(windowModelId);
        }

        public override void Update()
        {
        }

        public override bool MouseIsOver(out float depth)
        {
            return ModelMouseOverManager.MouseIsOverWallMountedModel(this, out depth);
        }

        public override void Destroy()
        {
            WindowManager.RemoveWindow(this);
        }

        // -------- Private Methods ---------
        protected override void DrawModel()
        {
            ModelGraphicsManager.DrawWallMountedModel(this);
        }
    }
}
