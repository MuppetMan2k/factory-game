﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    class DivTextHeaderUI : UIEntity
    {
        // -------- Private Fields --------
        private string setUpText;

        // -------- Constructors --------
        public DivTextHeaderUI(Point inPosition, string inText, int inWidth, eUIAnchor inAnchor)
            : base(inPosition, inAnchor)
        {
            setUpText = inText;
            Rectangle = new Rectangle(inPosition.X, inPosition.Y, inWidth, 18);
        }

        // -------- Public Methods --------
        public override void Initialize()
        {
            TextUI text = new TextUI(setUpText, new Point(4, 0), eUIAnchor.TOP_LEFT, UIStyleManager.Style.DivHeaderFont, 1, GetRectangle().Width, UIStyleManager.Style.DivHeaderTextColor,
                eTextAlignment.LEFT);
            text.Initialize();

            base.Initialize();

            SetChild(text);
        }

        public override void SBDraw()
        {
            Rectangle absRect = GetAbsoluteRectangle();
            Rectangle drawRect = new Rectangle(absRect.Left, absRect.Bottom - 1, absRect.Width, 1);
            GraphicsManager.GetSpriteBatch().Draw(ContentStorage.GetTexture("white-pixel"), drawRect, SetColorAlpha(UIStyleManager.Style.DivHeaderLineColor));
            base.SBDraw();
        }
    }
}
