﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    abstract class DraggableUI : UIEntity, IInteractableUI
    {
        // -------- Properties --------
        public bool Dragging { get; protected set; }

        // -------- Private Fields --------
        protected Point mouseDragPosition;

        // -------- Constructors --------
        public DraggableUI(Point inPosition, eUIAnchor inAnchor)
            : base(inPosition, inAnchor)
        {
            Dragging = false;
        }

        // -------- Public Methods --------
        public override void Initialize()
        {
            DraggableUIManager.AddDraggableUI(this);
            base.Initialize();
        }

        public override void Denitialize()
        {
            DraggableUIManager.RemoveDraggableUI(this);
            base.Denitialize();
        }

        public override void Update()
        {
            if (Dragging)
            {
                UpdateDragging();
                CheckForMouseRelease();
            }

            base.Update();
        }

        public override void OnMouseClick()
        {
            Dragging = true;
            OnStartDragging();
            base.OnMouseClick();
        }

        // -------- Private Methods ---------
        protected abstract void OnStartDragging();
        protected abstract void OnStopDragging();
        protected abstract void UpdateDragging();

        private void CheckForMouseRelease()
        {
            if (InputManager.GetButtonPressState(eMouseButton.LEFT) == ePressState.RELEASING)
            {
                Dragging = false;
                OnStopDragging();
            }
        }
    }
}
