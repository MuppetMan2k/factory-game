﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    class HoursSliderPinUI : DraggableUI
    {
        // -------- Private Fields --------
        private StaffHours hours;
        private bool isStartTimePin;

        // -------- Constructors --------
        public HoursSliderPinUI(StaffHours inHours, bool inStartTimePin)
            : base(GetInitialPosition(inHours, inStartTimePin), eUIAnchor.TOP_LEFT)
        {
            hours = inHours;
            isStartTimePin = inStartTimePin;

            Point position = GetInitialPosition(inHours, isStartTimePin);
            Rectangle = new Rectangle(position.X, position.Y, 10, 19);
        }

        // -------- Public Methods --------
        public override void SBDraw()
        {
            GraphicsManager.GetSpriteBatch().Draw(ContentStorage.GetTexture("hours-slider-pin"), GetAbsoluteRectangle(), UIStyleManager.Style.SecondaryUIColor);
            base.SBDraw();
        }

        public override void OnMouseOver()
        {
            CursorManager.SetCursorMode(eCursorMode.SELECT);
            base.OnMouseOver();
        }
        public override void OnMouseOut()
        {
            if (!Dragging)
                CursorManager.SetCursorMode(eCursorMode.POINTER);
            base.OnMouseOut();
        }

        // -------- Private Methods --------
        protected override void OnStartDragging()
        {
            SetMouseDragPosition();
        }
        protected override void OnStopDragging()
        {
            if (!MouseOver)
                CursorManager.SetCursorMode(eCursorMode.POINTER);
        }
        protected override void UpdateDragging()
        {
            if (Dragging)
                CheckForPinMovement();
        }

        private void SetMouseDragPosition()
        {
            Point mousePos = InputManager.GetMousePosition();
            Rectangle absRect = GetAbsoluteRectangle();
            int mouseDragX = mousePos.X - absRect.Left;
            int mouseDragY = mousePos.Y - absRect.Top;
            mouseDragPosition = new Point(mouseDragX, mouseDragY);
        }

        private void CheckForPinMovement()
        {
            Point mousePos = InputManager.GetMousePosition();
            Point absNewPinPos = new Point(mousePos.X - mouseDragPosition.X, mousePos.Y - mouseDragPosition.Y);
            Rectangle absParentRect = Parent.GetAbsoluteRectangleForParent(this);
            int newPinX = absNewPinPos.X - absParentRect.Left;

            float relHourFloat = (float)newPinX * (float)(StaffManager.LatestFinishTime - StaffManager.EarliestStartTime) / (float)HoursSliderUI.BarLength;
            int newHour = StaffManager.EarliestStartTime + Mathf.Round(relHourFloat);

            if (isStartTimePin)
                newHour = Mathf.Clamp(newHour, StaffManager.EarliestStartTime, hours.NewFinishTime - 1);
            else
                newHour = Mathf.Clamp(newHour, hours.NewStartTime + 1, StaffManager.LatestFinishTime);

            SetStaffHours(newHour);
            UpdatePosition(newHour);
        }
        private void SetStaffHours(int newHour)
        {
            if (isStartTimePin && newHour != hours.NewStartTime)
                hours.SetNewStartTime(newHour);

            if (!isStartTimePin && newHour != hours.NewFinishTime)
                hours.SetNewFinishTime(newHour);
        }
        private void UpdatePosition(int newHour)
        {
            Rectangle newRect = Rectangle;
            newRect.X = GetPinXPosition(newHour);
            Rectangle = newRect;
        }

        private static Point GetInitialPosition(StaffHours inHours, bool startTimePin)
        {
            int hour = startTimePin ? inHours.NewStartTime : inHours.NewFinishTime;

            return new Point(GetPinXPosition(hour), 0);
        }

        private static int GetPinXPosition(int hour)
        {
            float xFl = (float)(hour - StaffManager.EarliestStartTime) * (float)HoursSliderUI.BarLength / (float)(StaffManager.LatestFinishTime - StaffManager.EarliestStartTime);
            int x = (int)xFl;

            return x;
        }
    }
}
