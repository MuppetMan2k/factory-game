﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FactoryGame
{
    class ScrollBarSliderUI : DraggableUI
    {
        // -------- Private Fields --------
        private int height;
        private int sliderPosition;
        ScrollWindowUI scrollParent;

        // -------- Constructors --------
        public ScrollBarSliderUI(Point inPosition, eUIAnchor inAnchor, int inHeight, ScrollWindowUI inScrollWindow)
            : base(inPosition, inAnchor)
        {
            Rectangle = new Rectangle(inPosition.X, inPosition.Y, 10, inHeight);

            height = inHeight;
            scrollParent = inScrollWindow;
            sliderPosition = 0;
        }

        // -------- Public Methods --------
        public override void SBDraw()
        {
            Rectangle absRect = GetAbsoluteRectangle();

            Texture2D pixelTex = ContentStorage.GetTexture("white-pixel");
            Texture2D scrollEndTex = ContentStorage.GetTexture("scroll-bar-end");
            GraphicsManager.GetSpriteBatch().Draw(scrollEndTex, new Rectangle(absRect.Left, absRect.Top, 10, 5),
                SetColorAlpha(UIStyleManager.Style.ScrollBarColor));
            GraphicsManager.GetSpriteBatch().Draw(scrollEndTex, new Rectangle(absRect.Left, absRect.Bottom - 5,
                10, 5), null, SetColorAlpha(UIStyleManager.Style.ScrollBarColor), 0f, new Vector2(), SpriteEffects.FlipVertically, 0f);
            GraphicsManager.GetSpriteBatch().Draw(pixelTex, new Rectangle(absRect.Left, absRect.Top + 5, 10,
                absRect.Height - 10), SetColorAlpha(UIStyleManager.Style.ScrollBarColor));

            base.SBDraw();
        }

        public override void OnMouseOver()
        {
            CursorManager.SetCursorMode(eCursorMode.SELECT);
            base.OnMouseOver();
        }
        public override void OnMouseOut()
        {
            if (!Dragging)
                CursorManager.SetCursorMode(eCursorMode.POINTER);
            base.OnMouseOut();
        }

        public void MoveScrollBarSlider(int scrollDistance)
        {
            // Slider position change
            int lastSliderPosition = sliderPosition;
            sliderPosition += scrollDistance;
            int maxSliderPosition = scrollParent.Rectangle.Height - height;
            sliderPosition = Mathf.Clamp(sliderPosition, 0, maxSliderPosition);
            int sliderPositionChange = sliderPosition - lastSliderPosition;

            // Rectangle position change
            Rectangle newRect = Rectangle;
            newRect.Y += sliderPositionChange;
            Rectangle = newRect;

            // Update scroll window position
            float sliderPositionRatio = (float)sliderPosition / (float)maxSliderPosition;
            scrollParent.SetScrollPositionRatio(sliderPositionRatio);
        }

        // -------- Private Methods ---------
        protected override void OnStartDragging()
        {
            SetMouseDragPosition();
        }
        protected override void OnStopDragging()
        {
            if (!MouseOver)
                CursorManager.SetCursorMode(eCursorMode.POINTER);
        }
        protected override void UpdateDragging()
        {
            CalculateScrollBarSliderMovement();
        }

        private void SetMouseDragPosition()
        {
            Point mousePos = InputManager.GetMousePosition();
            Rectangle absRect = GetAbsoluteRectangle();
            mouseDragPosition = new Point(mousePos.X - absRect.Left, mousePos.Y - absRect.Top);
        }
        private void CalculateScrollBarSliderMovement()
        {
            Point mousePos = InputManager.GetMousePosition();
            Rectangle absRect = GetAbsoluteRectangle();
            Point effectiveNewMouseDragPos = new Point(mousePos.X - absRect.Left, mousePos.Y - absRect.Top);
            Point mouseMovement = new Point(effectiveNewMouseDragPos.X - mouseDragPosition.X, effectiveNewMouseDragPos.Y - mouseDragPosition.Y);

            MoveScrollBarSlider(mouseMovement.Y);
        }
    }
}
