﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FactoryGame
{
    class HappinessUI : UIEntity
    {
        // -------- Private Fields --------
        private Staff parentStaff;
        private Texture2D texture;
        private string textureId;
        private eNeedLevel happinessLevel;

        // -------- Constructors --------
        public HappinessUI(Staff inStaff, Point inPosition, eUIAnchor inAnchor)
            : base(inPosition, inAnchor)
        {
            parentStaff = inStaff;
        }

        // -------- Public Methods --------
        public override void Initialize()
        {
            SetTexture();
            Rectangle = new Rectangle(setUpPosition.X, setUpPosition.Y, 20, 20);
            base.Initialize();
        }

        public override void Denitialize()
        {
            texture = null;
            base.Denitialize();
        }

        public override void Update()
        {
            SetTexture();
            base.Update();
        }

        public override void SBDraw()
        {
            Color color = StaffNeedManager.GetNeedLevelUIColor(happinessLevel);
            GraphicsManager.GetSpriteBatch().Draw(texture, GetAbsoluteRectangle(), SetColorAlpha(color));
            base.SBDraw();
        }

        // -------- Private Methods ---------
        private void SetTexture()
        {
            happinessLevel = StaffNeedManager.ConvertNeedToNeedLevel(parentStaff.Needs.GetHappiness());
            string newTextureId = StaffNeedManager.GetHappinessTextureId(happinessLevel);

            if (newTextureId == textureId)
                return;

            textureId = newTextureId;
            texture = ContentStorage.GetTexture(textureId);
        }
    }
}
