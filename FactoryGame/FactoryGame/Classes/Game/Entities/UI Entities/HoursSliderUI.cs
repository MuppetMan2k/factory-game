﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FactoryGame
{
    class HoursSliderUI : UIEntity
    {
        // -------- Properties --------
        public static int BarLength { get { return 300; } }

        // -------- Private Fields --------
        private Staff staff;

        // -------- Constructors --------
        public HoursSliderUI(Point inPosition, Staff inStaff, eUIAnchor inAnchor)
            : base(inPosition, inAnchor)
        {
            staff = inStaff;
            Rectangle = new Rectangle(inPosition.X, inPosition.Y, 310, 53);
        }

        // -------- Public Methods --------
        public override void Initialize()
        {
            base.Initialize();

            HoursSliderPinUI pin1 = new HoursSliderPinUI(staff.Hours, true);
            pin1.Initialize();
            pin1.SetTooltip(Localization.LocalizeString("text_tooltip_staff_start_time"), ePortAnchor.BOTTOM);
            SetChild(pin1);
            HoursSliderPinUI pin2 = new HoursSliderPinUI(staff.Hours, false);
            pin2.Initialize();
            pin2.SetTooltip(Localization.LocalizeString("text_tooltip_staff_end_time"), ePortAnchor.BOTTOM);
            SetChild(pin2);

            TextUI amText = new TextUI(Localization.LocalizeString("text_hours_am"), new Point(18, 0), eUIAnchor.BOTTOM_LEFT, UIStyleManager.Style.UISmallFont, 1, 100, UIStyleManager.Style.BarBGColor, eTextAlignment.CENTER);
            amText.Initialize();
            SetChild(amText);
            TextUI pmText = new TextUI(Localization.LocalizeString("text_hours_pm"), new Point(194, 0), eUIAnchor.BOTTOM_LEFT, UIStyleManager.Style.UISmallFont, 1, 100, UIStyleManager.Style.BarBGColor, eTextAlignment.CENTER);
            pmText.Initialize();
            SetChild(pmText);
        }

        public override void SBDraw()
        {
            Rectangle absRect = GetAbsoluteRectangle();

            // Bar BG
            Texture2D barEndTex = ContentStorage.GetTexture("hours-slider-bar-end");
            Texture2D whitePixelTex = ContentStorage.GetTexture("white-pixel");
            GraphicsManager.GetSpriteBatch().Draw(barEndTex, new Rectangle(absRect.Left, absRect.Top + 9, 5, 10), UIStyleManager.Style.BarBGColor);
            GraphicsManager.GetSpriteBatch().Draw(whitePixelTex, new Rectangle(absRect.Left + 5, absRect.Top + 9, absRect.Width - 10, 10), UIStyleManager.Style.BarBGColor);
            GraphicsManager.GetSpriteBatch().Draw(barEndTex, new Rectangle(absRect.Right - 5, absRect.Top + 9, 5, 10), null, UIStyleManager.Style.BarBGColor, 0f, new Vector2(), SpriteEffects.FlipHorizontally, 0f);

            // Bar
            float relStartX = (float)BarLength * (float)(staff.Hours.NewStartTime - StaffManager.EarliestStartTime) / (float)(StaffManager.LatestFinishTime - StaffManager.EarliestStartTime);
            int startX = absRect.Left + 5 + (int)relStartX;
            float relEndX = (float)BarLength * (float)(staff.Hours.NewFinishTime - StaffManager.EarliestStartTime) / (float)(StaffManager.LatestFinishTime - StaffManager.EarliestStartTime);
            int endX = absRect.Left + 5 + (int)relEndX;
            GraphicsManager.GetSpriteBatch().Draw(whitePixelTex, new Rectangle(startX, absRect.Top + 9, endX - startX, 10), UIStyleManager.Style.HoursSliderBarColor);

            base.SBDraw();
        }
    }
}
