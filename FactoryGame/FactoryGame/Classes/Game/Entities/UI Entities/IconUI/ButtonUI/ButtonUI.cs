﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FactoryGame
{
    // -------- Enumerations --------
    public enum eButtonGraphicsType
    {
        SOLID,
        FLOATING
    }

    class ButtonUI : IconUI, IInteractableUI
    {
        // -------- Properties --------
        public eButtonGraphicsType GraphicsType { get; set; }
        public Color FillColor { get; set; }

        // -------- Delegates --------
        public delegate void OnPressButton();

        // -------- Private Fields --------
        private OnPressButton buttonPressMethod;
        private Texture2D texture;
        private string textureId;

        // -------- Constructors --------
        public ButtonUI(Point inPosition, eIconSize inSize, eUIAnchor inAnchor, eButtonGraphicsType inGraphicsType, string inTextureId, OnPressButton inButtonPressMethod, Color inFillColor)
            : base(inPosition, inSize, inAnchor)
        {
            GraphicsType = inGraphicsType;
            textureId = inTextureId;
            buttonPressMethod = inButtonPressMethod;
            FillColor = inFillColor;
        }

        // -------- Public Methods --------
        public override void Initialize()
        {
            texture = ContentStorage.GetTexture(textureId);
            base.Initialize();
        }

        public override void Denitialize()
        {
            texture = null;
            base.Denitialize();
        }

        public override void SBDraw()
        {
            // Icon fill
            if (GraphicsType == eButtonGraphicsType.SOLID)
                GraphicsManager.GetSpriteBatch().Draw(iconTexture, GetAbsoluteRectangle(), SetColorAlpha(FillColor));

            // Central texture
            GraphicsManager.GetSpriteBatch().Draw(texture, GetAbsoluteRectangle(), SetColorAlpha(Color.White));

            // Highlight
            if (DrawHighlight)
            {
                if (GraphicsType == eButtonGraphicsType.SOLID)
                    GraphicsManager.GetSpriteBatch().Draw(iconTexture, GetAbsoluteRectangle(), SetColorAlpha(UIStyleManager.Style.ButtonHighlightColor));
                else if (GraphicsType == eButtonGraphicsType.FLOATING)
                    GraphicsManager.GetSpriteBatch().Draw(texture, GetAbsoluteRectangle(), SetColorAlpha(UIStyleManager.Style.ButtonHighlightColor));
            }

            base.SBDraw();
        }

        public override void OnMouseOver()
        {
            DrawHighlight = true;
            CursorManager.SetCursorMode(eCursorMode.SELECT);
            base.OnMouseOver();
        }
        public override void OnMouseOut()
        {
            DrawHighlight = false;
            CursorManager.SetCursorMode(eCursorMode.POINTER);
            base.OnMouseOut();
        }
        public override void OnMouseClick()
        {
            if (buttonPressMethod != null)
                buttonPressMethod();
            base.OnMouseClick();
        }
    }
}
