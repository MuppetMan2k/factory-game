﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    class ToggleButtonUI : ButtonUI
    {
        // -------- Delegates --------
        public delegate bool IsToggled();

        // -------- Public Fields --------
        private IsToggled isToggledMethod;

        // -------- Constructors --------
        public ToggleButtonUI(Point inPosition, eIconSize inSize, eUIAnchor inAnchor, eButtonGraphicsType inGraphicsType, string inTextureId, OnPressButton inButtonPressMethod, Color inFillColor, IsToggled inIsToggledMethod)
            : base(inPosition, inSize, inAnchor, inGraphicsType, inTextureId, inButtonPressMethod, inFillColor)
        {
            isToggledMethod = inIsToggledMethod;
        }

        // -------- Public Methods --------
        public override void Update()
        {
            if (isToggledMethod())
                DrawHighlight = true;
            else if (!MouseOver)
                DrawHighlight = false;

            base.Update();
        }

        public override void OnMouseOut()
        {
            base.OnMouseOut();

            if (isToggledMethod())
                DrawHighlight = true;
        }
    }
}
