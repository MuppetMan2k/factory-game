﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    class ElementIconUI : IconUI
    {
        // -------- Properties --------
        public ElementHolder ElementHolder { get; private set; }

        // -------- Constructors --------
        public ElementIconUI(Point inPosition, eUIAnchor inAnchor, ElementHolder inElementHolder)
            : base(inPosition, eIconSize.ICON_50, inAnchor)
        {
            ElementHolder = inElementHolder;
        }

        // -------- Public Methods --------
        public override void SBDraw()
        {
            if (!ElementHolder.IsHoldingElement())
            {
                GraphicsManager.GetSpriteBatch().Draw(ContentStorage.GetTexture(UIUtilities.GetIconTextureId(IconSize)), GetAbsoluteRectangle(), SetColorAlpha(UIStyleManager.Style.EmptyIconColor));
                base.SBDraw();
                return;
            }

            GraphicsManager.GetSpriteBatch().Draw(ContentStorage.GetTexture(UIUtilities.GetIconTextureId(IconSize)), GetAbsoluteRectangle(), SetColorAlpha(UIStyleManager.Style.ElementIconColor));
            GraphicsManager.GetSpriteBatch().Draw(ContentStorage.GetTexture(ElementHolder.Element.Data.IconTextureId), GetAbsoluteRectangle(), SetColorAlpha(Color.White));

            // TODO - draw quality

            base.SBDraw();
        }
    }
}
