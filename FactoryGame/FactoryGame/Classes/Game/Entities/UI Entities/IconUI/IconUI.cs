﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FactoryGame
{
    // -------- Enumerations --------
    public enum eIconSize
    {
        ICON_20,
        ICON_25,
        ICON_30,
        ICON_40,
        ICON_50
    }

    abstract class IconUI : UIEntity
    {
        // -------- Properties --------
        public eIconSize IconSize { get; set; }

        // -------- Private Fields --------
        protected Texture2D iconTexture;

        // -------- Constructors --------
        public IconUI(Point inPosition, eIconSize inIconSize, eUIAnchor inAnchor)
            : base(inPosition, inAnchor)
        {
            IconSize = inIconSize;
            Anchor = inAnchor;
        }

        // -------- Public Methods --------
        public override void Initialize()
        {
            iconTexture = ContentStorage.GetTexture(UIUtilities.GetIconTextureId(IconSize));
            Rectangle = new Rectangle(setUpPosition.X, setUpPosition.Y, UIUtilities.GetIconSize(IconSize), UIUtilities.GetIconSize(IconSize));
            base.Initialize();
        }

        public override void Denitialize()
        {
            iconTexture = null;
            base.Denitialize();
        }

        public override bool MouseIsOver(out float depth)
        {
            if (UIUtilities.PointIsOverTexturedRectangle(InputManager.GetMousePosition(), GetAbsoluteRectangle(), iconTexture))
            {
                depth = 0f;
                return true;
            }

            depth = 1f;
            return false;
        }
    }
}
