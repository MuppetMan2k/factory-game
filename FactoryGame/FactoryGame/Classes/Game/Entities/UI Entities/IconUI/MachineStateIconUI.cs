﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FactoryGame
{
    class MachineStateIconUI : IconUI
    {
        // -------- Private Fields --------
        private Texture2D texture;
        private string textureId;
        private Machine machineParent;

        // -------- Constructors --------
        public MachineStateIconUI(Point inPosition, Machine inMachine, eUIAnchor inAnchor)
            : base(inPosition, eIconSize.ICON_30, inAnchor)
        {
            machineParent = inMachine;
        }

        // -------- Public Methods --------
        public override void Initialize()
        {
            base.Initialize();

            ProgressBarUI bar = new ProgressBarUI(new Point(3, 6), 24, 3, machineParent.GetProcessCompletion, UIStyleManager.Style.BarBGColor, UIStyleManager.Style.CompletionBarColor,
                eUIAnchor.BOTTOM_LEFT);
            bar.Initialize();
            SetChild(bar);

            SetTexture();
        }

        public override void Denitialize()
        {
            texture = null;
            base.Denitialize();
        }

        public override void Update()
        {
            SetTexture();
            base.Update();
        }

        public override void SBDraw()
        {
            GraphicsManager.GetSpriteBatch().Draw(iconTexture, GetAbsoluteRectangle(), SetColorAlpha(UIStyleManager.Style.GeneralIconColor));
            GraphicsManager.GetSpriteBatch().Draw(texture, GetAbsoluteRectangle(), SetColorAlpha(Color.White));

            base.SBDraw();
        }

        // -------- Private Methods ---------
        private void SetTexture()
        {
            bool stateIsOperate;
            string newTextureId = MachineManager.GetTextureIdForMachineState(machineParent, out stateIsOperate);
            if (textureId == newTextureId)
                return;

            GetChildBar().transparency = stateIsOperate ? 1f : 0f;

            textureId = newTextureId;
            texture = ContentStorage.GetTexture(textureId);
        }

        private ProgressBarUI GetChildBar()
        {
            foreach (UIEntity ui in children)
            {
                ProgressBarUI bar = ui as ProgressBarUI;
                if (bar != null)
                    return bar;
            }

            return null;
        }
    }
}
