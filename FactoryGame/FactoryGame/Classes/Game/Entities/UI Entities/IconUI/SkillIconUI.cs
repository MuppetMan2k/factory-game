﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FactoryGame
{
    class SkillIconUI : IconUI
    {
        // -------- Private Fields --------
        private Skill skill;

        // -------- Constructors --------
        public SkillIconUI(Point inPosition, Skill inSkill, eUIAnchor inAnchor)
            : base(inPosition, eIconSize.ICON_50, inAnchor)
        {
            skill = inSkill;
        }

        // -------- Public Methods --------
        public override void Initialize()
        {
            base.Initialize();

            // TODO - create progress bar
        }

        public override void SBDraw()
        {
            if (skill == null)
            {
                GraphicsManager.GetSpriteBatch().Draw(iconTexture, GetAbsoluteRectangle(), SetColorAlpha(UIStyleManager.Style.EmptyIconColor));
                base.SBDraw();
                return;
            }

            GraphicsManager.GetSpriteBatch().Draw(iconTexture, GetAbsoluteRectangle(), SetColorAlpha(UIStyleManager.Style.SkillIconColor));
            GraphicsManager.GetSpriteBatch().Draw(ContentStorage.GetTexture(skill.Data.IconTextureId), GetAbsoluteRectangle(), SetColorAlpha(Color.White));

            // TODO - Draw skill level and progress

            base.SBDraw();
        }
    }
}
