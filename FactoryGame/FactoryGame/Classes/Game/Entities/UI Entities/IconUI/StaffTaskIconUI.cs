﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FactoryGame
{
    class StaffTaskIconUI : IconUI
    {
        // -------- Private Fields --------
        private Texture2D texture;
        private string textureId;
        private Staff staffParent;
        private eTaskType taskType;
        private bool representsSpecificTask;

        // -------- Constructors --------
        public StaffTaskIconUI(Point inPosition, Staff inStaff, eUIAnchor inAnchor)
            : base(inPosition, eIconSize.ICON_30, inAnchor)
        {
            staffParent = inStaff;
            representsSpecificTask = false;
        }
        public StaffTaskIconUI(Point inPosition, eTaskType inTaskType, eUIAnchor inAnchor)
            : base(inPosition, eIconSize.ICON_30, inAnchor)
        {
            taskType = inTaskType;
            representsSpecificTask = true;
        }

        // -------- Public Methods --------
        public override void Initialize()
        {
            SetTexture();
            base.Initialize();
        }

        public override void Denitialize()
        {
            texture = null;
            base.Denitialize();
        }

        public override void Update()
        {
            if (!representsSpecificTask)
                SetTexture();
            base.Update();
        }

        public override void SBDraw()
        {
            GraphicsManager.GetSpriteBatch().Draw(iconTexture, GetAbsoluteRectangle(), SetColorAlpha(UIStyleManager.Style.GeneralIconColor));
            GraphicsManager.GetSpriteBatch().Draw(texture, GetAbsoluteRectangle(), SetColorAlpha(Color.White));

            base.SBDraw();
        }

        // -------- Private Methods ---------
        private void SetTexture()
        {
            if (representsSpecificTask)
            {
                textureId = TaskManager.GetTextureIdForTaskType(taskType);
                texture = ContentStorage.GetTexture(textureId);
                return;
            }

            string newTextureId = TaskManager.GetTextureIdForTask(staffParent.Tasks.CurrentTask, staffParent);
            if (textureId == newTextureId)
                return;

            textureId = newTextureId;
            texture = ContentStorage.GetTexture(textureId);
        }
    }
}
