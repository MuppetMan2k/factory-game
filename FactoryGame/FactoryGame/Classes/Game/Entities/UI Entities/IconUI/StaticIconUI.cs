﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FactoryGame
{
    class StaticIconUI : IconUI
    {
        // -------- Properties --------
        public eButtonGraphicsType GraphicsType { get; set; }
        public Color FillColor { get; set; }

        // -------- Delegates --------
        public delegate void OnPressButton();

        // -------- Private Fields --------
        private Texture2D texture;
        private string textureId;

        // -------- Constructors --------
        public StaticIconUI(Point inPosition, eIconSize inSize, eUIAnchor inAnchor, eButtonGraphicsType inGraphicsType, string inTextureId, Color inFillColor)
            : base(inPosition, inSize, inAnchor)
        {
            GraphicsType = inGraphicsType;
            textureId = inTextureId;
            FillColor = inFillColor;
        }

        // -------- Public Methods --------
        public override void Initialize()
        {
            texture = ContentStorage.GetTexture(textureId);
            base.Initialize();
        }

        public override void Denitialize()
        {
            texture = null;
            base.Denitialize();
        }

        public override void SBDraw()
        {
            // Icon fill
            if (GraphicsType == eButtonGraphicsType.SOLID)
                GraphicsManager.GetSpriteBatch().Draw(iconTexture, GetAbsoluteRectangle(), SetColorAlpha(FillColor));

            // Central texture
            GraphicsManager.GetSpriteBatch().Draw(texture, GetAbsoluteRectangle(), SetColorAlpha(Color.White));

            base.SBDraw();
        }
    }
}
