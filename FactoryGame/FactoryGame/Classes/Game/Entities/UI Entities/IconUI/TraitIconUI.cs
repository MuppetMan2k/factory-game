﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    class TraitIconUI : IconUI
    {
        // -------- Private Fields --------
        private Trait trait;

        // -------- Constructors --------
        public TraitIconUI(Point inPosition, Trait inTrait, eUIAnchor inAnchor)
            : base(inPosition, eIconSize.ICON_50, inAnchor)
        {
            trait = inTrait;
        }

        // -------- Public Methods --------
        public override void SBDraw()
        {
            if (trait == null)
            {
                GraphicsManager.GetSpriteBatch().Draw(iconTexture, GetAbsoluteRectangle(), SetColorAlpha(UIStyleManager.Style.EmptyIconColor));
                base.SBDraw();
                return;
            }

            GraphicsManager.GetSpriteBatch().Draw(iconTexture, GetAbsoluteRectangle(), SetColorAlpha(UIStyleManager.Style.SkillIconColor));
            GraphicsManager.GetSpriteBatch().Draw(ContentStorage.GetTexture(trait.GetIconTextureId()), GetAbsoluteRectangle(), SetColorAlpha(Color.White));

            // TODO - Draw skill level and progress

            base.SBDraw();
        }
    }
}
