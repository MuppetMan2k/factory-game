﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    // -------- Enumerations --------
    public enum ePortAnchor
    {
        LEFT,
        RIGHT,
        TOP,
        BOTTOM
    }

    abstract class AnchoredPortUI : PortUI
    {
        // -------- Properties --------
        public ePortAnchor PortAnchor { get; set; }
        public UIEntity AnchorEntity { get; set; }
        public Point ArrowSize { get; set; }

        // -------- Constructors --------
        public AnchoredPortUI(eCornerRoundSize inCornerRoundSize, UIEntity inAnchorEntity, ePortAnchor inPortAnchor)
            : base(new Point(), inCornerRoundSize)
        {
            AnchorEntity = inAnchorEntity;
            PortAnchor = inPortAnchor;
            FadeValue = 0f;
            transparency = 0f;
        }

        // -------- Public Methods --------
        public void SetRectangle(int width, int height)
        {
            int x = 0;
            int y = 0;

            switch (PortAnchor)
            {
            case ePortAnchor.LEFT:
                x = AnchorEntity.GetAbsoluteRectangle().Right + ArrowSize.Y;
                y = AnchorEntity.GetAbsoluteRectangle().Center.Y - height / 2;
                break;
            case ePortAnchor.RIGHT:
                x = AnchorEntity.GetAbsoluteRectangle().Left - width - ArrowSize.Y;
                y = AnchorEntity.GetAbsoluteRectangle().Center.Y - height / 2;
                break;
            case ePortAnchor.BOTTOM:
                x = AnchorEntity.GetAbsoluteRectangle().Center.X - width / 2;
                y = AnchorEntity.GetAbsoluteRectangle().Top - height - ArrowSize.Y;
                break;
            case ePortAnchor.TOP:
                x = AnchorEntity.GetAbsoluteRectangle().Center.X - width / 2;
                y = AnchorEntity.GetAbsoluteRectangle().Bottom + ArrowSize.Y;
                break;
            }

            Rectangle = new Rectangle(x, y, width, height);
        }
    }
}
