﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FactoryGame
{
    class BubbleUI : PortUI
    {
        // -------- Properties --------
        public Entity AnchorEntity { get; set; }
        public Point ArrowSize { get; set; }

        // -------- Private Fields --------
        public int headerHeight;
        private bool repositioned;
        private TextUI.UpdateText headerTextHandler;
        private string headerText;

        // -------- Constructors --------
        public BubbleUI(Entity inEntity, TextUI.UpdateText inHeaderTextMethod)
            : base(inEntity.GetBubbleAnchorPoint(), eCornerRoundSize.RADIUS_20)
        {
            AnchorEntity = inEntity;
            ArrowSize = BubbleManager.BubbleArrowSize;
            headerHeight = BubbleManager.BubbleHeaderHeight;
            headerTextHandler = inHeaderTextMethod;
        }
        public BubbleUI(Entity inEntity, string inHeaderText)
            : base(inEntity.GetBubbleAnchorPoint(), eCornerRoundSize.RADIUS_20)
        {
            AnchorEntity = inEntity;
            ArrowSize = BubbleManager.BubbleArrowSize;
            headerHeight = BubbleManager.BubbleHeaderHeight;
            headerText = inHeaderText;
        }

        // -------- Public Methods --------
        public override void Initialize()
        {
            base.Initialize();

            int textBorder = 10;
            TextUI text;
            if (headerTextHandler == null)
                text = new TextUI(headerText, new Point(textBorder, 5), eUIAnchor.TOP_LEFT, UIStyleManager.Style.UIHeaderFont, 1, GetWidth() - 2 * textBorder,
                    UIStyleManager.Style.PortHeaderTextColor, eTextAlignment.LEFT);
            else
                text = new TextUI(headerTextHandler, new Point(textBorder, 5), eUIAnchor.TOP_LEFT, UIStyleManager.Style.UIHeaderFont, 1, GetWidth() - 2 * textBorder,
                    UIStyleManager.Style.PortHeaderTextColor, eTextAlignment.LEFT);
            text.Initialize();
            SetChild(text);

            SetPosition();
        }

        public override void Update()
        {
            SetPosition();
            base.Update();
        }

        public override void SBDraw()
        {
            Rectangle absRect = GetAbsoluteRectangle();
            Texture2D cornerTex = ContentStorage.GetTexture(UIUtilities.GetCornerRoundTextureId(CornerRoundSize));
            Texture2D pixelTex = ContentStorage.GetTexture("white-pixel");
            int cornerSize = UIUtilities.GetCornerRoundSize(CornerRoundSize);

            // Header
            GraphicsManager.GetSpriteBatch().Draw(cornerTex, new Rectangle(absRect.X, absRect.Y, cornerSize, cornerSize), SetColorAlpha(UIStyleManager.Style.PortHeaderColor));
            GraphicsManager.GetSpriteBatch().Draw(cornerTex, new Rectangle(absRect.Right, absRect.Y, cornerSize, cornerSize), null, SetColorAlpha(UIStyleManager.Style.PortHeaderColor),
                Mathf.ToRads(90f), new Vector2(), SpriteEffects.None, 0f);
            GraphicsManager.GetSpriteBatch().Draw(pixelTex, new Rectangle(absRect.X + cornerSize, absRect.Y, absRect.Width - 2 * cornerSize, cornerSize), SetColorAlpha(UIStyleManager.Style.PortHeaderColor));
            GraphicsManager.GetSpriteBatch().Draw(pixelTex, new Rectangle(absRect.X, absRect.Y + cornerSize, absRect.Width, headerHeight - cornerSize), SetColorAlpha(UIStyleManager.Style.PortHeaderColor));

            // Body
            GraphicsManager.GetSpriteBatch().Draw(cornerTex, new Rectangle(absRect.X, absRect.Bottom, cornerSize, cornerSize), null, SetColorAlpha(UIStyleManager.Style.PortBodyColor),
                Mathf.ToRads(-90f), new Vector2(), SpriteEffects.None, 0f);
            GraphicsManager.GetSpriteBatch().Draw(cornerTex, new Rectangle(absRect.Right, absRect.Bottom, cornerSize, cornerSize), null, SetColorAlpha(UIStyleManager.Style.PortBodyColor),
                Mathf.ToRads(180f), new Vector2(), SpriteEffects.None, 0f);
            GraphicsManager.GetSpriteBatch().Draw(pixelTex, new Rectangle(absRect.X, absRect.Y + headerHeight, absRect.Width, absRect.Height - cornerSize - headerHeight),
                SetColorAlpha(UIStyleManager.Style.PortBodyColor));
            GraphicsManager.GetSpriteBatch().Draw(pixelTex, new Rectangle(absRect.X + cornerSize, absRect.Bottom - cornerSize, absRect.Width - 2 * cornerSize, cornerSize),
                SetColorAlpha(UIStyleManager.Style.PortBodyColor));

            // Arrow
            if (!repositioned)
            {
                Texture2D arrowTexture = ContentStorage.GetTexture("tooltip-arrow");
                GraphicsManager.GetSpriteBatch().Draw(arrowTexture, new Rectangle(absRect.Center.X - ArrowSize.X / 2, absRect.Bottom, ArrowSize.X, ArrowSize.Y),
                    SetColorAlpha(UIStyleManager.Style.PortBodyColor));
            }

            base.SBDraw();
        }

        public override void Destroy()
        {
            BubbleManager.RemoveBubble(this);
            base.Destroy();
        }

        public int GetHeight()
        {
            if (children.Count == 0)
                return BubbleManager.MinBubbleHeight;

            Rectangle union = children[0].GetAbsoluteRectangle();

            for (int i = 1; i < children.Count; i++)
            {
                Rectangle rect = children[i].GetAbsoluteRectangle();
                union = Rectangle.Union(union, rect);
            }

            return union.Height + 2 * BubbleManager.BubbleBorder;
        }
        public int GetWidth()
        {
            return BubbleManager.BubbleWidth;
        }

        // -------- Private Methods ---------
        private void SetPosition()
        {
            int height = GetHeight();
            int width = GetWidth();
            Point anchor = AnchorEntity.GetBubbleAnchorPoint();

            int x = anchor.X - width / 2;
            int y = anchor.Y - height - ArrowSize.Y;

            repositioned = RepositionBubble(ref x, ref y, width, height);

            Rectangle = new Rectangle(x, y, width, height);

            if (x == 0)
            { }
        }

        private bool RepositionBubble(ref int x, ref int y, int width, int height)
        {
            bool mustReposition = false;

            if (x < 0)
            {
                x = 0;
                mustReposition = true;
            }
            else if (x > GraphicsManager.ScreenWidth - width)
            {
                x = GraphicsManager.ScreenWidth - width;
                mustReposition = true;
            }

            if (y < 0)
            {
                y = 0;
                mustReposition = true;
            }

            return mustReposition;
        }
    }
}
