﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FactoryGame
{
    class ElementCarryingUI : PortUI
    {
        // -------- Properties --------
        public GameEntity GameEntity { get; private set; }

        // -------- Private Fields --------
        private List<ElementCarrying> carryings;
        private int numCarryings;
        private int maxElementRows;
        private bool reducedSingleElementUI;
        private bool shouldBeDrawn;
        // Constant params
        private const int ELEMENTS_PER_ROW = 3;
        private const int PIXEL_BORDER = 15;
        private const int ELEMENT_OFFSET = 5;
        private const int CARRYING_OFFSET = 15;
        private const int ELEMENT_ICON_SIZE = 50;
        private const int TITLE_HEIGHT = 10;
        private const int ARROW_X = 20;
        private const int ARROW_Y = 10;

        // -------- Constructors --------
        public ElementCarryingUI(GameEntity inEntity, List<ElementCarrying> inCarryings)
            : base(inEntity.GetBubbleAnchorPoint(), eCornerRoundSize.RADIUS_20)
        {
            GameEntity = inEntity;
            carryings = inCarryings;
            reducedSingleElementUI = IsReducedSingleElementUI();

            CalculateCarryingNumbers();

            Point anchorPoint = GameEntity.GetBubbleAnchorPoint();
            int width = GetWindowWidth();
            int height = GetWindowHeight();

            UpdateRectangle();
        }

        // -------- Public Methods --------
        public override void Initialize()
        {
            base.Initialize();

            if (reducedSingleElementUI)
                SetUpChildren_ReducedUI();
            else
                SetUpChildren();

            UpdateShouldBeDrawn();
        }

        public override void Denitialize()
        {
            GameEntity = null;
            carryings = null;

            base.Denitialize();
        }

        public override void Update()
        {
            UpdateRectangle();
            base.Update();
        }

        public override void SBDraw()
        {
            if (!shouldBeDrawn)
                return;

            Rectangle absRect = GetAbsoluteRectangle();
            Texture2D cornerTex = ContentStorage.GetTexture(UIUtilities.GetCornerRoundTextureId(CornerRoundSize));
            Texture2D pixelTex = ContentStorage.GetTexture("white-pixel");
            int cornerSize = UIUtilities.GetCornerRoundSize(CornerRoundSize);

            // Body
            GraphicsManager.GetSpriteBatch().Draw(cornerTex, new Rectangle(absRect.X, absRect.Y, cornerSize, cornerSize), SetColorAlpha(UIStyleManager.Style.PortBodyColor));
            GraphicsManager.GetSpriteBatch().Draw(cornerTex, new Rectangle(absRect.Right, absRect.Y, cornerSize, cornerSize), null, SetColorAlpha(UIStyleManager.Style.PortBodyColor),
                Mathf.ToRads(90f), new Vector2(), SpriteEffects.None, 0f);
            GraphicsManager.GetSpriteBatch().Draw(cornerTex, new Rectangle(absRect.X, absRect.Bottom, cornerSize, cornerSize), null, SetColorAlpha(UIStyleManager.Style.PortBodyColor),
                Mathf.ToRads(-90f), new Vector2(), SpriteEffects.None, 0f);
            GraphicsManager.GetSpriteBatch().Draw(cornerTex, new Rectangle(absRect.Right, absRect.Bottom, cornerSize, cornerSize), null, SetColorAlpha(UIStyleManager.Style.PortBodyColor), Mathf.ToRads(180f), new Vector2(), SpriteEffects.None, 0f);
            GraphicsManager.GetSpriteBatch().Draw(pixelTex, new Rectangle(absRect.X + cornerSize, absRect.Y, absRect.Width - 2 * cornerSize, cornerSize), SetColorAlpha(UIStyleManager.Style.PortBodyColor));
            GraphicsManager.GetSpriteBatch().Draw(pixelTex, new Rectangle(absRect.X, absRect.Y + cornerSize, absRect.Width, absRect.Height - 2 * cornerSize), SetColorAlpha(UIStyleManager.Style.PortBodyColor));
            GraphicsManager.GetSpriteBatch().Draw(pixelTex, new Rectangle(absRect.X + cornerSize, absRect.Bottom - cornerSize, absRect.Width - 2 * cornerSize, cornerSize),
                SetColorAlpha(UIStyleManager.Style.PortBodyColor));

            // Lines
            for (int i = 1; i < numCarryings; i++)
            {
                int x = absRect.Left + PIXEL_BORDER + i * (ELEMENTS_PER_ROW * ELEMENT_ICON_SIZE + (ELEMENTS_PER_ROW - 1) * ELEMENT_OFFSET) + (i - 1) * CARRYING_OFFSET + CARRYING_OFFSET / 2;
                Rectangle lineRect = new Rectangle(x, absRect.Top + PIXEL_BORDER + TITLE_HEIGHT, 1, absRect.Height - 2 * PIXEL_BORDER - TITLE_HEIGHT);
                GraphicsManager.GetSpriteBatch().Draw(pixelTex, lineRect, UIStyleManager.Style.UIBGColor);
            }

            // Arrow
            Texture2D arrowTexture = ContentStorage.GetTexture("tooltip-arrow");
            GraphicsManager.GetSpriteBatch().Draw(arrowTexture, new Rectangle(absRect.Center.X - ARROW_X / 2, absRect.Bottom, ARROW_X, ARROW_Y),
                SetColorAlpha(UIStyleManager.Style.PortBodyColor));

            base.SBDraw();
        }

        public void OnElementMoved()
        {
            UpdateShouldBeDrawn();
        }

        // -------- Private Methods --------
        private void CalculateCarryingNumbers()
        {
            numCarryings = carryings.Count;
            maxElementRows = 0;
            foreach (ElementCarrying c in carryings)
            {
                int numRows = Mathf.Ceiling((float)c.NumElementHolders / (float)ELEMENTS_PER_ROW);
                maxElementRows = Mathf.Max(maxElementRows, numRows);
            }
        }

        private int GetWindowWidth()
        {
            if (reducedSingleElementUI)
                return 2 * PIXEL_BORDER + ELEMENT_ICON_SIZE;

            int width = 2 * PIXEL_BORDER;
            width += numCarryings * (ELEMENTS_PER_ROW * ELEMENT_ICON_SIZE + (ELEMENTS_PER_ROW - 1) * ELEMENT_OFFSET);
            width += (numCarryings - 1) * CARRYING_OFFSET;

            return width;
        }
        private int GetWindowHeight()
        {
            if (reducedSingleElementUI)
                return 2 * PIXEL_BORDER + ELEMENT_ICON_SIZE;

            int height = 2 * PIXEL_BORDER;
            height += maxElementRows * ELEMENT_ICON_SIZE;
            height += (maxElementRows - 1) * ELEMENT_OFFSET;
            height += TITLE_HEIGHT;

            return height;
        }

        private void SetUpChildren()
        {
            int carryingXDiff = ELEMENTS_PER_ROW * ELEMENT_ICON_SIZE + (ELEMENTS_PER_ROW - 1) * ELEMENT_OFFSET + CARRYING_OFFSET;

            for (int c = 0; c < numCarryings; c++)
            {
                ElementCarrying carrying = carryings[c];

                Point textPos = new Point(PIXEL_BORDER + c * carryingXDiff, PIXEL_BORDER - 10);
                TextUI text = new TextUI(Localization.LocalizeString(carrying.LocId), textPos, eUIAnchor.TOP_LEFT, UIStyleManager.Style.UIFont, 1, carryingXDiff, UIStyleManager.Style.PortBodyTextColor, eTextAlignment.LEFT);
                text.Initialize();
                SetChild(text);

                int row = 0;
                int col = 0;
                for (int e = 0; e < carrying.NumElementHolders; e++)
                {
                    Point elementPos = new Point(PIXEL_BORDER + c * carryingXDiff + col * (ELEMENT_ICON_SIZE + ELEMENT_OFFSET), PIXEL_BORDER + TITLE_HEIGHT + row * (ELEMENT_ICON_SIZE + ELEMENT_OFFSET));
                    ElementIconUI elementIcon = new ElementIconUI(elementPos, eUIAnchor.TOP_LEFT, carrying.ElementHolders[e]);
                    elementIcon.Initialize();
                    SetChild(elementIcon);

                    col++;
                    if (col >= ELEMENTS_PER_ROW)
                    {
                        col = 0;
                        row++;
                    }
                }

                if (carrying.Limitless)
                {
                    Point numberMoreElementsTextPos = new Point(PIXEL_BORDER + c * carryingXDiff, 0);
                    TextUI numberMoreElementsText = new TextUI(carrying.GetNumberMoreElementsText, numberMoreElementsTextPos, eUIAnchor.BOTTOM_LEFT, UIStyleManager.Style.UISmallFont, 1, 250, UIStyleManager.Style.PortBodyTextColor, eTextAlignment.LEFT);
                    numberMoreElementsText.Initialize();
                    SetChild(numberMoreElementsText);
                }
            }
        }
        private void SetUpChildren_ReducedUI()
        {
            ElementCarrying carrying = carryings[0];

            Point elementPos = new Point(PIXEL_BORDER, PIXEL_BORDER);
            ElementIconUI elementIcon = new ElementIconUI(elementPos, eUIAnchor.TOP_LEFT, carrying.ElementHolders[0]);
            elementIcon.Initialize();
            SetChild(elementIcon);
        }

        private void UpdateRectangle()
        {
            Point anchorPoint = GameEntity.GetBubbleAnchorPoint();
            int width = GetWindowWidth();
            int height = GetWindowHeight();

            Rectangle = new Rectangle(anchorPoint.X - width / 2, anchorPoint.Y - ARROW_Y - height, width, height);
        }

        private void UpdateShouldBeDrawn()
        {
            Staff staff = GameEntity as Staff;
            if (staff != null)
            {
                shouldBeDrawn = staff.CarryElement.IsHoldingElement();
                return;
            }

            shouldBeDrawn = true;
        }

        private bool IsReducedSingleElementUI()
        {
            if (GameEntity is Character)
                return true;

            return false;
        }
    }
}
