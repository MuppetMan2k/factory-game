﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class TooltipInfo
    {
        // -------- Properties --------
        public string Text { get; set; }
        public ePortAnchor PortAnchor { get; set; }

        // -------- Constructors --------
        public TooltipInfo(string inText, ePortAnchor inPortAnchor)
        {
            Text = inText;
            PortAnchor = inPortAnchor;
        }
    }
}
