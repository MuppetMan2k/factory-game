﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    // -------- Enumerations --------
    public enum eCornerRoundSize
    {
        RADIUS_10,
        RADIUS_20
    }

    abstract class PortUI : UIEntity
    {
        // -------- Properties --------
        public float FadeValue { get; set; }
        public eCornerRoundSize CornerRoundSize { get; private set; }

        // -------- Constructors --------
        public PortUI(Point inPosition, eCornerRoundSize inCornerRoundSize)
            : base(inPosition, eUIAnchor.TOP_LEFT)
        {
            CornerRoundSize = inCornerRoundSize;
        }

        // -------- Public Methods --------
        public override bool MouseIsOver(out float depth)
        {
            if (UIUtilities.PointIsInRectangle(InputManager.GetMousePosition(), GetAbsoluteRectangle()))
            {
                depth = 0f;
                return true;
            }

            depth = 1f;
            return false;
        }
    }
}
