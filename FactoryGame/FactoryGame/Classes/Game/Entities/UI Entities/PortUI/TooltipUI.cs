﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FactoryGame
{
    class TooltipUI : AnchoredPortUI
    {
        // -------- Private Fields --------
        private string setUpText;
        private int width;
        private int height;

        // -------- Constructors --------
        public TooltipUI(UIEntity inAnchorEntity, string inText, ePortAnchor inPortAnchor)
            : base(eCornerRoundSize.RADIUS_10, inAnchorEntity, inPortAnchor)
        {
            AnchorEntity = inAnchorEntity;
            PortAnchor = inPortAnchor;
            ArrowSize = new Point(20, 10);
            setUpText = inText;
        }

        // -------- Public Methods --------
        public override void Initialize()
        {
            base.Initialize();

            TextUI text = new TextUI(setUpText, new Point(TooltipManager.TooltipTextHorizBorder, TooltipManager.TooltipTextVertBorder), eUIAnchor.TOP_LEFT, UIStyleManager.Style.TooltipFont, 0,
                TooltipManager.GetTooltipMaxWidth(setUpText), UIStyleManager.Style.TooltipTextColor, eTextAlignment.LEFT);
            text.Initialize();
            text.Alignment = eTextAlignment.CENTER;
            Rectangle textRect = text.GetRelativeRectangle();

            width = textRect.Width + 2 * TooltipManager.TooltipTextHorizBorder;
            height = textRect.Height + 2 * TooltipManager.TooltipTextVertBorder;

            // Make sure sideways arrows fit on side edges
            if (PortAnchor == ePortAnchor.LEFT || PortAnchor == ePortAnchor.RIGHT)
            {
                height = Mathf.Max(height, ArrowSize.X + 2 * UIUtilities.GetCornerRoundSize(CornerRoundSize));
                int newY = (height - text.Rectangle.Height) / 2;
                text.Rectangle = new Rectangle(text.Rectangle.X, newY, text.Rectangle.Width, text.Rectangle.Height);
            }

            SetRectangle(width, height);

            SetChild(text);
        }

        public override void Update()
        {
            SetRectangle(width, height);
            base.Update();
        }

        public override void SBDraw()
        {
            Rectangle absRect = GetAbsoluteRectangle();

            // Draw corners
            Texture2D cornerTexture = ContentStorage.GetTexture(UIUtilities.GetCornerRoundTextureId(CornerRoundSize));
            int cornerSize = UIUtilities.GetCornerRoundSize(CornerRoundSize);
            GraphicsManager.GetSpriteBatch().Draw(cornerTexture, new Rectangle(absRect.Left, absRect.Top, cornerSize, cornerSize), SetColorAlpha(UIStyleManager.Style.TooltipPortColor));
            GraphicsManager.GetSpriteBatch().Draw(cornerTexture, new Rectangle(absRect.Right, absRect.Top, cornerSize, cornerSize), null, SetColorAlpha(UIStyleManager.Style.TooltipPortColor), Mathf.ToRads(90f), new Vector2(), SpriteEffects.None, 0f);
            GraphicsManager.GetSpriteBatch().Draw(cornerTexture, new Rectangle(absRect.Left, absRect.Bottom, cornerSize, cornerSize), null, SetColorAlpha(UIStyleManager.Style.TooltipPortColor), Mathf.ToRads(-90f), new Vector2(), SpriteEffects.None, 0f);
            GraphicsManager.GetSpriteBatch().Draw(cornerTexture, new Rectangle(absRect.Right, absRect.Bottom, cornerSize, cornerSize), null, SetColorAlpha(UIStyleManager.Style.TooltipPortColor), Mathf.ToRads(180f), new Vector2(), SpriteEffects.None, 0f);

            // Draw body
            Texture2D pixelTexture = ContentStorage.GetTexture("white-pixel");
            GraphicsManager.GetSpriteBatch().Draw(pixelTexture, new Rectangle(absRect.Left, absRect.Top + cornerSize, absRect.Width, absRect.Height - 2 * cornerSize), SetColorAlpha(UIStyleManager.Style.TooltipPortColor));
            GraphicsManager.GetSpriteBatch().Draw(pixelTexture, new Rectangle(absRect.Left + cornerSize, absRect.Top, absRect.Width - 2 * cornerSize, cornerSize), SetColorAlpha(UIStyleManager.Style.TooltipPortColor));
            GraphicsManager.GetSpriteBatch().Draw(pixelTexture, new Rectangle(absRect.Left + cornerSize, absRect.Bottom - cornerSize, absRect.Width - 2 * cornerSize, cornerSize), SetColorAlpha(UIStyleManager.Style.TooltipPortColor));

            // Draw arrow
            Texture2D arrowTexture = ContentStorage.GetTexture("tooltip-arrow");
            int x;
            int y;
            switch (PortAnchor)
            {
                case ePortAnchor.BOTTOM:
                    x = absRect.Center.X - ArrowSize.X / 2;
                    y = absRect.Bottom;
                    GraphicsManager.GetSpriteBatch().Draw(arrowTexture, new Rectangle(x, y, ArrowSize.X, ArrowSize.Y), SetColorAlpha(UIStyleManager.Style.TooltipPortColor));
                    break;
                case ePortAnchor.TOP:
                    x = absRect.Center.X + ArrowSize.X / 2;
                    y = absRect.Top;
                    GraphicsManager.GetSpriteBatch().Draw(arrowTexture, new Rectangle(x, y, ArrowSize.X, ArrowSize.Y), null, SetColorAlpha(UIStyleManager.Style.TooltipPortColor), Mathf.ToRads(180f), new Vector2(), SpriteEffects.None, 0f);
                    break;
                case ePortAnchor.LEFT:
                    x = absRect.Left;
                    y = absRect.Center.Y - ArrowSize.X / 2;
                    GraphicsManager.GetSpriteBatch().Draw(arrowTexture, new Rectangle(x, y, ArrowSize.X, ArrowSize.Y), null, SetColorAlpha(UIStyleManager.Style.TooltipPortColor), Mathf.ToRads(90f), new Vector2(), SpriteEffects.None, 0f);
                    break;
                case ePortAnchor.RIGHT:
                    x = absRect.Right;
                    y = absRect.Center.Y + ArrowSize.X / 2;
                    GraphicsManager.GetSpriteBatch().Draw(arrowTexture, new Rectangle(x, y, ArrowSize.X, ArrowSize.Y), null, SetColorAlpha(UIStyleManager.Style.TooltipPortColor), Mathf.ToRads(-90f), new Vector2(), SpriteEffects.None, 0f);
                    break;
            }

            base.SBDraw();
        }

        public override void Destroy()
        {
            base.Destroy();
            TooltipManager.RemoveTooltip(this);
        }

        // -------- Private Methods ---------
        private TextUI GetChildTextUI()
        {
            foreach (UIEntity ui in children)
                if (ui is TextUI)
                    return (TextUI)ui;

            return null;
        }
    }
}
