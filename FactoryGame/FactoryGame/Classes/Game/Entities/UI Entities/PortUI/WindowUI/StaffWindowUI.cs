﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    class StaffWindowUI : WindowUI
    {
        // -------- Private Fields --------
        private Staff staff;
        private int scrollWidth;

        // -------- Constructors --------
        public StaffWindowUI(Point inPosition, Staff inStaff)
            : base(inPosition, WindowUIManager.WindowWidth, WindowUIManager.WindowHeight, inStaff.GetNameTitle)
        {
            staff = inStaff;
        }

        // -------- Public Methods --------
        public override void Initialize()
        {
            int scrollBorder = 10;
            int rightScrollBorder = 5;
            ScrollWindowUI scroll = new ScrollWindowUI(new Point(scrollBorder, headerHeight + scrollBorder), width - scrollBorder - rightScrollBorder,
                height - headerHeight - 2 * scrollBorder, eUIAnchor.TOP_LEFT, 10);
            scroll.Initialize();
            scrollWidth = width - scrollBorder - rightScrollBorder - 20;

            if (StaffManager.StaffCanLoadElements(staff.Type))
                scroll.SetDivChild(CreateDiv_CarryElement());

            scroll.SetDivChild(CreateDiv_Needs());
            scroll.SetDivChild(CreateDiv_Skills());
            scroll.SetDivChild(CreateDiv_Traits());
            scroll.SetDivChild(CreateDiv_Tasks());
            scroll.SetDivChild(CreateDiv_Hours());
            scroll.SetDivChild(CreateDiv_Options());

            scroll.OnFinishCreatingDivs();
            
            base.Initialize();

            SetChild(scroll);
        }

        public override void Update()
        {
            // TODO

            base.Update();
        }

        public override Entity GetWindowUIEntity()
        {
            return staff;
        }

        // -------- Private Methods ---------
        // Create div methods
        private UIDiv CreateDiv_CarryElement()
        {
            UIDiv carryDiv = new UIDiv(new Point(), scrollWidth, GetDivHeight_CarryElement(), eUIAnchor.TOP_LEFT);
            carryDiv.Initialize();

            DivTextHeaderUI carryHeader = new DivTextHeaderUI(new Point(), Localization.LocalizeString("text_window_carry_element"), scrollWidth, eUIAnchor.TOP_LEFT);
            carryHeader.Initialize();
            carryDiv.SetChild(carryHeader);

            ElementIconUI carryElement = new ElementIconUI(new Point(20, 26), eUIAnchor.TOP_LEFT, staff.CarryElement);
            carryElement.Initialize();
            carryDiv.SetChild(carryElement);

            TextUI carryText = new TextUI(staff.GetCarryElementString, new Point(85, 40), eUIAnchor.TOP_LEFT, UIStyleManager.Style.UIFont, 1, 200,
                UIStyleManager.Style.PortBodyTextColor, eTextAlignment.LEFT);
            carryText.Initialize();
            carryDiv.SetChild(carryText);

            return carryDiv;
        }
        private UIDiv CreateDiv_Needs()
        {
            UIDiv needsDiv = new UIDiv(new Point(), scrollWidth, GetDivHeight_Needs(), eUIAnchor.TOP_LEFT);
            needsDiv.Initialize();

            DivTextHeaderUI needsHeader = new DivTextHeaderUI(new Point(), Localization.LocalizeString("text_window_needs"), scrollWidth, eUIAnchor.TOP_LEFT);
            needsHeader.Initialize();
            needsDiv.SetChild(needsHeader);

            // Hunger
            TextUI hungerText = new TextUI(Localization.LocalizeString("text_staff_hunger"), new Point(9, 17), eUIAnchor.TOP_LEFT, UIStyleManager.Style.UIFont, 1, 100,
                UIStyleManager.Style.PortBodyTextColor, eTextAlignment.LEFT);
            hungerText.Initialize();
            hungerText.SetTooltip(Localization.LocalizeString("text_tooltip_staff_hunger"), ePortAnchor.BOTTOM);
            needsDiv.SetChild(hungerText);
            ProgressBarUI hungerBar = new ProgressBarUI(new Point(5, 36), 160, 15, staff.Needs.GetHunger, UIStyleManager.Style.BarBGColor, eProgressBarColorSet.NEED_COLORS, eUIAnchor.TOP_LEFT);
            hungerBar.Initialize();
            hungerBar.SetTooltip(Localization.LocalizeString("text_tooltip_staff_hunger"), ePortAnchor.BOTTOM);
            needsDiv.SetChild(hungerBar);

            // Energy
            TextUI energyText = new TextUI(Localization.LocalizeString("text_staff_energy"), new Point(204, 17), eUIAnchor.TOP_LEFT, UIStyleManager.Style.UIFont, 1, 100,
                UIStyleManager.Style.PortBodyTextColor, eTextAlignment.LEFT);
            energyText.Initialize();
            energyText.SetTooltip(Localization.LocalizeString("text_tooltip_staff_energy"), ePortAnchor.BOTTOM);
            needsDiv.SetChild(energyText);
            ProgressBarUI energyBar = new ProgressBarUI(new Point(200, 36), 160, 15, staff.Needs.GetEnergy, UIStyleManager.Style.BarBGColor, eProgressBarColorSet.NEED_COLORS, eUIAnchor.TOP_LEFT);
            energyBar.Initialize();
            energyBar.SetTooltip(Localization.LocalizeString("text_tooltip_staff_energy"), ePortAnchor.BOTTOM);
            needsDiv.SetChild(energyBar);

            // Bladder
            TextUI bladderText = new TextUI(Localization.LocalizeString("text_staff_bladder"), new Point(9, 52), eUIAnchor.TOP_LEFT, UIStyleManager.Style.UIFont, 1, 100,
                UIStyleManager.Style.PortBodyTextColor, eTextAlignment.LEFT);
            bladderText.Initialize();
            bladderText.SetTooltip(Localization.LocalizeString("text_tooltip_staff_bladder"), ePortAnchor.BOTTOM);
            needsDiv.SetChild(bladderText);
            ProgressBarUI bladderBar = new ProgressBarUI(new Point(5, 71), 160, 15, staff.Needs.GetBladder, UIStyleManager.Style.BarBGColor, eProgressBarColorSet.NEED_COLORS, eUIAnchor.TOP_LEFT);
            bladderBar.Initialize();
            bladderBar.SetTooltip(Localization.LocalizeString("text_tooltip_staff_bladder"), ePortAnchor.BOTTOM);
            needsDiv.SetChild(bladderBar);

            // Environment
            TextUI environmentText = new TextUI(Localization.LocalizeString("text_staff_environment"), new Point(204, 52), eUIAnchor.TOP_LEFT, UIStyleManager.Style.UIFont, 1, 100,
                UIStyleManager.Style.PortBodyTextColor, eTextAlignment.LEFT);
            environmentText.Initialize();
            environmentText.SetTooltip(Localization.LocalizeString("text_tooltip_staff_environment"), ePortAnchor.BOTTOM);
            needsDiv.SetChild(environmentText);
            ProgressBarUI environmentBar = new ProgressBarUI(new Point(200, 71), 160, 15, staff.Needs.GetEnvironment, UIStyleManager.Style.BarBGColor, eProgressBarColorSet.NEED_COLORS, eUIAnchor.TOP_LEFT);
            environmentBar.Initialize();
            environmentBar.SetTooltip(Localization.LocalizeString("text_tooltip_staff_environment"), ePortAnchor.BOTTOM);
            needsDiv.SetChild(environmentBar);

            // Sum
            string sumString = Localization.LocalizeString("text_staff_hunger") + " + " + Localization.LocalizeString("text_staff_energy") + " + " +
                Localization.LocalizeString("text_staff_bladder") + " + " + Localization.LocalizeString("text_staff_environment");
            TextUI sumText = new TextUI(sumString, new Point(0, 98), eUIAnchor.TOP_LEFT, UIStyleManager.Style.UIFont, 1, scrollWidth, UIStyleManager.Style.PortBodyTextColor, eTextAlignment.CENTER);
            sumText.Initialize();
            sumText.SetTooltip(Localization.LocalizeString("text_tooltip_staff_happiness_sum"), ePortAnchor.BOTTOM);
            needsDiv.SetChild(sumText);

            // Happiness
            TextUI happinessText = new TextUI("= " + Localization.LocalizeString("text_staff_happiness"), new Point(0, 118), eUIAnchor.TOP_LEFT, UIStyleManager.Style.UIFont, 1, 209,
                UIStyleManager.Style.PortBodyTextColor, eTextAlignment.RIGHT);
            happinessText.Initialize();
            happinessText.SetTooltip(Localization.LocalizeString("text_tooltip_staff_happiness_sum"), ePortAnchor.BOTTOM);
            needsDiv.SetChild(happinessText);
            HappinessUI happiness = new HappinessUI(staff, new Point(219, 119), eUIAnchor.TOP_LEFT);
            happiness.Initialize();
            happiness.SetTooltip(Localization.LocalizeString("text_tooltip_staff_happiness"), ePortAnchor.BOTTOM);
            needsDiv.SetChild(happiness);

            return needsDiv;
        }
        private UIDiv CreateDiv_Skills()
        {
            UIDiv skillsDiv = new UIDiv(new Point(), scrollWidth, GetDivHeight_Skills(), eUIAnchor.TOP_LEFT);
            skillsDiv.Initialize();

            DivTextHeaderUI skillsHeader = new DivTextHeaderUI(new Point(), Localization.LocalizeString("text_window_skills"), scrollWidth, eUIAnchor.TOP_LEFT);
            skillsHeader.Initialize();
            skillsDiv.SetChild(skillsHeader);

            List<Skill> displaySkills = staff.Skills.GetDisplayedSkills();
            for (int row = 0; row < GetSkillsRowCount(); row++)
            {
                for (int col = 0; col < WindowUIManager.IconsPerRow; col++)
                {
                    int index = row * WindowUIManager.IconsPerRow + col;
                    Skill skill = index < displaySkills.Count ? displaySkills[index] : null;
                    SkillIconUI skillIcon = new SkillIconUI(new Point(col * GetIconHorizontalOffset(), 26 + row * 60), skill, eUIAnchor.TOP_LEFT);
                    skillIcon.Initialize();
                    skillsDiv.SetChild(skillIcon);
                }
            }

            return skillsDiv;
        }
        private UIDiv CreateDiv_Traits()
        {
            UIDiv traitsDiv = new UIDiv(new Point(), scrollWidth, GetDivHeight_Traits(), eUIAnchor.TOP_LEFT);
            traitsDiv.Initialize();

            DivTextHeaderUI traitsHeader = new DivTextHeaderUI(new Point(), Localization.LocalizeString("text_window_traits"), scrollWidth, eUIAnchor.TOP_LEFT);
            traitsHeader.Initialize();
            traitsDiv.SetChild(traitsHeader);

            List<Trait> traits = staff.Traits.Traits;
            for (int row = 0; row < GetTraitsRowCount(); row++)
            {
                for (int col = 0; col < WindowUIManager.IconsPerRow; col++)
                {
                    int index = row * WindowUIManager.IconsPerRow + col;
                    Trait trait = index < traits.Count ? traits[index] : null;
                    TraitIconUI traitIcon = new TraitIconUI(new Point(col * GetIconHorizontalOffset(), 26 + row * 60), trait, eUIAnchor.TOP_LEFT);
                    traitIcon.Initialize();
                    traitsDiv.SetChild(traitIcon);
                }
            }

            string qualityString = Localization.LocalizeString("text_element_quality") + " = " + Localization.LocalizeString("text_staff_happiness") + " + " +
                Localization.LocalizeString("text_window_skills") + " + " + Localization.LocalizeString("text_window_traits");
            TextUI qualityText = new TextUI(qualityString, new Point(0, 5), eUIAnchor.BOTTOM_LEFT, UIStyleManager.Style.UIFont, 1, scrollWidth, UIStyleManager.Style.PortBodyTextColor,
                eTextAlignment.CENTER);
            qualityText.Initialize();
            qualityText.SetTooltip(Localization.LocalizeString("text_tooltip_staff_quality_sum"), ePortAnchor.BOTTOM);

            traitsDiv.SetChild(qualityText);

            return traitsDiv;
        }
        private UIDiv CreateDiv_Tasks()
        {
            UIDiv tasksDiv = new UIDiv(new Point(), scrollWidth, GetDivHeight_Tasks(), eUIAnchor.TOP_LEFT);
            tasksDiv.Initialize();

            DivTextHeaderUI tasksHeader = new DivTextHeaderUI(new Point(), Localization.LocalizeString("text_window_tasks"), scrollWidth, eUIAnchor.TOP_LEFT);
            tasksHeader.Initialize();
            tasksDiv.SetChild(tasksHeader);

            // Staff tasks

            List<StaffTaskUI> taskUI = new List<StaffTaskUI>();
            Point taskUIPosition = new Point(10, 26);

            List<PassiveTask> passiveTasks = staff.Tasks.GetPassiveTasks();
            foreach (PassiveTask pt in passiveTasks)
            {
                PassiveTaskUI ptui = new PassiveTaskUI(taskUIPosition, eUIAnchor.TOP_LEFT, pt);
                ptui.Initialize();
                tasksDiv.SetChild(ptui);
                taskUI.Add(ptui);
                taskUIPosition.Y += 41;
            }

            List<ActiveTask> activeTasks = staff.Tasks.GetActiveTasks();
            foreach (ActiveTask at in activeTasks)
            {
                ActiveTaskUI atui = new ActiveTaskUI(taskUIPosition, eUIAnchor.TOP_LEFT, at);
                atui.Initialize();
                tasksDiv.SetChild(atui);
                taskUI.Add(atui);
                taskUIPosition.Y += 41;
            }

            List<eTaskType> creatableTasks = StaffManager.GetStaffActiveTaskTypes(staff.Type);
            foreach (eTaskType tt in creatableTasks)
            {
                NewTaskUI ntui = new NewTaskUI(taskUIPosition, eUIAnchor.TOP_LEFT, tt);
                ntui.Initialize();
                tasksDiv.SetChild(ntui);
                taskUI.Add(ntui);
                taskUIPosition.Y += 41;
            }

            for (int i = 0; i < taskUI.Count - 1; i++)
                taskUI[i].IsLastListItem = false;

            // Current staff task

            TextUI currentlyText = new TextUI(Localization.LocalizeString("text_task_currently"), new Point(0, 14), eUIAnchor.BOTTOM_LEFT, UIStyleManager.Style.UIFont, 1, 75,
                UIStyleManager.Style.PortBodyTextColor, eTextAlignment.RIGHT);
            currentlyText.Initialize();
            tasksDiv.SetChild(currentlyText);

            StaffTaskIconUI staffTaskIcon = new StaffTaskIconUI(new Point(85, 9), staff, eUIAnchor.BOTTOM_LEFT);
            staffTaskIcon.Initialize();
            staffTaskIcon.SetTooltip(Localization.LocalizeString("text_tooltip_current_task"), ePortAnchor.BOTTOM);
            tasksDiv.SetChild(staffTaskIcon);

            TextUI currentTaskDescription = new TextUI(staff.GetCurrentTaskDescriptionString, new Point(127, 14), eUIAnchor.BOTTOM_LEFT,
                UIStyleManager.Style.UISmallFont, 1, 228, UIStyleManager.Style.PortBodyTextColor, eTextAlignment.LEFT);
            currentTaskDescription.Initialize();
            currentTaskDescription.SetTooltip(Localization.LocalizeString("text_tooltip_current_task"), ePortAnchor.BOTTOM);
            tasksDiv.SetChild(currentTaskDescription);

            return tasksDiv;
        }
        private UIDiv CreateDiv_Hours()
        {
            UIDiv hoursDiv = new UIDiv(new Point(), scrollWidth, GetDivHeight_Hours(), eUIAnchor.TOP_LEFT);
            hoursDiv.Initialize();

            DivTextHeaderUI hoursHeader = new DivTextHeaderUI(new Point(), Localization.LocalizeString("text_window_hours"), scrollWidth, eUIAnchor.TOP_LEFT);
            hoursHeader.Initialize();
            hoursDiv.SetChild(hoursHeader);

            HoursSliderUI hoursSlider = new HoursSliderUI(new Point(27, 32), staff, eUIAnchor.TOP_LEFT);
            hoursSlider.Initialize();
            hoursDiv.SetChild(hoursSlider);

            TextUI dailyPayText = new TextUI(staff.GetNewDailyPayString, new Point(82, 92), eUIAnchor.TOP_LEFT, UIStyleManager.Style.UIFont, 1, 200, UIStyleManager.Style.PortBodyTextColor, eTextAlignment.CENTER);
            dailyPayText.Initialize();
            dailyPayText.SetTooltip(Localization.LocalizeString("text_tooltip_staff_daily_pay"), ePortAnchor.BOTTOM);
            hoursDiv.SetChild(dailyPayText);

            TextUI hoursApplyText = new TextUI(staff.Hours.GetHoursApplyText, new Point(82, 112), eUIAnchor.TOP_LEFT, UIStyleManager.Style.UISmallFont, 1, 200, UIStyleManager.Style.PortBodyTextColor, eTextAlignment.CENTER);
            hoursApplyText.Initialize();
            hoursDiv.SetChild(hoursApplyText);

            return hoursDiv;
        }
        private UIDiv CreateDiv_Options()
        {
            UIDiv optionsDiv = new UIDiv(new Point(), scrollWidth, GetDivHeight_Options(), eUIAnchor.TOP_LEFT);
            optionsDiv.Initialize();

            DivTextHeaderUI optionsHeader = new DivTextHeaderUI(new Point(), Localization.LocalizeString("text_window_options"), scrollWidth, eUIAnchor.TOP_LEFT);
            optionsHeader.Initialize();
            optionsDiv.SetChild(optionsHeader);

            ButtonUI sackButton = new ButtonUI(new Point(0, 26), eIconSize.ICON_50, eUIAnchor.TOP_LEFT, eButtonGraphicsType.SOLID, "icon-element-test-1", null, UIStyleManager.Style.ButtonColor);
            sackButton.Initialize();
            sackButton.SetTooltip(Localization.LocalizeString("text_tooltip_staff_sack"), ePortAnchor.BOTTOM);
            optionsDiv.SetChild(sackButton);

            // TODO - make rest of this

            return optionsDiv;
        }

        // Div height methods
        private static int GetDivHeight_CarryElement()
        {
            return 80;
        }
        private static int GetDivHeight_Needs()
        {
            return 137;
        }
        private int GetDivHeight_Skills()
        {
            int rows = GetSkillsRowCount();

            int height = 81;

            if (rows > 1)
                height += 60 * (rows - 1);

            return height;
        }
        private int GetDivHeight_Traits()
        {
            int rows = GetTraitsRowCount();

            int height = 105;

            if (rows > 1)
                height += 60 * (rows - 1);

            return height;
        }
        private int GetDivHeight_Tasks()
        {
            int height = 72;

            int taskListEntries = staff.Tasks.GetPassiveTaskCount();
            taskListEntries += staff.Tasks.GetActiveTaskCount();
            taskListEntries += StaffManager.GetStaffActiveTaskTypes(staff.Type).Count;

            height += taskListEntries * 40;

            if (taskListEntries > 1)
                height += 1;

            return height;
        }
        private int GetDivHeight_Hours()
        {
            return 122;
        }
        private int GetDivHeight_Options()
        {
            return 81;
        }

        private int GetSkillsRowCount()
        {
            int skillNum = staff.Skills.GetDisplayedSkillCount();
            int rows = skillNum / WindowUIManager.IconsPerRow + 1;

            return rows;
        }
        private int GetTraitsRowCount()
        {
            int traitNum = staff.Traits.Traits.Count;
            int rows = traitNum / WindowUIManager.IconsPerRow + 1;

            return rows;
        }
        private int GetIconHorizontalOffset()
        {
            int offset = (scrollWidth - WindowUIManager.IconsPerRow * 50) / (WindowUIManager.IconsPerRow - 1) + 50;

            return offset;
        }
    }
}
