﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FactoryGame
{
    class WindowUI : PortUI
    {
        // -------- Private Fields --------
        protected int width;
        protected int height;
        protected int headerHeight;
        protected TextUI.UpdateText headerTextHandler;

        // -------- Constructors --------
        public WindowUI(Point inPosition, int inWidth, int inHeight, TextUI.UpdateText inHeaderTextMethod)
            : base(inPosition, eCornerRoundSize.RADIUS_20)
        {
            width = inWidth;
            height = inHeight;
            headerTextHandler = inHeaderTextMethod;

            Rectangle = new Rectangle(setUpPosition.X, setUpPosition.Y, width, height);

            headerHeight = WindowUIManager.WindowHeaderHeight;
        }

        // -------- Public Methods --------
        public override void Initialize()
        {
            int textBorder = 10;
            TextUI text = new TextUI(headerTextHandler, new Point(textBorder, 5), eUIAnchor.TOP_LEFT, UIStyleManager.Style.UIHeaderFont, 1, WindowUIManager.WindowWidth - 2 * textBorder,
                UIStyleManager.Style.PortHeaderTextColor, eTextAlignment.LEFT);
            text.Initialize();

            ButtonUI closeButton = new ButtonUI(new Point(10, 5), eIconSize.ICON_25, eUIAnchor.TOP_RIGHT, eButtonGraphicsType.FLOATING, "button-close-window", WindowUIManager.RemoveWindow, Color.White);
            closeButton.Initialize();
            
            base.Initialize();

            SetChild(text);
            SetChild(closeButton);
        }

        public override void Denitialize()
        {
            base.Denitialize();
        }

        public override void SBDraw()
        {
            Rectangle absRect = GetAbsoluteRectangle();
            Texture2D cornerTex = ContentStorage.GetTexture(UIUtilities.GetCornerRoundTextureId(CornerRoundSize));
            Texture2D pixelTex = ContentStorage.GetTexture("white-pixel");
            int cornerSize = UIUtilities.GetCornerRoundSize(CornerRoundSize);

            // Header
            GraphicsManager.GetSpriteBatch().Draw(cornerTex, new Rectangle(absRect.X, absRect.Y, cornerSize, cornerSize), SetColorAlpha(UIStyleManager.Style.PortHeaderColor));
            GraphicsManager.GetSpriteBatch().Draw(cornerTex, new Rectangle(absRect.Right, absRect.Y, cornerSize, cornerSize), null, SetColorAlpha(UIStyleManager.Style.PortHeaderColor),
                Mathf.ToRads(90f), new Vector2(), SpriteEffects.None, 0f);
            GraphicsManager.GetSpriteBatch().Draw(pixelTex, new Rectangle(absRect.X + cornerSize, absRect.Y, absRect.Width - 2 * cornerSize, cornerSize), SetColorAlpha(UIStyleManager.Style.PortHeaderColor));
            GraphicsManager.GetSpriteBatch().Draw(pixelTex, new Rectangle(absRect.X, absRect.Y + cornerSize, absRect.Width, headerHeight - cornerSize), SetColorAlpha(UIStyleManager.Style.PortHeaderColor));

            // Body
            GraphicsManager.GetSpriteBatch().Draw(cornerTex, new Rectangle(absRect.X, absRect.Bottom, cornerSize, cornerSize), null, SetColorAlpha(UIStyleManager.Style.PortBodyColor),
                Mathf.ToRads(-90f), new Vector2(), SpriteEffects.None, 0f);
            GraphicsManager.GetSpriteBatch().Draw(cornerTex, new Rectangle(absRect.Right, absRect.Bottom, cornerSize, cornerSize), null, SetColorAlpha(UIStyleManager.Style.PortBodyColor),
                Mathf.ToRads(180f), new Vector2(), SpriteEffects.None, 0f);
            GraphicsManager.GetSpriteBatch().Draw(pixelTex, new Rectangle(absRect.X, absRect.Y + headerHeight, absRect.Width, absRect.Height - cornerSize - headerHeight),
                SetColorAlpha(UIStyleManager.Style.PortBodyColor));
            GraphicsManager.GetSpriteBatch().Draw(pixelTex, new Rectangle(absRect.X + cornerSize, absRect.Bottom - cornerSize, absRect.Width - 2 * cornerSize, cornerSize),
                SetColorAlpha(UIStyleManager.Style.PortBodyColor));

            base.SBDraw();
        }

        public virtual Entity GetWindowUIEntity()
        {
            return null;
        }
    }
}
