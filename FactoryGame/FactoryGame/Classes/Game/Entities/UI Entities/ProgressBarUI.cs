﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FactoryGame
{
    // -------- Enumerations --------
    public enum eProgressBarColorSet
    {
        NEED_COLORS,
        NEED_COLORS_REVERSED
    }

    class ProgressBarUI : UIEntity
    {
        // -------- Delegates --------
        public delegate float GetBarCompletion();

        // -------- Private Fields --------
        private float completionFraction;
        private GetBarCompletion barCompletionHandler;
        private Color bgColor;
        private Color barColor;
        private Texture2D pixelTex;
        private bool useColorSet;
        private eProgressBarColorSet colorSet;

        // -------- Constructors --------
        public ProgressBarUI(Point inPosition, int width, int height, GetBarCompletion inBarCompletionMethod, Color inBgColor, Color inBarColor, eUIAnchor inAnchor)
            : base(inPosition, inAnchor)
        {
            useColorSet = false;

            barCompletionHandler = inBarCompletionMethod;
            UpdateCompletionFraction();
            bgColor = inBgColor;
            barColor = inBarColor;

            Rectangle = new Rectangle(setUpPosition.X, setUpPosition.Y, width, height);
        }

        public ProgressBarUI(Point inPosition, int width, int height, GetBarCompletion inBarCompletionMethod, Color inBgColor, eProgressBarColorSet inColorSet, eUIAnchor inAnchor)
            : base(inPosition, inAnchor)
        {
            useColorSet = true;

            barCompletionHandler = inBarCompletionMethod;
            UpdateCompletionFraction();
            bgColor = inBgColor;
            colorSet = inColorSet;

            Rectangle = new Rectangle(setUpPosition.X, setUpPosition.Y, width, height);
        }

        // -------- Public Methods --------
        public override void Initialize()
        {
            pixelTex = ContentStorage.GetTexture("white-pixel");
            base.Initialize();
        }

        public override void Denitialize()
        {
            pixelTex = null;
            base.Denitialize();
        }

        public override void Update()
        {
            UpdateCompletionFraction();
            base.Update();
        }

        public override void SBDraw()
        {
            Rectangle absRect = GetAbsoluteRectangle();

            // BG
            GraphicsManager.GetSpriteBatch().Draw(pixelTex, absRect, SetColorAlpha(bgColor));
            // Bar
            int barLength = (int)(completionFraction * (float)absRect.Width);
            GraphicsManager.GetSpriteBatch().Draw(pixelTex, new Rectangle(absRect.X, absRect.Y, barLength, absRect.Height), SetColorAlpha(GetBarColor()));

            base.SBDraw();
        }

        // -------- Private Methods ---------
        private void UpdateCompletionFraction()
        {
            completionFraction = Mathf.Clamp01(barCompletionHandler());
        }

        private Color GetBarColor()
        {
            if (useColorSet)
            {
                switch (colorSet)
                {
                    case eProgressBarColorSet.NEED_COLORS:
                    {
                        eNeedLevel needLevel = StaffNeedManager.ConvertNeedToNeedLevel(completionFraction);
                        return StaffNeedManager.GetNeedLevelUIColor(needLevel);
                    }
                    case eProgressBarColorSet.NEED_COLORS_REVERSED:
                    {
                        eNeedLevel needLevel = StaffNeedManager.ConvertNeedToNeedLevel(1f - completionFraction);
                        return StaffNeedManager.GetNeedLevelUIColor(needLevel);
                    }
                    default:
                        return Color.White;
                }
            }

            return barColor;
        }
    }
}
