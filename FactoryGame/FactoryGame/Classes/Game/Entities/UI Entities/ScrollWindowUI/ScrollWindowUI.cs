﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FactoryGame
{
    class ScrollWindowUI : UIEntity
    {
        // -------- Private Fields --------
        private int scrollWidth;
        private int scrollHeight;
        private int scrollBarGap;
        private int scrollPosition;
        private RenderTarget2D renderTarget;
        private ScrollBarSliderUI scrollBarSlider;

        // -------- Constructors --------
        public ScrollWindowUI(Point inPosition, int inWidth, int inHeight, eUIAnchor inAnchor, int inScrollBarGap)
            : base(inPosition, inAnchor)
        {
            Rectangle = new Rectangle(inPosition.X, inPosition.Y, inWidth, inHeight);
            scrollWidth = inWidth - inScrollBarGap - 10;
            scrollBarGap = inScrollBarGap;
            scrollPosition = 0;
        }

        // -------- Public Methods --------
        public override void Initialize()
        {
            ScrollWindowManager.AddScrollWindow(this);
            base.Initialize();
        }

        public override void Denitialize()
        {
            ScrollWindowManager.RemoveScrollWindow(this);

            base.Denitialize();

            if (HasScrollSlider())
                scrollBarSlider.Denitialize();
        }

        public override void Update()
        {
            if (HasScrollSlider())
                CheckMouseWheelInput();

            CheckDivHeightChange();

            base.Update();
        }

        public override void SBDraw()
        {
            // Render target
            Rectangle absRect = GetAbsoluteRectangle();
            Rectangle drawRect = new Rectangle(absRect.X, absRect.Y, scrollWidth, GetScrollDrawHeight());
            GraphicsManager.GetSpriteBatch().Draw((Texture2D)renderTarget, drawRect, GetRenderTargetSourceRectangle(), SetColorAlpha(Color.White));

            // Scroll bar
            Texture2D pixelTex = ContentStorage.GetTexture("white-pixel");
            Texture2D scrollEndTex = ContentStorage.GetTexture("scroll-bar-end");
            GraphicsManager.GetSpriteBatch().Draw(scrollEndTex, new Rectangle(absRect.Right - 10, absRect.Top, 10, 5),
                SetColorAlpha(UIStyleManager.Style.UIBGColor));
            GraphicsManager.GetSpriteBatch().Draw(scrollEndTex, new Rectangle(absRect.Right - 10, absRect.Bottom - 5,
                10, 5), null, SetColorAlpha(UIStyleManager.Style.UIBGColor), 0f, new Vector2(), SpriteEffects.FlipVertically, 0f);
            GraphicsManager.GetSpriteBatch().Draw(pixelTex, new Rectangle(absRect.Right - 10, absRect.Top + 5, 10,
                absRect.Height - 10), SetColorAlpha(UIStyleManager.Style.UIBGColor));

            if (HasScrollSlider())
                scrollBarSlider.SBDraw();
        }

        public void SBDrawToRenderTarget()
        {
            GraphicsManager.GetGraphicsDevice().SetRenderTarget(renderTarget);
            GraphicsManager.GetGraphicsDevice().Clear(Color.Transparent);
            GraphicsManager.BeginSpriteBatch();
            foreach (UIEntity ui in children)
            {
                ui.IgnoreParentForAbsoluteRectangle = true;
                ui.SBDraw();
                ui.IgnoreParentForAbsoluteRectangle = false;
            }
            GraphicsManager.EndSpriteBatch();
        }

        public void SetUpRenderTarget()
        {
            renderTarget = new RenderTarget2D(GraphicsManager.GetGraphicsDevice(), scrollWidth, GetRenderTargetHeight());
        }

        public override Rectangle GetAbsoluteRectangleForParent(UIEntity childEntity)
        {
            Rectangle absRect = GetAbsoluteRectangle();

            if (childEntity is UIDiv)
                absRect.Y -= scrollPosition;

            return absRect;
        }

        public override List<UIEntity> GetChildren()
        {
            List<UIEntity> children = base.GetChildren();
            var newChildren = new List<UIEntity>();

            foreach (UIEntity c in children)
                newChildren.Add(c);

            if (HasScrollSlider())
                newChildren.Add(scrollBarSlider);

            return newChildren;
        }

        public void SetDivChild(UIDiv div)
        {
            div.SetPosition(new Point(0, CalculateScrollHeight()));
            SetChild(div);
        }

        public void OnFinishCreatingDivs()
        {
            scrollHeight = CalculateScrollHeight();
            if (IsScrollable())
                CreateScrollBarSlider();

            SetUpRenderTarget();
        }

        public void SetScrollPositionRatio(float scrollPositionRatio)
        {
            // Set position directly. This could be extended to some fancy scroll animation.
            int newScrollPosition = (int)(scrollPositionRatio * (float)GetMaxScrollPosition());
            scrollPosition = newScrollPosition;
        }

        // -------- Private Methods ---------
        private void CheckDivHeightChange()
        {
            // TODO - perform update logic on different divs. Get bool whether need to do repositioning and update render target

        }

        private void OnScrollHeightChanged()
        {
            if (IsScrollable())
            {
                if (!HasScrollSlider())
                    CreateScrollBarSlider();
                else
                    UpdateScrollBarSlider();
            }
            else
            {
                if (HasScrollSlider())
                    RemoveScrollBarSlider();
            }
        }

        private void CreateScrollBarSlider()
        {
            scrollBarSlider = new ScrollBarSliderUI(new Point(0, 0), eUIAnchor.TOP_RIGHT, GetScrollBarHeight(), this);
            scrollBarSlider.Initialize();
            scrollBarSlider.Parent = this;
        }
        private void UpdateScrollBarSlider()
        {
            // TODO
            scrollHeight = CalculateScrollHeight();
        }
        private void RemoveScrollBarSlider()
        {
            scrollBarSlider.Denitialize();
            scrollBarSlider = null;
        }

        private int CalculateScrollHeight()
        {
            Rectangle union = new Rectangle(0, 0, 0, 0);
            foreach (UIEntity ui in children)
                union = Rectangle.Union(union, ui.GetRelativeRectangle());

            return union.Height;
        }
        private int GetScrollDrawHeight()
        {
            return Mathf.Min(scrollHeight, Rectangle.Height);
        }
        private int GetRenderTargetHeight()
        {
            return scrollHeight;
        }

        private int GetMaxScrollPosition()
        {
            return scrollHeight - Rectangle.Height;
        }

        private Rectangle GetRenderTargetSourceRectangle()
        {
            if (!IsScrollable())
                return new Rectangle(0, 0, scrollWidth, scrollHeight);

            return new Rectangle(0, scrollPosition, scrollWidth, Rectangle.Height);
        }

        private bool IsScrollable()
        {
            return (scrollHeight > Rectangle.Height);
        }
        private bool HasScrollSlider()
        {
            return (scrollBarSlider != null);
        }
        private int GetScrollBarHeight()
        {
            float windowSizeFraction = (float)Rectangle.Height / (float)scrollHeight;
            int scrollBarHeight = (int)(windowSizeFraction * (float)Rectangle.Height);

            return scrollBarHeight;
        }

        private void CheckMouseWheelInput()
        {
            if (scrollBarSlider.Dragging)
                return;

            int mouseWheelMovement = InputManager.GetElapsedMouseScroll();
            int sliderMovement = -((int)((float)mouseWheelMovement * ScrollWindowManager.ScrollMovementPerMouseWheelUnit));
            scrollBarSlider.MoveScrollBarSlider(sliderMovement);
        }
    }
}
