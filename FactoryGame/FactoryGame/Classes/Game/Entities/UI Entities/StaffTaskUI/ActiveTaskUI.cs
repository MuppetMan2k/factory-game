﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FactoryGame
{
    class ActiveTaskUI : StaffTaskUI
    {
        // -------- Private Fields --------
        private ActiveTask task;

        // -------- Constructors --------
        public ActiveTaskUI(Point inPosition, eUIAnchor inAnchor, ActiveTask inTask)
            : base(inPosition, inAnchor)
        {
            task = inTask;
        }

        // -------- Public Methods --------
        public override void Initialize()
        {
            base.Initialize();

            // Icon
            StaffTaskIconUI taskIcon = new StaffTaskIconUI(new Point(5, 5), task.Type, eUIAnchor.TOP_LEFT);
            taskIcon.Initialize();
            SetChild(taskIcon);
            
            // Task name text
            string taskName = Localization.LocalizeString(TaskManager.GetLocIdForTaskType(task.Type));
            TextUI taskNameText = new TextUI(taskName, new Point(43, 7), eUIAnchor.TOP_LEFT, UIStyleManager.Style.UIFont, 1, 260, UIStyleManager.Style.ListTextColor, eTextAlignment.LEFT);
            taskNameText.Initialize();
            SetChild(taskNameText);

            // Task description text
            string taskDescription = TaskManager.GetTaskDescription(task);
            TextUI taskDescriptionText = new TextUI(taskDescription, new Point(43, 23), eUIAnchor.TOP_LEFT, UIStyleManager.Style.UISmallFont, 1, 260, UIStyleManager.Style.ListTextColor,
                eTextAlignment.LEFT);
            taskDescriptionText.Initialize();
            SetChild(taskDescriptionText);

            // Remove button
            ButtonUI removeButton = new ButtonUI(new Point(10, 10), eIconSize.ICON_20, eUIAnchor.TOP_RIGHT, eButtonGraphicsType.FLOATING, "button-close-small", task.OnPressActiveTaskUIRemoveButton,
                Color.White);
            removeButton.Initialize();
            removeButton.SetTooltip(Localization.LocalizeString("text_tooltip_remove_task"), ePortAnchor.BOTTOM);
            SetChild(removeButton);

            // View button
            ButtonUI viewButton = new ButtonUI(new Point(35, 10), eIconSize.ICON_20, eUIAnchor.TOP_RIGHT, eButtonGraphicsType.FLOATING, "button-view-small", task.OnPressActiveTaskUIViewButton,
                Color.White);
            viewButton.Initialize();
            viewButton.SetTooltip(Localization.LocalizeString("text_tooltip_view_task"), ePortAnchor.BOTTOM);
            SetChild(viewButton);
        }

        public override void SBDraw()
        {
            Rectangle absRect = GetAbsoluteRectangle();
            Texture2D pixelTex = ContentStorage.GetTexture("white-pixel");
            GraphicsManager.GetSpriteBatch().Draw(pixelTex, absRect,UIStyleManager.Style.ListItemColor);

            if (!IsLastListItem)
                GraphicsManager.GetSpriteBatch().Draw(pixelTex, new Rectangle(absRect.Left, absRect.Bottom, absRect.Width, 1), UIStyleManager.Style.UIBGColor);

            base.SBDraw();
        }
    }
}
