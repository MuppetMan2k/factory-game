﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FactoryGame
{
    class NewTaskUI : StaffTaskUI, IInteractableUI
    {
        // -------- Private Fields --------
        private eTaskType taskType;

        // -------- Constructors --------
        public NewTaskUI(Point inPosition, eUIAnchor inAnchor, eTaskType inTaskType)
            : base(inPosition, inAnchor)
        {
            taskType = inTaskType;
        }

        // -------- Public Methods --------
        public override void Initialize()
        {
            base.Initialize();

            // Plus sign
            ButtonUI plusButton = new ButtonUI(new Point(78, 10), eIconSize.ICON_20, eUIAnchor.TOP_LEFT, eButtonGraphicsType.FLOATING, "icon-plus-sign", null, Color.White); // TODO
            plusButton.Initialize();
            plusButton.SetTooltip(Localization.LocalizeString("text_tooltip_new_task"), ePortAnchor.BOTTOM);
            SetChild(plusButton);

            // Task name text
            string taskName = Localization.LocalizeString(TaskManager.GetLocIdForTaskType(taskType));
            string addTask = string.Format(Localization.LocalizeString("text_task_add_new"), taskName);
            TextUI addTaskText = new TextUI(addTask, new Point(105, 15), eUIAnchor.TOP_LEFT, UIStyleManager.Style.UIFont, 1, 240, UIStyleManager.Style.ListTextColor, eTextAlignment.LEFT);
            addTaskText.Initialize();
            SetChild(addTaskText);
        }

        public override void SBDraw()
        {
            Rectangle absRect = GetAbsoluteRectangle();
            Texture2D pixelTex = ContentStorage.GetTexture("white-pixel");
            GraphicsManager.GetSpriteBatch().Draw(pixelTex, absRect,UIStyleManager.Style.AlternateListItemColor);

            if (!IsLastListItem)
                GraphicsManager.GetSpriteBatch().Draw(pixelTex, new Rectangle(absRect.Left, absRect.Bottom, absRect.Width, 1), UIStyleManager.Style.UIBGColor);

            base.SBDraw();
        }
    }
}
