﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FactoryGame
{
    class PassiveTaskUI : StaffTaskUI
    {
        // -------- Private Fields --------
        private PassiveTask task;

        // -------- Constructors --------
        public PassiveTaskUI(Point inPosition, eUIAnchor inAnchor, PassiveTask inTask)
            : base(inPosition, inAnchor)
        {
            task = inTask;
        }

        // -------- Public Methods --------
        public override void Initialize()
        {
            base.Initialize();

            // Icon
            StaffTaskIconUI taskIcon = new StaffTaskIconUI(new Point(5, 5), task.Type, eUIAnchor.TOP_LEFT);
            taskIcon.Initialize();
            SetChild(taskIcon);
            
            // Task name text
            string taskName = Localization.LocalizeString(TaskManager.GetLocIdForTaskType(task.Type));
            TextUI taskNameText = new TextUI(taskName, new Point(43, 7), eUIAnchor.TOP_LEFT, UIStyleManager.Style.UIFont, 1, 260, UIStyleManager.Style.ListTextColor, eTextAlignment.LEFT);
            taskNameText.Initialize();
            SetChild(taskNameText);

            // Task description text
            string taskDescription = TaskManager.GetTaskDescription(task);
            TextUI taskDescriptionText = new TextUI(taskDescription, new Point(43, 23), eUIAnchor.TOP_LEFT, UIStyleManager.Style.UISmallFont, 1, 260, UIStyleManager.Style.ListTextColor,
                eTextAlignment.LEFT);
            taskDescriptionText.Initialize();
            SetChild(taskDescriptionText);
        }

        public override void SBDraw()
        {
            Rectangle absRect = GetAbsoluteRectangle();
            Texture2D pixelTex = ContentStorage.GetTexture("white-pixel");
            GraphicsManager.GetSpriteBatch().Draw(pixelTex, absRect, UIStyleManager.Style.AlternateListItemColor);

            if (!IsLastListItem)
                GraphicsManager.GetSpriteBatch().Draw(pixelTex, new Rectangle(absRect.Left, absRect.Bottom, absRect.Width, 1), UIStyleManager.Style.UIBGColor);

            base.SBDraw();
        }
    }
}
