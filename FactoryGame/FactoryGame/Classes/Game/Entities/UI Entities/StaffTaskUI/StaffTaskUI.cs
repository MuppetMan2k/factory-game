﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    abstract class StaffTaskUI : UIEntity
    {
        // -------- Properties --------
        public bool IsLastListItem { get; set; }

        // -------- Constructors --------
        public StaffTaskUI(Point inPosition, eUIAnchor inAnchor)
            : base(inPosition, inAnchor)
        {
            Rectangle = new Rectangle(inPosition.X, inPosition.Y, 345, 40);
            IsLastListItem = true;
        }

        // -------- Public Methods --------
        public override bool MouseIsOver(out float depth)
        {
            if (UIUtilities.PointIsInRectangle(InputManager.GetMousePosition(), GetAbsoluteRectangle()))
            {
                depth = 0f;
                return true;
            }

            depth = 1f;
            return false;
        }
    }
}
