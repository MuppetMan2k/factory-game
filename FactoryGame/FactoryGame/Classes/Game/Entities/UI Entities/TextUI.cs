﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FactoryGame
{
    // -------- Enumerations --------
    public enum eTextAlignment
    {
        LEFT,
        RIGHT,
        CENTER
    }

    class TextUI : UIEntity
    {
        // -------- Properties --------
        public int MaxLines { get; set; }
        public int MaxWidth { get; set; }
        public Color Color { get; set; }
        public eTextAlignment Alignment { get; set; }
        public int LineSpacing { get; set; }
        public SpriteFont SpriteFont { get; set; }
        public string SpriteFontId { get; set; }

        // -------- Delegates --------
        public delegate string UpdateText();

        // -------- Private Fields --------
        private string text;
        private List<string> lines;
        private string setUpText;
        private int fontHeight;
        private UpdateText textUpdateHandler;

        // -------- Constructors --------
        public TextUI(string inText, Point inPosition, eUIAnchor inAnchor, string inSpriteFontId, int inMaxLines, int inMaxWidth, Color inColor, eTextAlignment inAlignment, int inLineSpacing = 2)
            : base(inPosition, inAnchor)
        {
            setUpText = inText;
            textUpdateHandler = null;
            setUpPosition = inPosition;
            SpriteFontId = inSpriteFontId;
            MaxLines = inMaxLines;
            MaxWidth = inMaxWidth;
            Color = inColor;
            Alignment = inAlignment;
            LineSpacing = inLineSpacing;
        }
        public TextUI(UpdateText inTextUpdateMethod, Point inPosition, eUIAnchor inAnchor, string inSpriteFontId, int inMaxLines, int inMaxWidth, Color inColor, eTextAlignment inAlignment, int inLineSpacing = 3)
            : base(inPosition, inAnchor)
        {
            setUpText = inTextUpdateMethod();
            textUpdateHandler = inTextUpdateMethod;
            setUpPosition = inPosition;
            SpriteFontId = inSpriteFontId;
            MaxLines = inMaxLines;
            MaxWidth = inMaxWidth;
            Color = inColor;
            Alignment = inAlignment;
            LineSpacing = inLineSpacing;
        }

        // -------- Public Methods --------
        public override void Initialize()
        {
            base.Initialize();

            SpriteFont = ContentStorage.GetSpriteFont(SpriteFontId);
            MeasureFontHeight();
            SetText(setUpText);
        }

        public override void Denitialize()
        {
            base.Denitialize();

            SpriteFont = null;
        }

        public override void Update()
        {
            if (textUpdateHandler != null)
                SetText(textUpdateHandler());

            base.Update();
        }

        public override void SBDraw()
        {
            Rectangle absRect = GetAbsoluteRectangle();

            int l = 0;
            foreach (string line in lines)
            {
                int posX = UIUtilities.CalculateBoundedAlignedTextLeftEdge(absRect.Left, absRect.Width, (int)SpriteFont.MeasureString(line).X, Alignment);
                int posY = absRect.Top + l * (fontHeight + LineSpacing);
                Vector2 pos = new Vector2((float)posX, (float)posY);

                GraphicsManager.GetSpriteBatch().DrawString(SpriteFont, line, pos, SetColorAlpha(Color));
                l++;
            }

            base.SBDraw();
        }

        public override bool MouseIsOver(out float depth)
        {
            if (UIUtilities.PointIsInRectangle(InputManager.GetMousePosition(), GetAbsoluteRectangle()))
            {
                depth = 0f;
                return true;
            }

            depth = 1f;
            return false;
        }

        public override void Destroy()
        {
        }

        public void SetText(string inText)
        {
            if (text == inText)
                return;

            text = inText;
            lines = UIUtilities.ConvertTextToLines(text, MaxWidth, MaxLines, SpriteFont);

            SetRectangle();
        }

        public string GetText()
        {
            return text;
        }

        // -------- Private Methods ---------
        private void MeasureFontHeight()
        {
            fontHeight = (int)SpriteFont.MeasureString("Ap").Y;
        }

        private void SetRectangle()
        {
            int rectHeight = (lines.Count * fontHeight + (lines.Count - 1) * LineSpacing);
            int rectWidth = 0;
            foreach (string line in lines)
                rectWidth = Mathf.Max(rectWidth, (int)SpriteFont.MeasureString(line).X);
            int rectX;
            if (MaxWidth == 0)
                rectX = setUpPosition.X;
            else
                rectX = UIUtilities.CalculateBoundedAlignedTextLeftEdge(setUpPosition.X, MaxWidth, rectWidth, Alignment);

            Rectangle = new Rectangle(rectX, setUpPosition.Y, rectWidth, rectHeight);
        }
    }
}
