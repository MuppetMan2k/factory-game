﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FactoryGame
{
    class HudPanelUI : TextureUI
    {
        // -------- Constructors --------
        public HudPanelUI(Point inPosition, eUIAnchor inAnchor, string inTextureId)
            : base(inPosition, inAnchor, inTextureId)
        {
        }

        // -------- Public Methods --------
        public override void Update()
        {
            base.Update();
        }

        public override void SBDraw()
        {
            GraphicsManager.GetSpriteBatch().Draw(Texture, GetAbsoluteRectangle(), null, SetColorAlpha(UIStyleManager.Style.HudPanelColor), 0f, new Vector2(), SpriteEffects.None, 0f);
            base.SBDraw();
        }

        public override void Destroy()
        {
            base.Destroy();
            HudManager.RemoveHud(this);
        }
    }
}
