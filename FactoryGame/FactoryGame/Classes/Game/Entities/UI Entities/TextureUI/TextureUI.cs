﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FactoryGame
{
    abstract class TextureUI : UIEntity
    {
        // -------- Properties --------
        public Texture2D Texture { get; set; }
        public string TextureId { get; set; }

        // -------- Constructors --------
        public TextureUI(Point inPosition, eUIAnchor inAnchor, string inTextureId)
            : base(inPosition, inAnchor)
        {
            setUpPosition = inPosition;
            TextureId = inTextureId;
        }

        // -------- Public Methods --------
        public override void Initialize()
        {
            base.Initialize();
            Texture = ContentStorage.GetTexture(TextureId);
            Rectangle = new Rectangle(setUpPosition.X, setUpPosition.Y, Texture.Width, Texture.Height);
        }

        public override void Denitialize()
        {
            Texture = null;
            base.Denitialize();
        }

        public override bool MouseIsOver(out float depth)
        {
            if (UIUtilities.PointIsOverTexturedRectangle(InputManager.GetMousePosition(), Rectangle, Texture))
            {
                depth = 0f;
                return true;
            }

            depth = 1f;
            return false;
        }
    }
}
