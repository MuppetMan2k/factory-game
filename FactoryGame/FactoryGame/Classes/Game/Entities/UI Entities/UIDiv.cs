﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    class UIDiv : UIEntity
    {
        // -------- Constructors --------
        public UIDiv(Point inPosition, int inWidth, int inHeight, eUIAnchor inAnchor)
            : base(inPosition, inAnchor)
        {
            Rectangle = new Rectangle(inPosition.X, inPosition.Y, inWidth, inHeight);
        }

        // -------- Public Methods --------
        public void SetPosition(Point inPosition)
        {
            Rectangle = new Rectangle(inPosition.X, inPosition.Y, Rectangle.Width, Rectangle.Height);
        }
    }
}
