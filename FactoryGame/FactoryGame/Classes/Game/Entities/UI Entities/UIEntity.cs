﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    // -------- Enumerations --------
    public enum eUIAnchor
    {
        TOP_LEFT,
        TOP_CENTER,
        TOP_RIGHT,
        CENTER_LEFT,
        CENTER_CENTER,
        CENTER_RIGHT,
        BOTTOM_LEFT,
        BOTTOM_CENTER,
        BOTTOM_RIGHT
    }

    abstract class UIEntity : Entity
    {
        // -------- Properties --------
        public UIEntity Parent { get; set; }
        public eUIAnchor Anchor { get; set; }
        public Rectangle Rectangle { get; set; }
        public bool IgnoreParentForAbsoluteRectangle { get; set; }
        // Tooltip and Infotip info
        public TooltipInfo TooltipInfo { get; set; }

        // -------- Private Fields --------
        protected Point setUpPosition;
        protected List<UIEntity> children;
        public float transparency;

        // -------- Constructors --------
        public UIEntity(Point inSetUpPosition, eUIAnchor inAnchor)
        {
            setUpPosition = inSetUpPosition;
            Anchor = inAnchor;
            transparency = 1f;
            IgnoreParentForAbsoluteRectangle = false;
        }

        // -------- Public Methods --------
        public override void Initialize()
        {
            children = new List<UIEntity>();
        }

        public override void Denitialize()
        {
            foreach (UIEntity ui in children)
                ui.Denitialize();

            Parent = null;
            children = null;
        }

        public override void Update()
        {
            foreach (UIEntity ui in GetChildren())
                ui.Update();
        }

        public virtual void SBDraw()
        {
            foreach (UIEntity ui in GetChildren())
                ui.SBDraw();
        }

        public override bool MouseIsOver(out float depth)
        {
            if (UIUtilities.PointIsInRectangle(InputManager.GetMousePosition(), GetAbsoluteRectangle()))
            {
                depth = 0f;
                return true;
            }

            depth = 1f;
            return false;
        }

        public override Point GetBubbleAnchorPoint()
        {
            Rectangle absRect = GetAbsoluteRectangle();
            return new Point(absRect.Center.X, absRect.Top);
        }

        public Rectangle GetRelativeRectangle()
        {
            return GetRectangle();
        }
        public Rectangle GetAbsoluteRectangle()
        {
            Rectangle parentRectangle;

            if (Parent == null || IgnoreParentForAbsoluteRectangle)
                parentRectangle = GraphicsManager.GetFullScreenRectangle();
            else
                parentRectangle = Parent.GetAbsoluteRectangleForParent(this);

            Point anchor = UIUtilities.GetRectangleAnchorPosition(parentRectangle, Anchor);

            Rectangle rectangle = GetRelativeRectangle();

            switch (Anchor)
            {
                case eUIAnchor.TOP_LEFT:
                    rectangle.X = anchor.X + rectangle.X;
                    rectangle.Y = anchor.Y + rectangle.Y;
                    break;
                case eUIAnchor.TOP_CENTER:
                    rectangle.X = anchor.X + rectangle.X;
                    rectangle.Y = anchor.Y + rectangle.Y;
                    break;
                case eUIAnchor.TOP_RIGHT:
                    rectangle.X = anchor.X - rectangle.X - rectangle.Width;
                    rectangle.Y = anchor.Y + rectangle.Y;
                    break;
                case eUIAnchor.CENTER_LEFT:
                    rectangle.X = anchor.X + rectangle.X;
                    rectangle.Y = anchor.Y + rectangle.Y;
                    break;
                case eUIAnchor.CENTER_CENTER:
                    rectangle.X = anchor.X + rectangle.X;
                    rectangle.Y = anchor.Y + rectangle.Y;
                    break;
                case eUIAnchor.CENTER_RIGHT:
                    rectangle.X = anchor.X - rectangle.X - rectangle.Width;
                    rectangle.Y = anchor.Y + rectangle.Y;
                    break;
                case eUIAnchor.BOTTOM_LEFT:
                    rectangle.X = anchor.X + rectangle.X;
                    rectangle.Y = anchor.Y - rectangle.Y - rectangle.Height;
                    break;
                case eUIAnchor.BOTTOM_CENTER:
                    rectangle.X = anchor.X + rectangle.X;
                    rectangle.Y = anchor.Y - rectangle.Y - rectangle.Height;
                    break;
                case eUIAnchor.BOTTOM_RIGHT:
                    rectangle.X = anchor.X - rectangle.X - rectangle.Width;
                    rectangle.Y = anchor.Y - rectangle.Y - rectangle.Height;
                    break;
            }

            return rectangle;
        }
        public virtual Rectangle GetAbsoluteRectangleForParent(UIEntity childEntity)
        {
            return GetAbsoluteRectangle();
        }

        public void SetChild(UIEntity child)
        {
            children.Add(child);
            child.Parent = this;
        }
        public virtual List<UIEntity> GetChildren()
        {
            return children;
        }

        public void SetTooltip(string inText, ePortAnchor inPortAnchor)
        {
            TooltipInfo = new TooltipInfo(inText, inPortAnchor);
        }

        // -------- Private Methods ---------
        protected virtual Rectangle GetRectangle()
        {
            return Rectangle;
        }

        protected float GetTransparency()
        {
            if (Parent != null)
                return transparency * Parent.GetTransparency();

            return transparency;
        }

        protected Color SetColorAlpha(Color color)
        {
            float alpha = ((float)color.A) / 255f;
            alpha *= GetTransparency();

            color.A = (Byte)(alpha * 255f);

            return color;
        }
    }
}
