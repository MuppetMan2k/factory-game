﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class Room
    {
        // -------- Properties --------
        public List<Tile> Tiles { get; set; }
        public AmbientLight AmbientLight { get; set; }
        public bool Purchased
        {
            get
            {
                if (Tiles == null)
                    return false;

                return Tiles[0].Purchased;
            }
        }
        public int WindowFactor { get; set; }
        public int LightFactor { get; set; }
        public bool LightsOn { get; set; }

        // -------- Private Fields --------
        // Parameters
        private const float SPECIFIC_WINDOW_MULTIPLIER = 2f;
        private const float SPECIFIC_LIGHT_MULTIPLIER = 4f;

        // -------- Constructors --------
        public Room()
        {
            Tiles = new List<Tile>();
            AmbientLight = AmbientLight.Zero;
        }

        // -------- Public Methods --------
        public int GetRoomArea()
        {
            return Tiles.Count;
        }

        public float GetSpecificWindowFactor()
        {
            return SPECIFIC_WINDOW_MULTIPLIER * (float)WindowFactor / (float)GetRoomArea();
        }

        public float GetSpecificLightFactor()
        {
            return SPECIFIC_LIGHT_MULTIPLIER * (float)LightFactor / (float)GetRoomArea();
        }

        public float GetAmbientLightIntensityMultiplier()
        {
            return SPECIFIC_LIGHT_MULTIPLIER / (float)GetRoomArea();
        }

        public void AddAmbientLight(AmbientLight newLight)
        {
            AmbientLight = AmbientLightingManager.CombineAmbientLights(AmbientLight, newLight);
        }
    }
}
