﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    // -------- Enumerations --------
    public enum eWallPillarHeight
    {
        FULL,
        CUT
    }

    class WallPillar
    {
        // -------- Properties --------
        public eWallPillarHeight Height { get; set; }
    }
}
