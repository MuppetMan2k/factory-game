﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FactoryGame
{
    class EffectSettings
    {
        // -------- Properties --------
        public string Technique { get; set; }
        public Matrix World { get; set; }
        public Matrix View { get; set; }
        public Matrix Projection { get; set; }
        // Direct light
        public Vector3 DirectLightColor { get; set; }
        public float DirectLightIntensity { get; set; }
        public Vector3 DirectLightDirection { get; set; }
        // Ambient light
        public Vector3 AmbientLightColor { get; set; }
        public float AmbientLightIntensity { get; set; }
        // After-effects
        public float Alpha { get; set; }
        public float WhiteLerp { get; set; }
        public Vector3 ForcedColor { get; set; }
    }
}
