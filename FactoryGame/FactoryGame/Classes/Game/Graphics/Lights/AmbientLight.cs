﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    class AmbientLight : Light
    {
        // -------- Static Methods --------
        public static AmbientLight Zero
        {
            get
            {
                var al = new AmbientLight();
                al.Color = Color.Black;
                al.Intensity = 0f;

                return al;
            }
        }
    }
}
