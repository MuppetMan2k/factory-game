﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    class DirectionalLight : Light
    {
        // -------- Properties --------
        public Vector3 Direction { get; set; }

        // -------- Static Methods --------
        public static DirectionalLight Zero
        {
            get
            {
                var dl = new DirectionalLight();
                dl.Direction = Vector3.Zero;
                dl.Color = Color.Black;
                dl.Intensity = 0f;

                return dl;
            }
        }
    }
}
