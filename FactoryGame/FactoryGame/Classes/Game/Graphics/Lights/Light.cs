﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    abstract class Light
    {
        // -------- Properties --------
        public float Intensity { get; set; }
        public Color Color { get; set; }

        // -------- Public Methods --------
        public Light Clone()
        {
            return (Light)MemberwiseClone();
        }
    }
}
