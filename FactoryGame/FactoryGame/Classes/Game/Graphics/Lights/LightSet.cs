﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class LightSet
    {
        // -------- Properties --------
        public AmbientLight Ambient { get; set; }
        public DirectionalLight Directional { get; set; }
    }
}
