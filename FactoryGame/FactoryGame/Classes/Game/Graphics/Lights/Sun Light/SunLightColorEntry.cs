﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    class SunLightColorEntry
    {
        // -------- Properties --------
        public float Time { get; set; }
        public Color Color { get; set; }

        // -------- Constructors --------
        public SunLightColorEntry(float inTime, Color inColor)
        {
            Time = inTime;
            Color = inColor;
        }
    }
}
