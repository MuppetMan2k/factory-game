﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class Interactability
    {
        // -------- Properties --------
        public List<InteractableElement> InteractableElements { get; set; }

        // -------- Constructors --------
        public Interactability()
        {
            InteractableElements = new List<InteractableElement>();
        }

        // -------- Public Methods --------
        public void Update()
        {
            foreach (InteractableElement e in InteractableElements)
                e.Update();
        }

        public InteractableElement GetInteractableElement_CleanUp()
        {
            foreach (InteractableElement ie in InteractableElements)
                if (ie.Interaction is CleanUpInteraction)
                    return ie;

            return null;
        }
        public InteractableElement GetInteractableElement_Fix()
        {
            foreach (InteractableElement ie in InteractableElements)
                if (ie.Interaction is FixInteraction)
                    return ie;

            return null;
        }
        public InteractableElement GetInteractableElement_Operate()
        {
            foreach (InteractableElement ie in InteractableElements)
                if (ie.Interaction is OperateInteraction)
                    return ie;

            return null;
        }
        public InteractableElement GetInteractableElement_PickUp()
        {
            foreach (InteractableElement ie in InteractableElements)
                if (ie.Interaction is PickUpInteraction)
                    return ie;

            return null;
        }
        public InteractableElement GetInteractableElement_PutDown()
        {
            foreach (InteractableElement ie in InteractableElements)
                if (ie.Interaction is PutDownInteraction)
                    return ie;

            return null;
        }
        public InteractableElement GetInteractableElement_TakeBreak()
        {
            foreach (InteractableElement ie in InteractableElements)
                if (ie.Interaction is TakeBreakInteraction)
                    return ie;

            return null;
        }
    }
}
