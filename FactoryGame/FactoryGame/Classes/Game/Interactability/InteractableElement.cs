﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    class InteractableElement
    {
        // -------- Properties --------
        // Entities
        public GameEntity Parent { get; set; }
        public Character InteractingCharacter { get; set; }
        // Visual
        public TileCoordinate InteractionTileRelativeCoordinate { get; set; }
        public Vector2 InteractionRelativePosition { get; set; }
        public eSquareRotation InteractionRelativeRotation { get; set; }
        public string InteractionAnimation { get; set; }
        // Interaction
        public Interaction Interaction { get; set; }

        // -------- Public Methods --------
        public void Update()
        {
            Interaction.Update();
        }

        public void CreateInteraction(eInteractionType type)
        {
            switch (type)
            {
                case eInteractionType.CLEAN_UP:
                    Interaction = new CleanUpInteraction(Parent, this);
                    break;
                case eInteractionType.FIX:
                    Interaction = new FixInteraction(Parent, this);
                    break;
                case eInteractionType.OPERATE:
                    Interaction = new OperateInteraction(Parent, this);
                    break;
                case eInteractionType.PICK_UP:
                    Interaction = new PickUpInteraction(Parent, this);
                    break;
                case eInteractionType.PUT_DOWN:
                    Interaction = new PutDownInteraction(Parent, this);
                    break;
                case eInteractionType.TAKE_BREAK:
                    Debug.LogWarning("Do not use this CreateInteraction method for take break interactions, use CreateTakeBreakInteraction instead!");
                    Interaction = new TakeBreakInteraction(Parent, this, null);
                    break;
            }
        }
        public void CreateTakeBreakInteraction(StaffNeedChange needChange)
        {
            Interaction = new TakeBreakInteraction(Parent, this, needChange);
        }

        public void SetInteractionPutDownElementStore(ElementStore store)
        {
            (Interaction as PutDownInteraction).ParentElementStore = store;
        }
        public void SetInteractionPickUpElementStore(ElementStore store)
        {
            (Interaction as PickUpInteraction).ParentElementStore = store;
        }

        public void StartInteraction(Character character, Instruction instruction)
        {
            InteractingCharacter = character;
            Interaction.StartInteraction(character, instruction);
        }
        public void StopInteraction()
        {
            Interaction.OnInteractionStopped();
            Character tempCharacter = InteractingCharacter;
            InteractingCharacter = null;
            tempCharacter.OnFinishInstruction();
        }
        public void OnInteractionStopped()
        {
            Character tempCharacter = InteractingCharacter;
            InteractingCharacter = null;
            tempCharacter.OnFinishInstruction();
        }

        public bool IsAvailable(Character testCharacter)
        {
            if (Interaction is OperateInteraction)
            {
                Machine parentMachine = Parent as Machine;
                if (parentMachine.Broken)
                    return false;
            }

            if (InteractingCharacter == null)
                return true;

            if (InteractingCharacter.Equals(testCharacter))
                return true;

            return false;
        }

        public TileCoordinate GetInteractionTileCoordinate()
        {
            TileCoordinate parentCoord = GameEntityUtilities.GetGameEntityTileCoordinate(Parent);
            eSquareRotation parentRotation = GameEntityUtilities.GetGameEntityRotation(Parent);

            TileCoordinate interactionCoord = TileUtilities.AddRotatedTileToTileOrigin(parentCoord, InteractionTileRelativeCoordinate, parentRotation);

            return interactionCoord;
        }
    }
}
