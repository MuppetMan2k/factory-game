﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    struct InteractionData
    {
        // -------- Public Fields --------
        public TileCoordinate TileRelCoord;
        public Vector2 RelPosition;
        public eSquareRotation RelRotation;
        public string Animation;
        public eInteractionType Type;
    }
}
