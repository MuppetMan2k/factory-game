﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class CleanUpInteraction : Interaction
    {
        // -------- Constructors --------
        public CleanUpInteraction(GameEntity inParent, InteractableElement inParentElement)
            : base(inParent, inParentElement)
        {
            Type = eInteractionType.CLEAN_UP;
        }

        // -------- Private Methods ---------
        public override void StartInteraction(Character character, Instruction instruction)
        {
            base.StartInteraction(character, instruction);

            Staff staff = character as Staff;
            if (staff == null)
                return;

            InteractionSpeed = ModifierManager.GetTrashPickupSpeed(staff);
        }

        protected override void UpdateInteraction()
        {
        }

        protected override void OnInteractionFinished()
        {
            Parent.Destroy();
        }
    }
}
