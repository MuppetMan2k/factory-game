﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class FixInteraction : Interaction
    {
        // -------- Constructors --------
        public FixInteraction(GameEntity inParent, InteractableElement inParentElement)
            : base(inParent, inParentElement)
        {
            Type = eInteractionType.FIX;
        }

        // -------- Private Methods ---------
        protected override void UpdateInteraction()
        {
        }

        protected override void OnInteractionFinished()
        {
        }
    }
}
