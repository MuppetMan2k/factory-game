﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    // -------- Enumerations --------
    public enum eInteractionType
    {
        CLEAN_UP,
        FIX,
        OPERATE,
        PICK_UP,
        PUT_DOWN,
        TAKE_BREAK
    }

    abstract class Interaction
    {
        // -------- Properties --------
        public eInteractionType Type { get; protected set; }
        // Entities
        public Character InteractingCharacter { get; set; }
        public GameEntity Parent { get; set; }
        public InteractableElement ParentElement { get; set; }
        // Interaction
        public float InteractionTime { get; set; }
        public float InteractionSpeed { get; set; }
        public bool Active { get; set; }

        // -------- Private Fields --------
        private float timer;

        // -------- Constructors --------
        public Interaction(GameEntity inParent, InteractableElement inParentElement)
        {
            Parent = inParent;
            ParentElement = inParentElement;
            InteractionSpeed = 1f;
        }

        // -------- Public Methods --------
        public void Update()
        {
            if (!Active)
                return;

            UpdateTimer();
            UpdateInteraction();
        }

        public virtual void StartInteraction(Character character, Instruction instruction)
        {
            Active = true;
            InteractingCharacter = character;
            timer = 0f;
        }

        public void StopInteraction()
        {
            Active = false;
            InteractingCharacter = null;
            ParentElement.OnInteractionStopped();
        }

        public void OnInteractionStopped()
        {
            Active = false;
            InteractingCharacter = null;
        }

        public float GetCompletionPercentage()
        {
            if (InteractionTime == 0)
                return 0f;

            return timer / InteractionTime;
        }

        // -------- Private Methods ---------
        private void UpdateTimer()
        {
            timer += GameSpeedManager.ElapsedGameTime * InteractionSpeed;

            if (timer >= InteractionTime)
            {
                timer = 0f;
                OnInteractionFinished();
                StopInteraction();
            }
        }

        protected abstract void UpdateInteraction();
        protected abstract void OnInteractionFinished();
    }
}
