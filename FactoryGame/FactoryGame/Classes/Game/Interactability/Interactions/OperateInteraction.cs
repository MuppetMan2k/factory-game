﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class OperateInteraction : Interaction
    {
        // -------- Private Fields --------
        private ProcessData process;

        // -------- Constructors --------
        public OperateInteraction(GameEntity inParent, InteractableElement inParentElement)
            : base(inParent, inParentElement)
        {
            Type = eInteractionType.OPERATE;
        }

        // -------- Public Methods --------
        public ProcessData GetProcess()
        {
            return process;
        }

        public override void StartInteraction(Character character, Instruction instruction)
        {
            base.StartInteraction(character, instruction);

            OperateTask task = instruction.ParentTask as OperateTask;
            Machine machine = Parent as Machine;
            Staff staff = character as Staff;

            if (task == null || machine == null || staff == null)
                return;

            float baseTime = task.OperationProcess.BaseTime;
            process = task.OperationProcess;
            InteractionTime = baseTime;
            InteractionSpeed = ModifierManager.GetMachineProcessSpeed(task.OperationProcess, staff, machine);
        }

        // -------- Private Methods ---------
        protected override void UpdateInteraction()
        {
        }

        protected override void OnInteractionFinished()
        {
            Staff staff = InteractingCharacter as Staff;

            if (staff == null)
                return;

            (Parent as Machine).CarryOutProcess(process, staff);
        }
    }
}
