﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class PickUpInteraction : Interaction
    {
        // -------- Properties --------
        public ElementStore ParentElementStore { get; set; }

        // -------- Private Fields --------
        // Constant params
        private const float PICK_UP_BASE_TIME = 1.0f;

        // -------- Constructors --------
        public PickUpInteraction(GameEntity inParent, InteractableElement inParentElement)
            : base(inParent, inParentElement)
        {
            Type = eInteractionType.PICK_UP;
        }

        // -------- Private Methods ---------
        public override void StartInteraction(Character character, Instruction instruction)
        {
            base.StartInteraction(character, instruction);

            Staff staff = character as Staff;
            if (staff == null)
                return;

            InteractionTime = PICK_UP_BASE_TIME;
            InteractionSpeed = ModifierManager.GetElementPickUpSpeed(staff);
        }

        protected override void UpdateInteraction()
        {
        }

        protected override void OnInteractionFinished()
        {
            Staff staff = InteractingCharacter as Staff;

            if (staff == null)
                return;

            LoadTask loadTask = staff.Tasks.CurrentTask as LoadTask;
            if (loadTask == null)
                return;

            int elementIndex = ElementManager.GetElementIndexToLoad(loadTask);
            Element element = ParentElementStore.TakeElement(elementIndex);
            staff.PickUpElement(element);

            ElementManager.OnElementMoved();
        }
    }
}
