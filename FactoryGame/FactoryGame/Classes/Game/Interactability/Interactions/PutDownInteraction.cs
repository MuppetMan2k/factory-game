﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class PutDownInteraction : Interaction
    {
        // -------- Properties --------
        public ElementStore ParentElementStore { get; set; }

        // -------- Private Fields --------
        // Constant params
        private const float PUT_DOWN_BASE_TIME = 1.0f;

        // -------- Constructors --------
        public PutDownInteraction(GameEntity inParent, InteractableElement inParentElement)
            : base(inParent, inParentElement)
        {
            Type = eInteractionType.PUT_DOWN;
        }

        // -------- Private Methods ---------
        public override void StartInteraction(Character character, Instruction instruction)
        {
            base.StartInteraction(character, instruction);

            Staff staff = character as Staff;
            if (staff == null)
                return;

            InteractionTime = PUT_DOWN_BASE_TIME;
            InteractionSpeed = ModifierManager.GetElementPutDownSpeed(staff);
        }

        protected override void UpdateInteraction()
        {
        }

        protected override void OnInteractionFinished()
        {
            Staff staff = InteractingCharacter as Staff;

            if (staff == null)
                return;

            Element element = staff.PutDownElement();
            ParentElementStore.AddElement(element);
            
            ElementManager.OnElementMoved();
        }
    }
}
