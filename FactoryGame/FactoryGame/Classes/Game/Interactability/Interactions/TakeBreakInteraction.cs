﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class TakeBreakInteraction : Interaction
    {
        // -------- Properties --------
        public StaffNeedChange NeedChange { get; set; }

        // -------- Constructors --------
        public TakeBreakInteraction(GameEntity inParent, InteractableElement inParentElement, StaffNeedChange inNeedChange)
            : base(inParent, inParentElement)
        {
            Type = eInteractionType.TAKE_BREAK;
            NeedChange = inNeedChange;
        }

        // -------- Private Methods ---------
        protected override void UpdateInteraction()
        {
        }

        protected override void OnInteractionFinished()
        {
        }
    }
}
