﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class MouseOverSelectionFilter
    {
        // -------- Properties --------
        public List<eMouseOverType> SelectableMouseOverTypes { get; private set; }

        // -------- Constructors --------
        public MouseOverSelectionFilter()
        {
            SelectableMouseOverTypes = new List<eMouseOverType>();
        }

        // -------- Static Instances ---------
        public static MouseOverSelectionFilter Default
        {
            get
            {
                var mosf = new MouseOverSelectionFilter();
                mosf.SelectableMouseOverTypes.Add(eMouseOverType.FACTORY_OBJECT);
                mosf.SelectableMouseOverTypes.Add(eMouseOverType.CHARACTER);
                mosf.SelectableMouseOverTypes.Add(eMouseOverType.FLAT_UI);
                mosf.SelectableMouseOverTypes.Add(eMouseOverType.STATIC_UI);
                mosf.SelectableMouseOverTypes.Add(eMouseOverType.SCROLL_WINDOW_UI);
                mosf.SelectableMouseOverTypes.Add(eMouseOverType.INTERACTABLE_UI);
                return mosf;
            }
        }

        public static MouseOverSelectionFilter UIOnly
        {
            get
            {
                var mosf = new MouseOverSelectionFilter();
                mosf.SelectableMouseOverTypes.Add(eMouseOverType.FLAT_UI);
                mosf.SelectableMouseOverTypes.Add(eMouseOverType.INTERACTABLE_UI);
                mosf.SelectableMouseOverTypes.Add(eMouseOverType.SCROLL_WINDOW_UI);
                mosf.SelectableMouseOverTypes.Add(eMouseOverType.STATIC_UI);
                return mosf;
            }
        }

        public static MouseOverSelectionFilter Everything
        {
            get
            {
                var mosf = new MouseOverSelectionFilter();
                foreach (eMouseOverType type in EnumUtilities.GetEnumValues<eMouseOverType>())
                    mosf.SelectableMouseOverTypes.Add(type);
                return mosf;
            }
        }

        public static MouseOverSelectionFilter Debug
        {
            get
            {
                var mosf = new MouseOverSelectionFilter();
                mosf.SelectableMouseOverTypes.Add(eMouseOverType.OTHER);
                mosf.SelectableMouseOverTypes.Add(eMouseOverType.TILE);
                mosf.SelectableMouseOverTypes.Add(eMouseOverType.WALL);
                mosf.SelectableMouseOverTypes.Add(eMouseOverType.WINDOW);
                mosf.SelectableMouseOverTypes.Add(eMouseOverType.DOOR);
                mosf.SelectableMouseOverTypes.Add(eMouseOverType.FACTORY_OBJECT);
                mosf.SelectableMouseOverTypes.Add(eMouseOverType.CHARACTER);
                mosf.SelectableMouseOverTypes.Add(eMouseOverType.FLAT_UI);
                mosf.SelectableMouseOverTypes.Add(eMouseOverType.INTERACTABLE_UI);
                return mosf;
            }
        }
    }
}
