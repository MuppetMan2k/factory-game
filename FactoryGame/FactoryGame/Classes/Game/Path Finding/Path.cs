﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class Path
    {
        // -------- Properties --------
        public List<TileCoordinate> Tiles { get; set; }

        // -------- Constructors --------
        public Path()
        {
            Tiles = new List<TileCoordinate>();
        }
    }
}
