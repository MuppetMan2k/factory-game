﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    class UIStyle
    {
        // -------- Properties --------
        // ######## Colors ########
        // Panels
        public Color HudPanelColor { get; set; }
        public Color HudPanelTextColor { get; set; }
        // Tooltips
        public Color TooltipPortColor { get; set; }
        public Color TooltipTextColor { get; set; }
        // Buttons
        public Color ButtonHighlightColor { get; set; }
        // Ports
        public Color PortHeaderColor { get; set; }
        public Color PortHeaderTextColor { get; set; }
        public Color PortBodyColor { get; set; }
        public Color PortBodyTextColor { get; set; }
        // General UI
        public Color UIBGColor { get; set; }
        public Color ScrollBarColor { get; set; }
        public Color ListTextColor { get; set; }
        public Color ListItemColor { get; set; }
        public Color AlternateListItemColor { get; set; }
        // Window scroll divs
        public Color DivHeaderTextColor { get; set; }
        public Color DivHeaderLineColor { get; set; }
        // Icon colors
        public Color GeneralIconColor { get; set; }
        public Color ElementIconColor { get; set; }
        public Color SkillIconColor { get; set; }
        public Color ButtonColor { get; set; }
        public Color EmptyIconColor { get; set; }
        // Need level colors
        public Color NeedLevel1Color { get; set; }
        public Color NeedLevel2Color { get; set; }
        public Color NeedLevel3Color { get; set; }
        public Color NeedLevel4Color { get; set; }
        public Color NeedLevel5Color { get; set; }
        // Bar colors
        public Color CompletionBarColor { get; set; }
        public Color BarBGColor { get; set; }
        public Color HoursSliderBarColor { get; set; }
        // General colors
        public Color SecondaryUIColor { get; set; }

        // ######## Fonts ########
        public string HudFont { get; set; }
        public string TooltipFont { get; set; }
        public string UIHeaderFont { get; set; }
        public string UIFont { get; set; }
        public string UISmallFont { get; set; }
        public string DivHeaderFont { get; set; }

        // -------- Static Properties --------
        public static UIStyle Default
        {
            get
            {
                UIStyle style = new UIStyle();

                // ######## Colors ########
                // Panels
                style.HudPanelColor = new Color(0xEE, 0xEE, 0xEE, 0xA0);
                style.HudPanelTextColor = new Color(0x33, 0x33, 0x33, 0xFF);
                // Tooltips
                style.TooltipPortColor = new Color(0x38, 0x7C, 0xC0, 0xFF);
                style.TooltipTextColor = new Color(0xFF, 0xFF, 0xFF, 0xFF);
                // Buttons
                style.ButtonHighlightColor = new Color(0x00, 0x00, 0x00, 0x80);
                // Ports
                style.PortHeaderColor = new Color(0x22, 0x22, 0x22, 0xFF);
                style.PortHeaderTextColor = new Color(0xFF, 0xFF, 0xFF, 0xFF);
                style.PortBodyColor = new Color(0xFF, 0xFF, 0xFF, 0xFF);
                style.PortBodyTextColor = new Color(0x00, 0x00, 0x00, 0xFF);
                // General UI
                style.UIBGColor = new Color(0x99, 0x99, 0x99, 0xFF);
                style.ScrollBarColor = new Color(0xCC, 0xCC, 0xCC, 0xFF);
                style.ListTextColor = new Color(0x00, 0x00, 0x00, 0xFF);
                style.ListItemColor = new Color(0xCC, 0xCC, 0xCC, 0xFF);
                style.AlternateListItemColor = new Color(0xEE, 0xEE, 0xEE, 0xFF);
                // Window scroll divs
                style.DivHeaderTextColor = new Color(0x33, 0x33, 0x33, 0xFF);
                style.DivHeaderLineColor = new Color(0x66, 0x66, 0x66, 0xFF);
                // Icon colors
                style.GeneralIconColor = new Color(0x99, 0x99, 0x99, 0xFF);
                style.ElementIconColor = new Color(0xEE, 0xEE, 0xEE, 0xFF);
                style.SkillIconColor = new Color(0xEE, 0xEE, 0xEE, 0xFF);
                style.ButtonColor = new Color(0xEE, 0xEE, 0xEE, 0xFF);
                style.EmptyIconColor = new Color(0xCC, 0xCC, 0xCC, 0xFF);
                // Need level colors
                style.NeedLevel1Color = new Color(0xA7, 0x32, 0x32, 0xFF);
                style.NeedLevel2Color = new Color(0xD5, 0x62, 0x2B, 0xFF);
                style.NeedLevel3Color = new Color(0xEF, 0xCB, 0x38, 0xFF);
                style.NeedLevel4Color = new Color(0x8E, 0xB8, 0x38, 0xFF);
                style.NeedLevel5Color = new Color(0x29, 0x70, 0x25, 0xFF);
                // Bar colors
                style.CompletionBarColor = style.NeedLevel4Color;
                style.BarBGColor = new Color(0x33, 0x33, 0x33, 0xFF);
                style.HoursSliderBarColor = style.NeedLevel1Color;
                // General colors
                style.SecondaryUIColor = new Color(0xCC, 0xCC, 0xCC, 0xFF);

                // ######## Fonts ########
                style.HudFont = "calibri-16-b";
                style.TooltipFont = "calibri-12";
                style.UIHeaderFont = "calibri-14-b";
                style.UIFont = "calibri-12";
                style.UISmallFont = "calibri-8";
                style.DivHeaderFont = "calibri-12";

                return style;
            }
        }
    }
}
