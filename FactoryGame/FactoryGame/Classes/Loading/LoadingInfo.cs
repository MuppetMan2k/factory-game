﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    // -------- Enumerations --------
    public enum eLoadingTargetFlowState
    {
        MENU,
        GAME
    }

    class LoadingInfo
    {
        // -------- Properties --------
        public eLoadingTargetFlowState TargetFlowState { get; set; }
    }
}
