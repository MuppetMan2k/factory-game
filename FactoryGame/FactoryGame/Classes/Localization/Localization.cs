﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataTypes;

namespace FactoryGame
{
    static class Localization
    {
        // -------- Private Fields --------
        private static Dictionary<string, string> localizationDict;
        private static LocalizationListItem[] localizationList;

        // -------- Public Methods --------
        public static void Initialize()
        {
            localizationDict = new Dictionary<string, string>();
        }

        public static void LoadContent()
        {
            localizationList = ContentLoader.LoadGameContent<LocalizationListItem[]>("XML/Localization/Loc_en-us");

            PopulateLocalizationDict();
        }

        public static string LocalizeString(string id)
        {
            if (localizationDict.ContainsKey(id))
                return localizationDict[id];

            return ("?: " + id);
        }

        public static string LocalizeCurrencyNumber(int number)
        {
            string locNumber = number.ToString("#,##0");
            locNumber.Replace(",", LocalizeString("text_thousand_separator"));

            return locNumber;
        }

        // -------- Private Methods ---------
        private static void PopulateLocalizationDict()
        {
            foreach (LocalizationListItem item in localizationList)
                localizationDict.Add(item.id, item.text);

            localizationList = null;
        }
    }
}
