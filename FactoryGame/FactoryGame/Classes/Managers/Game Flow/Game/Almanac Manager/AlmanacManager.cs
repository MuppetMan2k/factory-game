﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    static class AlmanacManager
    {
        // -------- Private Fields --------
        private static Dictionary<string, ElementData> elements;
        private static Dictionary<string, SkillData> skills;
        private static Dictionary<string, ProcessData> processes;
        private static Dictionary<string, MachineData> machines;

        // -------- Public Methods --------
        public static void Initialize()
        {
            elements = ElementDataStore.GetElementTypes();
            skills = SkillDataStore.GetSkills();
            processes = ProcessDataStore.GetProcesses();
            machines = MachineDataStore.GetMachines();
        }

        public static void Denitialize()
        {
            elements = null;
            skills = null;
            processes = null;
            machines = null;
        }

        // Data get methods
        public static ElementData GetElement(string id)
        {
            if (elements == null)
            {
                Debug.LogError("Tried to get almanac element type, but dictionary not initialized yet!");
                return ElementData.Null;
            }

            if (!elements.ContainsKey(id))
            {
                Debug.LogWarning("Almanac does not contain element type: " + id);
                return ElementData.Null;
            }

            return elements[id];
        }
        public static SkillData GetSkill(string id)
        {
            if (skills == null)
            {
                Debug.LogError("Tried to get almanac skill, but dictionary not initialized yet!");
                return SkillData.Null;
            }

            if (!skills.ContainsKey(id))
            {
                Debug.LogWarning("Almanac does not contain skill: " + id);
                return SkillData.Null;
            }

            return skills[id];
        }
        public static ProcessData GetProcess(string id)
        {
            if (processes == null)
            {
                Debug.LogError("Tried to get almanac process, but dictionary not initialized yet!");
                return ProcessData.Null;
            }

            if (!processes.ContainsKey(id))
            {
                Debug.LogWarning("Almanac does not contain process: " + id);
                return ProcessData.Null;
            }

            return processes[id];
        }
        public static MachineData GetMachine(string id)
        {
            if (machines == null)
            {
                Debug.LogError("Tried to get almanac machine, but dictionary not initialized yet!");
                return MachineData.Null;
            }

            if (!machines.ContainsKey(id))
            {
                Debug.LogWarning("Almanac does not contain machine: " + id);
                return MachineData.Null;
            }

            return machines[id];
        }
    }
}
