﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Keys = Microsoft.Xna.Framework.Input.Keys;

namespace FactoryGame
{
    // -------- Enumerations --------
    public enum eCameraType
    {
        ORTHOGRAPHIC,
        PERSPECTIVE
    }

    static class CameraManager
    {
        // -------- Properties --------
        public static Matrix ViewMatrix { get; private set; }
        public static Matrix ProjectionMatrix { get; private set; }

        // -------- Private Fields --------
        private static Vector3 position;
        private static eCameraType type;
        // Movement
        private const float MOVE_SPEED = 10f;
        private const float TARGET_LIMIT_BORDER = 1f;
        // Rotation
        private static float rotationAngle;
        private static float startRotationAngle;
        private static float targetRotationAngle;
        private static float liftAngle;
        private static AnimationCurve rotationAnimation;
        private const float ROTATION_TIME = 0.25f;
        // Zoom
        private static float orthHeight;
        private static float startOrthHeight;
        private static float targetOrthHeight;
        private static float perspFov;
        private static float startPerspFov;
        private static float targetPerspFov;
        private static int zoomLevel;
        private static float[] orthHeightLevels = { 50f, 20f, 10f, 5f };
        private static float[] perspFovLevels = { 90f, 45f, 25f, 15f };
        private static AnimationCurve zoomAnimation;
        private const float ZOON_TIME = 0.2f;
        // Planes
        private const float CAMERA_PLANE_HEIGHT = 50f;
        private const float NEAR_CLIPPING_PLANE = 1f;
        private const float FAR_CLIPPING_PLANE = 200f;

        // -------- Public Methods --------
        public static void Initialize()
        {
            type = eCameraType.ORTHOGRAPHIC;
            rotationAnimation = new AnimationCurve(new SCurveAnimationFunction(), ROTATION_TIME, false);
            zoomAnimation = new AnimationCurve(new SCurveAnimationFunction(), ZOON_TIME, false);
            SetInitialCameraPosition();
        }

        public static void Denitialize()
        {
            rotationAnimation = null;
            zoomAnimation = null;
        }

        public static void Update()
        {
            CheckForMovement();
            CheckForZoom();
            UpdateZoom();
            CheckForRotation();
            UpdateRotation();

            SetCameraMatrices();
        }

        public static void RotateCameraLeft()
        {
            RotateCameraTarget(eSquareRotation.ROTATION_90, eRotationDirection.CLOCKWISE);
        }
        public static void RotateCameraRight()
        {
            RotateCameraTarget(eSquareRotation.ROTATION_90, eRotationDirection.ANTICLOCKWISE);
        }

        public static void ChangeZoom(int zoomLevelChange)
        {
            int newZoomLevel = zoomLevel + zoomLevelChange;
            newZoomLevel = Mathf.Clamp(newZoomLevel, 0, orthHeightLevels.Count() - 1);

            if (zoomLevel == newZoomLevel)
                return;

            zoomLevel = newZoomLevel;
            targetOrthHeight = orthHeightLevels[zoomLevel];
            startOrthHeight = orthHeight;
            targetPerspFov = perspFovLevels[zoomLevel];
            startPerspFov = perspFov;

            zoomAnimation.Reset();
        }

        public static eDiagonalCompassDirection GetApproxViewDirection()
        {
            float normalizedRotation = rotationAngle;
            while (normalizedRotation < 0)
                normalizedRotation += 360f;
            while (normalizedRotation >= 360f)
                normalizedRotation -= 360f;

            if (normalizedRotation >= 0f && normalizedRotation <= 90f)
                return eDiagonalCompassDirection.NORTHEAST;

            if (normalizedRotation > 90f && normalizedRotation <= 180f)
                return eDiagonalCompassDirection.SOUTHEAST;

            if (normalizedRotation > 180f && normalizedRotation <= 270f)
                return eDiagonalCompassDirection.SOUTHWEST;

            return eDiagonalCompassDirection.NORTHWEST;
        }

        // -------- Private Methods ---------
        // Initialization
        private static void SetInitialCameraPosition()
        {
            position = new Vector3(0f, CAMERA_PLANE_HEIGHT, 0f);
            SetLiftAngleToIso();
            SetInitialRotationAngle();
            SetInitialZoom();
            SetCameraTargetPosition(FloorManager.GetTileSpaceCenter());
        }

        private static void SetInitialRotationAngle()
        {
            rotationAngle = CompassUtilities.ConvertDirectionToCompassRotation(eCompassDirection.NORTHEAST);
            startRotationAngle = CompassUtilities.ConvertDirectionToCompassRotation(eCompassDirection.NORTHEAST);
            targetRotationAngle = CompassUtilities.ConvertDirectionToCompassRotation(eCompassDirection.NORTHEAST);
        }

        private static void SetInitialZoom()
        {
            zoomLevel = 2;

            targetOrthHeight = orthHeightLevels[zoomLevel];
            startOrthHeight = targetOrthHeight;
            orthHeight = targetOrthHeight;

            targetPerspFov = perspFovLevels[zoomLevel];
            startPerspFov = targetPerspFov;
            perspFov = targetPerspFov;
        }

        // Input response
        private static void CheckForMovement()
        {
            bool up = InputManager.KeyIsDown(Keys.W, false);
            bool right = InputManager.KeyIsDown(Keys.D, false);
            bool down = InputManager.KeyIsDown(Keys.S, false);
            bool left = InputManager.KeyIsDown(Keys.A, false);

            if (!up && !right && !down && !left)
                return;

            float movementRotation = 0f;

            if (up)
                movementRotation = 0f;
            if (right)
                movementRotation = 90f;
            if (down)
                movementRotation = 180f;
            if (left)
                movementRotation = 270f;
            if (up && right)
                movementRotation = 45f;
            if (right && down)
                movementRotation = 135f;
            if (down && left)
                movementRotation = 225f;
            if (left && up)
                movementRotation = 315f;

            MoveCamera(movementRotation);
        }
        private static void CheckForRotation()
        {
            if (InputManager.KeyIsDown(Keys.Q))
                RotateCameraLeft();
            else if (InputManager.KeyIsDown(Keys.E))
                RotateCameraRight();
        }
        private static void CheckForZoom()
        {
            if (InputManager.KeyIsDown(Keys.PageUp))
                ChangeZoom(1);
            else if (InputManager.KeyIsDown(Keys.PageDown))
                ChangeZoom(-1);
        }

        // Update
        private static void UpdateRotation()
        {
            if (rotationAngle == targetRotationAngle)
                return;

            eDiagonalCompassDirection oldDirection = GetApproxViewDirection();
            rotationAnimation.Update();
            Vector2 target = GetCameraTargetPosition();
            float lerpFactor = rotationAnimation.GetOutput();
            rotationAngle = Mathf.Lerp(startRotationAngle, targetRotationAngle, lerpFactor);
            SetCameraTargetPosition(target);
            eDiagonalCompassDirection newDirection = GetApproxViewDirection();

            if (oldDirection != newDirection)
                OnCameraRotate();
        }
        private static void UpdateZoom()
        {
            if (orthHeight == targetOrthHeight && perspFov == targetPerspFov)
                return;

            zoomAnimation.Update();
            float lerpFactor = zoomAnimation.GetOutput();
            orthHeight = Mathf.Lerp(startOrthHeight, targetOrthHeight, lerpFactor);
            perspFov = Mathf.Lerp(startPerspFov, targetPerspFov, lerpFactor);
        }

        private static void MoveCamera(float movementRotation)
        {
            movementRotation += rotationAngle;
            Vector3 movement = new Vector3(0f, 0f, -MOVE_SPEED * GameTimeManager.ElapsedGameTime);
            movement = VectorUtilities.RotateCompassRotation(movement, movementRotation);
            position += movement;

            LimitCameraPosition();
        }

        private static void LimitCameraPosition()
        {
            Vector2 target = GetCameraTargetPosition();
            target = LimitCameraTarget(target);
            SetCameraTargetPosition(target);
        }
        private static Vector2 LimitCameraTarget(Vector2 target)
        {
            target.X = Mathf.Clamp(target.X, -TARGET_LIMIT_BORDER, (float)FloorManager.FloorWidth + TARGET_LIMIT_BORDER);
            target.Y = Mathf.Clamp(target.Y, -TARGET_LIMIT_BORDER, (float)FloorManager.FloorHeight + TARGET_LIMIT_BORDER);

            return target;
        }

        private static void OnCameraRotate()
        {
            WallCutAwayManager.UpdatePillarHeights();
        }

        private static void SetCameraMatrices()
        {
            // View
            ViewMatrix = Matrix.CreateLookAt(position, position + GetCameraLookVector(), GetCameraUpVector());

            // Perspective
            if (type == eCameraType.ORTHOGRAPHIC)
            {
                ProjectionMatrix = Matrix.CreateOrthographic(GraphicsManager.AspectRatio * orthHeight, orthHeight, NEAR_CLIPPING_PLANE, FAR_CLIPPING_PLANE);
            }
            else if (type == eCameraType.PERSPECTIVE)
            {
                ProjectionMatrix = Matrix.CreatePerspectiveFieldOfView(Mathf.ToRads(perspFov), GraphicsManager.AspectRatio, NEAR_CLIPPING_PLANE, FAR_CLIPPING_PLANE);
            }
        }

        private static Vector3 GetCameraLookVector()
        {
            Vector3 look = new Vector3(0f, 0f, -1f);
            look = VectorUtilities.RotateVectorAboutY(look, -rotationAngle);
            Vector3 left = VectorUtilities.RotateVectorAboutY(look, 90f);
            look = VectorUtilities.RotateVectorAboutAxis(look, left, -liftAngle);

            return look;
        }

        private static Vector3 GetCameraUpVector()
        {
            Vector3 look = new Vector3(0f, 0f, -1f);
            look = VectorUtilities.RotateVectorAboutY(look, -rotationAngle);
            Vector3 left = VectorUtilities.RotateVectorAboutY(look, 90f);
            Vector3 up = VectorUtilities.RotateVectorAboutAxis(look, left, -(liftAngle + 90f));

            return up;
        }

        private static void RotateCameraTarget(eSquareRotation rotation, eRotationDirection direction)
        {
            eDiagonalCompassDirection viewDirection = GetApproxViewDirection();
            eDiagonalCompassDirection newDirection = CompassUtilities.RotateDiagonalCompassDirection(viewDirection, rotation, direction);
            targetRotationAngle = CompassUtilities.ConvertDirectionToCompassRotation(newDirection);

            CheckForAngleShift();
            startRotationAngle = rotationAngle;

            rotationAnimation.Reset();
        }

        private static void CheckForAngleShift()
        {
            if (Mathf.Abs(targetRotationAngle - rotationAngle) <= 180f)
                return;

            if (rotationAngle < targetRotationAngle)
                rotationAngle += 360f;
            else
                rotationAngle -= 360f;
        }

        private static void SetLiftAngleToIso()
        {
            liftAngle = -35.26f;
        }

        private static Vector2 GetCameraTargetPosition()
        {
            Vector3 look = GetCameraLookVector();

            // Look perpendicular with plane
            if (look.Y == 0)
                return new Vector2(0f);

            Vector3 target = position - (position.Y / look.Y) * look;
            Vector2 tsTarget = SpaceUtilities.ConvertGsToTs(target);

            return tsTarget;
        }
        private static void SetCameraTargetPosition(Vector2 tsPos)
        {
            Vector3 look = GetCameraLookVector();

            // Look perpendicular with plane
            if (look.Y == 0)
                return;

            float cameraHeight = position.Y;

            Vector3 newTarget = SpaceUtilities.ConvertTsToGs(tsPos);

            Vector3 newPosition = newTarget + (cameraHeight / look.Y) * look;

            position = newPosition;
        }
    }
}
