﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    static class CharacterAnimationManager
    {
        // -------- Properties --------
        // Character sizings
        public const float HIP_HEIGHT = 0.42f;
        public const float SHOULDER_HEIGHT = 0.79f;
        public const float NECK_HEIGHT = 0.84f;
        public const float ARM_LENGTH = 0.245f;

        // -------- Public Methods --------
        public static void Initialize()
        {
            CharacterAnimationStore.Initialize();
        }

        public static void Denitialize()
        {
            CharacterAnimationStore.Denitialize();
        }

        public static Matrix GetCharacterMeshTransform(string meshName, CharacterPosition position)
        {
            switch (meshName)
            {
                case "Head":
                    return GetCharacterMeshTransform_Head(position);
                case "HeadPiece":
                    return GetCharacterMeshTransform_Head(position);
                case "Body":
                    return GetCharacterMeshTransform_Body(position);
                case "LeftArm":
                    return GetCharacterMeshTransform_LeftArm(position);
                case "LeftHand":
                    return GetCharacterMeshTransform_LeftHand(position);
                case "RightArm":
                    return GetCharacterMeshTransform_RightArm(position);
                case "RightHand":
                    return GetCharacterMeshTransform_RightHand(position);
                case "LeftLeg":
                    return GetCharacterMeshTransform_LeftLeg(position);
                case "LeftFoot":
                    return GetCharacterMeshTransform_LeftLeg(position);
                case "RightLeg":
                    return GetCharacterMeshTransform_RightLeg(position);
                case "RightFoot":
                    return GetCharacterMeshTransform_RightLeg(position);
                default:
                    return Matrix.Identity;
            }
        }

        // Situation-specific animation
        public static string GetStationaryCharacterAnimationId(Character character)
        {
            // TODO
            return "idle";
        }
        public static string GetMovingCharacterAnimationId(Character character)
        {
            // TODO
            Staff staff = character as Staff;
            if (staff != null && staff.IsCarryingElement())
                return "carry-walk";

            return "walk";
        }
        public static string GetIdlingCharacterAnimationId(Character character)
        {
            // TODO
            return "idle";
        }

        // -------- Private Methods ---------
        private static Matrix GetCharacterMeshTransform_Head(CharacterPosition position)
        {
            Matrix rotationMatrix = MatrixUtilities.CreateRotationY(-position.TurnHead);
            Matrix translationMatrix = MatrixUtilities.CreateTranslationMatrix(new Vector3(0.5f, NECK_HEIGHT, 0.5f));

            return rotationMatrix * translationMatrix * GetCharacterMeshTransform_Body(position);
        }
        private static Matrix GetCharacterMeshTransform_Body(CharacterPosition position)
        {
            Matrix preRotationTranslation = MatrixUtilities.CreateTranslationMatrix(new Vector3(0f, -HIP_HEIGHT, -0.5f));
            Matrix postRotationTranslation = MatrixUtilities.CreateTranslationMatrix(new Vector3(0f, HIP_HEIGHT, 0.5f));
            Matrix rotationMatrix = MatrixUtilities.CreateRotationX(-position.BendOver);

            return preRotationTranslation * rotationMatrix * postRotationTranslation;
        }
        private static Matrix GetCharacterMeshTransform_LeftArm(CharacterPosition position)
        {
            Matrix armRotation = GetRelativeMeshTransform_Arm(position.LeftShoulder);

            return armRotation * GetCharacterMeshTransform_Body(position);
        }
        private static Matrix GetCharacterMeshTransform_LeftHand(CharacterPosition position)
        {
            Matrix handRotation = GetRelativeMeshTransform_Hand(position.LeftWrist);

            return handRotation * GetCharacterMeshTransform_LeftArm(position);
        }
        private static Matrix GetCharacterMeshTransform_RightArm(CharacterPosition position)
        {
            Matrix armRotation = GetRelativeMeshTransform_Arm(position.RightShoulder);

            return armRotation * GetCharacterMeshTransform_Body(position);
        }
        private static Matrix GetCharacterMeshTransform_RightHand(CharacterPosition position)
        {
            Matrix handRotation = GetRelativeMeshTransform_Hand(position.RightWrist);

            return handRotation * GetCharacterMeshTransform_RightArm(position);
        }
        private static Matrix GetCharacterMeshTransform_LeftLeg(CharacterPosition position)
        {
            return GetRelativeMeshTransform_Leg(position.LeftKick);
        }
        private static Matrix GetCharacterMeshTransform_RightLeg(CharacterPosition position)
        {
            return GetRelativeMeshTransform_Leg(position.RightKick);
        }
        private static Matrix GetRelativeMeshTransform_Arm(float shoulderAngle)
        {
            Matrix preRotationTranslation = MatrixUtilities.CreateTranslationMatrix(new Vector3(0f, -SHOULDER_HEIGHT, -0.5f));
            Matrix postRotationTranslation = MatrixUtilities.CreateTranslationMatrix(new Vector3(0f, SHOULDER_HEIGHT, 0.5f));
            Matrix rotationMatrix = MatrixUtilities.CreateRotationX(shoulderAngle);

            return preRotationTranslation * rotationMatrix * postRotationTranslation;
        }
        private static Matrix GetRelativeMeshTransform_Hand(float wristAngle)
        {
            Matrix preRotationTranslation = MatrixUtilities.CreateTranslationMatrix(new Vector3(0f, -(SHOULDER_HEIGHT - ARM_LENGTH), -0.5f));
            Matrix postRotationTranslation = MatrixUtilities.CreateTranslationMatrix(new Vector3(0f, SHOULDER_HEIGHT - ARM_LENGTH, 0.5f));
            Matrix rotationMatrix = MatrixUtilities.CreateRotationX(wristAngle);

            return preRotationTranslation * rotationMatrix * postRotationTranslation;
        }
        private static Matrix GetRelativeMeshTransform_Leg(float kickAngle)
        {
            Matrix preRotationTranslation = MatrixUtilities.CreateTranslationMatrix(new Vector3(0f, -HIP_HEIGHT, -0.5f));
            Matrix postRotationTranslation = MatrixUtilities.CreateTranslationMatrix(new Vector3(0f, HIP_HEIGHT, 0.5f));
            Matrix rotationMatrix = MatrixUtilities.CreateRotationX(kickAngle);

            return preRotationTranslation * rotationMatrix * postRotationTranslation;
        }
    }
}
