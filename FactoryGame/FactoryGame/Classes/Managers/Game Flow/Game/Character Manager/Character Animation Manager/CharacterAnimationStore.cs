﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    static class CharacterAnimationStore
    {
        // -------- Private Fields --------
        private static Dictionary<string, CharacterAnimation> animationStore;
        // Constant params
        private const float CARRY_SHOULDER_ANGLE = 80f;

        // -------- Public Methods --------
        public static void Initialize()
        {
            animationStore = new Dictionary<string, CharacterAnimation>();
            SetUpAnimations();
        }

        public static void Denitialize()
        {
            animationStore = null;
        }

        public static CharacterAnimation GetAnimation(string animationId)
        {
            if (!animationStore.ContainsKey(animationId))
                return animationStore["idle"].Clone();

            return animationStore[animationId].Clone();
        }

        // -------- Private Methods ---------
        private static void SetUpAnimations()
        {
            animationStore.Add("idle", CreateAnimation_Idle());
            animationStore.Add("walk", CreateAnimation_Walk());
            animationStore.Add("carry-walk", CreateAnimation_CarryWalk());
            animationStore.Add("sit", CreateAnimation_Sit());
            animationStore.Add("clean", CreateAnimation_Clean());
            // Add new animations here

            CheckForIdleAnimation();
        }
        private static void CheckForIdleAnimation()
        {
            if (!animationStore.ContainsKey("idle"))
                Debug.LogError("Animation Store is set up but does not contain the 'idle' animation");
        }

        // Separate animations
        private static CharacterAnimation CreateAnimation_Idle()
        {
            var ca = new CharacterAnimation();

            var cp0 = new CharacterPosition();
            cp0.BendOver = 0f;
            cp0.TurnHead = 0f;
            cp0.LeftShoulder = 0f;
            cp0.LeftWrist = 0f;
            cp0.RightShoulder = 0f;
            cp0.RightWrist = 0f;
            cp0.LeftKick = 0f;
            cp0.RightKick = 0f;

            ca.AddPosition(0f, cp0);

            return ca;
        }
        private static CharacterAnimation CreateAnimation_Walk()
        {
            float legLength = CharacterAnimationManager.HIP_HEIGHT;
            float stepLegAngle = 25f;
            float stepArmAngle = 15f;
            float stepTime = stepLegAngle / Mathf.ToDeg(CharacterManager.DefaultWalkSpeed / legLength);

            var ca = new CharacterAnimation();

            var cp0 = new CharacterPosition();
            cp0.BendOver = 0f;
            cp0.TurnHead = 0f;
            cp0.LeftShoulder = 0f;
            cp0.LeftWrist = 0f;
            cp0.RightShoulder = 0f;
            cp0.RightWrist = 0f;
            cp0.LeftKick = 0f;
            cp0.RightKick = 0f;

            ca.AddPosition(0f, cp0);

            ca.AddPosition_RightKick(stepTime, stepLegAngle);
            ca.AddPosition_LeftKick(stepTime, -stepLegAngle);
            ca.AddPosition_RightShoulder(stepTime, -stepArmAngle);
            ca.AddPosition_LeftShoulder(stepTime, stepArmAngle);

            ca.AddPosition_RightKick(2 * stepTime, 0);
            ca.AddPosition_LeftKick(2 * stepTime, 0);
            ca.AddPosition_RightShoulder(2 * stepTime, 0);
            ca.AddPosition_LeftShoulder(2 * stepTime, 0);

            ca.AddPosition_RightKick(3 * stepTime, -stepLegAngle);
            ca.AddPosition_LeftKick(3 * stepTime, stepLegAngle);
            ca.AddPosition_RightShoulder(3 * stepTime, stepArmAngle);
            ca.AddPosition_LeftShoulder(3 * stepTime, -stepArmAngle);

            ca.AddPosition_RightKick(4 * stepTime, 0);
            ca.AddPosition_LeftKick(4 * stepTime, 0);
            ca.AddPosition_RightShoulder(4 * stepTime, 0);
            ca.AddPosition_LeftShoulder(4 * stepTime, 0);

            return ca;
        }
        private static CharacterAnimation CreateAnimation_CarryWalk()
        {
            CharacterAnimation anim = CreateAnimation_Walk();
            foreach (CharacterPosition pos in anim.Positions)
            {
                pos.LeftShoulder = CARRY_SHOULDER_ANGLE;
                pos.RightShoulder = CARRY_SHOULDER_ANGLE;
                pos.LeftWrist = 90f - CARRY_SHOULDER_ANGLE;
                pos.RightWrist = 90f - CARRY_SHOULDER_ANGLE;
            }

            return anim;
        }
        private static CharacterAnimation CreateAnimation_Sit()
        {
            var ca = new CharacterAnimation();

            var cp0 = new CharacterPosition();
            cp0.BendOver = 0f;
            cp0.TurnHead = 0f;
            cp0.LeftShoulder = 30f;
            cp0.LeftWrist = 0f;
            cp0.RightShoulder = 30f;
            cp0.RightWrist = 0f;
            cp0.LeftKick = 90f;
            cp0.RightKick = 90f;

            ca.AddPosition(0f, cp0);

            ca.AddPosition_RightShoulder(1f, 10f);
            ca.AddPosition_RightShoulder(2f, 30f);

            ca.AddPosition_TurnHead(3f, 0f);
            ca.AddPosition_TurnHead(3.5f, 20f);
            ca.AddPosition_TurnHead(4.5f, -20f);
            ca.AddPosition_TurnHead(5f, 0f);

            return ca;
        }
        private static CharacterAnimation CreateAnimation_Clean()
        {
            var ca = new CharacterAnimation();

            var cp0 = new CharacterPosition();
            cp0.BendOver = 90f;
            cp0.TurnHead = 0f;
            cp0.LeftShoulder = 90f;
            cp0.LeftWrist = 0f;
            cp0.RightShoulder = 90f;
            cp0.RightWrist = 0f;
            cp0.LeftKick = 0f;
            cp0.RightKick = 0f;

            ca.AddPosition(0f, cp0);

            return ca;
        }
        // Create animations in methods here
    }
}
