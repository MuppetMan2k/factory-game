﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    static class CharacterManager
    {
        // -------- Properties --------
        public static float DefaultWalkSpeed { get { return 1f; } }

        // -------- Private Fields --------
        private static List<Character> characterList;
        private static List<Character> characterRemovalList;

        // -------- Public Methods --------
        public static void Initialize()
        {
            characterList = new List<Character>();
            characterRemovalList = new List<Character>();

            CharacterAnimationManager.Initialize();
            StaffManager.Initialize();
        }

        public static void Denitialize()
        {
            characterList = null;
            characterRemovalList = null;

            CharacterAnimationManager.Denitialize();
            StaffManager.Denitialize();
        }

        public static void Update()
        {
            foreach (Character c in characterList)
                c.Update();

            StaffManager.Update();

            UpdateRemovalList();
        }

        public static void Draw()
        {
            foreach (Character c in characterList)
                c.Draw();
        }

        public static List<Character> GetAllCharacters()
        {
            return characterList;
        }

        public static void AddCharacter(Character characterToAdd)
        {
            characterList.Add(characterToAdd);
        }

        public static void RemoveCharacter(Character characterToRemove)
        {
            characterRemovalList.Add(characterToRemove);
        }

        public static Task GetCharacterIdleTask()
        {
            return new IdleTask();
        }

        public static void OnNewDay()
        {
            StaffManager.OnNewDay();
        }

        public static void OnStaffArriveForWork(Staff staff)
        {
            AddCharacter(staff);
            GameManager.OnStaffEnterFactory(staff);

            staff.CharacterCoordinate.SetStationaryCoordinate(FloorManager.FactoryMapEdgeTile, CompassUtilities.ConvertSquareDirectionToRotation(FloorManager.FactoryEntryDirection));

            EnterFactoryTask enterTask = new EnterFactoryTask();
            eTaskPriority priority;
            staff.GiveInstructions(enterTask.GetInstructionSetForTask(staff, out priority));
        }

        // -------- Private Methods ---------
        private static void UpdateRemovalList()
        {
            if (characterRemovalList.Count == 0)
                return;

            foreach (Character c in characterRemovalList)
            {
                if (ShouldDenitializeCharacter(c))
                    c.Denitialize();

                if (!characterList.Remove(c))
                    Debug.LogWarning("Tried to remove character but not in character list: " + c.ToString());
            }

            characterRemovalList.Clear();
        }
        private static bool ShouldDenitializeCharacter(Character character)
        {
            // TODO - Change this if characters are added that should be denitialized when removed from character list
            return false;
        }
    }
}
