﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    static class CharacterStyleManager
    {
        // -------- Public Methods --------
        public static CharacterStyle CreateCharacterStyle(eStaffType staffType)
        {
            string bodyModel = "";

            // Body according to staff type
            switch(staffType)
            {
                case eStaffType.CLEANER:
                    bodyModel = "cleaner-body";
                    break;
                case eStaffType.LOADER:
                    bodyModel = "loader-body";
                    break;
                case eStaffType.OPERATOR:
                    bodyModel = "operator-body";
                    break;
                case eStaffType.TECHNICIAN:
                    bodyModel = "technician-body";
                    break;
                    // TODO
            }

            string headModel = GetCharacterHeadModelId();
            string headPieceModel = GetCharacterHeadPieceModelId(staffType);

            return new CharacterStyle(bodyModel, headModel, headPieceModel);
        }

        // -------- Private Methods ---------
        private static string GetCharacterHeadModelId()
        {
            // TODO
            return "test-head";
        }

        private static string GetCharacterHeadPieceModelId(eStaffType staffType)
        {
            // TODO
            return "";// "test-head-piece";
        }
    }
}
