﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    static class InstructionManager
    {
        // -------- Private Fields --------
        // Parameters
        private const float MIN_IDLE_TIME = 5f;
        private const float MAX_IDLE_TIME = 10f;

        // -------- Public Methods --------
        public static InstructionSet CreateInstructionsFromPath(Path path, Task task, eInstructionSetType type)
        {
            var instructions = new InstructionSet();

            instructions.PathTiles.Clear();
            instructions.PathTiles.AddRange(path.Tiles);
            instructions.Type = type;

            for (int i = 0; i < path.Tiles.Count - 1; i++)
            {
                TileCoordinate thisTile = path.Tiles[i];
                TileCoordinate nextTile = path.Tiles[i + 1];

                eSquareCompassDirection direction = CompassUtilities.GetSquareDirectionBetweenTiles(thisTile, nextTile);
                instructions.Instructions.Add(new MoveInstruction(task, direction));
            }

            return instructions;
        }

        public static float GetIdleInstructionTime()
        {
            return RandomManager.RandomNumber(MIN_IDLE_TIME, MAX_IDLE_TIME);
        }
    }
}
