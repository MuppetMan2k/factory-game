﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    // -------- Enumerations --------
    public enum eStaffType
    {
        LOADER,
        OPERATOR,
        CLEANER,
        TECHNICIAN,
        // TODO - add further staff types here
    }

    static class StaffManager
    {
        // -------- Properties --------
        public static int DefaultStartTime { get { return 8; } }
        public static int DefaultFinishTime { get { return 17; } }
        public static int EarliestStartTime { get { return 6; } }
        public static int LatestFinishTime { get { return 18; } }

        // -------- Private Fields --------
        private static List<Staff> staffList;

        // -------- Public Methods --------
        public static void Initialize()
        {
            staffList = new List<Staff>();

            CreateDebugStaff();
        }

        public static void Denitialize()
        {
            staffList = null;
        }

        public static void Update()
        {
            CheckForStaffArrival();
        }

        public static List<Staff> GetAllStaff()
        {
            return staffList;
        }

        public static void AddStaff(Staff staffToAdd)
        {
            staffList.Add(staffToAdd);
        }

        public static void RemoveStaff(Staff staffToRemove)
        {
            staffList.Remove(staffToRemove);
        }

        public static string GetStaffTypeLocId(eStaffType staffType)
        {
            switch (staffType)
            {
                case eStaffType.CLEANER:
                    return "text_staff_cleaner";
                case eStaffType.OPERATOR:
                    return "text_staff_operator";
                case eStaffType.TECHNICIAN:
                    return "text_staff_technician";
                case eStaffType.LOADER:
                    return "text_staff_loader";
            }

            return "";
        }

        public static int GetStaffHourlyPayRate(eStaffType staffType)
        {
            switch (staffType)
            {
                case eStaffType.CLEANER:
                    return BalanceValuesManager.GetHourlyPayRate_Cleaner();
                case eStaffType.LOADER:
                    return BalanceValuesManager.GetHourlyPayRate_Loader();
                case eStaffType.OPERATOR:
                    return BalanceValuesManager.GetHourlyPayRate_Operator();
                case eStaffType.TECHNICIAN:
                    return BalanceValuesManager.GetHourlyPayRate_Technician();
                default:
                    return 0;
            }
        }

        public static bool StaffCanLoadElements(eStaffType staffType)
        {
            return (GetStaffActiveTaskTypes(staffType).Contains(eTaskType.LOAD));
        }

        public static List<eTaskType> GetStaffActiveTaskTypes(eStaffType staffType)
        {
            switch (staffType)
            {
                case eStaffType.CLEANER:
                    return new List<eTaskType>();
                case eStaffType.LOADER:
                    return new List<eTaskType>() { eTaskType.LOAD };
                case eStaffType.OPERATOR:
                    return new List<eTaskType>() { eTaskType.LOAD, eTaskType.OPERATE };
                case eStaffType.TECHNICIAN:
                    return new List<eTaskType>();
                default:
                    return new List<eTaskType>();
            }
        }

        public static void OnNewDay()
        {
            foreach (Staff s in staffList)
                s.OnNewDay();
        }

        public static float GetStaffStartTimeToday(Staff staff, int baseStartTime)
        {
            float earlyArrivalWindow = ModifierManager.ModifyStaffEarlyArrivalWindow(staff, GameBalanceManager.StaffEarlyArrivalWindow);
            float laterArrivalWindow = ModifierManager.ModifyStaffLateArrivalWindow(staff, GameBalanceManager.StaffLateArrivalWindow);

            float startTime = RandomManager.RandomNumber((float)baseStartTime - earlyArrivalWindow, (float)baseStartTime + laterArrivalWindow);
            return startTime;
        }

        // -------- Private Methods --------
        private static void CheckForStaffArrival()
        {
            foreach (Staff s in staffList)
                s.CheckForArrival();
        }

        // DEBUG
        private static void CreateDebugStaff()
        {
            var c3 = new Staff(eStaffType.OPERATOR, new CharacterName("Bob3", "Test3"), CharacterStyleManager.CreateCharacterStyle(eStaffType.OPERATOR));
            c3.SetCoordinate(new CharacterCoordinate(c3, new TileCoordinate(9, 4), eSquareRotation.ROTATION_0));
            c3.Initialize();

            OperateTask t1 = new OperateTask();
            t1.OperateInteractability = (FactoryObjectManager.GetAllMachines())[0].Interactability.GetInteractableElement_Operate();
            t1.OperationProcess = AlmanacManager.GetProcess("test-process");
            c3.Tasks.Tasks.Add(t1);

            LoadTask t2 = new LoadTask();
            t2.PickUpInteractability = (FactoryObjectManager.GetAllMachines())[0].Interactability.GetInteractableElement_PickUp();
            t2.PutDownInteractability = FactoryPortManager.GetFactoryOutPort().Interactability.GetInteractableElement_PutDown();
            c3.Tasks.Tasks.Add(t2);

            LoadTask t3 = new LoadTask();
            t3.PickUpInteractability = FactoryPortManager.GetFactoryInPort().Interactability.GetInteractableElement_PickUp();
            t3.PutDownInteractability = (FactoryObjectManager.GetAllMachines())[0].Interactability.GetInteractableElement_PutDown();
            c3.Tasks.Tasks.Add(t3);

            AddStaff(c3);
        }
    }
}
