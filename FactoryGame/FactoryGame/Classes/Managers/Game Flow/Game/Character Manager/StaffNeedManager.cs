﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    static class StaffNeedManager
    {
        // -------- Private Fields --------
        // Parameters
        private const float NEED_LEVEL_FOR_ZERO_PRIORITY = 0.9f;
        private const float NEED_LEVEL_FOR_LOW_PRIORITY = 0.75f;
        private const float NEED_LEVEL_FOR_MEDIUM_PRIORITY = 0.5f;
        private const float NEED_LEVEL_FOR_HIGH_PRIORITY = 0.2f;

        // -------- Public Methods --------
        public static eTaskPriority GetMaxPriorityForBreak(StaffNeeds needs, StaffNeedChange needsChange)
        {
            eTaskPriority hungerPriority = GetBreakNeedPriority(needs.GetHunger(), needsChange.HungerFactor);
            eTaskPriority bladderPriority = GetBreakNeedPriority(needs.GetBladder(), needsChange.BladderFactor);
            eTaskPriority energyPriority = GetBreakNeedPriority(needs.GetEnergy(), needsChange.EnergyFactor);

            return TaskManager.MaxPriority(TaskManager.MaxPriority(hungerPriority, bladderPriority), energyPriority);
        }

        public static int ConvertChangeFactorToInt(eChangeLevel changeFactor)
        {
            switch (changeFactor)
            {
                case eChangeLevel.PLUS_3:
                    return 3;
                case eChangeLevel.PLUS_2:
                    return 2;
                case eChangeLevel.PLUS_1:
                    return 1;
                case eChangeLevel.ZERO:
                    return 0;
                case eChangeLevel.MINUS_3:
                    return -3;
                case eChangeLevel.MINUS_2:
                    return -2;
                case eChangeLevel.MINUS_1:
                    return -1;
                default:
                    return 0;
            }
        }

        public static eNeedLevel ConvertNeedToNeedLevel(float need)
        {
            if (need <= 0.2f)
                return eNeedLevel.LEVEL_1;
            if (need <= 0.4f)
                return eNeedLevel.LEVEL_2;
            if (need <= 0.6f)
                return eNeedLevel.LEVEL_3;
            if (need <= 0.8f)
                return eNeedLevel.LEVEL_4;

            return eNeedLevel.LEVEL_5;
        }

        public static string GetHappinessTextureId(eNeedLevel happinessNeedLevel)
        {
            switch (happinessNeedLevel)
            {
                case eNeedLevel.LEVEL_1:
                    return "happiness-level-1";
                case eNeedLevel.LEVEL_2:
                    return "happiness-level-2";
                case eNeedLevel.LEVEL_3:
                    return "happiness-level-3";
                case eNeedLevel.LEVEL_4:
                    return "happiness-level-4";
                case eNeedLevel.LEVEL_5:
                    return "happiness-level-5";
            }

            return "";
        }

        public static Color GetNeedLevelUIColor(eNeedLevel needLevel)
        {
            switch (needLevel)
            {
                case eNeedLevel.LEVEL_1:
                    return UIStyleManager.Style.NeedLevel1Color;
                case eNeedLevel.LEVEL_2:
                    return UIStyleManager.Style.NeedLevel2Color;
                case eNeedLevel.LEVEL_3:
                    return UIStyleManager.Style.NeedLevel3Color;
                case eNeedLevel.LEVEL_4:
                    return UIStyleManager.Style.NeedLevel4Color;
                case eNeedLevel.LEVEL_5:
                    return UIStyleManager.Style.NeedLevel5Color;
            }

            return Color.White;
        }

        // -------- Private Methods ---------
        public static eTaskPriority GetBreakNeedPriority(float needLevel, eChangeLevel needChange)
        {
            if (ConvertChangeFactorToInt(needChange) <= 0)
                return eTaskPriority.ZERO_PRIORITY;

            if (needLevel >= NEED_LEVEL_FOR_ZERO_PRIORITY)
                return eTaskPriority.ZERO_PRIORITY;

            if (needLevel <= NEED_LEVEL_FOR_HIGH_PRIORITY)
                return eTaskPriority.HIGH_PRIORITY_BREAK;

            if (needLevel <= NEED_LEVEL_FOR_MEDIUM_PRIORITY)
                return eTaskPriority.MEDIUM_PRIORITY_BREAK;

            return eTaskPriority.LOW_PRIORITY_BREAK;
        }
    }
}
