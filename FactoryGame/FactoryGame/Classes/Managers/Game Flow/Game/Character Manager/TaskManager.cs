﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    // -------- Enumerations --------
    public enum eTaskPriority
    {
        ZERO_PRIORITY,
        LOW_PRIORITY_BREAK,
        LOW_PRIORITY_JOB,
        MEDIUM_PRIORITY_BREAK,
        MEDIUM_PRIORITY_JOB,
        HIGH_PRIORITY_BREAK,
        HIGH_PRIORITY_JOB,
        MAX_PRIORITY_SYSTEM
    }

    static class TaskManager
    {
        // -------- Public Methods --------
        public static TaskSet GetDefaultTaskSet(Staff staff)
        {
            switch (staff.Type)
            {
                case eStaffType.LOADER:
                    return GetDefaultTaskSet_Loader(staff);
                case eStaffType.OPERATOR:
                    return GetDefaultTaskSet_Operator(staff);
                case eStaffType.CLEANER:
                    return GetDefaultTaskSet_Cleaner(staff);
                case eStaffType.TECHNICIAN:
                    return GetDefaultTaskSet_Technician(staff);
                default:
                    Debug.LogError("Tried to get default task set for unknown staff type.");
                    return null;
            }
        }

        public static InstructionSet GetInstructionSetForTasks(Staff staff, out Task chosenTask)
        {
            List<Task> tasks = staff.Tasks.Tasks;
            List<InstructionSet> instructionSets = new List<InstructionSet>();
            List<eTaskPriority> priorities = new List<eTaskPriority>();

            foreach (Task t in tasks)
            {
                eTaskPriority priority;
                InstructionSet instructionSet = t.GetInstructionSetForTask(staff, out priority);

                instructionSets.Add(instructionSet);
                priorities.Add(priority);
            }

            int chosenIndex;
            InstructionSet chosenInstructions = ChooseTaskToPerform(instructionSets, priorities, out chosenIndex);
            if (chosenIndex == -1)
                chosenTask = new IdleTask();
            else
                chosenTask = tasks[chosenIndex];
            return chosenInstructions;
        }

        public static bool TaskShouldBePrioritizedOverCurrent(eTaskPriority currentPriority, eTaskPriority newPriority)
        {
            return ((int)newPriority > (int)currentPriority);
        }

        public static eTaskPriority MaxPriority(eTaskPriority priority1, eTaskPriority priority2)
        {
            return (eTaskPriority)Mathf.Max((int)priority1, (int)priority2);
        }

        public static string GetTextureIdForTask(Task task, Staff staff)
        {
            if (task is CleanTask)
                return "staff-task-clean";
            if (task is FixTask)
                return "staff-task-fix";
            if (task is IdleTask)
                return "staff-task-idle";
            if (task is LoadTask)
                return "staff-task-load";
            if (task is OperateTask)
                return "staff-task-operate";
            if (task is TakeBreakTask)
                return "staff-task-take-break";

            return "";
        }
        public static string GetTextureIdForTaskType(eTaskType taskType)
        {
            switch (taskType)
            {
                case eTaskType.CLEAN:
                    return "staff-task-clean";
                case eTaskType.FIX:
                    return "staff-task-fix";
                case eTaskType.IDLE:
                    return "staff-task-idle";
                case eTaskType.LOAD:
                    return "staff-task-load";
                case eTaskType.OPERATE:
                    return "staff-task-operate";
                case eTaskType.TAKE_BREAK:
                    return "staff-task-take-break";
                default:
                    return "";
            }
        }

        public static string GetLocIdForTaskType(eTaskType taskType)
        {
            switch (taskType)
            {
                case eTaskType.CLEAN:
                    return "text_tasks_clean";
                case eTaskType.FIX:
                    return "text_tasks_fix";
                case eTaskType.IDLE:
                    return "text_tasks_idle";
                case eTaskType.LOAD:
                    return "text_tasks_load";
                case eTaskType.OPERATE:
                    return "text_tasks_operate";
                case eTaskType.TAKE_BREAK:
                    return "text_tasks_take_break";
                default:
                    return "";
            }
        }

        public static string GetTaskDescription(Task task)
        {
            switch (task.Type)
            {
                case eTaskType.CLEAN:
                    return Localization.LocalizeString("text_task_description_clean");
                case eTaskType.FIX:
                    return Localization.LocalizeString("text_task_description_fix");
                case eTaskType.IDLE:
                    return Localization.LocalizeString("text_task_description_idle");
                case eTaskType.LOAD:
                    {
                        LoadTask loadTask = task as LoadTask;
                        string locString = Localization.LocalizeString("text_task_description_load");
                        string machine1NameLocId = GameEntityUtilities.GetGameEntityNameLocId(loadTask.PickUpInteractability.Parent);
                        string machine2NameLocId = GameEntityUtilities.GetGameEntityNameLocId(loadTask.PutDownInteractability.Parent);
                        string machine1Name = Localization.LocalizeString(machine1NameLocId);
                        string machine2Name = Localization.LocalizeString(machine2NameLocId);
                        return string.Format(locString, machine1Name, machine2Name);
                    }
                case eTaskType.OPERATE:
                    {
                        OperateTask opTask = task as OperateTask;
                        string locString = Localization.LocalizeString("text_task_description_operate");
                        string elementNameLocId = opTask.OperationProcess.ElementOut.NameLocId;
                        string elementName = Localization.LocalizeString(elementNameLocId);
                        string machineNameLocId = GameEntityUtilities.GetGameEntityNameLocId(opTask.OperateInteractability.Parent);
                        string machineName = Localization.LocalizeString(machineNameLocId);
                        return string.Format(locString, elementName, machineName);
                    }
                case eTaskType.TAKE_BREAK:
                    return Localization.LocalizeString("text_task_description_take_break");
                default:
                    return "";
            }
        }

        public static string GetCurrentTaskDescription(Task task)
        {
            switch (task.Type)
            {
                case eTaskType.CLEAN:
                    return Localization.LocalizeString("text_task_current_clean");
                case eTaskType.FIX:
                    return Localization.LocalizeString("text_task_current_fix");
                case eTaskType.IDLE:
                    return Localization.LocalizeString("text_task_current_idle");
                case eTaskType.LOAD:
                    {
                        LoadTask loadTask = task as LoadTask;
                        string locString = Localization.LocalizeString("text_task_current_load");
                        string machine1NameLocId = GameEntityUtilities.GetGameEntityNameLocId(loadTask.PickUpInteractability.Parent);
                        string machine2NameLocId = GameEntityUtilities.GetGameEntityNameLocId(loadTask.PutDownInteractability.Parent);
                        string machine1Name = Localization.LocalizeString(machine1NameLocId);
                        string machine2Name = Localization.LocalizeString(machine2NameLocId);
                        return string.Format(locString, machine1Name, machine2Name);
                    }
                case eTaskType.OPERATE:
                    {
                        OperateTask opTask = task as OperateTask;
                        string locString = Localization.LocalizeString("text_task_current_operate");
                        string elementNameLocId = opTask.OperationProcess.ElementOut.NameLocId;
                        string elementName = Localization.LocalizeString(elementNameLocId);
                        string machineNameLocId = GameEntityUtilities.GetGameEntityNameLocId(opTask.OperateInteractability.Parent);
                        string machineName = Localization.LocalizeString(machineNameLocId);
                        return string.Format(locString, elementName, machineName);
                    }
                case eTaskType.TAKE_BREAK:
                    return Localization.LocalizeString("text_task_current_take_break");
                default:
                    return "";
            }
        }

        // -------- Private Methods ---------
        // Default staff task set methods
        private static TaskSet GetDefaultTaskSet_Loader(Staff staff)
        {
            TaskSet taskSet = new TaskSet(staff);
            taskSet.Tasks.Add(new TakeBreakTask());
            return taskSet;
        }
        private static TaskSet GetDefaultTaskSet_Operator(Staff staff)
        {
            TaskSet taskSet = new TaskSet(staff);
            taskSet.Tasks.Add(new TakeBreakTask());
            return taskSet;
        }
        private static TaskSet GetDefaultTaskSet_Cleaner(Staff staff)
        {
            TaskSet taskSet = new TaskSet(staff);
            taskSet.Tasks.Add(new CleanTask());
            taskSet.Tasks.Add(new TakeBreakTask());
            return taskSet;
        }
        private static TaskSet GetDefaultTaskSet_Technician(Staff staff)
        {
            TaskSet taskSet = new TaskSet(staff);
            taskSet.Tasks.Add(new FixTask());
            taskSet.Tasks.Add(new TakeBreakTask());
            return taskSet;
        }

        private static InstructionSet ChooseTaskToPerform(List<InstructionSet> instructionSets, List<eTaskPriority> priorities, out int chosenIndex)
        {
            chosenIndex = -1;

            // Form short lists
            List<InstructionSet> instructionShortList = new List<InstructionSet>();
            List<eTaskPriority> priorityShortList = new List<eTaskPriority>();
            for (int i = 0; i < instructionSets.Count; i++)
            {
                if (instructionSets[i] != null)
                {
                    instructionShortList.Add(instructionSets[i]);
                    priorityShortList.Add(priorities[i]);
                }
            }

            if (instructionShortList.Count == 0)
                return null;

            InstructionSet chosenInstructionSet = instructionShortList[0];
            eTaskPriority chosenPriority = priorityShortList[0];

            for (int j = 1; j < instructionShortList.Count; j++)
            {
                if (TaskShouldBePrioritizedOverCurrent(chosenPriority, priorityShortList[j]))
                {
                    chosenInstructionSet = instructionShortList[j];
                    chosenPriority = priorityShortList[j];
                }
            }

            for (int i = 0; i < instructionSets.Count; i++)
                if (instructionSets[i] != null)
                    if (instructionSets[i].Equals(chosenInstructionSet))
                        chosenIndex = i;

            return chosenInstructionSet;
        }

        // Task description methods
        private static string GetTaskDescription_Clean()
        {
            return Localization.LocalizeString("text_task_description_clean");
        }
        private static string GetTaskDescription_Fix()
        {
            return Localization.LocalizeString("text_task_description_fix");
        }
        private static string GetTaskDescription_Load(LoadTask task)
        {
            string pickUpEntityName = Localization.LocalizeString(GameEntityUtilities.GetGameEntityNameLocId(task.PickUpInteractability.Parent));
            string putDownEntityName = Localization.LocalizeString(GameEntityUtilities.GetGameEntityNameLocId(task.PutDownInteractability.Parent));

            string description = string.Format(Localization.LocalizeString("text_task_description_load"), pickUpEntityName, putDownEntityName);

            return description;
        }
        private static string GetTaskDescription_Operate(OperateTask task)
        {
            // TODO - add in different cases for e.g. QA stations

            string entityName = Localization.LocalizeString(GameEntityUtilities.GetGameEntityNameLocId(task.OperateInteractability.Parent));
            string elementName = Localization.LocalizeString(task.OperationProcess.ElementOut.NameLocId);

            string description = string.Format(Localization.LocalizeString("text_task_description_operate"), elementName, entityName);

            return description;
        }
    }
}
