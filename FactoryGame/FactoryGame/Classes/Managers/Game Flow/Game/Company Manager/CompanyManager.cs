﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    static class CompanyManager
    {
        // -------- Properties --------
        public static PlayerCompany PlayerCompany { get; set; }

        // -------- Public Methods --------
        public static void Initialize()
        {
            SetupDebugPlayerCompany();
        }

        public static void Denitialize()
        {
            PlayerCompany = null;
        }

        // -------- Private Methods --------
        private static void SetupDebugPlayerCompany()
        {
            PlayerCompany = new PlayerCompany();
            PlayerCompany.Name = "Test Company Ltd.";
            PlayerCompany.Skills = new SkillSet();
            PlayerCompany.Traits = new TraitSet();
        }
    }
}
