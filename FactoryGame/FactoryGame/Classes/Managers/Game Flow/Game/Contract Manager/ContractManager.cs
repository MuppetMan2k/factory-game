﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    static class ContractManager
    {
        // -------- Private Fields --------
        private static List<ImportContract> importContracts;
        private static List<ExportContract> exportContracts;

        // -------- Public Methods --------
        public static void Initialize()
        {
            importContracts = new List<ImportContract>();
            exportContracts = new List<ExportContract>();

            CreateDebugContracts();

            DeliveryManager.Initialize();

        }

        public static void Denitialize()
        {
            importContracts = null;
            exportContracts = null;

            DeliveryManager.Denitialize();
        }

        public static void Update()
        {
            DeliveryManager.Update();
        }

        public static void OnNewDay()
        {
            DeliveryManager.OnNewDay();
        }

        public static bool ElementIsExported(ElementData element)
        {
            foreach (ExportContract e in exportContracts)
                if (e.Element.Equals(element))
                    return true;

            return false;
        }

        public static int GetImportContractNumber()
        {
            return importContracts.Count;
        }
        public static int GetExportContractNumber()
        {
            return exportContracts.Count;
        }

        // -------- Private Methods --------
        private static void CreateDebugContracts()
        {
            ExportContract c = new ExportContract();
            c.Element = AlmanacManager.GetElement("test-element-2");
            exportContracts.Add(c);
        }
    }
}
