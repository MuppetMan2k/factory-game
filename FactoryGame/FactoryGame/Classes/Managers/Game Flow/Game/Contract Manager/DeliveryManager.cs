﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    // -------- Enumerations --------
    public enum eDeliveryState
    {
        IDLE,
        ARRIVING,
        DELIVERING,
        DEPARTING
    }

    static class DeliveryManager
    {
        // -------- Private Fields --------
        private static bool dailyInDeliveryMade;
        private static bool dailyOutDeliveryMade;
        private static eDeliveryState inDeliveryState;
        private static eDeliveryState outDeliveryState;
        private static float inDeliveryAnimationRatio;
        private static float outDeliveryAnimationRatio;
        private static float timeDeliveringIn;
        // Const params
        private const float IN_DELIVERY_TIME = 9.0f;
        private const float IN_DELIVERY_LEAVE_TIME = 18.0f;
        private const float OUT_DELIVERY_TIME = 18.0f;
        private const float OUT_DELIVERY_RETURN_TIME = 6.0f;
        private const float IN_DELIVERY_ANIMATION_TIME = 5.0f;
        private const float OUT_DELIVERY_ANIMATION_TIME = 5.0f;
        private const float MIN_TIME_DELIVERING_IN = 5.0f;
        private const float MIN_TIME_DELIVERING_OUT = 5.0f;

        // -------- Public Methods --------
        public static void Initialize()
        {
            dailyInDeliveryMade = false;
            dailyOutDeliveryMade = false;
            inDeliveryState = eDeliveryState.IDLE;
            outDeliveryState = eDeliveryState.IDLE;
            inDeliveryAnimationRatio = 0f;
            outDeliveryAnimationRatio = 0f;
            timeDeliveringIn = 0f;
        }

        public static void Denitialize()
        {
        }

        public static void Update()
        {
            UpdateDeliveryIn();
            UpdateDeliveryOut();
        }

        public static void OnNewDay()
        {
            dailyInDeliveryMade = false;
            dailyOutDeliveryMade = false;
        }

        // -------- Private Methods --------
        private static void UpdateDeliveryIn()
        {
            switch (inDeliveryState)
            {
                case eDeliveryState.IDLE:
                    if (GameClockManager.GetClockTime() > IN_DELIVERY_TIME && !dailyInDeliveryMade)
                    {
                        dailyInDeliveryMade = true;
                        inDeliveryState = eDeliveryState.ARRIVING;
                    }
                    break;
                case eDeliveryState.ARRIVING:
                    inDeliveryAnimationRatio += GameSpeedManager.ElapsedGameTime / IN_DELIVERY_ANIMATION_TIME;
                    if (inDeliveryAnimationRatio >= 1f)
                    {
                        inDeliveryAnimationRatio = 1f;
                        inDeliveryState = eDeliveryState.DELIVERING;
                        timeDeliveringIn = 0f;

                        MakeDeliveryIn();
                    }
                    break;
                case eDeliveryState.DEPARTING:
                    inDeliveryAnimationRatio -= GameSpeedManager.ElapsedGameTime / IN_DELIVERY_ANIMATION_TIME;
                    if (inDeliveryAnimationRatio <= 0f)
                    {
                        inDeliveryAnimationRatio = 0f;
                        inDeliveryState = eDeliveryState.IDLE;
                    }
                    break;
                case eDeliveryState.DELIVERING:
                    timeDeliveringIn += GameSpeedManager.ElapsedGameTime;

                    if ((timeDeliveringIn > MIN_TIME_DELIVERING_IN && InDeliveryIsFinished()) || GameClockManager.GetClockTime() > IN_DELIVERY_LEAVE_TIME)
                        inDeliveryState = eDeliveryState.DEPARTING;

                    break;
            }
        }
        private static bool InDeliveryIsFinished()
        {
            return (FactoryPortManager.GetFactoryInPort().ElementStore.NumberElements() == 0);
        }

        private static void UpdateDeliveryOut()
        {
            switch (outDeliveryState)
            {
                case eDeliveryState.IDLE:
                    if (GameClockManager.GetClockTime() > OUT_DELIVERY_TIME && CanMakeDeliveryOut() && !dailyOutDeliveryMade)
                    {
                        dailyOutDeliveryMade = true;
                        MakeDeliveryOut();
                        outDeliveryState = eDeliveryState.DEPARTING;
                    }
                    break;
                case eDeliveryState.ARRIVING:
                    outDeliveryAnimationRatio += GameSpeedManager.ElapsedGameTime / OUT_DELIVERY_ANIMATION_TIME;
                    if (outDeliveryAnimationRatio >= 1f)
                    {
                        outDeliveryAnimationRatio = 1f;
                        outDeliveryState = eDeliveryState.IDLE;
                    }
                    break;
                case eDeliveryState.DEPARTING:
                    outDeliveryAnimationRatio -= GameSpeedManager.ElapsedGameTime / OUT_DELIVERY_ANIMATION_TIME;
                    if (outDeliveryAnimationRatio <= 0f)
                    {
                        outDeliveryAnimationRatio = 0f;
                        outDeliveryState = eDeliveryState.DELIVERING;
                    }
                    break;
                case eDeliveryState.DELIVERING:
                    if (GameClockManager.GetClockTime() > OUT_DELIVERY_RETURN_TIME)
                        outDeliveryState = eDeliveryState.ARRIVING;
                    break;
            }
        }
        private static bool CanMakeDeliveryOut()
        {
            return (FactoryPortManager.GetFactoryOutPort().ElementStore.NumberElements() > 0);
        }

        private static void MakeDeliveryIn()
        {
            // TODO
        }
        private static void MakeDeliveryOut()
        {
            // TODO
        }
    }
}
