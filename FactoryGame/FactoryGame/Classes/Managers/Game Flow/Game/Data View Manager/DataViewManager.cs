﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    static class DataViewManager
    {
        // -------- Private Fields --------
        private static DataView dataView;

        // -------- Public Methods --------
        public static void Initialize()
        {
            dataView = null;

            // DEBUG
            //SetDataView_ElementView();
        }

        public static void Denitialize()
        {
            dataView = null;
        }

        public static bool DataViewActive()
        {
            return (dataView != null);
        }

        public static bool DataViewForcesColor()
        {
            if (!DataViewActive())
                return false;

            return dataView.ForcesColor();
        }

        public static Vector3 GetDataViewForcedColor(GameEntity entity, Room room)
        {
            return dataView.GetForcedColor(entity, room);
        }

        public static void SetDataView_ElementView()
        {
            SetDataview(new ElementDataView());
        }

        // -------- Private Methods --------
        private static void SetDataview(DataView newDataView)
        {
            RemoveDataView();
            dataView = newDataView;
            dataView.Activate();
        }
        private static void RemoveDataView()
        {
            if (dataView == null)
                return;

            dataView.Deactivate();
            dataView = null;
        }
    }
}
