﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    static class ElementManager
    {
        // -------- Public Methods --------
        public static void Initialize()
        {
        }

        public static void Denitialize()
        {
        }

        public static ElementQuality GetCreatedElementQuality(List<Element> elementsIn, ProcessData process, Staff staff)
        {
            float averageQualityIn = 0;
            foreach (Element e in elementsIn)
                averageQualityIn += e.Quality.Value;
            averageQualityIn /= elementsIn.Count;

            float qualityOut = ModifierManager.ModifiyCreatedElementQuality(process, staff, averageQualityIn);
            return new ElementQuality(qualityOut);
        }

        public static int GetElementIndexToLoad(LoadTask loadTask)
        {
            ElementStore pickUpStore = loadTask.GetPickUpElementStore();
            GameEntity putDownEntity = loadTask.PutDownInteractability.Parent;

            Machine putDownMachine = putDownEntity as Machine;
            if (putDownMachine != null)
            {
                List<ProcessData> processes = MachineManager.GetProcessesPerformedOnOperatingStation(putDownMachine);

                foreach (ProcessData p in processes)
                    foreach (ElementData et in p.ElementsIn)
                        for (int i = 0; i < pickUpStore.NumberElements(); i++)
                            if (et.Equals(pickUpStore.ElementHolders[i].Element.Data))
                                return i;
            }

            FactoryPort factoryPort = putDownEntity as FactoryPort;
            if (factoryPort != null)
            {
                for (int i = 0; i < pickUpStore.NumberElements(); i++)
                    if (FactoryPortManager.ElementIsDeliverableOut(pickUpStore.ElementHolders[i].Element.Data))
                        return i;
            }

            return -1;
        }

        public static void OnElementMoved()
        {
            UIManager.OnElementMoved();
        }
    }
}
