﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    static class FloorManager
    {
        // -------- Properties --------
        public static int FloorWidth { get; private set; }
        public static int FloorHeight { get; private set; }
        public static TileCoordinate FactoryDoorTile { get; private set; }
        public static TileCoordinate FactoryMapEdgeTile
        {
            get
            {
                return TileUtilities.GetOffsetTile(FactoryDoorTile, FactoryExitDirection, FactoryExitPathLength);
            }
        }
        public static eSquareCompassDirection FactoryExitDirection { get; private set; }
        public static eSquareCompassDirection FactoryEntryDirection
        {
            get
            {
                return CompassUtilities.RotateSquareCompassDirection(FactoryExitDirection, eSquareRotation.ROTATION_180, eRotationDirection.CLOCKWISE);
            }
        }
        public static int FactoryExitPathLength { get { return 2; } }

        // -------- Public Methods --------
        public static void Initialize()
        {
            // DEBUG
            FloorWidth = 9;
            FloorHeight = 12;
            FactoryDoorTile = new TileCoordinate(9, 4);
            FactoryExitDirection = eSquareCompassDirection.SOUTH;


            TileManager.Initialize();
            WallManager.Initialize();
            RoomManager.Initialize();
            PassPlaceMapManager.Initialize();
        }

        public static void Denitialize()
        {
            TileManager.Denitialize();
            WallManager.Denitialize();
            RoomManager.Denitialize();
            PassPlaceMapManager.Denitialize();
        }

        public static void Update()
        {
            TileManager.Update();
            WallManager.Update();
        }

        public static void Draw()
        {
            TileManager.Draw();
            WallManager.Draw();
        }

        public static void DrawTransparent()
        {
            TileManager.DrawTransparent();
            WallManager.DrawTransparent();
        }

        public static List<GameEntity> GetAllGameEntities()
        {
            List<GameEntity> gameEntities = new List<GameEntity>();

            gameEntities.AddRange(TileManager.GetAllTiles());
            gameEntities.AddRange(WallManager.GetAllWalls());

            return gameEntities;
        }

        public static Vector2 GetTileSpaceCenter()
        {
            return new Vector2((float)FloorWidth / 2f, (float)FloorHeight / 2f);
        }

        public static void OnCharacterStepOnTile(Character character, TileCoordinate coord, eSquareCompassDirection direction)
        {
            TrashManager.OnCharacterStepOnTile(character, coord);
            DoorManager.OnCharacterStepOnTile(coord, direction);
            // TODO
        }
    }
}
