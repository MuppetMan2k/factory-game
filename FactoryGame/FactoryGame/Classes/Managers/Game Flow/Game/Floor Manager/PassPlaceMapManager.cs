﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    static class PassPlaceMapManager
    {
        // -------- Private Fields --------
        private static bool[,] passableFloorMap;
        private static bool[,] placableFloorMap;
        private static bool[,] placableCeilingMap;

        // -------- Public Methods --------
        public static void Initialize()
        {
            CreateMaps();
        }

        public static void Denitialize()
        {
            passableFloorMap = null;
            placableFloorMap = null;
            placableCeilingMap = null;
        }

        public static bool[,] GetPassableFloorMap()
        {
            return passableFloorMap;
        }
        public static bool[,] GetPlacableFloorMap()
        {
            bool[,] realPlacableMap = (bool[,])placableFloorMap.Clone();

            foreach (Character c in CharacterManager.GetAllCharacters())
                foreach (TileCoordinate t in c.CharacterCoordinate.GetOccupiedTiles())
                    realPlacableMap[t.Row, t.Col] = false;

            // TODO - other placable restrictions (factory door, factory inlet/outlet etc.)

            return realPlacableMap;
        }
        public static bool[,] GetPlacableCeilingMap()
        {
            return placableCeilingMap;
        }

        public static void ApplyPassableFloorMask(AreaCoordinate coord, bool[,] mask)
        {
            ApplyMaskToMap(passableFloorMap, coord, mask);
        }
        public static void ApplyPlacableFloorMask(AreaCoordinate coord, bool[,] mask)
        {
            ApplyMaskToMap(placableFloorMap, coord, mask);
        }
        public static void ApplyPlacableCeilingMask(AreaCoordinate coord, bool[,] mask)
        {
            ApplyMaskToMap(placableCeilingMap, coord, mask);
        }

        public static void AddPurchasedTileToMaps(TileCoordinate coord)
        {
            passableFloorMap[coord.Row, coord.Col] = true;
            placableFloorMap[coord.Row, coord.Col] = true;
            placableCeilingMap[coord.Row, coord.Col] = true;
        }

        // -------- Private Methods ---------
        private static void CreateMaps()
        {
            passableFloorMap = new bool[FloorManager.FloorHeight, FloorManager.FloorWidth];
            placableFloorMap = new bool[FloorManager.FloorHeight, FloorManager.FloorWidth];
            placableCeilingMap = new bool[FloorManager.FloorHeight, FloorManager.FloorWidth];

            for (int row = 0; row < FloorManager.FloorHeight; row++)
            {
                for (int col = 0; col < FloorManager.FloorWidth; col++)
                {
                    TileCoordinate coord = new TileCoordinate(row, col);
                    if (TileManager.TileIsPurchased(coord))
                    {
                        passableFloorMap[row, col] = true;
                        placableFloorMap[row, col] = true;
                        placableCeilingMap[row, col] = true;
                    }
                }
            }
        }

        private static void ApplyMaskToMap(bool[,] map, AreaCoordinate area, bool[,] mask)
        {
            for (int rMask = 0; rMask < mask.GetLength(0); rMask++)
            {
                for (int cMask = 0; cMask < mask.GetLength(1); cMask++)
                {
                    TileCoordinate localCoord = new TileCoordinate(rMask, cMask);
                    TileCoordinate absCoord = AreaUtilities.ConvertAreaLocalCoordToAbsolute(area, localCoord);

                    if (MapCoordinateExists(map, absCoord.Row, absCoord.Col) && !mask[rMask, cMask])
                        map[absCoord.Row, absCoord.Col] = false;
                }
            }
        }

        private static bool MapCoordinateExists(bool[,] map, int row, int col)
        {
            if (row < 0 || row >= map.GetLength(0))
                return false;

            if (col < 0 || col >= map.GetLength(1))
                return false;

            return true;
        }
    }
}
