﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    static class RoomManager
    {
        // -------- Private Fields --------
        private static List<Room> rooms;
        private static bool[,] tilesToCount;
        private static Room[,] roomReferenceMap;

        // -------- Public Methods --------
        public static void Initialize()
        {
            SetUpRooms();
        }

        public static void Denitialize()
        {
            rooms = null;
            tilesToCount = null;
            roomReferenceMap = null;
        }

        public static void UpdateRooms()
        {
            SetUpRooms();
            UpdateAllRoomFactors();
        }

        public static void UpdateAllRoomFactors()
        {
            foreach (Room room in rooms)
                UpdateRoomFactors(room);
        }
        public static void UpdateRoomFactors(Room room)
        {
            // TODO

            UpdateRoomWindowFactors(room);
        }

        public static Room GetRoom(TileCoordinate coord)
        {
            if (!TileManager.CoordinateIsWithinLimits(coord))
                return null;

            return roomReferenceMap[coord.Row, coord.Col];
        }
        public static Room GetRoom(WallFaceCoordinate coord)
        {
            TileCoordinate tileCoord = WallUtilities.GetTileCoordAdjacentToWallFace(coord);
            return GetRoom(tileCoord);
        }

        // -------- Private Methods --------
        // Room creation
        private static void SetUpRooms()
        {
            rooms = new List<Room>();

            SetUpTilesToCount();
            CreateRooms();
            CreateRoomReferenceMap();
        }
        private static void SetUpTilesToCount()
        {
            tilesToCount = new bool[FloorManager.FloorHeight, FloorManager.FloorWidth];

            for (int r = 0; r < FloorManager.FloorHeight; r++)
            {
                for (int c = 0; c < FloorManager.FloorWidth; c++)
                {
                    if (TileManager.TileIsInFactory(new TileCoordinate(r, c)))
                        tilesToCount[r, c] = true;
                }
            }
        }
        private static void CreateRooms()
        {
            Tile kernelTile = GetTileNotYetInRoom();

            while (kernelTile != null)
            {
                var newRoom = new Room();
                AddTileToRoomRecursiveSearch(newRoom, kernelTile);
                rooms.Add(newRoom);

                kernelTile = GetTileNotYetInRoom();
            }
        }
        private static Tile GetTileNotYetInRoom()
        {
            for (int r = 0; r < FloorManager.FloorHeight; r++)
            {
                for (int c = 0; c < FloorManager.FloorWidth; c++)
                {
                    if (tilesToCount[r, c])
                        return TileManager.GetTile(r, c);
                }
            }

            return null;
        }
        private static void AddTileToRoomRecursiveSearch(Room room, Tile tile)
        {
            room.Tiles.Add(tile);
            TileCoordinate tileCoord = tile.TileCoordinate;
            tilesToCount[tileCoord.Row, tileCoord.Col] = false;

            // Recursive search
            foreach (eSquareCompassDirection direction in EnumUtilities.GetEnumValues<eSquareCompassDirection>())
            {
                TileCoordinate adjacentTile = TileUtilities.GetOffsetTile(tileCoord, direction);

                if (TileManager.CoordinateIsWithinLimits(adjacentTile))
                    if (tilesToCount[adjacentTile.Row, adjacentTile.Col])
                        if (!WallManager.WallExistsAdjacentToTile(tileCoord, direction))
                            AddTileToRoomRecursiveSearch(room, TileManager.GetTile(adjacentTile));
            }
        }

        private static void CreateRoomReferenceMap()
        {
            roomReferenceMap = new Room[FloorManager.FloorHeight, FloorManager.FloorWidth];
            
            foreach (Room room in rooms)
            {
                foreach (Tile tile in room.Tiles)
                {
                    TileCoordinate coord = tile.TileCoordinate;
                    roomReferenceMap[coord.Row, coord.Col] = room;
                }
            }
        }

        // Room factors
        private static void ResetRoomFactors(Room room)
        {
            room.AmbientLight = AmbientLight.Zero;
            room.WindowFactor = 0;
            room.LightFactor = 0;
        }
        private static void UpdateRoomWindowFactors(Room room)
        {
            foreach (Tile tile in room.Tiles)
            {
                foreach (eSquareCompassDirection direction in EnumUtilities.GetEnumValues<eSquareCompassDirection>())
                {
                    WallCoordinate wallCoord = WallUtilities.GetWallCoordAdjacentToTile(tile.TileCoordinate, direction);
                    if (WindowManager.WindowExists(wallCoord))
                        room.WindowFactor++;
                }
            }
        }
    }
}
