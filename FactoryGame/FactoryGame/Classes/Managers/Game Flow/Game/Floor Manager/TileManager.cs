﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    static class TileManager
    {
        // -------- Private Fields --------
        private static Tile[,] tileMap;
        private static List<Tile> tileRemovalList;

        // -------- Public Methods --------
        public static void Initialize()
        {
            SetUpDebugMap();
            tileRemovalList = new List<Tile>();
        }

        public static void Denitialize()
        {
            foreach (Tile t in GetAllTiles())
                t.Denitialize();

            tileMap = null;
            tileRemovalList = null;
        }

        public static void Update()
        {
            List<Tile> tiles = GetAllTiles();

            foreach (Tile t in tiles)
                t.Update();

            UpdateRemovalList();
        }

        public static void Draw()
        {
            List<Tile> tiles = GetAllTiles();

            foreach (Tile t in tiles)
                t.Draw();
        }

        public static void DrawTransparent()
        {
            List<Tile> tiles = GetAllTiles();

            foreach (Tile t in tiles)
                t.DrawTransparent();
        }

        public static Tile GetTile(TileCoordinate coord)
        {
            if (!CoordinateIsWithinLimits(coord))
                return null;

            return tileMap[coord.Row, coord.Col];
        }
        public static Tile GetTile(int row, int col)
        {
            return GetTile(new TileCoordinate(row, col));
        }

        public static TileCoordinate GetRandomPurchasedTileCoord()
        {
            bool foundResult = false;
            TileCoordinate coord = new TileCoordinate();

            while (!foundResult)
            {
                int row = RandomManager.RandomNumber(2, FloorManager.FloorHeight - 3);
                int col = RandomManager.RandomNumber(2, FloorManager.FloorWidth - 3);
                coord = new TileCoordinate(row, col);

                foundResult = TileIsPurchased(coord);
            }

            return coord;
        }

        public static List<Tile> GetAllTiles()
        {
            var tiles = new List<Tile>();

            for (int row = 0; row < FloorManager.FloorHeight; row++)
            {
                for (int col = 0; col < FloorManager.FloorWidth; col++)
                {
                    Tile tile = tileMap[row, col];

                    if (tile != null)
                        tiles.Add(tile);
                }
            }

            return tiles;
        }

        public static void RemoveTile(Tile tileToRemove)
        {
            tileRemovalList.Add(tileToRemove);
        }

        public static bool CoordinateIsWithinLimits(TileCoordinate coord)
        {
            if (coord.Row < 0)
                return false;
            if (coord.Row >= FloorManager.FloorHeight)
                return false;
            if (coord.Col < 0)
                return false;
            if (coord.Col >= FloorManager.FloorWidth)
                return false;

            return true;
        }
        public static bool TileExists(TileCoordinate coord)
        {
            if (!CoordinateIsWithinLimits(coord))
                return false;

            return (tileMap[coord.Row, coord.Col] != null);
        }
        public static bool TileIsInFactory(TileCoordinate coord)
        {
            if (!TileExists(coord))
                return false;

            return tileMap[coord.Row, coord.Col].InFactory;
        }
        public static bool TileIsOutsideFactory(TileCoordinate coord)
        {
            return (TileExists(coord) && !TileIsInFactory(coord));
        }
        public static bool TileIsPurchased(TileCoordinate coord)
        {
            if (!TileIsInFactory(coord))
                return false;

            return tileMap[coord.Row, coord.Col].Purchased;
        }
        public static bool TileIsForSale(TileCoordinate coord)
        {
            return (TileIsInFactory(coord) && !TileIsPurchased(coord));
        }

        // -------- Private Methods ---------
        private static void UpdateRemovalList()
        {
            if (tileRemovalList.Count == 0)
                return;

            foreach (Tile t in tileRemovalList)
            {
                TileCoordinate coord = t.TileCoordinate;
                if (tileMap[coord.Row, coord.Col] != null)
                {
                    tileMap[coord.Row, coord.Col].Denitialize();
                    tileMap[coord.Row, coord.Col] = null;
                }
                else
                    Debug.LogWarning("Tried to remove tile but couldn't find it: " + t.ToString());
            }

            tileRemovalList.Clear();
        }

        // DEBUG
        private static void SetUpDebugMap()
        {
            char[,] debugTileMap = new char[,]
            {
                { 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'x', 'x' },
                { 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'x', 'x' },
                { 'g', 'g', 'w', 'w', 'w', 'g', 'g', 'g', 'g' },
                { 'g', 'g', 'w', 'w', 'w', 'g', 'g', 'g', 'g' },
                { 'g', 'g', 'v', 'v', 'v', 'v', 'v', 'g', 'g' },
                { 'g', 'g', 'v', 'v', 'v', 'v', 'v', 'g', 'g' },
                { 'g', 'g', 'v', 'v', 'v', 'v', 'v', 'g', 'g' },
                { 'g', 'g', 'v', 'v', 'v', 'v', 'v', 'g', 'g' },
                { 'g', 'g', 'v', 'v', 'v', 'v', 'v', 'g', 'g' },
                { 'g', 'g', 'v', 'v', 'v', 'v', 'v', 'g', 'g' },
                { 'g', 'g', 'g', 'g', 'p', 'g', 'g', 'g', 'g' },
                { 'g', 'g', 'g', 'g', 'p', 'g', 'g', 'g', 'g' }
            };

            tileMap = new Tile[FloorManager.FloorHeight, FloorManager.FloorWidth];

            for (int row = 0; row < FloorManager.FloorHeight; row++)
            {
                for (int col = 0; col < FloorManager.FloorWidth; col++)
                {
                    char c = debugTileMap[row, col];

                    Tile tile = null;

                    switch (c)
                    {
                        case 'g':
                            tile = new Tile(new TileCoordinate(row, col), TileStyle.Grass, false, false);
                            break;
                        case 'x':
                            break;
                        case 'w':
                            tile = new Tile(new TileCoordinate(row, col), TileStyle.Vinyl, true, false);
                            break;
                        case 'v':
                            tile = new Tile(new TileCoordinate(row, col), TileStyle.Vinyl, true, true);
                            break;
                        case 'p':
                            tile = new Tile(new TileCoordinate(row, col), TileStyle.Paved, false, false);
                            break;
                    }

                    tileMap[row, col] = tile;

                    if (tileMap[row, col] != null)
                        tileMap[row, col].Initialize();
                }
            }
        }
    }
}
