﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    // -------- Enumerations --------
    public enum eWallCutAwayMode
    {
        WALLS_UP,
        WALLS_CUT,
        WALLS_DOWN
    }

    static class WallCutAwayManager
    {
        // -------- Properties --------
        public static eWallCutAwayMode CutAwayMode { get; set; }

        // -------- Private Fields --------
        private static WallPillar[,] pillarMap;

        // -------- Public Methods --------
        public static void Initialize()
        {
            CutAwayMode = eWallCutAwayMode.WALLS_CUT;

            pillarMap = new WallPillar[FloorManager.FloorHeight + 1, FloorManager.FloorWidth + 1];
            UpdatePillars();
        }

        public static void Denitialize()
        {
            pillarMap = null;
        }

        public static void SetCutAwayMode(eWallCutAwayMode mode)
        {
            if (CutAwayMode == mode)
                return;

            CutAwayMode = mode;
            UpdatePillarHeights();
        }

        public static void UpdatePillars()
        {
            CreatePillarMap();
            UpdatePillarHeights();
        }

        public static void UpdatePillarHeights()
        {
            for (int r = 0; r <= FloorManager.FloorHeight; r++)
            {
                for (int c = 0; c <= FloorManager.FloorWidth; c++)
                {
                    if (pillarMap[r, c] == null)
                        continue;

                    if (CutAwayMode == eWallCutAwayMode.WALLS_UP)
                    {
                        pillarMap[r, c].Height = eWallPillarHeight.FULL;
                        continue;
                    }

                    if (CutAwayMode == eWallCutAwayMode.WALLS_DOWN)
                    {
                        pillarMap[r, c].Height = eWallPillarHeight.CUT;
                        continue;
                    }

                    if (PillarShouldBeCut(r, c))
                        pillarMap[r, c].Height = eWallPillarHeight.CUT;
                    else
                        pillarMap[r, c].Height = eWallPillarHeight.FULL;
                }
            }

            SmoothPillarHeights();
        }

        public static void GetWallPillars(WallFaceCoordinate coord, out WallPillar leftPillar, out WallPillar rightPillar)
        {
            int r = coord.WallCoordinate.Coordinate.Row;
            int c = coord.WallCoordinate.Coordinate.Col;

            if (coord.WallCoordinate.Direction == eWallDirection.NORTH)
            {
                if (coord.Face == eWallFace.INSIDE)
                {
                    leftPillar = pillarMap[r, c];
                    rightPillar = pillarMap[r, c + 1];
                    return;
                }
                else
                {
                    leftPillar = pillarMap[r, c + 1];
                    rightPillar = pillarMap[r, c];
                    return;
                }
            }
            else
            {
                if (coord.Face == eWallFace.INSIDE)
                {
                    leftPillar = pillarMap[r + 1, c];
                    rightPillar = pillarMap[r, c];
                    return;
                }
                else
                {
                    leftPillar = pillarMap[r, c];
                    rightPillar = pillarMap[r+ 1, c];
                    return;
                }
            }
        }
        public static void GetWallPillars(WallCoordinate coord, out WallPillar pillar1, out WallPillar pillar2)
        {
            WallFaceCoordinate effectiveFaceCoord = new WallFaceCoordinate(coord, eWallFace.INSIDE);
            GetWallPillars(effectiveFaceCoord, out pillar1, out pillar2);
        }

        // -------- Private Methods ---------
        private static void CreatePillarMap()
        {
            pillarMap = new WallPillar[FloorManager.FloorHeight + 1, FloorManager.FloorWidth + 1];

            for (int r = 0; r <= FloorManager.FloorHeight; r++)
            {
                for (int c = 0; c <= FloorManager.FloorWidth; c++)
                {
                    if (WallManager.WallExists(new WallCoordinate(r, c, eWallDirection.NORTH)))
                    {
                        pillarMap[r, c] = new WallPillar();
                        pillarMap[r, c + 1] = new WallPillar();
                    }

                    if (WallManager.WallExists(new WallCoordinate(r, c, eWallDirection.WEST)))
                    {
                        pillarMap[r, c] = new WallPillar();
                        pillarMap[r + 1, c] = new WallPillar();
                    }
                }
            }
        }

        private static bool PillarShouldBeCut(int row, int col)
        {
            List<Tile> tilesCoveredByPillar = GetTilesCoveredByPillar(row, col);

            foreach (Tile tile in tilesCoveredByPillar)
            {
                if (tile.InFactory)
                    return true;
            }

            return false;
        }

        private static List<Tile> GetTilesCoveredByPillar(int row, int col)
        {
            eDiagonalCompassDirection viewDirection = CameraManager.GetApproxViewDirection();

            var tiles = new List<Tile>();

            // First tile
            int row1 = row;
            int col1 = col;
            switch (viewDirection)
            {
                case eDiagonalCompassDirection.NORTHEAST:
                    row1--;
                    break;
                case eDiagonalCompassDirection.SOUTHEAST:
                    break;
                case eDiagonalCompassDirection.SOUTHWEST:
                    col1--;
                    break;
                case eDiagonalCompassDirection.NORTHWEST:
                    row1--;
                    col1--;
                    break;
            }
            TileCoordinate tile1Coord = new TileCoordinate(row1, col1);

            // Second tile
            TileCoordinate tile2Coord = TileUtilities.GetOffsetTile(tile1Coord, viewDirection, 1);

            // Add to tiles list
            if (TileManager.TileExists(tile1Coord))
                tiles.Add(TileManager.GetTile(tile1Coord));
            if (TileManager.TileExists(tile2Coord))
                tiles.Add(TileManager.GetTile(tile2Coord));

            return tiles;
        }

        private static void SmoothPillarHeights()
        {
            for (int r = 0; r <= FloorManager.FloorHeight; r++)
            {
                for (int c = 0; c <= FloorManager.FloorWidth; c++)
                {
                    if (pillarMap[r, c] != null && PillarShouldBeSmoothed(r, c))
                        pillarMap[r, c].Height = eWallPillarHeight.CUT;
                }
            }
        }
        private static bool PillarShouldBeSmoothed(int row, int col)
        {
            WallPillar centerPillar = pillarMap[row, col];
            if (centerPillar.Height == eWallPillarHeight.CUT)
                return false;

            List<WallPillar> surroundingPillars = GetSurroundingPillars(row, col);

            foreach (WallPillar pillar in surroundingPillars)
                if (pillar.Height == eWallPillarHeight.FULL)
                    return false;

            return true;
        }
        private static List<WallPillar> GetSurroundingPillars(int row, int col)
        {
            var pillars = new List<WallPillar>();

            // North
            if (row > 0)
            {
                WallPillar pillar = pillarMap[row - 1, col];
                if (pillar != null)
                    pillars.Add(pillar);
            }

            // East
            if (col < FloorManager.FloorWidth)
            {
                WallPillar pillar = pillarMap[row, col + 1];
                if (pillar != null)
                    pillars.Add(pillar);
            }

            // South
            if (row < FloorManager.FloorHeight)
            {
                WallPillar pillar = pillarMap[row + 1, col];
                if (pillar != null)
                    pillars.Add(pillar);
            }

            // West
            if (col > 0)
            {
                WallPillar pillar = pillarMap[row, col - 1];
                if (pillar != null)
                    pillars.Add(pillar);
            }

            return pillars;
        }
    }
}
