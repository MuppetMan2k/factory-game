﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    static class WallManager
    {
        // -------- Private Fields --------
        private static Wall[, ,] wallMap;
        private static List<Wall> wallRemovalList;

        // -------- Public Methods --------
        public static void Initialize()
        {
            SetupDebugMap();
            wallRemovalList = new List<Wall>();

            WallCutAwayManager.Initialize();
        }

        public static void Denitialize()
        {
            foreach (Wall w in GetAllWalls())
                w.Denitialize();

            wallMap = null;
            wallRemovalList = null;

            WallCutAwayManager.Denitialize();
        }

        public static void Update()
        {
            List<Wall> walls = GetAllWalls();

            foreach (Wall w in walls)
                w.Update();

            UpdateRemovalList();
        }

        public static void Draw()
        {
            List<Wall> walls = GetAllWalls();

            foreach (Wall w in walls)
                w.Draw();
        }

        public static void DrawTransparent()
        {
            List<Wall> walls = GetAllWalls();

            foreach (Wall w in walls)
                w.DrawTransparent();
        }

        public static Wall GetWall(WallCoordinate coord)
        {
            if (!WallCoordinateExists(coord))
                return null;

            return wallMap[coord.Coordinate.Row, coord.Coordinate.Col, WallUtilities.ConvertWallDirectionToMapIndex(coord.Direction)];
        }

        public static List<Wall> GetAllWalls()
        {
            var walls = new List<Wall>();

            for (int row = 0; row <= FloorManager.FloorHeight; row++)
            {
                for (int col = 0; col <= FloorManager.FloorWidth; col++)
                {
                    for (int dir = 0; dir <= 1; dir++)
                    {
                        Wall wall = wallMap[row, col, dir];

                        if (wall != null)
                            walls.Add(wall);
                    }
                }
            }

            return walls;
        }

        public static void SetWall(Wall newWall)
        {
            wallMap[newWall.WallCoordinate.Coordinate.Row, newWall.WallCoordinate.Coordinate.Col, WallUtilities.ConvertWallDirectionToMapIndex(newWall.WallCoordinate.Direction)] = newWall;
            wallMap[newWall.WallCoordinate.Coordinate.Row, newWall.WallCoordinate.Coordinate.Col, WallUtilities.ConvertWallDirectionToMapIndex(newWall.WallCoordinate.Direction)].Initialize();
        }

        public static void RemoveWall(Wall wallToRemove)
        {
            wallRemovalList.Add(wallToRemove);
        }

        public static bool WallExists(WallCoordinate coord)
        {
            return (GetWall(coord) != null);
        }

        public static bool WallExistsAdjacentToTile(TileCoordinate coord, eSquareCompassDirection direction)
        {
            WallCoordinate wallCoord = WallUtilities.GetWallCoordAdjacentToTile(coord, direction);
            return WallExists(wallCoord);
        }

        public static bool ImpassableWallExistsAdjacentToTile(TileCoordinate coord, eSquareCompassDirection direction)
        {
            if (!WallExistsAdjacentToTile(coord, direction))
                return false;

            if (DoorManager.DoorExistsAdjacentToTile(coord, direction))
                return false;

            return true;
        }

        public static void RefreshPurchaseZoneWalls()
        {
            for (int row = 1; row < FloorManager.FloorWidth - 1; row++)
            {
                for (int col = 1; col < FloorManager.FloorHeight - 1; col++)
                {
                    WallCoordinate northCoord = new WallCoordinate(row, col, eWallDirection.NORTH);
                    WallCoordinate westCoord = new WallCoordinate(row, col, eWallDirection.WEST);

                    if (WallShouldBePurchasableZoneWall(northCoord) && !WallIsPurchasableZoneWall(northCoord))
                        SetWall(Wall.CreatePurchasableZoneWall(northCoord));

                    if (WallShouldBePurchasableZoneWall(westCoord) && !WallIsPurchasableZoneWall(westCoord))
                        SetWall(Wall.CreatePurchasableZoneWall(westCoord));
                }
            }
        }

        public static bool WallCoordinateExists(WallCoordinate coord)
        {
            if (coord.Coordinate.Row < 0)
                return false;
            if (coord.Coordinate.Row > FloorManager.FloorHeight)
                return false;
            if (coord.Coordinate.Col < 0)
                return false;
            if (coord.Coordinate.Col > FloorManager.FloorWidth)
                return false;

            return true;
        }

        // -------- Private Methods ---------
        private static void UpdateRemovalList()
        {
            if (wallRemovalList.Count == 0)
                return;

            foreach (Wall w in wallRemovalList)
            {
                WallCoordinate coord = w.WallCoordinate;
                int dir = WallUtilities.ConvertWallDirectionToMapIndex(w.WallCoordinate.Direction);

                if (wallMap[coord.Coordinate.Row, coord.Coordinate.Col, dir] != null)
                {
                    wallMap[coord.Coordinate.Row, coord.Coordinate.Col, dir].Denitialize();
                    wallMap[coord.Coordinate.Row, coord.Coordinate.Col, dir] = null;
                }
                else
                    Debug.LogWarning("Tried to remove wall but it is not in wall map: " + w.ToString());
            }

            wallRemovalList.Clear();
        }

        private static bool WallIsPurchasableZoneWall(WallCoordinate coord)
        {
            if (!WallExists(coord))
                return false;

            return (GetWall(coord).Type == eWallType.PURCHASABLE_ZONE);
        }
        private static bool WallShouldBePurchasableZoneWall(WallCoordinate coord)
        {
            TileCoordinate adjacentCoord = coord.Coordinate;

            if (coord.Direction == eWallDirection.NORTH)
                adjacentCoord.Row--;
            else
                adjacentCoord.Col--;

            if (TileManager.TileIsPurchased(coord.Coordinate) && TileManager.TileIsForSale(adjacentCoord))
                return true;

            if (TileManager.TileIsPurchased(adjacentCoord) && TileManager.TileIsForSale(coord.Coordinate))
                return true;

            return false;
        }

        // DEBUG
        private static void SetupDebugMap()
        {
            wallMap = new Wall[FloorManager.FloorHeight + 1, FloorManager.FloorWidth + 1, 2];

            for (int row = 1; row <= FloorManager.FloorHeight; row++)
            {
                for (int col = 1; col <= FloorManager.FloorWidth; col++)
                {
                    // Exterior wall to north
                    if (TileManager.TileIsInFactory(new TileCoordinate(row, col)) && TileManager.TileIsOutsideFactory(new TileCoordinate(row - 1, col)))
                    {
                        wallMap[row, col, 0] = new Wall(new WallCoordinate(new TileCoordinate(row, col), eWallDirection.NORTH), eWallType.EXTERIOR, WallStyle.Office, WallStyle.Brick);
                        wallMap[row, col, 0].Initialize();
                    }
                    if (TileManager.TileIsOutsideFactory(new TileCoordinate(row, col)) && TileManager.TileIsInFactory(new TileCoordinate(row - 1, col)))
                    {
                        wallMap[row, col, 0] = new Wall(new WallCoordinate(new TileCoordinate(row, col), eWallDirection.NORTH), eWallType.EXTERIOR, WallStyle.Brick, WallStyle.Office);
                        wallMap[row, col, 0].Initialize();
                    }

                    // Exterior wall to west
                    if (TileManager.TileIsInFactory(new TileCoordinate(row, col)) && TileManager.TileIsOutsideFactory(new TileCoordinate(row, col - 1)))
                    {
                        wallMap[row, col, 1] = new Wall(new WallCoordinate(new TileCoordinate(row, col), eWallDirection.WEST), eWallType.EXTERIOR, WallStyle.Office, WallStyle.Brick);
                        wallMap[row, col, 1].Initialize();
                    }
                    if (TileManager.TileIsOutsideFactory(new TileCoordinate(row, col)) && TileManager.TileIsInFactory(new TileCoordinate(row, col - 1)))
                    {
                        wallMap[row, col, 1] = new Wall(new WallCoordinate(new TileCoordinate(row, col), eWallDirection.WEST), eWallType.EXTERIOR, WallStyle.Brick, WallStyle.Office);
                        wallMap[row, col, 1].Initialize();
                    }
                }
            }

            wallMap[4, 5, 1] = new Wall(new WallCoordinate(new TileCoordinate(4, 5), eWallDirection.WEST), eWallType.INTERIOR, WallStyle.Office, WallStyle.Office);
            wallMap[4, 5, 1].Initialize();
            wallMap[5, 5, 1] = new Wall(new WallCoordinate(new TileCoordinate(5, 5), eWallDirection.WEST), eWallType.INTERIOR, WallStyle.Office, WallStyle.Office);
            wallMap[5, 5, 1].Initialize();
            wallMap[6, 5, 1] = new Wall(new WallCoordinate(new TileCoordinate(6, 5), eWallDirection.WEST), eWallType.INTERIOR, WallStyle.Office, WallStyle.Office);
            wallMap[6, 5, 1].Initialize();
            wallMap[7, 5, 1] = new Wall(new WallCoordinate(new TileCoordinate(7, 5), eWallDirection.WEST), eWallType.INTERIOR, WallStyle.Office, WallStyle.Office);
            wallMap[7, 5, 1].Initialize();
            wallMap[8, 5, 1] = new Wall(new WallCoordinate(new TileCoordinate(8, 5), eWallDirection.WEST), eWallType.INTERIOR, WallStyle.Office, WallStyle.Office);
            wallMap[8, 5, 1].Initialize();
            wallMap[9, 5, 1] = new Wall(new WallCoordinate(new TileCoordinate(9, 5), eWallDirection.WEST), eWallType.INTERIOR, WallStyle.Office, WallStyle.Office);
            wallMap[9, 5, 1].Initialize();

            RefreshPurchaseZoneWalls();
        }
    }
}
