﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    static class WallMeshManager
    {
        // -------- Public Methods --------
        public static bool WallMeshShouldBeDrawn(string meshName, WallFaceCoordinate coord)
        {
            WallPillar leftPillar;
            WallPillar rightPillar;

            WallCutAwayManager.GetWallPillars(coord, out leftPillar, out rightPillar);

            switch (meshName)
            {
                case "Upper":
                    return ShouldDrawWallMesh_Upper(leftPillar, rightPillar);
                case "LeftUpright":
                    return ShouldDrawWallMesh_LeftUpright(leftPillar);
                case "RightUpright":
                    return ShouldDrawWallMesh_RightUpright(rightPillar);
                case "Window":
                    return ShouldDrawWallMesh_Window(leftPillar, rightPillar, coord.WallCoordinate);
                case "UnderWindow":
                    return ShouldDrawWallMesh_UnderWindow(leftPillar, rightPillar, coord.WallCoordinate);
                case "Lower":
                    return ShouldDrawWallMesh_Lower(coord.WallCoordinate);
                case "LeftLower":
                    return ShouldDrawWallMesh_LeftLower();
                case "RightLower":
                    return ShouldDrawWallMesh_RightLower();
                default:
                    return false;
            }
        }

        // -------- Private Methods ---------
        // Wall mesh conditional draw methods
        private static bool ShouldDrawWallMesh_Upper(WallPillar leftPillar, WallPillar rightPillar)
        {
            return (leftPillar.Height == eWallPillarHeight.FULL && rightPillar.Height == eWallPillarHeight.FULL);
        }
        private static bool ShouldDrawWallMesh_LeftUpright(WallPillar leftPillar)
        {
            return (leftPillar.Height == eWallPillarHeight.FULL);
        }
        private static bool ShouldDrawWallMesh_RightUpright(WallPillar rightPillar)
        {
            return (rightPillar.Height == eWallPillarHeight.FULL);
        }
        private static bool ShouldDrawWallMesh_Window(WallPillar leftPillar, WallPillar rightPillar, WallCoordinate coord)
        {
            if (leftPillar.Height == eWallPillarHeight.CUT || rightPillar.Height == eWallPillarHeight.CUT)
                return false;

            if (WindowManager.WindowExists(coord))
                return false;

            if (DoorManager.DoorExists(coord))
                return false;

            return true;
        }
        private static bool ShouldDrawWallMesh_UnderWindow(WallPillar leftPillar, WallPillar rightPillar, WallCoordinate coord)
        {
            if (leftPillar.Height == eWallPillarHeight.CUT || rightPillar.Height == eWallPillarHeight.CUT)
                return false;

            if (DoorManager.DoorExists(coord))
                return false;

            return true;
        }
        private static bool ShouldDrawWallMesh_Lower(WallCoordinate coord)
        {
            return (!DoorManager.DoorExists(coord));
        }
        private static bool ShouldDrawWallMesh_LeftLower()
        {
            return true;
        }
        private static bool ShouldDrawWallMesh_RightLower()
        {
            return true;
        }
    }
}
