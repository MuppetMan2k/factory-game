﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    static class BalanceValuesManager
    {
        // -------- Public Methods --------
        public static int GetHourlyPayRate_Cleaner()
        {
            int baseHourRate = 5;
            return (int)(ModifierManager.GetPayRateModifier() * (float)baseHourRate);
        }
        public static int GetHourlyPayRate_Operator()
        {
            int baseHourRate = 9;
            return (int)(ModifierManager.GetPayRateModifier() * (float)baseHourRate);
        }
        public static int GetHourlyPayRate_Loader()
        {
            int baseHourRate = 6;
            return (int)(ModifierManager.GetPayRateModifier() * (float)baseHourRate);
        }
        public static int GetHourlyPayRate_Technician()
        {
            int baseHourRate = 11;
            return (int)(ModifierManager.GetPayRateModifier() * (float)baseHourRate);
        }
    }
}
