﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    // -------- Enumerations --------
    public enum eStatLevel
    {
        LEVEL_1,
        LEVEL_2,
        LEVEL_3
    }

    public enum eChangeLevel
    {
        PLUS_3,
        PLUS_2,
        PLUS_1,
        ZERO,
        MINUS_1,
        MINUS_2,
        MINUS_3
    }

    static class GameBalanceManager
    {
        // -------- Properties --------
        // Staff needs
        public static float PassiveHungerChangeRate { get { return -0.003f; } }
        public static float PassiveBladderChangeRate { get { return -0.001f; } }
        public static float HappinessLerpRate { get { return 0.05f; } }
        public static float HappinessMinToAvLerpFactor { get { return 0.2f; } }
        // Staff arrival times
        public static float StaffEarlyArrivalWindow { get { return 0.25f; } }
        public static float StaffLateArrivalWindow { get { return 0.25f; } }
        // Element quality
        public static float PercentElementQualityIncreasePerSkillLevel { get { return 3.0f; } }
        public static float PercentMachineOperateSpeedIncreasePerSKillLevel { get { return 1.0f; } }

        // -------- Public Methods --------
        // Staff needs
        public static float GetNeedChangeRate(eChangeLevel level)
        {
            float baseChangeRate = 0.05f;

            switch (level)
            {
                case eChangeLevel.PLUS_3:
                    return 3 * baseChangeRate;
                case eChangeLevel.PLUS_2:
                    return 2 * baseChangeRate;
                case eChangeLevel.PLUS_1:
                    return 1 * baseChangeRate;
                case eChangeLevel.ZERO:
                    return 0 * baseChangeRate;
                case eChangeLevel.MINUS_1:
                    return -1 * baseChangeRate;
                case eChangeLevel.MINUS_2:
                    return -2 * baseChangeRate;
                case eChangeLevel.MINUS_3:
                    return -3 * baseChangeRate;
                default:
                    Debug.LogWarning("Unknown change level for need change rate: " + level.ToString());
                    return 0f;
            }
        }
        public static float GetPassiveEnergyChangeRateForTask(eTaskType taskType)
        {
            float baseBusyChangeRate = -0.005f;
            float baseIdleChangeRate = -0.002f;

            switch (taskType)
            {
                case eTaskType.CLEAN:
                    return baseBusyChangeRate;
                case eTaskType.FIX:
                    return baseBusyChangeRate;
                case eTaskType.IDLE:
                    return baseIdleChangeRate;
                case eTaskType.LOAD:
                    return baseBusyChangeRate;
                case eTaskType.OPERATE:
                    return baseBusyChangeRate;
                case eTaskType.TAKE_BREAK:
                    return baseIdleChangeRate;
                case eTaskType.ENTER_FACTORY:
                    return baseIdleChangeRate;
                case eTaskType.EXIT_FACTORY:
                    return baseIdleChangeRate;
                default:
                    Debug.LogWarning("Unknown task type for passive energy change rate: " + taskType.ToString());
                    return 0f;
            }
        }
    }
}
