﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    static class ModifierManager
    {
        // -------- Public Methods --------
        public static float GetTrashPickupSpeed(Staff staff)
        {
            // TODO
            return 1.0f;
        }

        public static float GetMachineProcessSpeed(ProcessData process, Staff staff, Machine m)
        {
            float speed = 1.0f;
            speed = CompanyManager.PlayerCompany.Skills.ModifyMachineOperateSpeed(process, speed);
            speed = CompanyManager.PlayerCompany.Traits.ModifyMachineOperateSpeed(speed);
            speed = staff.Skills.ModifyMachineOperateSpeed(process, speed);
            speed = staff.Traits.ModifyMachineOperateSpeed(speed);
            return speed;
        }
        public static float GetElementPickUpSpeed(Staff staff)
        {
            float speed = 1.0f;
            speed = CompanyManager.PlayerCompany.Traits.ModifyElementPickUpSpeed(speed);
            speed = staff.Traits.ModifyElementPickUpSpeed(speed);
            return speed;
        }
        public static float GetElementPutDownSpeed(Staff staff)
        {
            float speed = 1.0f;
            speed = CompanyManager.PlayerCompany.Traits.ModifyElementPutDownSpeed(speed);
            speed = staff.Traits.ModifyElementPutDownSpeed(speed);
            return speed;
        }

        public static float GetPayRateModifier()
        {
            // TODO
            return 1.0f;
        }

        public static float ModifiyCreatedElementQuality(ProcessData process, Staff staff, float quality)
        {
            quality = CompanyManager.PlayerCompany.Skills.ModifyCreatedElementQuality(process, quality);
            quality = CompanyManager.PlayerCompany.Traits.ModifyCreatedElementQuality(quality);
            quality = staff.Skills.ModifyCreatedElementQuality(process, quality);
            quality = staff.Traits.ModifyCreatedElementQuality(quality);

            return quality;
        }

        public static float ModifyStaffHungerChangeRate(float rate, Staff staff)
        {
            rate = CompanyManager.PlayerCompany.Traits.ModifyStaffHungerChangeRate(rate);
            rate = staff.Traits.ModifyStaffHungerChangeRate(rate);
            return rate;
        }
        public static float ModifyStaffBladderChangeRate(float rate, Staff staff)
        {
            rate = CompanyManager.PlayerCompany.Traits.ModifyStaffBladderChangeRate(rate);
            rate = staff.Traits.ModifyStaffBladderChangeRate(rate);
            return rate;
        }
        public static float ModifyStaffEnergyChangeRate(float rate, Staff staff)
        {
            rate = CompanyManager.PlayerCompany.Traits.ModifyStaffEnergyChangeRate(rate);
            rate = staff.Traits.ModifyStaffEnergyChangeRate(rate);
            return rate;
        }
        
        public static float ModifyStaffPassiveEnergyChangeRate(float rate, Staff staff)
        {
            rate = CompanyManager.PlayerCompany.Traits.ModifyStaffPassiveEnergyChangeRate(rate);
            rate = staff.Traits.ModifyStaffPassiveEnergyChangeRate(rate);
            return rate;
        }
        public static float ModifyStaffPassiveHungerChangeRate(float rate, Staff staff)
        {
            rate = CompanyManager.PlayerCompany.Traits.ModifyStaffPassiveHungerChangeRate(rate);
            rate = staff.Traits.ModifyStaffPassiveHungerChangeRate(rate);
            return rate;
        }
        public static float ModifyStaffPassiveBladderChangeRate(float rate, Staff staff)
        {
            rate = CompanyManager.PlayerCompany.Traits.ModifyStaffPassiveBladderChangeRate(rate);
            rate = staff.Traits.ModifyStaffPassiveBladderChangeRate(rate);
            return rate;
        }

        public static float ModifyStaffTargetHappiness(float happiness, Staff staff)
        {
            happiness = CompanyManager.PlayerCompany.Traits.ModifyStaffTargetHappiness(happiness);
            happiness = staff.Traits.ModifyStaffTargetHappiness(happiness);
            return happiness;
        }

        public static float ModifyStaffEarlyArrivalWindow(Staff staff, float window)
        {
            window = CompanyManager.PlayerCompany.Traits.ModifyStaffEarlyArrivalWindow(window);
            window = staff.Traits.ModifyStaffEarlyArrivalWindow(window);

            return window;
        }
        public static float ModifyStaffLateArrivalWindow(Staff staff, float window)
        {
            window = CompanyManager.PlayerCompany.Traits.ModifyStaffLateArrivalWindow(window);
            window = staff.Traits.ModifyStaffLateArrivalWindow(window);

            return window;
        }
    }
}
