﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    static class DoorManager
    {
        // -------- Private Fields --------
        private static Door[, ,] doorMap;
        private static List<Door> doorRemovalList;

        // -------- Public Methods --------
        public static void Initialize()
        {
            CreateDoorMap();
            CreateDebugDoors();
            doorRemovalList = new List<Door>();
        }

        public static void Denitialize()
        {
            foreach (Door d in GetAllDoors())
                d.Denitialize();

            doorMap = null;
            doorRemovalList = null;
        }

        public static void Update()
        {
            List<Door> doors = GetAllDoors();

            foreach (Door d in doors)
                d.Update();

            UpdateRemovalList();
        }

        public static void Draw()
        {
            List<Door> doors = GetAllDoors();

            foreach (Door d in doors)
                d.Draw();
        }

        public static void DrawTransparent()
        {
            List<Door> doors = GetAllDoors();

            foreach (Door d in doors)
                d.DrawTransparent();
        }

        public static bool DoorExists(WallCoordinate coord)
        {
            if (!WallManager.WallCoordinateExists(coord))
                return false;

            return (GetDoor(coord) != null);
        }

        public static bool DoorExistsAdjacentToTile(TileCoordinate coord, eSquareCompassDirection direction)
        {
            WallCoordinate wallCoord = WallUtilities.GetWallCoordAdjacentToTile(coord, direction);
            return DoorExists(wallCoord);
        }

        public static Door GetDoor(WallCoordinate coord)
        {
            if (!WallManager.WallCoordinateExists(coord))
                return null;

            return doorMap[coord.Coordinate.Row, coord.Coordinate.Col, WallUtilities.ConvertWallDirectionToMapIndex(coord.Direction)];
        }

        public static List<Door> GetAllDoors()
        {
            var doors = new List<Door>();

            for (int r = 0; r <= FloorManager.FloorHeight; r++)
            {
                for (int c = 0; c <= FloorManager.FloorWidth; c++)
                {
                    for (int d = 0; d <= 1; d++)
                    {
                        Door door = doorMap[r, c, d];

                        if (door != null)
                            doors.Add(door);
                    }
                }
            }

            return doors;
        }

        public static void RemoveDoor(Door doorToRemove)
        {
            doorRemovalList.Add(doorToRemove);
        }

        public static void OnCharacterStepOnTile(TileCoordinate coord, eSquareCompassDirection direction)
        {
            WallCoordinate wallCoord = WallUtilities.GetWallCoordAdjacentToTile(coord, direction);
            Door door = GetDoor(wallCoord);

            if (door != null)
            {
                eHingeOpenDirection openDirection;
                if (direction == eSquareCompassDirection.NORTH || direction == eSquareCompassDirection.WEST)
                    openDirection = eHingeOpenDirection.OUTSIDE;
                else
                    openDirection = eHingeOpenDirection.INSIDE;

                door.OpenDoor(openDirection);
            }
        }

        // -------- Private Methods ---------
        private static void CreateDoorMap()
        {
            doorMap = new Door[FloorManager.FloorHeight + 1, FloorManager.FloorWidth + 1, 2];
        }

        private static void UpdateRemovalList()
        {
            if (doorRemovalList.Count == 0)
                return;

            foreach (Door d in doorRemovalList)
            {
                WallMountCoordinate coord = d.WallMountCoordinate;
                int dir = WallUtilities.ConvertWallDirectionToMapIndex(coord.WallCoordinate.Direction);
                if (doorMap[coord.WallCoordinate.Coordinate.Row, coord.WallCoordinate.Coordinate.Col, dir] != null)
                {
                    doorMap[coord.WallCoordinate.Coordinate.Row, coord.WallCoordinate.Coordinate.Col, dir].Denitialize();
                    doorMap[coord.WallCoordinate.Coordinate.Row, coord.WallCoordinate.Coordinate.Col, dir] = null;
                }
                else
                    Debug.LogWarning("Tried to remove door but couldn't find it: " + d.ToString());
            }

            doorRemovalList.Clear();
        }

        // DEBUG
        private static void CreateDebugDoors()
        {
            doorMap[10, 4, 0] = new HingeDoor(new WallCoordinate(10, 4, eWallDirection.NORTH));
            doorMap[10, 4, 0].Initialize();
            doorMap[8, 5, 1] = new HingeDoor(new WallCoordinate(8, 5, eWallDirection.WEST));
            doorMap[8, 5, 1].Initialize();
        }
    }
}
