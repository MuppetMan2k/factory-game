﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    static class FactoryObjectManager
    {
        // -------- Private Fields --------
        private static List<AreaFactoryObject> areaFOs;
        private static List<WallMountFactoryObject> wallMountFOs;
        private static List<AreaFactoryObject> areaFORemovalList;
        private static List<WallMountFactoryObject> wallMountFORemovalList;

        // -------- Public Methods --------
        public static void Initialize()
        {
            areaFOs = new List<AreaFactoryObject>();
            wallMountFOs = new List<WallMountFactoryObject>();
            areaFORemovalList = new List<AreaFactoryObject>();
            wallMountFORemovalList = new List<WallMountFactoryObject>();

            SetUpDebugMachines();
        }

        public static void Denitialize()
        {
            foreach (AreaFactoryObject afo in areaFOs)
                afo.Denitialize();

            foreach (WallMountFactoryObject wmfo in wallMountFOs)
                wmfo.Denitialize();

            areaFOs = null;
            wallMountFOs = null;
            areaFORemovalList = null;
            wallMountFORemovalList = null;
        }

        public static void Update()
        {
            foreach (AreaFactoryObject afo in areaFOs)
                afo.Update();

            foreach (WallMountFactoryObject wmfo in wallMountFOs)
                wmfo.Update();

            UpdateAreaFORemovalList();
            UpdateWallMountFORemovalList();
        }

        public static void Draw()
        {
            foreach (AreaFactoryObject afo in areaFOs)
                afo.Draw();

            foreach (WallMountFactoryObject wmfo in wallMountFOs)
                wmfo.Draw();
        }

        public static void DrawTransparent()
        {
            foreach (AreaFactoryObject afo in areaFOs)
                afo.DrawTransparent();

            foreach (WallMountFactoryObject wmfo in wallMountFOs)
                wmfo.DrawTransparent();
        }

        public static List<GameEntity> GetAllGameEntities()
        {
            List<GameEntity> gameEntities = new List<GameEntity>();

            gameEntities.AddRange(areaFOs);
            gameEntities.AddRange(wallMountFOs);

            return gameEntities;
        }
        public static List<Machine> GetAllMachines()
        {
            List<Machine> machines = new List<Machine>();

            foreach (AreaFactoryObject afo in areaFOs)
            {
                Machine machine = afo as Machine;
                if (machine != null)
                    machines.Add(machine);
            }

            return machines;
        }
        public static List<BreakFactoryObject> GetAllBreakFactoryObjects()
        {
            List<BreakFactoryObject> breakFOs = new List<BreakFactoryObject>();

            foreach (AreaFactoryObject afo in areaFOs)
            {
                BreakFactoryObject breakFO = afo as BreakFactoryObject;
                if (breakFO != null)
                    breakFOs.Add(breakFO);
            }

            return breakFOs;
        }

        public static void AddAreaFactoryObject(AreaFactoryObject newFactoryObject)
        {
            areaFOs.Add(newFactoryObject);
            ApplyAreaObjectPassPlaceMasks(newFactoryObject);
            newFactoryObject.Initialize();

            Machine newMachine = newFactoryObject as Machine;
            if (newMachine != null)
                MachineManager.OnMachineAdded(newMachine);
        }
        public static void AddWallMountFactoryObject(WallMountFactoryObject newWallMountFactoryObject)
        {
            wallMountFOs.Add(newWallMountFactoryObject);
        }

        public static void RemoveAreaFactoryObject(AreaFactoryObject factoryObject)
        {
            areaFORemovalList.Add(factoryObject);
        }
        public static void RemoveWallMountFactoryObject(WallMountFactoryObject wallMountFactoryObject)
        {
            wallMountFORemovalList.Add(wallMountFactoryObject);
        }

        // -------- Private Methods ---------
        private static void UpdateAreaFORemovalList()
        {
            if (areaFORemovalList.Count == 0)
                return;

            foreach (AreaFactoryObject afo in areaFORemovalList)
            {
                afo.Denitialize();
                if (!areaFOs.Remove(afo))
                    Debug.LogWarning("Tried to remove area factory object but wasn't found: " + afo.ToString());
            }

            areaFORemovalList.Clear();
        }
        private static void UpdateWallMountFORemovalList()
        {
            if (wallMountFORemovalList.Count == 0)
                return;

            foreach (WallMountFactoryObject wmfo in wallMountFORemovalList)
            {
                wmfo.Denitialize();
                if (!wallMountFOs.Remove(wmfo))
                    Debug.LogWarning("Tried to remove wall mount factory object but wasn't found: " + wmfo.ToString());
            }

            wallMountFORemovalList.Clear();
        }

        private static void ApplyAreaObjectPassPlaceMasks(AreaFactoryObject factoryObject)
        {
            PassPlaceMapManager.ApplyPassableFloorMask(factoryObject.AreaCoordinate, factoryObject.PassableMask);
            if (factoryObject.AreaCoordinate.OnCeiling)
                PassPlaceMapManager.ApplyPlacableCeilingMask(factoryObject.AreaCoordinate, factoryObject.PlacableMask);
            else
                PassPlaceMapManager.ApplyPlacableFloorMask(factoryObject.AreaCoordinate, factoryObject.PlacableMask);
        }

        // DEBUG
        private static void SetUpDebugMachines()
        {
            Machine m = new Machine(new TileCoordinate(5, 4), eSquareRotation.ROTATION_90, AlmanacManager.GetMachine("test-machine"));

            AddAreaFactoryObject(m);
            Element e = new Element(AlmanacManager.GetElement("test-element-1"), new ElementQuality(0.4f), false);
            m.InElementStore.AddElement(e);
        }
    }
}
