﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    static class FactoryPortManager
    {
        // -------- Properties --------
        public static int InStoreSize { get { return 12; } }
        public static int OutStoreSize { get { return 12; } }

        // -------- Private Fields --------
        private static FactoryPort inPort;
        private static FactoryPort outPort;

        // -------- Public Methods --------
        public static void Initialize()
        {
            CreateDebugPorts();
        }

        public static void Denitialize()
        {
            inPort.Denitialize();
            inPort = null;
            outPort.Denitialize();
            outPort = null;
        }

        public static void Update()
        {
            inPort.Update();
            outPort.Update();
        }

        public static void Draw()
        {
            inPort.Draw();
            outPort.Draw();
        }

        public static List<InteractableWallMountFactoryObject> GetAllPorts()
        {
            var ports = new List<InteractableWallMountFactoryObject>();
            ports.Add(inPort);
            ports.Add(outPort);
            return ports;
        }

        public static FactoryPort GetFactoryInPort()
        {
            return inPort;
        }
        public static FactoryPort GetFactoryOutPort()
        {
            return outPort;
        }

        public static void DeliverElementsIn(List<Element> elements)
        {
            foreach (Element e in elements)
                inPort.ElementStore.AddElement(e);
        }

        public static void DeliverElementsOut()
        {
            outPort.ElementStore.ClearElements();
        }

        public static bool ElementIsDeliverableOut(ElementData element)
        {
            return ContractManager.ElementIsExported(element);
        }

        // -------- Private Methods --------
        // DEBUG
        private static void CreateDebugPorts()
        {
            inPort = new FactoryPort(new WallMountCoordinate(new WallCoordinate(new TileCoordinate(5, 2), eWallDirection.WEST), eWallMountLocation.INSIDE), true);
            inPort.Initialize();

            for (int i = 0; i < 7; i++)
                inPort.ElementStore.AddElement(new Element(AlmanacManager.GetElement("test-element-1"), new ElementQuality(0.5f), false));

            outPort = new FactoryPort(new WallMountCoordinate(new WallCoordinate(new TileCoordinate(7, 2), eWallDirection.WEST), eWallMountLocation.INSIDE), false);
            outPort.Initialize();
        }
    }
}
