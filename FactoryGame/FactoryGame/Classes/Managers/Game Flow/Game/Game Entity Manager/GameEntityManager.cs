﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    static class GameEntityManager
    {
        // -------- Public Methods --------
        public static void Initialize()
        {
            DoorManager.Initialize();
            WindowManager.Initialize();
            FactoryObjectManager.Initialize();
            TrashManager.Initialize();
            MachineManager.Initialize();
            FactoryPortManager.Initialize();
        }

        public static void Denitialize()
        {
            DoorManager.Denitialize();
            WindowManager.Denitialize();
            FactoryObjectManager.Denitialize();
            TrashManager.Denitialize();
            MachineManager.Denitialize();
            FactoryPortManager.Denitialize();
        }

        public static void Update()
        {
            DoorManager.Update();
            WindowManager.Update();
            FactoryObjectManager.Update();
            TrashManager.Update();
            FactoryPortManager.Update();
        }

        public static void Draw()
        {
            DoorManager.Draw();
            WindowManager.Draw();
            FactoryObjectManager.Draw();
            TrashManager.Draw();
            FactoryPortManager.Draw();
        }

        public static void DrawTransparent()
        {
            DoorManager.DrawTransparent();
            WindowManager.DrawTransparent();
            FactoryObjectManager.DrawTransparent();
        }

        public static List<GameEntity> GetAllGameEntities()
        {
            List<GameEntity> gameEntities = new List<GameEntity>();

            gameEntities.AddRange(FloorManager.GetAllGameEntities());
            gameEntities.AddRange(DoorManager.GetAllDoors());
            gameEntities.AddRange(WindowManager.GetAllWindows());
            gameEntities.AddRange(FactoryObjectManager.GetAllGameEntities());
            gameEntities.AddRange(TrashManager.GetAllTrash());
            gameEntities.AddRange(FactoryPortManager.GetAllPorts());

            gameEntities.AddRange(CharacterManager.GetAllCharacters());

            return gameEntities;
        }
    }
}
