﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    // -------- Enumerations --------
    public enum eMachineOperationType
    {
        MANUAL,
        AUTOMATED
    }

    static class MachineManager
    {
        // -------- Properties --------
        public static int DefaultInStoreSize { get { return 6; } }
        public static int DefaultOutStoreSize { get { return 6; } }

        // -------- Public Methods --------
        public static void Initialize()
        {
        }

        public static void Denitialize()
        {
        }

        public static void OnMachineAdded(Machine machine)
        {
            MachineUsageManager.OnMachineAdded(machine);
        }

        public static string GetTextureIdForMachineState(Machine machine, out bool stateIsOperate)
        {
            if (machine.Broken)
            {
                stateIsOperate = false;

                if (machine.IsBeingFixed())
                    return "staff-task-fix";

                return "machine-state-broken";
            }

            if (machine.IsBeingOperated())
            {
                ProcessData process = machine.GetCurrentProcess();
                stateIsOperate = true;
                return process.ElementOut.IconTextureId;
            }

            stateIsOperate = false;
            return "staff-task-idle";
        }

        public static List<ProcessData> GetProcessesPerformedOnOperatingStation(Machine machine)
        {
            List<ProcessData> processes = new List<ProcessData>();

            foreach (Staff s in StaffManager.GetAllStaff())
            {
                foreach (Task t in s.Tasks.Tasks)
                {
                    OperateTask ot = t as OperateTask;
                    if (ot == null)
                        continue;

                    Machine m = ot.OperateInteractability.Parent as Machine;
                    if (!m.Equals(machine))
                        continue;

                    processes.Add(ot.OperationProcess);
                }
            }

            return processes;
        }
    }
}
