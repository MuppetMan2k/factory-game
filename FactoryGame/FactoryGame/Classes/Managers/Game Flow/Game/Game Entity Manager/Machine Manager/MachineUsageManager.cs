﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    static class MachineUsageManager
    {
        // -------- Public Methods --------
        public static void OnMachineAdded(Machine machine)
        {
            // TODO
        }

        public static List<MachineUsage> GetMachineUsages(Machine machine)
        {
            var usages = new List<MachineUsage>();

            foreach (Staff s in StaffManager.GetAllStaff())
            {
                foreach (Task t in s.Tasks.Tasks)
                {
                    OperateTask ot = t as OperateTask;
                    if (ot != null)
                    {
                        Machine taskMachine = ot.OperateInteractability.Parent as Machine;
                        if (taskMachine != null && taskMachine.Equals(machine))
                        {
                            MachineUsage mu = new MachineUsage();
                            mu.Process = ot.OperationProcess;
                            mu.Staff = s;

                            usages.Add(mu);
                        }
                    }
                }
            }

            return usages;
        }

        public static float GetMachineUsageFraction(Machine machine)
        {
            // TODO
            return 0.72f;
        }
    }
}
