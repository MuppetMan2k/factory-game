﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    static class TrashManager
    {
        // -------- Properties --------
        public static float TrashPositionSpread { get { return 0.2f; } }
        public static float TrashPickupTime { get { return 3.0f; } }

        // -------- Private Fields --------
        private static Trash[,] trashMap;
        private static List<Trash> trashRemovalList;

        // -------- Public Methods --------
        public static void Initialize()
        {
            CreateTrashMap();
            trashRemovalList = new List<Trash>();
        }

        public static void Denitialize()
        {
            foreach (Trash t in GetAllTrash())
                t.Denitialize();

            trashMap = null;
            trashRemovalList = null;
        }

        public static void Update()
        {
            List<Trash> trashList = GetAllTrash();

            foreach (Trash t in trashList)
                t.Update();

            UpdateRemovalList();
        }

        public static void Draw()
        {
            List<Trash> trashList = GetAllTrash();

            foreach (Trash t in trashList)
                t.Draw();
        }

        public static void OnCharacterStepOnTile(Character character, TileCoordinate coord)
        {
            // TODO
        }

        public static bool TrashExists(TileCoordinate coord)
        {
            return (trashMap[coord.Row, coord.Col] != null);
        }

        public static void AddTrash(TileCoordinate coord)
        {
            trashMap[coord.Row, coord.Col] = new Trash(coord);
            trashMap[coord.Row, coord.Col].Initialize();
        }

        public static List<Trash> GetAllTrash()
        {
            List<Trash> allTrash = new List<Trash>();

            for (int row = 0; row < FloorManager.FloorHeight; row++)
            {
                for (int col = 0; col < FloorManager.FloorWidth; col++)
                {
                    if (trashMap[row, col] != null)
                        allTrash.Add(trashMap[row, col]);
                }
            }

            return allTrash;
        }

        public static void RemoveTrash(Trash trashToRemove)
        {
            trashRemovalList.Add(trashToRemove);
        }

        public static string GetRandomTrashModelId()
        {
            // TODO
            return "trash-1";
        }

        // -------- Private Methods ---------
        private static void CreateTrashMap()
        {
            trashMap = new Trash[FloorManager.FloorHeight, FloorManager.FloorWidth];
            CreateDebugTrash();
        }

        private static void UpdateRemovalList()
        {
            if (trashRemovalList.Count == 0)
                return;

            foreach (Trash t in trashRemovalList)
            {
                TileCoordinate coord = t.MiniTileCoordinate.TileCoordinate;
                if (trashMap[coord.Row, coord.Col] != null)
                {
                    trashMap[coord.Row, coord.Col].Denitialize();
                    trashMap[coord.Row, coord.Col] = null;
                }
                else
                    Debug.LogWarning("Tried to remove trash but couldn't find it: " + t.ToString());
            }

            trashRemovalList.Clear();
        }

        // DEBUG
        private static void CreateDebugTrash()
        {
            AddTrash(new TileCoordinate(5, 3));
            AddTrash(new TileCoordinate(9, 4));
        }
    }
}
