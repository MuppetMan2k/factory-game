﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    static class WindowManager
    {
        // -------- Private Fields --------
        private static Window[, ,] windowMap;
        private static List<Window> windowRemovalList;

        // -------- Public Methods --------
        public static void Initialize()
        {
            CreateWindowMap();
            CreateDebugWindows();
            windowRemovalList = new List<Window>();
        }

        public static void Denitialize()
        {
            foreach (Window w in GetAllWindows())
                w.Denitialize();

            windowMap = null;
            windowRemovalList = null;
        }

        public static void Update()
        {
            List<Window> windows = GetAllWindows();

            foreach (Window w in windows)
                w.Update();

            UpdateRemovalList();
        }

        public static void Draw()
        {
            List<Window> windows = GetAllWindows();

            foreach (Window w in windows)
                w.Draw();
        }

        public static void DrawTransparent()
        {
            List<Window> windows = GetAllWindows();

            foreach (Window w in windows)
                w.DrawTransparent();
        }

        public static bool WindowExists(WallCoordinate coord)
        {
            if (!WallManager.WallCoordinateExists(coord))
                return false;

            return (GetWindow(coord) != null);
        }

        public static Window GetWindow(WallCoordinate coord)
        {
            if (!WallManager.WallCoordinateExists(coord))
                return null;

            return windowMap[coord.Coordinate.Row, coord.Coordinate.Col, WallUtilities.ConvertWallDirectionToMapIndex(coord.Direction)];
        }

        public static List<Window> GetAllWindows()
        {
            var windows = new List<Window>();

            for (int r = 0; r <= FloorManager.FloorHeight; r++)
            {
                for (int c = 0; c <= FloorManager.FloorWidth; c++)
                {
                    for (int d = 0; d <= 1; d++)
                    {
                        Window window = windowMap[r, c, d];

                        if (window != null)
                            windows.Add(window);
                    }
                }
            }

            return windows;
        }

        public static void RemoveWindow(Window windowToRemove)
        {
            windowRemovalList.Add(windowToRemove);
        }

        // -------- Private Methods ---------
        private static void CreateWindowMap()
        {
            windowMap = new Window[FloorManager.FloorHeight + 1, FloorManager.FloorWidth + 1, 2];
        }

        private static void UpdateRemovalList()
        {
            if (windowRemovalList.Count == 0)
                return;

            foreach (Window w in windowRemovalList)
            {
                WallMountCoordinate coord = w.WallMountCoordinate;
                int dir = WallUtilities.ConvertWallDirectionToMapIndex(coord.WallCoordinate.Direction);
                if (windowMap[coord.WallCoordinate.Coordinate.Row, coord.WallCoordinate.Coordinate.Col, dir] != null)
                {
                    windowMap[coord.WallCoordinate.Coordinate.Row, coord.WallCoordinate.Coordinate.Col, dir].Denitialize();
                    windowMap[coord.WallCoordinate.Coordinate.Row, coord.WallCoordinate.Coordinate.Col, dir] = null;
                }
                else
                    Debug.LogWarning("Tried to remove window but couldn't find it: " + w.ToString());
            }
        }

        // DEBUG
        private static void CreateDebugWindows()
        {
            windowMap[2, 3, 0] = new Window(new WallCoordinate(2, 3, eWallDirection.NORTH));
            windowMap[2, 3, 0].Initialize();
            windowMap[5, 2, 1] = new Window(new WallCoordinate(5, 2, eWallDirection.WEST));
            windowMap[5, 2, 1].Initialize();
            windowMap[7, 2, 1] = new Window(new WallCoordinate(7, 2, eWallDirection.WEST));
            windowMap[7, 2, 1].Initialize();
            windowMap[9, 2, 1] = new Window(new WallCoordinate(9, 2, eWallDirection.WEST));
            windowMap[9, 2, 1].Initialize();
            windowMap[5, 7, 1] = new Window(new WallCoordinate(5, 7, eWallDirection.WEST));
            windowMap[5, 7, 1].Initialize();
            windowMap[8, 7, 1] = new Window(new WallCoordinate(8, 7, eWallDirection.WEST));
            windowMap[8, 7, 1].Initialize();
            windowMap[10, 3, 0] = new Window(new WallCoordinate(10, 3, eWallDirection.NORTH));
            windowMap[10, 3, 0].Initialize();

            RoomManager.UpdateAllRoomFactors();
        }
    }
}
