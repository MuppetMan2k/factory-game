﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    static class EffectHighlightManager
    {
        // -------- Private Fields --------
        private const float HIGHLIGHT_WHITE_LERP = 0.2f;

        // -------- Public Methods --------
        public static float GetEffectWhiteLerp(bool highlight)
        {
            if (!highlight)
                return 0f;

            return HIGHLIGHT_WHITE_LERP;
        }
    }
}
