﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FactoryGame
{
    static class EffectManager
    {
        // -------- Public Methods --------
        public static string GetEffectName()
        {
            return "shaded";
        }

        public static void ApplySettingsToEffect(Effect effect, EffectSettings settings)
        {
            effect.CurrentTechnique = effect.Techniques[settings.Technique];

            effect.Parameters["xWorld"].SetValue(settings.World);
            effect.Parameters["xView"].SetValue(settings.View);
            effect.Parameters["xProjection"].SetValue(settings.Projection);

            effect.Parameters["xDirLightColor"].SetValue(settings.DirectLightColor);
            effect.Parameters["xDirLightIntensity"].SetValue(settings.DirectLightIntensity);
            effect.Parameters["xDirLightDir"].SetValue(settings.DirectLightDirection);
            effect.Parameters["xAmbLightColor"].SetValue(settings.AmbientLightColor);
            effect.Parameters["xAmbLightIntensity"].SetValue(settings.AmbientLightIntensity);

            effect.Parameters["xAlpha"].SetValue(settings.Alpha);
            effect.Parameters["xWhiteLerp"].SetValue(settings.WhiteLerp);
            effect.Parameters["xForcedColor"].SetValue(settings.ForcedColor);
        }
    }
}
