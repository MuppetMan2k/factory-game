﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FactoryGame
{
    static class EffectSettingsManager
    {
        // -------- Public Methods --------
        public static EffectSettings GetEffectSettings(GameEntity entity, Room room, Matrix model, Matrix world, LightSet lights, bool highlight)
        {
            if (DataViewManager.DataViewForcesColor())
                return CreateForcedColorEffectSettings(entity, room, model, world, lights, highlight);

            if (room != null && !room.Purchased)
                return CreateMonochromeEffectSettings(entity, model, world, lights, highlight);

            return CreateShadedEffectSettings(entity, model, world, lights, highlight);
        }

        // -------- Private Methods ---------
        private static EffectSettings CreateShadedEffectSettings(GameEntity entity, Matrix model, Matrix world, LightSet lights, bool highlight)
        {
            var settings = new EffectSettings();

            settings.Technique = "shaded";

            settings.World = model * world;
            settings.View = CameraManager.ViewMatrix;
            settings.Projection = CameraManager.ProjectionMatrix;

            settings.DirectLightColor = lights.Directional.Color.ToVector3();
            settings.DirectLightIntensity = lights.Directional.Intensity;
            settings.DirectLightDirection = lights.Directional.Direction;

            settings.AmbientLightColor = lights.Ambient.Color.ToVector3();
            settings.AmbientLightIntensity = lights.Ambient.Intensity;

            settings.Alpha = EffectTransparencyManager.GetEffectAlpha(entity);
            settings.WhiteLerp = EffectHighlightManager.GetEffectWhiteLerp(highlight);

            return settings;
        }
        private static EffectSettings CreateMonochromeEffectSettings(GameEntity entity, Matrix model, Matrix world, LightSet lights, bool highlight)
        {
            EffectSettings settings = CreateShadedEffectSettings(entity, model, world, lights, highlight);
            settings.Technique = "monochrome";
            return settings;
        }
        private static EffectSettings CreateForcedColorEffectSettings(GameEntity entity, Room room, Matrix model, Matrix world, LightSet lights, bool highlight)
        {
            EffectSettings settings = CreateShadedEffectSettings(entity, model, world, lights, highlight);
            settings.Technique = "forcedColor";
            settings.Alpha = 1f;
            settings.WhiteLerp = 0f;
            settings.ForcedColor = DataViewManager.GetDataViewForcedColor(entity, room);

            return settings;
        }
    }
}
