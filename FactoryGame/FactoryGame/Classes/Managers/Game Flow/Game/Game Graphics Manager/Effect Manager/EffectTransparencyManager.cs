﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    static class EffectTransparencyManager
    {
        // -------- Private Fields --------
        private static bool fadeEntities = false;
        // Parameters
        private const float FADED_ENTITY_ALPHA = 0.1f;

        // -------- Public Methods --------
        public static bool ShouldDrawTransparent(GameEntity entity)
        {
            // Wall mount entity on a cut-away wall
            WallMountEntity wallMountEntity = entity as WallMountEntity;
            if (wallMountEntity != null)
            {
                WallPillar pillar1;
                WallPillar pillar2;
                WallCutAwayManager.GetWallPillars(wallMountEntity.WallMountCoordinate.WallCoordinate, out pillar1, out pillar2);

                if (pillar1.Height == eWallPillarHeight.CUT || pillar2.Height == eWallPillarHeight.CUT)
                    return true;
            }

            // Area entity on ceiling
            AreaEntity areaEntity = entity as AreaEntity;
            if (areaEntity != null)
                if (areaEntity.AreaCoordinate.OnCeiling)
                    return true;

            return false;
        }

        public static float GetEffectAlpha(GameEntity entity)
        {
            if (ShouldDrawTransparent(entity))
                return GetFadedEntityAlpha();
            else
                return 1f;
        }

        // -------- Private Methods ---------
        private static float GetFadedEntityAlpha()
        {
            return (fadeEntities ? FADED_ENTITY_ALPHA : 0f);
        }
    }
}
