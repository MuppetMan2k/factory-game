﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FactoryGame
{
    static class GameBackgroundManager
    {
        // -------- Private Fields --------
        private static Color backgroundColor;
        // Parameters
        private static Color defaultColor = Color.White;
        private const float SUN_COLOR_LERP = 0.3f;

        // -------- Public Methods --------
        public static void Update()
        {
            Color sunColor = SunLightManager.GetLightColor();
            backgroundColor = Color.Lerp(defaultColor, sunColor, SUN_COLOR_LERP);
        }

        public static void Draw()
        {
            GraphicsManager.GetGraphicsDevice().Clear(backgroundColor);
        }
    }
}
