﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    static class GameGraphicsManager
    {
        // -------- Public Methods --------
        public static void Initialize()
        {
            LightingManager.Initialize();
        }

        public static void Denitialize()
        {
            LightingManager.Denitialize();
        }

        public static void Update()
        {
            LightingManager.Update();
            GameBackgroundManager.Update();
        }

        public static void Draw()
        {
            GameBackgroundManager.Draw();
            FloorManager.Draw();
            GameEntityManager.Draw();
            CharacterManager.Draw();
        }

        public static void DrawTransparent()
        {
            FloorManager.DrawTransparent();
            GameEntityManager.DrawTransparent();
        }

        public static void SBDraw()
        {
            UIManager.SBDraw();
        }

        public static void SBDrawToRenderTarget()
        {
            UIManager.SBDrawToRenderTarget();
        }
    }
}
