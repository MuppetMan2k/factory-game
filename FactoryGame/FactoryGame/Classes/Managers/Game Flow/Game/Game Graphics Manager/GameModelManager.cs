﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FactoryGame
{
    static class GameModelManager
    {
        // -------- Public Methods --------
        public static GameModel CreateGameModel(Model model)
        {
            GameModel gameModel = new GameModel();
            gameModel.Model = model;

            Matrix[] bones = new Matrix[gameModel.Model.Bones.Count];
            gameModel.Model.CopyAbsoluteBoneTransformsTo(bones);
            gameModel.SetBoneTransforms(bones);

            SetUpModelEffects(gameModel, model);
            SetUpGameModelTriangles(gameModel);
            return gameModel;
        }

        // -------- Private Methods ---------
        private static void SetUpModelEffects(GameModel gameModel, Model model)
        {
            string colorEffectId = EffectManager.GetEffectName() + "-color";
            Effect colorEffect = ContentStorage.GetEffect(colorEffectId);
            string textureEffectId = EffectManager.GetEffectName() + "-texture";
            Effect textureEffect = ContentStorage.GetEffect(textureEffectId);

            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (ModelMeshPart part in mesh.MeshParts)
                {
                    BasicEffect basicEffect = part.Effect as BasicEffect;
                    if (basicEffect != null)
                    {
                        if (basicEffect.TextureEnabled)
                        {
                            Texture2D texture = basicEffect.Texture;
                            part.Effect = textureEffect.Clone();
                            part.Effect.Parameters["xTexture"].SetValue(texture);
                        }
                        else
                        {
                            Vector3 color = basicEffect.DiffuseColor;
                            part.Effect = colorEffect.Clone();
                            part.Effect.Parameters["xColor"].SetValue(color);
                        }
                    }
                }
            }
        }

        private static void SetUpGameModelTriangles(GameModel gameModel)
        {
            gameModel.ModelTriangles = new List<Triangle>();

            foreach (ModelMesh mesh in gameModel.Model.Meshes)
            {
                Matrix boneTransform = gameModel.GetBoneTransforms()[mesh.ParentBone.Index];

                foreach (ModelMeshPart part in mesh.MeshParts)
                {
                    float[] vertices = new float[part.VertexBuffer.VertexCount * part.VertexBuffer.VertexDeclaration.VertexStride / 4];
                    part.VertexBuffer.GetData<float>(vertices);

                    VertexElement[] vertexElements = part.VertexBuffer.VertexDeclaration.GetVertexElements();

                    int vertexOffset = vertexElements.First(e => e.VertexElementUsage == VertexElementUsage.Position).Offset / 4;

                    short[] indices = new short[part.IndexBuffer.IndexCount];
                    part.IndexBuffer.GetData<short>(indices);

                    for (int i = part.StartIndex; i < part.IndexBuffer.IndexCount; i += 3)
                    {
                        // TODO - Find out why the vertex look-up index goes out of bounds when using a UV-texture-mapped model

                        int index1 = indices[i];
                        int index2 = indices[i + 1];
                        int index3 = indices[i + 2];

                        int vertexBaseIndex1 = index1 * (part.VertexBuffer.VertexDeclaration.VertexStride / 4);
                        int vertexBaseIndex2 = index2 * (part.VertexBuffer.VertexDeclaration.VertexStride / 4);
                        int vertexBaseIndex3 = index3 * (part.VertexBuffer.VertexDeclaration.VertexStride / 4);

                        float v1x = vertices[vertexBaseIndex1 + vertexOffset];
                        float v1y = vertices[vertexBaseIndex1 + vertexOffset + 1];
                        float v1z = vertices[vertexBaseIndex1 + vertexOffset + 2];

                        Vector3 v1 = new Vector3(v1x, v1y, v1z);

                        float v2x = vertices[vertexBaseIndex2 + vertexOffset];
                        float v2y = vertices[vertexBaseIndex2 + vertexOffset + 1];
                        float v2z = vertices[vertexBaseIndex2 + vertexOffset + 2];

                        Vector3 v2 = new Vector3(v2x, v2y, v2z);

                        float v3x = vertices[vertexBaseIndex3 + vertexOffset];
                        float v3y = vertices[vertexBaseIndex3 + vertexOffset + 1];
                        float v3z = vertices[vertexBaseIndex3 + vertexOffset + 2];

                        Vector3 v3 = new Vector3(v3x, v3y, v3z);

                        gameModel.ModelTriangles.Add(new Triangle(v1, v2, v3, mesh.Name, mesh.ParentBone.Index));
                    }
                }
            }
        }
    }
}
