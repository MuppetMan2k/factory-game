﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    static class AmbientLightingManager
    {
        // -------- Private Fields --------
        private static AmbientLight worldAmbientLight;
        // Parameters
        private const float MAX_SUN_AMBIENT_LIGHT_INTENSITY = 0.5f;
        private const float MIN_AMBIENT_LIGHT_INTENSITY = 0.15f;
        private const float INTERIOR_LIGHT_MULTIPLIER = 0.75f;

        // -------- Public Methods --------
        public static void Initialize()
        {
            worldAmbientLight = new AmbientLight();
        }

        public static void Denitialize()
        {
            worldAmbientLight = null;
        }

        public static void Update()
        {
            worldAmbientLight.Color = SunLightManager.GetLightColor();
            worldAmbientLight.Intensity = MAX_SUN_AMBIENT_LIGHT_INTENSITY * SunLightManager.GetSunIntensityFraction();
        }

        public static AmbientLight GetInteriorAmbientLight(Room room)
        {
            AmbientLight interiorAmbientLight = (AmbientLight)worldAmbientLight.Clone();
            interiorAmbientLight.Intensity = worldAmbientLight.Intensity * room.GetSpecificWindowFactor() * INTERIOR_LIGHT_MULTIPLIER;

            if (room.LightsOn)
                interiorAmbientLight = CombineAmbientLights(interiorAmbientLight, room.AmbientLight);

            return NormalizeAmbientLight(interiorAmbientLight);
        }

        public static AmbientLight GetExteriorAmbientLight()
        {
            return NormalizeAmbientLight((AmbientLight)worldAmbientLight.Clone());
        }

        public static AmbientLight MultiplyAmbientLightIntensity(AmbientLight light, float multiplier)
        {
            AmbientLight multiLight = (AmbientLight)light.Clone();
            multiLight.Intensity *= multiplier;

            return multiLight;
        }

        public static AmbientLight CombineAmbientLights(AmbientLight light1, AmbientLight light2)
        {
            AmbientLight combined = new AmbientLight();
            combined.Intensity = 1.0f;
            Vector3 newColor = light1.Intensity * light1.Color.ToVector3() + light2.Intensity * light2.Color.ToVector3();
            combined.Color = new Color(newColor);

            return combined;
        }

        // -------- Private Methods ---------
        private static AmbientLight NormalizeAmbientLight(AmbientLight light)
        {
            light.Intensity = Mathf.ClampLower(light.Intensity, MIN_AMBIENT_LIGHT_INTENSITY);
            Vector3 color = light.Color.ToVector3();
            color.X = Mathf.Clamp01(color.X);
            color.Y = Mathf.Clamp01(color.Y);
            color.Z = Mathf.Clamp01(color.Z);

            return light;
        }
    }
}
