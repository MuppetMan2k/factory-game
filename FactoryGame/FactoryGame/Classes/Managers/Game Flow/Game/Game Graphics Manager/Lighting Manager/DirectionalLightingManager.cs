﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    static class DirectionalLightingManager
    {
        // -------- Private Fields --------
        private static DirectionalLight worldDirectionalLight;
        // Parameters
        private const float MAX_LIGHT_INTENSITY = 0.8f;
        private const float INTERIOR_LIGHT_MULTIPLIER = 0.75f;

        // -------- Public Methods --------
        public static void Initialize()
        {
            worldDirectionalLight = new DirectionalLight();
        }

        public static void Denitialize()
        {
            worldDirectionalLight = null;
        }

        public static void Update()
        {
            worldDirectionalLight.Color = SunLightManager.GetLightColor();
            worldDirectionalLight.Intensity = MAX_LIGHT_INTENSITY * SunLightManager.GetSunIntensityFraction();
            worldDirectionalLight.Direction = SunLightManager.SunDirection;
        }

        public static DirectionalLight GetInteriorDirectionalLight(Room room)
        {
            DirectionalLight interiorDirectionalLight = (DirectionalLight)worldDirectionalLight.Clone();
            interiorDirectionalLight.Intensity *= room.GetSpecificWindowFactor() * INTERIOR_LIGHT_MULTIPLIER;
            return interiorDirectionalLight;
        }

        public static DirectionalLight GetExteriorDirectionalLight()
        {
            return worldDirectionalLight;
        }
    }
}
