﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    static class LightingManager
    {
        // -------- Public Methods --------
        public static void Initialize()
        {
            DirectionalLightingManager.Initialize();
            AmbientLightingManager.Initialize();
        }

        public static void Denitialize()
        {
            DirectionalLightingManager.Denitialize();
            AmbientLightingManager.Denitialize();
        }

        public static void Update()
        {
            SunLightManager.Update();
            DirectionalLightingManager.Update();
            AmbientLightingManager.Update();
        }

        public static LightSet GetLightSet(TileCoordinate coord)
        {
            if (TileManager.TileIsInFactory(coord))
                return GetInteriorLightSet(RoomManager.GetRoom(coord));
            else
                return GetExteriorLightSet();
        }
        public static LightSet GetLightSet(AreaCoordinate area)
        {
            return GetLightSet(area.Coordinate);
        }
        public static LightSet GetLightSet(WallFaceCoordinate coord)
        {
            return GetLightSet(WallUtilities.GetTileCoordAdjacentToWallFace(coord));
        }
        public static LightSet GetLightSet(Room room)
        {
            if (room == null)
                return GetExteriorLightSet();
            else
                return GetInteriorLightSet(room);
        }

        // -------- Private Methods ---------
        private static LightSet GetInteriorLightSet(Room room)
        {
            var ls = new LightSet();
            if (room.Purchased)
                ls.Directional = DirectionalLightingManager.GetInteriorDirectionalLight(room);
            else
                ls.Directional = DirectionalLight.Zero;
            ls.Ambient = AmbientLightingManager.GetInteriorAmbientLight(room);

            return ls;
        }
        private static LightSet GetExteriorLightSet()
        {
            var ls = new LightSet();
            ls.Directional = DirectionalLightingManager.GetExteriorDirectionalLight();
            ls.Ambient = AmbientLightingManager.GetExteriorAmbientLight();

            return ls;
        }
    }
}
