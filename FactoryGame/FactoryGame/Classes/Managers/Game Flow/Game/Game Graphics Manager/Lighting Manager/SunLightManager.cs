﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    static class SunLightManager
    {
        // -------- Properties --------
        public static Vector3 SunDirection { get; private set; }

        // -------- Private Fields --------
        // Parameters
        private const float SUN_RISE_TIME = 6f;
        private const float SUN_SET_TIME = 18f;
        private const float SUN_RISE_ROTATION_ANGLE = 90f;
        private const float SUN_SET_ROTATION_ANGLE = 270f;
        private const float SUN_MAX_LIFT_ANGLE = 60f;
        private static List<SunLightColorEntry> lightColorMap = new List<SunLightColorEntry>()
        {
            new SunLightColorEntry(0f, Color.White),
            new SunLightColorEntry(5f, Color.Orange),
            new SunLightColorEntry(6f, Color.Orange),
            new SunLightColorEntry(9f, Color.LightYellow),
            new SunLightColorEntry(12f, Color.White),
            new SunLightColorEntry(16f, Color.White),
            new SunLightColorEntry(18f, Color.PeachPuff),
            new SunLightColorEntry(19f, Color.White),
            new SunLightColorEntry(24f, Color.White)
        };

        // -------- Public Methods --------
        public static void Update()
        {
            SetSunDirection();
        }

        public static Color GetLightColor()
        {
            float time = GameClockManager.GetClockTime();

            for (int i = 0; i < lightColorMap.Count; i++)
            {
                SunLightColorEntry entry = lightColorMap[i];

                if (time == entry.Time)
                    return entry.Color;

                SunLightColorEntry nextEntry = lightColorMap[i + 1];

                if (time > entry.Time && time < nextEntry.Time)
                {
                    float interp = (time - entry.Time) / (nextEntry.Time - entry.Time);
                    return Color.Lerp(entry.Color, nextEntry.Color, interp);
                }
            }

            return lightColorMap[0].Color;
        }

        public static float GetSunIntensityFraction()
        {
            return (GetSunLiftAngle() / SUN_MAX_LIFT_ANGLE);
        }

        // -------- Private Methods --------
        private static void SetSunDirection()
        {
            Vector3 direction = new Vector3(0f, 0f, 1f);
            direction = VectorUtilities.RotateVectorAboutY(direction, -GetSunRotationAngle());

            Vector3 left = VectorUtilities.RotateVectorAboutY(direction, 90f);

            direction = VectorUtilities.RotateVectorAboutAxis(direction, left, GetSunLiftAngle());

            SunDirection = direction;
        }

        private static float GetSunRotationAngle()
        {
            return SUN_RISE_ROTATION_ANGLE + GetSunLifeFraction() * (SUN_SET_ROTATION_ANGLE - SUN_RISE_ROTATION_ANGLE);
        }

        private static float GetSunLiftAngle()
        {
            return (SUN_MAX_LIFT_ANGLE * Mathf.SafeSqrt(1 - Mathf.Square(2 * GetSunLifeFraction() - 1)));
        }

        private static float GetSunLifeFraction()
        {
            float time = GameClockManager.GetClockTime();

            float lifeFraction = (time - SUN_RISE_TIME) / (SUN_SET_TIME - SUN_RISE_TIME);
            lifeFraction = Mathf.Clamp01(lifeFraction);

            return lifeFraction;
        }
    }
}
