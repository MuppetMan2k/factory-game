﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FactoryGame
{
    static class ModelGraphicsManager
    {
        // -------- Public Methods --------
        public static void DrawTileModel(TileEntity entity)
        {
            Room room = RoomManager.GetRoom(entity.TileCoordinate);

            Matrix world = WorldMatrixManager.GetWorldMatrix(entity.TileCoordinate);
            GameModel model = entity.Model;
            LightSet lights = LightingManager.GetLightSet(room);

            foreach (ModelMesh mesh in model.Model.Meshes)
            {
                Matrix boneTransform = model.GetBoneTransforms()[mesh.ParentBone.Index];
                EffectSettings settings = EffectSettingsManager.GetEffectSettings(entity, room, boneTransform, world, lights, entity.DrawHighlight);

                DrawMesh(mesh, settings);
            }
        }

        public static void DrawMiniTileModel(MiniTileEntity entity)
        {
            Room room = RoomManager.GetRoom(entity.MiniTileCoordinate.TileCoordinate);

            Matrix world = WorldMatrixManager.GetWorldMatrix(entity.MiniTileCoordinate);
            GameModel model = entity.Model;
            LightSet lights = LightingManager.GetLightSet(room);

            foreach (ModelMesh mesh in model.Model.Meshes)
            {
                Matrix boneTransform = model.GetBoneTransforms()[mesh.ParentBone.Index];
                EffectSettings settings = EffectSettingsManager.GetEffectSettings(entity, room, boneTransform, world, lights, entity.DrawHighlight);

                DrawMesh(mesh, settings);
            }
        }

        public static void DrawAreaModel(AreaEntity entity)
        {
            Room room = RoomManager.GetRoom(entity.AreaCoordinate.Coordinate);

            Matrix world = WorldMatrixManager.GetWorldMatrix(entity.AreaCoordinate);
            GameModel model = entity.Model;
            LightSet lights = LightingManager.GetLightSet(room);

            foreach (ModelMesh mesh in model.Model.Meshes)
            {
                Matrix boneTransform = model.GetBoneTransforms()[mesh.ParentBone.Index];
                EffectSettings settings = EffectSettingsManager.GetEffectSettings(entity, room, boneTransform, world, lights, entity.DrawHighlight);

                DrawMesh(mesh, settings);
            }
        }

        public static void DrawWallModels(Wall wall)
        {
            DrawWallFaceModel(wall, eWallFace.INSIDE);
            DrawWallFaceModel(wall, eWallFace.OUTSIDE);
        }

        public static void DrawWallMountedModel(WallMountEntity entity)
        {
            if (entity.WallMountCoordinate.Location == eWallMountLocation.INSIDE)
            {
                DrawWallMountedModelFace(entity, eWallFace.INSIDE);
            }
            else if (entity.WallMountCoordinate.Location == eWallMountLocation.OUTSIDE)
            {
                DrawWallMountedModelFace(entity, eWallFace.OUTSIDE);
            }
            else
            {
                // Through
                DrawWallMountedModelFace(entity, eWallFace.INSIDE);
                DrawWallMountedModelFace(entity, eWallFace.OUTSIDE);
            }
        }

        public static void DrawHingeDoorModels(HingeDoor door)
        {
            DrawHingeDoorModelFace(door, eWallFace.INSIDE);
            DrawHingeDoorModelFace(door, eWallFace.OUTSIDE);
        }

        public static void DrawCharacterModel(Character character)
        {
            Room room = RoomManager.GetRoom(character.CharacterCoordinate.GetApproxTileCoordinate());

            Matrix world = WorldMatrixManager.GetWorldMatrix(character.CharacterCoordinate);
            CharacterPosition position = character.CharacterCoordinate.Animation.GetPosition();
            LightSet lights = LightingManager.GetLightSet(room);

            foreach (ModelMesh mesh in character.CharacterModel.Body.Model.Meshes)
            {
                Matrix boneTransform = character.CharacterModel.Body.GetBoneTransforms()[mesh.ParentBone.Index];
                Matrix modelTransform = CharacterAnimationManager.GetCharacterMeshTransform(mesh.Name, position);
                EffectSettings settings = EffectSettingsManager.GetEffectSettings(character, room, boneTransform * modelTransform, world, lights, character.DrawHighlight);

                DrawMesh(mesh, settings);
            }

            foreach (ModelMesh mesh in character.CharacterModel.Head.Model.Meshes)
            {
                Matrix boneTransform = character.CharacterModel.Head.GetBoneTransforms()[mesh.ParentBone.Index];
                Matrix modelTransform = CharacterAnimationManager.GetCharacterMeshTransform("Head", position);
                EffectSettings settings = EffectSettingsManager.GetEffectSettings(character, room, boneTransform * modelTransform, world, lights, character.DrawHighlight);

                DrawMesh(mesh, settings);
            }

            if (character.CharacterModel.HeadPiece != null)
            {
                foreach (ModelMesh mesh in character.CharacterModel.HeadPiece.Model.Meshes)
                {
                    Matrix boneTransform = character.CharacterModel.HeadPiece.GetBoneTransforms()[mesh.ParentBone.Index];
                    Matrix modelTransform = CharacterAnimationManager.GetCharacterMeshTransform("HeadPiece", position);
                    EffectSettings settings = EffectSettingsManager.GetEffectSettings(character, room, boneTransform * modelTransform, world, lights, character.DrawHighlight);

                    DrawMesh(mesh, settings);
                }
            }

            Staff staff = character as Staff;
            if (staff != null && staff.IsCarryingElement())
            {
                GameModel elementModel = ContentStorage.GetModel("element");
                
                foreach (ModelMesh mesh in elementModel.Model.Meshes)
                {
                    Matrix boneTransform = elementModel.GetBoneTransforms()[mesh.ParentBone.Index];
                }
            }
        }

        // -------- Private Methods ---------
        private static void DrawWallFaceModel(Wall wall, eWallFace face)
        {
            WallFaceCoordinate faceCoord = new WallFaceCoordinate(wall.WallCoordinate, face);
            Room room = RoomManager.GetRoom(faceCoord);

            Matrix world = WorldMatrixManager.GetWorldMatrix(faceCoord);
            GameModel model = face == eWallFace.INSIDE ? wall.InnerModel : wall.OuterModel;
            LightSet lights = LightingManager.GetLightSet(room);

            foreach (ModelMesh mesh in model.Model.Meshes)
            {
                if (!WallMeshManager.WallMeshShouldBeDrawn(mesh.Name, faceCoord))
                    continue;

                Matrix boneTransform = model.GetBoneTransforms()[mesh.ParentBone.Index];
                EffectSettings settings = EffectSettingsManager.GetEffectSettings(wall, room, boneTransform, world, lights, wall.DrawHighlight);

                DrawMesh(mesh, settings);
            }
        }

        private static void DrawWallMountedModelFace(WallMountEntity entity, eWallFace face)
        {
            WallFaceCoordinate effectiveFaceCoord = new WallFaceCoordinate(entity.WallMountCoordinate.WallCoordinate, face);
            Room room = RoomManager.GetRoom(effectiveFaceCoord);

            Matrix world = WorldMatrixManager.GetWorldMatrix(effectiveFaceCoord);
            GameModel model = entity.Model;
            LightSet lights = LightingManager.GetLightSet(room);

            foreach (ModelMesh mesh in model.Model.Meshes)
            {
                Matrix boneTransform = model.GetBoneTransforms()[mesh.ParentBone.Index];
                EffectSettings settings = EffectSettingsManager.GetEffectSettings(entity, room, boneTransform, world, lights, entity.DrawHighlight);

                DrawMesh(mesh, settings);
            }
        }

        private static void DrawHingeDoorModelFace(HingeDoor door, eWallFace face)
        {
            WallFaceCoordinate effectiveFaceCoord = new WallFaceCoordinate(door.WallMountCoordinate.WallCoordinate, face);
            Room room = RoomManager.GetRoom(effectiveFaceCoord);

            Matrix world = WorldMatrixManager.GetHingeDoorWorldMatrix(door.WallMountCoordinate.WallCoordinate, door.Rotation);
            GameModel model = face == eWallFace.INSIDE ? door.InnerModel : door.OuterModel;
            LightSet lights = LightingManager.GetLightSet(room);

            foreach (ModelMesh mesh in model.Model.Meshes)
            {
                Matrix boneTransform = model.GetBoneTransforms()[mesh.ParentBone.Index];
                EffectSettings settings = EffectSettingsManager.GetEffectSettings(door, room, boneTransform, world, lights, door.DrawHighlight);

                DrawMesh(mesh, settings);
            }
        }

        private static void DrawMesh(ModelMesh mesh, EffectSettings settings)
        {
            foreach (ModelMeshPart part in mesh.MeshParts)
                EffectManager.ApplySettingsToEffect(part.Effect, settings);

            mesh.Draw();
        }
    }
}
