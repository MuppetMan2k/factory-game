﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    static class WorldMatrixManager
    {
        // -------- Public Methods --------
        public static Matrix GetWorldMatrix(TileCoordinate coord)
        {
            Matrix translation = MatrixUtilities.CreateTranslationMatrix(coord);

            return translation;
        }

        public static Matrix GetWorldMatrix(MiniTileCoordinate coord)
        {
            Matrix translation = MatrixUtilities.CreateTranslationMatrix((float)coord.TileCoordinate.Col + coord.Position.X, (float)coord.TileCoordinate.Row + coord.Position.Y);

            return translation;
        }

        public static Matrix GetWorldMatrix(AreaCoordinate coord)
        {
            Matrix centralizeTranslation = MatrixUtilities.CreateTranslationMatrix(-0.5f, -0.5f);
            Matrix decentralizeTranslation = MatrixUtilities.CreateTranslationMatrix(0.5f, 0.5f);
            Matrix rotation = MatrixUtilities.CreateRotationMatrix(coord.Rotation);
            Matrix translation = MatrixUtilities.CreateTranslationMatrix(coord.Coordinate);

            return centralizeTranslation * rotation * decentralizeTranslation * translation;
        }

        public static Matrix GetWorldMatrix(WallFaceCoordinate coord)
        {
            float angle = 0f;

            if (coord.WallCoordinate.Direction == eWallDirection.NORTH)
            {
                if (coord.Face == eWallFace.OUTSIDE)
                {
                    coord.WallCoordinate.Coordinate.Col++;
                    angle = 180f;
                }
            }
            else
            {
                if (coord.Face == eWallFace.INSIDE)
                {
                    coord.WallCoordinate.Coordinate.Row++;
                    angle = -90f;
                }
                else
                {
                    angle = 90f;
                }
            }

            Matrix rotation = MatrixUtilities.CreateCompassSpaceRotation(angle);
            Matrix translation = MatrixUtilities.CreateTranslationMatrix(coord.WallCoordinate.Coordinate);

            return rotation * translation;
        }

        public static Matrix GetWorldMatrix(CharacterCoordinate coord)
        {
            switch (coord.State)
            {
                case eCharacterCoordinateState.STATIONARY:
                    return GetCharacterWorldMatrix_Stationary(coord);
                case eCharacterCoordinateState.TRANSITIONING:
                    return GetCharacterWorldMatrix_Transitioning(coord);
                case eCharacterCoordinateState.INTERACTING:
                    return GetCharacterWorldMatrix_Interacting(coord);
                case eCharacterCoordinateState.IDLING:
                    return GetCharacterWorldMatrix_Stationary(coord);
                default:
                    return Matrix.Identity;
            }
        }

        public static Matrix GetHingeDoorWorldMatrix(WallCoordinate coord, float doorRotation)
        {
            Matrix preRotationTranslation = MatrixUtilities.CreateTranslationMatrix(-0.25f, 0f);
            Matrix rotation = MatrixUtilities.CreateCompassSpaceRotation(doorRotation);
            Matrix postRotationTranslation = MatrixUtilities.CreateTranslationMatrix(0.25f, 0f);
            Matrix tileRotation;
            if (coord.Direction == eWallDirection.NORTH)
                tileRotation = Matrix.Identity;
            else
            {
                tileRotation = MatrixUtilities.CreateCompassSpaceRotation(-90f);
                coord.Coordinate.Row++;
            }
            Matrix translation = MatrixUtilities.CreateTranslationMatrix(coord.Coordinate);

            return preRotationTranslation * rotation * postRotationTranslation * tileRotation * translation;
        }

        // -------- Private Methods ---------
        private static Matrix GetCharacterWorldMatrix_Stationary(CharacterCoordinate coord)
        {
            AreaCoordinate effectiveArea = new AreaCoordinate(coord.StationaryCoordinate, 1, 1, coord.Rotation);
            return GetWorldMatrix(effectiveArea);
        }
        private static Matrix GetCharacterWorldMatrix_Transitioning(CharacterCoordinate coord)
        {
            Matrix centralizeTranslation = MatrixUtilities.CreateTranslationMatrix(-0.5f, -0.5f);
            Matrix decentralizeTranslation = MatrixUtilities.CreateTranslationMatrix(0.5f, 0.5f);
            Matrix rotation = MatrixUtilities.CreateRotationMatrix(coord.Rotation);

            Vector2 position = coord.GetTransitioningTileSpacePosition();
            Matrix translation = MatrixUtilities.CreateTranslationMatrix(position.X, position.Y);

            return centralizeTranslation * rotation * decentralizeTranslation * translation;
        }
        private static Matrix GetCharacterWorldMatrix_Interacting(CharacterCoordinate coord)
        {
            eSquareRotation entityRotation = GameEntityUtilities.GetGameEntityRotation(coord.Interaction.Parent);
            eSquareRotation interactionRotation = coord.Interaction.InteractionRelativeRotation;
            eSquareRotation totalRotation = TileUtilities.GetInteractionRotation(entityRotation, interactionRotation);
            Matrix rotation = MatrixUtilities.CreateRotationMatrix(totalRotation);

            Matrix centralizeTranslation = MatrixUtilities.CreateTranslationMatrix(-0.5f, -0.5f);

            TileCoordinate entityCoord = GameEntityUtilities.GetGameEntityTileCoordinate(coord.Interaction.Parent);
            Vector2 tsPosition = TileUtilities.GetInteractionTsPosition(entityCoord, entityRotation, coord.Interaction.InteractionRelativePosition);
            Matrix position = MatrixUtilities.CreateTranslationMatrix(tsPosition);

            return centralizeTranslation * rotation * position;
        }
    }
}
