﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    // -------- Enumerations --------
    public enum eDay
    {
        MONDAY,
        TUESDAY,
        WEDNESDAY,
        THURSDAY,
        FRIDAY,
        SATURDAY,
        SUNDAY
    }

    static class GameClockManager
    {
        // -------- Private Fields --------
        private static float gameClockTime;
        private static eDay gameClockDay;
        private static bool skipWeekends = true;
        // Parameters
        private const float DEFAULT_CLOCK_SPEED = 120f; // How many times faster than real life

        // -------- Public Methods --------
        public static void Initialize()
        {
            gameClockTime = 7.5f;
            gameClockDay = eDay.MONDAY;
            OnNewDay();
        }

        public static void Update()
        {
            gameClockTime += DEFAULT_CLOCK_SPEED * TimeUtilities.ConvertSecondsToHours(GameSpeedManager.ElapsedGameTime);

            if (gameClockTime > 24)
            {
                gameClockTime -= 24;
                OnNewDay();
            }
        }

        public static float GetClockTime()
        {
            return gameClockTime;
        }
        public static float GetClockMinutesComponent()
        {
            return gameClockTime % 1f;
        }
        public static int GetClockHours()
        {
            return (int)gameClockTime;
        }
        public static int GetClockMinutes()
        {
            int minutes = (int)(60f * (gameClockTime % 1f));
            return minutes;
        }

        public static string GetClockTimeString()
        {
            int hours = GetClockHours();
            int minutes = GetClockMinutes();
            string hoursString;
            string minutesString;
            if (hours < 10)
                hoursString = "0" + hours.ToString();
            else
                hoursString = hours.ToString();
            if (minutes < 10)
                minutesString = "0" + minutes.ToString();
            else
                minutesString = minutes.ToString();

            return (hoursString + ":" + minutesString);
        }
        public static string GetFullClockString()
        {
            return (GetClockTimeString() + ", " + TimeUtilities.GetShortDayName(gameClockDay));
        }

        // -------- Private Methods --------
        private static void OnNewDay()
        {
            GameManager.OnNewDay();
        }

        private static void SetToNextDay()
        {
            switch(gameClockDay)
            {
                case eDay.MONDAY:
                    gameClockDay = eDay.TUESDAY;
                    break;
                case eDay.TUESDAY:
                    gameClockDay = eDay.WEDNESDAY;
                    break;
                case eDay.WEDNESDAY:
                    gameClockDay = eDay.THURSDAY;
                    break;
                case eDay.THURSDAY:
                    gameClockDay = eDay.FRIDAY;
                    break;
                case eDay.FRIDAY:
                    if (skipWeekends)
                        gameClockDay = eDay.MONDAY;
                    else
                        gameClockDay = eDay.SATURDAY;
                    break;
                case eDay.SATURDAY:
                    if (skipWeekends)
                        gameClockDay = eDay.MONDAY;
                    else
                        gameClockDay = eDay.SUNDAY;
                    break;
                case eDay.SUNDAY:
                    gameClockDay = eDay.MONDAY;
                    break;
            }
        }
    }
}
