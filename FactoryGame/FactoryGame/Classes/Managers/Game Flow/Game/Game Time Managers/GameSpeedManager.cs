﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    // -------- Enumerations --------
    public enum eGameSpeed
    {
        PAUSED,
        PLAY_1,
        PLAY_2,
        PLAY_3
    }

    static class GameSpeedManager
    {
        // -------- Properties --------
        public static eGameSpeed GameSpeed { get; private set; }
        public static float ElapsedGameTime
        {
            get
            {
                return GameTimeManager.ElapsedGameTime * GetGameSpeedFactor();
            }
        }

        // -------- Private Fields --------
        private static float[] gameSpeedFactors = new float[] { 0f, 1f, 2f, 5f };

        // -------- Public Methods --------
        public static void Initialize()
        {
            GameSpeed = eGameSpeed.PLAY_1;
        }

        public static void SetGameSpeed(eGameSpeed speed)
        {
            GameSpeed = speed;
        }
        public static void SetGameSpeedPaused()
        {
            SetGameSpeed(eGameSpeed.PAUSED);
        }
        public static void SetGameSpeedPlay1()
        {
            SetGameSpeed(eGameSpeed.PLAY_1);
        }
        public static void SetGameSpeedPlay2()
        {
            SetGameSpeed(eGameSpeed.PLAY_2);
        }
        public static void SetGameSpeedPlay3()
        {
            SetGameSpeed(eGameSpeed.PLAY_3);
        }

        public static float GetGameSpeedFactor()
        {
            return gameSpeedFactors[(int)GameSpeed];
        }

        public static bool GameSpeedIsPaused()
        {
            return (GameSpeed == eGameSpeed.PAUSED);
        }
        public static bool GameSpeedIsPlay1()
        {
            return (GameSpeed == eGameSpeed.PLAY_1);
        }
        public static bool GameSpeedIsPlay2()
        {
            return (GameSpeed == eGameSpeed.PLAY_2);
        }
        public static bool GameSpeedIsPlay3()
        {
            return (GameSpeed == eGameSpeed.PLAY_3);
        }
    }
}
