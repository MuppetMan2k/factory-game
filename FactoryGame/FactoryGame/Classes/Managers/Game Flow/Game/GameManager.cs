﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    static class GameManager
    {
        // -------- Public Methods --------
        public static void Initialize()
        {
            CursorManager.SetCursorMode(eCursorMode.POINTER);

            AlmanacManager.Initialize();
            GameSpeedManager.Initialize();
            FloorManager.Initialize();
            GameEntityManager.Initialize();
            ElementManager.Initialize();
            GameGraphicsManager.Initialize();
            CameraManager.Initialize();
            UIManager.Initialize();
            MouseOverManager.Initialize();
            CompanyManager.Initialize();
            CharacterManager.Initialize();
            DataViewManager.Initialize();
            MoneyManager.Initialize();
            ContractManager.Initialize();
            GameClockManager.Initialize();
        }

        public static void Denitialize()
        {
            AlmanacManager.Denitialize();
            FloorManager.Denitialize();
            GameGraphicsManager.Denitialize();
            GameEntityManager.Denitialize();
            ElementManager.Denitialize();
            CameraManager.Denitialize();
            UIManager.Denitialize();
            MouseOverManager.Denitialize();
            CompanyManager.Denitialize();
            CharacterManager.Denitialize();
            DataViewManager.Denitialize();
            ContractManager.Denitialize();
        }

        public static void Update()
        {
            GameClockManager.Update();
            GameGraphicsManager.Update();
            FloorManager.Update();
            GameEntityManager.Update();
            UIManager.Update();
            CameraManager.Update();
            MouseOverManager.Update();
            CharacterManager.Update();
            ContractManager.Update();
        }

        public static void Draw()
        {
            GameGraphicsManager.Draw();
        }

        public static void DrawTransparent()
        {
            GameGraphicsManager.DrawTransparent();
        }

        public static void SBDraw()
        {
            GameGraphicsManager.SBDraw();
        }

        public static void SBDrawToRenderTarget()
        {
            GameGraphicsManager.SBDrawToRenderTarget();
        }

        public static void OnNewDay()
        {
            CharacterManager.OnNewDay();
            ContractManager.OnNewDay();
        }

        public static void OnStaffEnterFactory(Staff staff)
        {
            UIManager.OnStaffEnterFactory(staff);
        }
        public static void OnStaffExitFactory(Staff staff)
        {
            UIManager.OnStaffExitFactory(staff);
        }
    }
}
