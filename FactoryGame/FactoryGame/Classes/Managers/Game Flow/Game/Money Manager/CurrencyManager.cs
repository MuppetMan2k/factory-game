﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    // -------- Enumerations --------
    public enum eCurrency
    {
        USD,
        GBP,
        EURO
    }

    static class CurrencyManager
    {
        // -------- Properties --------
        public static eCurrency Currency { get; private set; }

        // -------- Public Methods --------
        public static void Initialize()
        {
            Currency = eCurrency.USD;
        }

        public static int GetCurrencyQuantityMultiplier(eCurrency currency)
        {
            switch (currency)
            {
                case eCurrency.USD:
                    return 1;
                case eCurrency.GBP:
                    return 1;
                case eCurrency.EURO:
                    return 1;
                default:
                    return 1;
            }
        }

        public static int ConvertMoneyIntoCurrency(int amount)
        {
            return amount * GetCurrencyQuantityMultiplier(Currency);
        }

        public static string AddCurrencySymbolToMoneyString(string moneyString, eCurrency currency)
        {
            switch (currency)
            {
                case eCurrency.USD:
                    return "$" + moneyString;
                case eCurrency.GBP:
                    return "£" + moneyString;
                case eCurrency.EURO:
                    return "€" + moneyString;
                default:
                    return moneyString;
            }
        }
        public static string AddCurrencySymbolToMoneyString(string moneyString)
        {
            return AddCurrencySymbolToMoneyString(moneyString, Currency);
        }
    }
}
