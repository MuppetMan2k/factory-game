﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    static class DailyProfitManager
    {
        // -------- Private Fields --------
        private static int dailyProfit;

        // -------- Public Methods --------
        public static void Initialize()
        {
            dailyProfit = -5;
        }

        public static string GetDailyProfitString()
        {
            string incomeString = string.Format(Localization.LocalizeString("text_per_day"), MoneyManager.GetMoneyString(dailyProfit, true));

            return incomeString;
        }
    }
}
