﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    static class MoneyManager
    {
        // -------- Private Fields --------
        private static int moneyBalance;

        // -------- Public Methods --------
        public static void Initialize()
        {
            moneyBalance = 24000;
            CurrencyManager.Initialize();
            DailyProfitManager.Initialize();
        }

        public static string GetBalanceString()
        {
            return GetMoneyString(moneyBalance);
        }
        
        public static string GetMoneyString(int moneyAmount, bool showPlus = false)
        {
            bool writeMinus = (moneyAmount < 0);
            bool writePlus = (moneyAmount >= 0 && showPlus);

            int currencyAmount = Mathf.Abs(CurrencyManager.ConvertMoneyIntoCurrency(moneyAmount));
            string amountString = Localization.LocalizeCurrencyNumber(currencyAmount);
            amountString = CurrencyManager.AddCurrencySymbolToMoneyString(amountString);

            if (writeMinus)
                amountString = "-" + amountString;
            else if (writePlus)
                amountString = "+" + amountString;

            return amountString;
        }
    }
}
