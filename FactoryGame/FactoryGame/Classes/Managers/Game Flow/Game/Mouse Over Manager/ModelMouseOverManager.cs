﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    static class ModelMouseOverManager
    {
        // -------- Public Methods --------
        public static bool MouseIsOverTileModel(TileEntity entity, out float depth)
        {
            Matrix transform = WorldMatrixManager.GetWorldMatrix(entity.TileCoordinate) * CameraManager.ViewMatrix * CameraManager.ProjectionMatrix;

            GameModel model = entity.Model;

            return MouseIsOverTriangleList(model.ModelTriangles, model.GetBoneTransforms(), transform, out depth);
        }

        public static bool MouseIsOverMiniTileModel(MiniTileEntity entity, out float depth)
        {
            Matrix transform = WorldMatrixManager.GetWorldMatrix(entity.MiniTileCoordinate) * CameraManager.ViewMatrix * CameraManager.ProjectionMatrix;

            GameModel model = entity.Model;

            return MouseIsOverTriangleList(model.ModelTriangles, model.GetBoneTransforms(), transform, out depth);
        }

        public static bool MouseIsOverAreaModel(AreaEntity entity, out float depth)
        {
            Matrix transform = WorldMatrixManager.GetWorldMatrix(entity.AreaCoordinate) * CameraManager.ViewMatrix * CameraManager.ProjectionMatrix;

            GameModel model = entity.Model;

            return MouseIsOverTriangleList(model.ModelTriangles, model.GetBoneTransforms(), transform, out depth);
        }

        public static bool MouseIsOverWallModel(Wall wall, out float depth)
        {
            if (MouseIsOverWallFaceModel(wall, wall.InnerModel, eWallFace.INSIDE, out depth))
                return true;
            if (MouseIsOverWallFaceModel(wall, wall.OuterModel, eWallFace.OUTSIDE, out depth))
                return true;

            depth = 1f;
            return false;
        }

        public static bool MouseIsOverWallMountedModel(WallMountEntity entity, out float depth)
        {
            if (entity.WallMountCoordinate.Location == eWallMountLocation.INSIDE)
            {
                return MouseIsOverWallMountedModelFace(entity, eWallFace.INSIDE, out depth);
            }
            else if (entity.WallMountCoordinate.Location == eWallMountLocation.OUTSIDE)
            {
                return MouseIsOverWallMountedModelFace(entity, eWallFace.OUTSIDE, out depth);
            }
            else
            {
                // Through
                float outDepth = 1f;
                bool mouseIsOver = false;
                float thisDepth;
                if (MouseIsOverWallMountedModelFace(entity, eWallFace.INSIDE, out thisDepth))
                {
                    mouseIsOver = true;
                    outDepth = thisDepth;
                }
                if (MouseIsOverWallMountedModelFace(entity, eWallFace.OUTSIDE, out thisDepth))
                {
                    mouseIsOver = true;
                    outDepth = Mathf.Min(outDepth, thisDepth);
                }

                depth = outDepth;
                return mouseIsOver;
            }
        }

        public static bool MouseIsOverHingeDoorModel(HingeDoor door, out float depth)
        {
            float outDepth = 1f;
            float thisDepth;
            bool mouseIsOver = false;
            if (MouseIsOverHingeDoorModelFace(door, door.InnerModel, eWallFace.INSIDE, out thisDepth))
            {
                mouseIsOver = true;
                outDepth = thisDepth;
            }
            if (MouseIsOverHingeDoorModelFace(door, door.OuterModel, eWallFace.OUTSIDE, out thisDepth))
            {
                mouseIsOver = true;
                outDepth = Mathf.Min(outDepth, thisDepth);
            }

            depth = outDepth;
            return mouseIsOver;
        }

        public static bool MouseIsOverCharacterModel(Character character, out float depth)
        {
            bool overCharacter = false;
            float minDepth = 1f;

            Matrix transform = WorldMatrixManager.GetWorldMatrix(character.CharacterCoordinate) * CameraManager.ViewMatrix * CameraManager.ProjectionMatrix;
            CharacterPosition position = character.CharacterCoordinate.Animation.GetPosition();

            // Body
            foreach (Triangle t in character.CharacterModel.Body.ModelTriangles)
            {
                Matrix boneTransform = character.CharacterModel.Body.GetBoneTransforms()[t.boneIndex];
                Matrix modelTransform = CharacterAnimationManager.GetCharacterMeshTransform(t.MeshName, position);
                Triangle screenTriangle = t.Transform(boneTransform * modelTransform * transform);
                float thisDepth;
                if (MouseIsOverScreenTriangle(screenTriangle, out thisDepth))
                {
                    overCharacter = true;
                    minDepth = Mathf.Min(minDepth, thisDepth);
                }
            }

            Matrix headTransform = CharacterAnimationManager.GetCharacterMeshTransform("Head", position);
            // Head
            float headDepth;
            if (MouseIsOverTriangleList(character.CharacterModel.Head.ModelTriangles, character.CharacterModel.Head.GetBoneTransforms(), headTransform * transform, out headDepth))
            {
                overCharacter = true;
                minDepth = Mathf.Min(minDepth, headDepth);
            }

            // Head Piece
            if (character.CharacterModel.HeadPiece != null)
            {
                float headPieceDepth;
                if (MouseIsOverTriangleList(character.CharacterModel.HeadPiece.ModelTriangles, character.CharacterModel.HeadPiece.GetBoneTransforms(), headTransform * transform, out headPieceDepth))
                {
                    overCharacter = true;
                    minDepth = Mathf.Min(minDepth, headPieceDepth);
                }
            }

            depth = minDepth;
            return overCharacter;
        }

        // -------- Private Methods ---------
        private static bool MouseIsOverWallFaceModel(Wall wall, GameModel model, eWallFace face, out float depth)
        {
            var effectiveFaceCoord = new WallFaceCoordinate(wall.WallCoordinate, face);
            Matrix transform = WorldMatrixManager.GetWorldMatrix(effectiveFaceCoord) * CameraManager.ViewMatrix * CameraManager.ProjectionMatrix;

            bool overTriangle = false;
            float minDepth = 1f;
            foreach (Triangle t in model.ModelTriangles)
            {
                if (!WallMeshManager.WallMeshShouldBeDrawn(t.MeshName, effectiveFaceCoord))
                    continue;

                Matrix triangleTransform = model.GetBoneTransforms()[t.boneIndex] * transform;
                Triangle screenTriangle = t.Transform(triangleTransform);
                float thisDepth;
                if (MouseIsOverScreenTriangle(screenTriangle, out thisDepth))
                {
                    overTriangle = true;
                    minDepth = Mathf.Min(minDepth, thisDepth);
                }
            }

            depth = minDepth;
            return overTriangle;
        }

        private static bool MouseIsOverWallMountedModelFace(WallMountEntity entity, eWallFace face, out float depth)
        {
            var effectiveWallFaceCoord = new WallFaceCoordinate(entity.WallMountCoordinate.WallCoordinate, face);
            GameModel model = entity.Model;
            Matrix transform = WorldMatrixManager.GetWorldMatrix(effectiveWallFaceCoord) * CameraManager.ViewMatrix * CameraManager.ProjectionMatrix;

            return MouseIsOverTriangleList(model.ModelTriangles, model.GetBoneTransforms(), transform, out depth);
        }

        private static bool MouseIsOverHingeDoorModelFace(HingeDoor door, GameModel model, eWallFace face, out float depth)
        {
            Matrix transform = WorldMatrixManager.GetHingeDoorWorldMatrix(door.WallMountCoordinate.WallCoordinate, door.Rotation) * CameraManager.ViewMatrix * CameraManager.ProjectionMatrix;

            return MouseIsOverTriangleList(model.ModelTriangles, model.GetBoneTransforms(), transform, out depth);
        }

        // Triangle tests
        private static bool MouseIsOverTriangleList(List<Triangle> triangles, Matrix[] boneTransforms, Matrix transform, out float depth)
        {
            bool overTriangle = false;
            float minDepth = 1f;
            foreach (Triangle t in triangles)
            {
                Matrix triangleTransform = boneTransforms[t.boneIndex] * transform;
                Triangle screenTriangle = t.Transform(triangleTransform);
                float thisDepth;
                if (MouseIsOverScreenTriangle(screenTriangle, out thisDepth))
                {
                    overTriangle = true;
                    minDepth = Mathf.Min(minDepth, thisDepth);
                }
            }

            depth = minDepth;
            return overTriangle;
        }
        private static bool MouseIsOverScreenTriangle(Triangle screenTriangle, out float depth)
        {
            if (TriangleUtilities.PositionIsWithinSsTriangle(screenTriangle, InputManager.GetMouseSSPosition()))
            {
                depth = GetScreenTriangleDepth(screenTriangle);
                return true;
            }

            depth = 1f;
            return false;
        }
        private static float GetScreenTriangleDepth(Triangle screenTriangle)
        {
            float amount1;
            float amount2;
            float amount3;

            TriangleUtilities.GetSsTriangleBarycentricAmounts(screenTriangle, InputManager.GetMouseSSPosition(), out amount1, out amount2, out amount3);

            return Mathf.Barycentric(screenTriangle.Vertex1.Z, screenTriangle.Vertex2.Z, screenTriangle.Vertex3.Z, amount1, amount2);
        }
    }
}
