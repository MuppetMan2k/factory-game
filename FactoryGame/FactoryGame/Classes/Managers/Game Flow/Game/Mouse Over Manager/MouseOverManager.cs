﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    static class MouseOverManager
    {
        // -------- Properties --------
        public static Entity MouseOverEntity { get; private set; }

        // -------- Private Fields --------
        private static List<Entity> mouseOverEntities;
        private static List<float> mouseOverEntityDepths;

        // -------- Public Methods --------
        public static void Initialize()
        {
            mouseOverEntities = new List<Entity>();
            mouseOverEntityDepths = new List<float>();

            MouseOverSelectionFilterManager.Initialize();
        }

        public static void Denitialize()
        {
            mouseOverEntities = null;
            mouseOverEntityDepths = null;

            MouseOverSelectionFilterManager.Denitialize();
        }

        public static void Update()
        {
            FindMouseOverEntities();
            ChooseMouseOverEntity();
            CheckForMouseClick();
        }

        // -------- Private Methods ---------
        private static void FindMouseOverEntities()
        {
            mouseOverEntities.Clear();
            mouseOverEntityDepths.Clear();

            List<UIEntity> uiEntities = UIManager.GetAllUIEntities();
            foreach (UIEntity e in uiEntities)
            {
                CheckFindMouseOverEntity(e);
            }

            if (mouseOverEntities.Count > 0)
                return;

            List<GameEntity> gameEntities = GameEntityManager.GetAllGameEntities();
            foreach (GameEntity e in gameEntities)
            {
                CheckFindMouseOverEntity(e);
            }
        }

        private static void ChooseMouseOverEntity()
        {
            if (mouseOverEntities.Count == 0)
            {
                ResetMouseOverEntity();
                return;
            }

            Entity chosenEntity = MouseOverSelectionManager.SelectMouseOverEntity(mouseOverEntities, mouseOverEntityDepths);

            if (chosenEntity == null)
            {
                ResetMouseOverEntity();
                return;
            }

            SetNewMouseOverEntity(chosenEntity);
        }

        private static void SetNewMouseOverEntity(Entity newEntity)
        {
            if (MouseOverEntity != null)
            {
                if (MouseOverEntity.Equals(newEntity))
                    return;

                MouseOverEntity.OnMouseOut();
            }

            MouseOverEntity = newEntity;
            MouseOverEntity.OnMouseOver();
        }
        private static void ResetMouseOverEntity()
        {
            if (MouseOverEntity == null)
                return;

            MouseOverEntity.OnMouseOut();
            MouseOverEntity = null;
        }

        private static void CheckForMouseClick()
        {
            if (InputManager.ButtonIsDown(eMouseButton.LEFT))
                if (MouseOverEntity != null)
                    MouseOverEntity.OnMouseClick();
        }

        private static void CheckFindMouseOverEntity(Entity e)
        {
            UIEntity uie = e as UIEntity;
            if (uie != null)
            {
                foreach (UIEntity c in uie.GetChildren())
                    CheckFindMouseOverEntity(c);
            }

            if (!MouseOverSelectionManager.EntityIsSelectable(e))
                return;

            float depth;
            if (e.MouseIsOver(out depth))
            {
                mouseOverEntities.Add(e);
                mouseOverEntityDepths.Add(depth);
            }
        }
    }
}
