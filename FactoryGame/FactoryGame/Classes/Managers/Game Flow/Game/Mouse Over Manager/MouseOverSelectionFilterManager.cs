﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    static class MouseOverSelectionFilterManager
    {
        // -------- Private Fields --------
        private static MouseOverSelectionFilter filter;

        // -------- Public Methods --------
        public static void Initialize()
        {
            filter = MouseOverSelectionFilter.Default;
        }

        public static void Denitialize()
        {
            filter = null;
        }

        public static void SetSelectionFilter(MouseOverSelectionFilter newFilter)
        {
            filter = newFilter;
        }

        // -------- Public Methods --------
        public static bool CanSelectMouseOverType(eMouseOverType type)
        {
            return (filter.SelectableMouseOverTypes.Contains(type));
        }
    }
}
