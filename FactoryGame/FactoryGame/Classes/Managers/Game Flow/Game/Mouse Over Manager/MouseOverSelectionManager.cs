﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    // -------- Enumerations --------
    public enum eMouseOverType
    {
        OTHER,

        // Game entities
        TILE,
        WALL,
        WINDOW,
        DOOR,
        FACTORY_OBJECT,
        CHARACTER,
        // TODO: Add other entity types here

        // UI entities
        FLAT_UI,
        STATIC_UI,
        SCROLL_WINDOW_UI,
        INTERACTABLE_UI
    }

    class MouseOverSelectionManager
    {
        // -------- Private Fields --------
        private static Entity chosenEntity;
        private static float chosenDepth;
        private static eMouseOverType chosenType;

        // -------- Public Methods --------
        public static Entity SelectMouseOverEntity(List<Entity> entities, List<float> entityDepths)
        {
            SetUpChosenEntity();

            int i = 0;
            foreach (Entity entity in entities)
            {
                eMouseOverType type = GetMouseOverType(entity);
                float depth = entityDepths[i];
                if (EntityShouldOverwriteChosen(entity, depth, type))
                {
                    chosenEntity = entity;
                    chosenDepth = depth;
                    chosenType = type;
                }

                i++;
            }

            return chosenEntity;
        }

        public static bool EntityIsSelectable(Entity entity)
        {
            eMouseOverType type = GetMouseOverType(entity);
            return MouseOverSelectionFilterManager.CanSelectMouseOverType(type);
        }

        // -------- Private Methods ---------
        private static void SetUpChosenEntity()
        {
            chosenEntity = null;
            chosenDepth = 1f;
        }

        private static bool EntityShouldOverwriteChosen(Entity entity, float depth, eMouseOverType type)
        {
            if (chosenEntity == null)
                return true;

            // UI overwrites game entities
            if (MouseOverTypeIsUI(type) && !MouseOverTypeIsUI(chosenType))
                return true;
            if (!MouseOverTypeIsUI(type) && MouseOverTypeIsUI(chosenType))
                return false;

            // UI overwites UI of a lower enum value
            if (MouseOverTypeIsUI(type) && MouseOverTypeIsUI(chosenType))
                return ((int)type > (int)chosenType);

            // Add any additional tests here

            return (depth < chosenDepth);
        }

        private static bool MouseOverTypeIsUI(eMouseOverType mouseOverType)
        {
            return ((int)mouseOverType >= (int)eMouseOverType.FLAT_UI);
        }

        private static eMouseOverType GetMouseOverType(Entity entity)
        {
            if (entity is Tile)
                return eMouseOverType.TILE;

            if (entity is Wall)
                return eMouseOverType.WALL;

            if (entity is Window)
                return eMouseOverType.WINDOW;

            if (entity is Door)
                return eMouseOverType.DOOR;

            if (entity is AreaFactoryObject)
                return eMouseOverType.FACTORY_OBJECT;

            if (entity is WallMountFactoryObject)
                return eMouseOverType.FACTORY_OBJECT;

            if (entity is Character)
                return eMouseOverType.CHARACTER;

            if (entity is IInteractableUI)
                return eMouseOverType.INTERACTABLE_UI;

            if (entity is HudPanelUI)
                return eMouseOverType.FLAT_UI;

            if (entity is PortUI)
                return eMouseOverType.FLAT_UI;

            if (entity is TextUI)
                return eMouseOverType.STATIC_UI;

            ElementIconUI elementIcon = entity as ElementIconUI;
            if (elementIcon != null)
            {
                if (elementIcon.ElementHolder.IsHoldingElement())
                    return eMouseOverType.STATIC_UI;
                else
                    return eMouseOverType.FLAT_UI;
            }

            if (entity is IconUI)
                return eMouseOverType.STATIC_UI;

            if (entity is StaffTaskUI)
                return eMouseOverType.STATIC_UI;

            if (entity is ScrollWindowUI)
                return eMouseOverType.SCROLL_WINDOW_UI;

            if (entity is UIEntity)
                return eMouseOverType.FLAT_UI;

            // TODO: Add other entity type tests here

            return eMouseOverType.OTHER;
        }
    }
}
