﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    static class PathFindingManager
    {
        // -------- Public Methods --------
        public static bool TryFindPath(TileCoordinate start, TileCoordinate end, out Path path)
        {
            path = new Path();

            bool[,] passableMap = PassPlaceMapManager.GetPassableFloorMap();

            // Check start and end
            if (!passableMap[start.Row, start.Col])
                return false;
            if (!passableMap[end.Row, end.Col])
                return false;

            // Check trivial path
            if (start.Equals(end))
                return true;

            return TryFindPath_StepNumberMap(start, end, passableMap, out path);
        }

        // -------- Private Methods ---------
        // Step-number map method
        // Step numbers: 0+ is step numbers, -1 is not yet set, -2 is impassable
        private static bool TryFindPath_StepNumberMap(TileCoordinate start, TileCoordinate end, bool[,] passableMap, out Path path)
        {
            int[,] stepNumberMap = CreateStepNumberMap(passableMap, start);

            TileCoordinate kernel = start;
            bool reachedEnd = false;
            bool firstPass = true;

            while (!reachedEnd)
            {
                PerformAreaExpandStepCount(stepNumberMap, kernel, !firstPass);
                firstPass = false;
                reachedEnd = HasReachedEndTile(stepNumberMap, end);

                if (!reachedEnd)
                {
                    if (!TryFindNewKernel(stepNumberMap, out kernel))
                    {
                        // Not found end and exhausted all kernels
                        path = null;
                        return false;
                    }
                }
            }

            // Reached end
            // Recalculate end tile
            int stepsToEnd = CalculateTileStepCount(stepNumberMap, end);
            stepNumberMap[end.Row, end.Col] = stepsToEnd;
            // Find path backwards
            var tileSteps = new List<TileCoordinate>();
            TileCoordinate backwardsOrigin = end;
            tileSteps.Add(end);
            for (int step = stepsToEnd; step > 0; step--)
            {
                bool canContinuePath = false;

                foreach (eSquareCompassDirection dir in EnumUtilities.GetEnumValues<eSquareCompassDirection>())
                {
                    TileCoordinate testTile = TileUtilities.GetOffsetTile(backwardsOrigin, dir);

                    if (!TileManager.TileExists(testTile))
                        continue;

                    if (WallManager.ImpassableWallExistsAdjacentToTile(backwardsOrigin, dir))
                        continue;

                    if (stepNumberMap[testTile.Row, testTile.Col] == step - 1)
                    {
                        tileSteps.Add(testTile);
                        backwardsOrigin = testTile;
                        canContinuePath = true;
                        break;
                    }
                }

                if (canContinuePath)
                    continue;
                
                // Couldn't create complete path
                path = null;
                return false;
            }

            // Turn tile steps into path
            path = new Path();
            for (int t = tileSteps.Count - 1; t >= 0; t--)
                path.Tiles.Add(tileSteps[t]);

            return true;
        }

        private static int[,] CreateStepNumberMap(bool[,] passableMap, TileCoordinate start)
        {
            int[,] stepNumberMap = new int[FloorManager.FloorHeight, FloorManager.FloorWidth];

            for (int row = 0; row < FloorManager.FloorHeight; row++)
            {
                for (int col = 0; col < FloorManager.FloorWidth; col++)
                {
                    if (!passableMap[row, col])
                        stepNumberMap[row, col] = -2;
                    else if (start.Row == row && start.Col == col)
                        stepNumberMap[row, col] = 0;
                    else
                        stepNumberMap[row, col] = -1;
                }
            }

            return stepNumberMap;
        }

        private static void PerformAreaExpandStepCount(int[,] stepNumberMap, TileCoordinate kernel, bool calculateKernel)
        {
            if (calculateKernel)
                stepNumberMap[kernel.Row, kernel.Col] = CalculateTileStepCount(stepNumberMap, kernel);

            int maxRingRadius = Mathf.Max(Mathf.Max(kernel.Row, FloorManager.FloorHeight - kernel.Row), Mathf.Max(kernel.Col, FloorManager.FloorWidth - kernel.Col));

            for (int r = 1; r <= maxRingRadius; r++)
                PerformRingStepCount(stepNumberMap, kernel, r);
        }
        private static void PerformRingStepCount(int[,] stepNumberMap, TileCoordinate kernel, int radius)
        {
            foreach (eSquareCompassDirection radiusDirection in EnumUtilities.GetEnumValues<eSquareCompassDirection>())
            {
                eSquareCompassDirection circumferenceDirection = CompassUtilities.RotateSquareCompassDirection(radiusDirection, eSquareRotation.ROTATION_90, eRotationDirection.CLOCKWISE);
                TileCoordinate edgeCenter = TileUtilities.GetOffsetTile(kernel, radiusDirection, radius);

                for (int i = -radius + 1; i <= radius; i++)
                {
                    TileCoordinate edgeTile = TileUtilities.GetOffsetTile(edgeCenter, circumferenceDirection, i);

                    if (!TileManager.TileExists(edgeTile))
                        continue;

                    if (stepNumberMap[edgeTile.Row, edgeTile.Col] == -2)
                        continue;

                    // Don't recalculate the start tile!
                    if (stepNumberMap[edgeTile.Row, edgeTile.Col] == 0)
                        continue;

                    int stepCount = CalculateTileStepCount(stepNumberMap, edgeTile);
                    if (stepCount >= 0)
                        stepNumberMap[edgeTile.Row, edgeTile.Col] = stepCount;
                }
            }
        }

        private static int CalculateTileStepCount(int[,] stepNumberMap, TileCoordinate coord)
        {
            int result = -1;

            foreach (eSquareCompassDirection dir in EnumUtilities.GetEnumValues<eSquareCompassDirection>())
            {
                if (WallManager.ImpassableWallExistsAdjacentToTile(coord, dir))
                    continue;

                TileCoordinate adjTile = TileUtilities.GetOffsetTile(coord, dir);
                int adjSteps = stepNumberMap[adjTile.Row, adjTile.Col];
                if (adjSteps == -1 || adjSteps == -2)
                    continue;

                if (result == -1)
                {
                    result = adjSteps;
                    continue;
                }

                result = Mathf.Min(result, adjSteps);
            }

            if (result == -1)
                return -1;

            return result + 1;
        }

        private static bool HasReachedEndTile(int[,] stepNumberMap, TileCoordinate end)
        {
            return (stepNumberMap[end.Row, end.Col] >= 0);
        }

        private static bool TryFindNewKernel(int[,] stepNumberMap, out TileCoordinate kernel)
        {
            for (int row = 0; row < FloorManager.FloorHeight; row++)
            {
                for (int col = 0; col < FloorManager.FloorWidth; col++)
                {
                    TileCoordinate testTile = new TileCoordinate(row, col);
                    if (stepNumberMap[row, col] == -1 && CalculateTileStepCount(stepNumberMap, testTile) >= 0)
                    {
                        kernel = testTile;
                        return true;
                    }
                }
            }

            kernel = new TileCoordinate();
            return false;
        }
    }
}
