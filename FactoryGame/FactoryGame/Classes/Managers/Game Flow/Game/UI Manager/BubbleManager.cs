﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    static class BubbleManager
    {
        // -------- Properties --------
        public static int BubbleBorder { get { return 10; } }
        public static int BubbleWidth { get { return 250; } }
        public static int MinBubbleHeight { get { return 100; } }
        public static int BubbleHeaderHeight { get { return 35; } }
        public static Point BubbleArrowSize { get { return new Point(40, 20); } }

        // -------- Private Fields --------
        private static List<BubbleUI> bubbleList;
        private static List<BubbleUI> bubbleRemovalList;
        private static Entity mouseOverEntity;
        private static float timer = 0f;
        // Const params
        private const float HOLD_TIME = 0.2f;
        private const float FADE_TIME = 0.2f;

        // -------- Public Methods --------
        public static void Initialize()
        {
            bubbleList = new List<BubbleUI>();
            bubbleRemovalList = new List<BubbleUI>();
        }

        public static void Denitialize()
        {
            foreach (BubbleUI bui in bubbleList)
                bui.Denitialize();

            bubbleList = null;
            bubbleRemovalList = null;
        }

        public static void Update()
        {
            UpdateFades();

            foreach (BubbleUI bui in bubbleList)
                bui.Update();

            UpdateTimer();
            UpdateRemovalList();
        }

        public static void SBDraw()
        {
            foreach (BubbleUI bui in bubbleList)
                bui.SBDraw();
        }

        public static void OnMouseOverEntity(Entity entity)
        {
            timer = 0f;
            mouseOverEntity = entity;
        }
        public static void OnMouseOutEntity()
        {
            timer = 0f;
            mouseOverEntity = null;
        }

        public static void RemoveBubble(BubbleUI bubbleToRemove)
        {
            bubbleRemovalList.Add(bubbleToRemove);
        }

        public static List<BubbleUI> GetAllBubbles()
        {
            return bubbleList;
        }

        // -------- Private Methods ---------
        private static void UpdateRemovalList()
        {
            if (bubbleRemovalList.Count == 0)
                return;

            foreach (BubbleUI bui in bubbleRemovalList)
            {
                bui.Denitialize();
                if (!bubbleList.Remove(bui))
                    Debug.LogWarning("Tried to remove bubble, but couldn't find it in list: " + bui.ToString());
            }

            bubbleRemovalList.Clear();
        }

        private static void UpdateTimer()
        {
            if (timer >= HOLD_TIME)
                return;

            if (mouseOverEntity == null)
                return;

            timer += GameTimeManager.ElapsedGameTime;

            if (timer >= HOLD_TIME)
            {
                CreateBubbleForMouseOverEntity();
                timer = HOLD_TIME;
            }
        }

        private static void UpdateFades()
        {
            foreach (BubbleUI bui in bubbleList)
            {
                bool fadeIn = (bui.AnchorEntity.Equals(mouseOverEntity));

                if (fadeIn)
                {
                    if (bui.FadeValue < 1f)
                    {
                        bui.FadeValue += GameTimeManager.ElapsedGameTime / FADE_TIME;
                        if (bui.FadeValue > 1f)
                            bui.FadeValue = 1f;
                    }
                }
                else
                {
                    bui.FadeValue -= GameTimeManager.ElapsedGameTime / FADE_TIME;
                    if (bui.FadeValue <= 0f)
                    {
                        bui.FadeValue = 0f;
                        bui.Destroy();
                    }
                }

                bui.transparency = bui.FadeValue;
            }
        }

        private static void CreateBubbleForMouseOverEntity()
        {
            // Check if already have a bubble for that entity
            foreach (BubbleUI bui in bubbleList)
                if (bui.AnchorEntity.Equals(mouseOverEntity))
                    return;

            BubbleUI bubble = CreateBubbleForEntity(mouseOverEntity);
            if (bubble == null)
                return;

            bubbleList.Add(bubble);
        }

        private static bool CanCreateBubbleForEntity(Entity entity)
        {
            return (CreateBubbleForEntity(entity) != null);
        }
        // Create bubble methods
        private static BubbleUI CreateBubbleForEntity(Entity entity)
        {
            BubbleUI bubble = null;

            Staff staff = entity as Staff;
            if (staff != null)
                bubble = CreateBubble_Staff(staff);

            Machine machine = entity as Machine;
            if (machine != null)
                bubble = CreateBubble_Machine(machine);

            ElementIconUI elementIcon = entity as ElementIconUI;
            if (elementIcon != null && elementIcon.ElementHolder.IsHoldingElement())
                bubble = CreateBubble_ElementIcon(elementIcon);

            // Add more bubble creation cases here...

            if (bubble != null)
                bubble.transparency = 0f;
            return bubble;
        }
        private static BubbleUI CreateBubble_Staff(Staff staff)
        {
            BubbleUI bubble = new BubbleUI(staff, staff.GetNameTitle());
            bubble.Initialize();

            StaffTaskIconUI task = new StaffTaskIconUI(new Point(BubbleBorder, BubbleHeaderHeight + BubbleBorder), staff, eUIAnchor.TOP_LEFT);
            task.Initialize();
            bubble.SetChild(task);

            HappinessUI happiness = new HappinessUI(staff, new Point(BubbleBorder + 50, BubbleHeaderHeight + BubbleBorder + 5), eUIAnchor.TOP_LEFT);
            happiness.Initialize();
            bubble.SetChild(happiness);

            TextUI dailyPay = new TextUI(staff.GetDailyPayString, new Point(BubbleBorder + 85, BubbleHeaderHeight + BubbleBorder + 7), eUIAnchor.TOP_LEFT,
                UIStyleManager.Style.UIFont, 1, 75, UIStyleManager.Style.PortBodyTextColor, eTextAlignment.CENTER);
            dailyPay.Initialize();
            bubble.SetChild(dailyPay);

            TextUI tasks = new TextUI(staff.GetTaskNumberString, new Point(BubbleBorder + 160, BubbleHeaderHeight + BubbleBorder + 7), eUIAnchor.TOP_LEFT,
                UIStyleManager.Style.UIFont, 1, 70, UIStyleManager.Style.PortBodyTextColor, eTextAlignment.CENTER);
            tasks.Initialize();
            bubble.SetChild(tasks);

            return bubble;
        }
        private static BubbleUI CreateBubble_Machine(Machine machine)
        {
            BubbleUI bubble = new BubbleUI(machine, machine.GetMachineName());
            bubble.Initialize();

            MachineStateIconUI state = new MachineStateIconUI(new Point(BubbleBorder, BubbleHeaderHeight + BubbleBorder), machine, eUIAnchor.TOP_LEFT);
            state.Initialize();
            bubble.SetChild(state);

            int numUsages = MachineUsageManager.GetMachineUsages(machine).Count;
            string usageString = (numUsages == 1 ? Localization.LocalizeString("text_machine_usage") : Localization.LocalizeString("text_machine_usages"));
            string usageNumString = string.Format(usageString, numUsages.ToString());
            TextUI processNum = new TextUI(usageNumString, new Point(BubbleBorder + 42, BubbleHeaderHeight + BubbleBorder + 7), eUIAnchor.TOP_LEFT, UIStyleManager.Style.UIFont, 1, 80,
                UIStyleManager.Style.PortBodyTextColor, eTextAlignment.CENTER);
            processNum.Initialize();
            bubble.SetChild(processNum);

            string percUsageString = string.Format(Localization.LocalizeString("text_machine_perc_usage"), TextUtilities.GetPercentageString(MachineUsageManager.GetMachineUsageFraction(machine)));
            TextUI percUsage = new TextUI(percUsageString, new Point(BubbleBorder + 135, BubbleHeaderHeight + BubbleBorder + 7), eUIAnchor.TOP_LEFT, UIStyleManager.Style.UIFont, 1, 80,
                UIStyleManager.Style.PortBodyTextColor, eTextAlignment.CENTER);
            percUsage.Initialize();
            bubble.SetChild(percUsage);

            return bubble;
        }
        private static BubbleUI CreateBubble_ElementIcon(ElementIconUI elementIcon)
        {
            Element element = elementIcon.ElementHolder.Element;

            BubbleUI bubble = new BubbleUI(elementIcon, Localization.LocalizeString(element.Data.NameLocId));
            bubble.Initialize();

            /*ElementIconUI bubbleIcon = new ElementIconUI(new Point(5, BubbleHeaderHeight + 5), eUIAnchor.TOP_LEFT, elementIcon.Element);
            bubbleIcon.Initialize();
            bubble.SetChild(bubbleIcon);*/

            TextUI quality = new TextUI(element.GetWholeQualityString, new Point(8, BubbleHeaderHeight + 8), eUIAnchor.TOP_LEFT, UIStyleManager.Style.UIFont, 1, 200, UIStyleManager.Style.PortBodyTextColor, eTextAlignment.LEFT);
            quality.Initialize();
            bubble.SetChild(quality);

            return bubble;
        }
        // Add more bubble creation mathods here...
    }
}
