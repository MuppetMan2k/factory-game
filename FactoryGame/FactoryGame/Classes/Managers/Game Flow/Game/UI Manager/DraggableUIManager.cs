﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    static class DraggableUIManager
    {
        // -------- Private Fields --------
        private static List<DraggableUI> draggableUiList;

        // -------- Public Methods --------
        public static void Initialize()
        {
            draggableUiList = new List<DraggableUI>();
        }

        public static void Denitialize()
        {
            draggableUiList = null;
        }

        public static void AddDraggableUI(DraggableUI draggableUiToAdd)
        {
            draggableUiList.Add(draggableUiToAdd);
        }

        public static void RemoveDraggableUI(DraggableUI draggableUiToRemove)
        {
            draggableUiList.Remove(draggableUiToRemove);
        }
    }
}
