﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    static class ElementCarryingUIManager
    {
        // -------- Properties --------
        public static bool Activated { get; set; }

        // -------- Private Fields --------
        private static List<ElementCarryingUI> carryingUI;

        // -------- Public Methods --------
        public static void Initialize()
        {
            Activated = false;
            carryingUI = new List<ElementCarryingUI>();
        }

        public static void Denitialize()
        {
            DestroyAllElementCarryingUI();
            carryingUI = null;
        }

        public static void Update()
        {
            foreach (ElementCarryingUI ui in carryingUI)
                ui.Update();
        }

        public static void SBDraw()
        {
            foreach (ElementCarryingUI ui in carryingUI)
                ui.SBDraw();
        }

        public static List<ElementCarryingUI> GetAllElementcarryingUI()
        {
            return carryingUI;
        }

        public static void CreateAllElementCarryingUI()
        {
            foreach (GameEntity ge in GameEntityManager.GetAllGameEntities())
            {
                List<ElementCarrying> carryings = GameEntityUtilities.GetGameEntityElementCarryings(ge);

                if (carryings.Count > 0)
                    CreateElementCarryingUI(ge, carryings);
            }
        }

        public static void DestroyAllElementCarryingUI()
        {
            foreach (ElementCarryingUI c in carryingUI)
                c.Denitialize();

            carryingUI.Clear();
        }

        public static void DestroyElementCarryingUI(GameEntity entity)
        {
            ElementCarryingUI carryingUIToRemove = null;
            foreach (ElementCarryingUI c in carryingUI)
                if (c.GameEntity.Equals(entity))
                    carryingUIToRemove = c;

            if (carryingUIToRemove == null)
                return;

            carryingUIToRemove.Denitialize();
            carryingUI.Remove(carryingUIToRemove);
        }

        public static void OnElementMoved()
        {
            foreach (ElementCarryingUI ui in carryingUI)
                ui.OnElementMoved();
        }

        public static void OnStaffEnterFactory(Staff staff)
        {
            if (!Activated)
                return;

            List<ElementCarrying> carryings = GameEntityUtilities.GetGameEntityElementCarryings(staff);

            if (carryings.Count > 0)
                CreateElementCarryingUI(staff, carryings);
        }
        public static void OnStaffExitFactory(Staff staff)
        {
            if (!Activated)
                return;

            DestroyElementCarryingUI(staff);
        }

        // -------- Private Methods --------
        private static void CreateElementCarryingUI(GameEntity entity, List<ElementCarrying> carryings)
        {
            ElementCarryingUI c = new ElementCarryingUI(entity, carryings);
            c.Initialize();
            carryingUI.Add(c);
        }
    }
}
