﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    static class HudManager
    {
        // -------- Private Fields --------
        private static List<HudPanelUI> huds;
        private static List<HudPanelUI> hudRemovalList;

        // -------- Public Methods --------
        public static void Initialize()
        {
            huds = new List<HudPanelUI>();
            hudRemovalList = new List<HudPanelUI>();

            SetUpHud();
        }

        public static void Denitialize()
        {
            foreach (HudPanelUI hud in huds)
                hud.Denitialize();

            huds = null;
            hudRemovalList = null;
        }

        public static void Update()
        {
            foreach (HudPanelUI hud in huds)
                hud.Update();

            UpdateRemovalList();
        }

        public static void SBDraw()
        {
            foreach (HudPanelUI hud in huds)
                hud.SBDraw();
        }

        public static List<UIEntity> GetAllHuds()
        {
            List<UIEntity> hudList = new List<UIEntity>();
            hudList.AddRange(huds);
            return hudList;
        }

        public static void RemoveHud(HudPanelUI hudToRemove)
        {
            hudRemovalList.Add(hudToRemove);
        }

        // -------- Private Methods ---------
        private static void UpdateRemovalList()
        {
            if (hudRemovalList.Count == 0)
                return;

            foreach (HudPanelUI hud in hudRemovalList)
            {
                hud.Denitialize();
                if (!huds.Remove(hud))
                    Debug.LogWarning("Tried to remove hud panel but wasn't found: " + hud.ToString());
            }

            hudRemovalList.Clear();
        }

        private static void SetUpHud()
        {
            huds.Add(QuickInfoHudManager.SetUpHud());
            huds.Add(OverviewHudManager.SetUpHud());
        }
    }
}
