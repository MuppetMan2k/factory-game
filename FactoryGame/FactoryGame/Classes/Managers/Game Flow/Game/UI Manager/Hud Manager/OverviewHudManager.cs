﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    static class OverviewHudManager
    {
        // -------- Public Methods --------
        public static HudPanelUI SetUpHud()
        {
            HudPanelUI overview = new HudPanelUI(new Point(), eUIAnchor.TOP_RIGHT, "overview-panel");
            overview.Initialize();

            // Menu
            ButtonUI menu = new ButtonUI(new Point(7, 7), eIconSize.ICON_40, eUIAnchor.TOP_RIGHT, eButtonGraphicsType.FLOATING, "button-menu", null, Color.White);
            menu.SetTooltip(Localization.LocalizeString("text_tooltip_menu"), ePortAnchor.TOP);
            menu.Initialize();
            overview.SetChild(menu);

            // Encyclopedia
            ButtonUI encyclopedia = new ButtonUI(new Point(68, 5), eIconSize.ICON_25, eUIAnchor.TOP_RIGHT, eButtonGraphicsType.FLOATING, "button-encyclopedia", null, Color.White);
            encyclopedia.SetTooltip(Localization.LocalizeString("text_tooltip_encyclopedia"), ePortAnchor.TOP);
            encyclopedia.Initialize();
            overview.SetChild(encyclopedia);

            // Almanac
            ButtonUI almanac = new ButtonUI(new Point(103, 5), eIconSize.ICON_25, eUIAnchor.TOP_RIGHT, eButtonGraphicsType.FLOATING, "button-almanac", null, Color.White);
            almanac.SetTooltip(Localization.LocalizeString("text_tooltip_almanac"), ePortAnchor.TOP);
            almanac.Initialize();
            overview.SetChild(almanac);

            // Factory info
            ButtonUI factoryInfo = new ButtonUI(new Point(138, 5), eIconSize.ICON_25, eUIAnchor.TOP_RIGHT, eButtonGraphicsType.FLOATING, "button-factory-info", null, Color.White);
            factoryInfo.SetTooltip(Localization.LocalizeString("text_tooltip_factory_info"), ePortAnchor.TOP);
            factoryInfo.Initialize();
            overview.SetChild(factoryInfo);

            // Purchasing and sales
            ButtonUI purchasingAndSales = new ButtonUI(new Point(173, 5), eIconSize.ICON_25, eUIAnchor.TOP_RIGHT, eButtonGraphicsType.FLOATING, "button-purchasing-and-sales", null, Color.White);
            purchasingAndSales.SetTooltip(Localization.LocalizeString("text_tooltip_purchasing_and_sales"), ePortAnchor.TOP);
            purchasingAndSales.Initialize();
            overview.SetChild(purchasingAndSales);

            // Money
            ButtonUI money = new ButtonUI(new Point(208, 5), eIconSize.ICON_25, eUIAnchor.TOP_RIGHT, eButtonGraphicsType.FLOATING, "button-money", null, Color.White);
            money.SetTooltip(Localization.LocalizeString("text_tooltip_money"), ePortAnchor.TOP);
            money.Initialize();
            overview.SetChild(money);

            // Staff
            ButtonUI staff = new ButtonUI(new Point(243, 5), eIconSize.ICON_25, eUIAnchor.TOP_RIGHT, eButtonGraphicsType.FLOATING, "button-staff", null, Color.White);
            staff.SetTooltip(Localization.LocalizeString("text_tooltip_staff"), ePortAnchor.TOP);
            staff.Initialize();
            overview.SetChild(staff);

            // Machines
            ButtonUI machines = new ButtonUI(new Point(278, 5), eIconSize.ICON_25, eUIAnchor.TOP_RIGHT, eButtonGraphicsType.FLOATING, "button-machines", null, Color.White);
            machines.SetTooltip(Localization.LocalizeString("text_tooltip_machines"), ePortAnchor.TOP);
            machines.Initialize();
            overview.SetChild(machines);

            return overview;
        }
    }
}
