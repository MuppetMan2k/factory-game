﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    static class QuickInfoHudManager
    {
        // -------- Public Methods --------
        public static HudPanelUI SetUpHud()
        {
            int topLineHeight = 6;
            int bottomLineHeight = 32;

            HudPanelUI quickInfoPanel = new HudPanelUI(new Point(0, 0), eUIAnchor.BOTTOM_LEFT, "quick-info-panel");
            quickInfoPanel.Initialize();

            // Clock
            TextUI clock = new TextUI(GameClockManager.GetFullClockString, new Point(0, topLineHeight), eUIAnchor.TOP_LEFT, UIStyleManager.Style.HudFont, 1, 137, UIStyleManager.Style.HudPanelTextColor,
                eTextAlignment.CENTER);
            clock.SetTooltip(Localization.LocalizeString("text_tooltip_clock"), ePortAnchor.BOTTOM);
            clock.Initialize();
            quickInfoPanel.SetChild(clock);

            // Play controls
            ToggleButtonUI pause = new ToggleButtonUI(new Point(13, 8), eIconSize.ICON_20, eUIAnchor.BOTTOM_LEFT, eButtonGraphicsType.FLOATING, "button-pause", GameSpeedManager.SetGameSpeedPaused,
                Color.White, GameSpeedManager.GameSpeedIsPaused);
            pause.SetTooltip(Localization.LocalizeString("text_pause"), ePortAnchor.BOTTOM);
            pause.Initialize();
            quickInfoPanel.SetChild(pause);
            ToggleButtonUI play1 = new ToggleButtonUI(new Point(43, 8), eIconSize.ICON_20, eUIAnchor.BOTTOM_LEFT, eButtonGraphicsType.FLOATING, "button-play1", GameSpeedManager.SetGameSpeedPlay1,
                Color.White, GameSpeedManager.GameSpeedIsPlay1);
            play1.SetTooltip(Localization.LocalizeString("text_play1"), ePortAnchor.BOTTOM);
            play1.Initialize();
            quickInfoPanel.SetChild(play1);
            ToggleButtonUI play2 = new ToggleButtonUI(new Point(73, 8), eIconSize.ICON_20, eUIAnchor.BOTTOM_LEFT, eButtonGraphicsType.FLOATING, "button-play2", GameSpeedManager.SetGameSpeedPlay2,
                Color.White, GameSpeedManager.GameSpeedIsPlay2);
            play2.SetTooltip(Localization.LocalizeString("text_play2"), ePortAnchor.BOTTOM);
            play2.Initialize();
            quickInfoPanel.SetChild(play2);
            ToggleButtonUI play3 = new ToggleButtonUI(new Point(103, 8), eIconSize.ICON_20, eUIAnchor.BOTTOM_LEFT, eButtonGraphicsType.FLOATING, "button-play3", GameSpeedManager.SetGameSpeedPlay3,
                Color.White, GameSpeedManager.GameSpeedIsPlay3);
            play3.SetTooltip(Localization.LocalizeString("text_play3"), ePortAnchor.BOTTOM);
            play3.Initialize();
            quickInfoPanel.SetChild(play3);


            // Balance
            TextUI balance = new TextUI(MoneyManager.GetBalanceString, new Point(138, topLineHeight), eUIAnchor.TOP_LEFT, UIStyleManager.Style.HudFont, 1, 145, UIStyleManager.Style.HudPanelTextColor,
                eTextAlignment.CENTER);
            balance.SetTooltip(Localization.LocalizeString("text_tooltip_balance"), ePortAnchor.BOTTOM);
            balance.Initialize();
            quickInfoPanel.SetChild(balance);

            // Profit
            TextUI profit = new TextUI(DailyProfitManager.GetDailyProfitString, new Point(138, bottomLineHeight), eUIAnchor.TOP_LEFT, UIStyleManager.Style.HudFont, 1, 145,
                UIStyleManager.Style.HudPanelTextColor, eTextAlignment.CENTER);
            profit.SetTooltip(Localization.LocalizeString("text_tooltip_profit"), ePortAnchor.BOTTOM);
            profit.Initialize();
            quickInfoPanel.SetChild(profit);

            return quickInfoPanel;
        }
    }
}
