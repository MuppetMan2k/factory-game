﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    static class ScrollWindowManager
    {
        // -------- Properties --------
        public static float ScrollMovementPerMouseWheelUnit { get { return 0.2f; } }

        // -------- Private Fields --------
        private static List<ScrollWindowUI> scrollWindowList;

        // -------- Public Methods --------
        public static void Initialize()
        {
            scrollWindowList = new List<ScrollWindowUI>();
        }

        public static void Denitialize()
        {
            scrollWindowList = null;
        }

        public static void SBDrawToRenderTarget()
        {
            foreach (ScrollWindowUI sw in scrollWindowList)
                sw.SBDrawToRenderTarget();
        }

        public static void AddScrollWindow(ScrollWindowUI newScrollWindow)
        {
            scrollWindowList.Add(newScrollWindow);
        }

        public static void RemoveScrollWindow(ScrollWindowUI scrollWindowToRemove)
        {
            scrollWindowList.Remove(scrollWindowToRemove);
        }
    }
}
