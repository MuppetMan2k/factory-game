﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    static class TooltipManager
    {
        // -------- Properties --------
        public static int TooltipTextVertBorder { get { return 5; } }
        public static int TooltipTextHorizBorder { get { return 8; } }

        // -------- Private Fields --------
        private static List<TooltipUI> tooltipList;
        private static List<TooltipUI> tooltipRemovalList;
        private static UIEntity mouseOverUI;
        private static float timer = 0f;
        // Const params
        private const float HOLD_TIME = 0.2f;
        private const float FADE_TIME = 0.2f;

        // -------- Public Methods --------
        public static void Initialize()
        {
            tooltipList = new List<TooltipUI>();
            tooltipRemovalList = new List<TooltipUI>();
        }

        public static void Denitialize()
        {
            foreach (TooltipUI ttui in tooltipList)
                ttui.Denitialize();

            tooltipList = null;
            tooltipRemovalList = null;
        }

        public static void Update()
        {
            UpdateFades();

            foreach (TooltipUI ttui in tooltipList)
                ttui.Update();

            UpdateTimer();
            UpdateRemovalList();
        }

        public static void SBDraw()
        {
            foreach (TooltipUI ttui in tooltipList)
                ttui.SBDraw();
        }

        public static void OnMouseOverUI(UIEntity inMouseOverUI)
        {
            if (inMouseOverUI.TooltipInfo == null)
                return;

            timer = 0f;
            mouseOverUI = inMouseOverUI;
        }
        public static void OnMouseOutUI()
        {
            timer = 0f;
            mouseOverUI = null;
        }

        public static void RemoveTooltip(TooltipUI tooltipToRemove)
        {
            tooltipRemovalList.Add(tooltipToRemove);
        }

        public static List<TooltipUI> GetAllTooltips()
        {
            return tooltipList;
        }

        public static int GetTooltipMaxWidth(string text)
        {
            // TODO
            return 250;
        }

        // -------- Private Methods ---------
        private static void UpdateRemovalList()
        {
            if (tooltipRemovalList.Count == 0)
                return;

            foreach (TooltipUI ttui in tooltipRemovalList)
            {
                ttui.Denitialize();
                if (!tooltipList.Remove(ttui))
                    Debug.LogWarning("Tried to remove tooltip but it is not in tooltip list: " + ttui.ToString());
            }

            tooltipRemovalList.Clear();
        }

        private static void UpdateTimer()
        {
            if (timer >= HOLD_TIME)
                return;

            if (mouseOverUI == null)
                return;

            timer += GameTimeManager.ElapsedGameTime;

            if (timer >= HOLD_TIME)
            {
                CreateTooltipForMouseOverUI();
                timer = HOLD_TIME;
            }
        }

        private static void UpdateFades()
        {
            foreach (TooltipUI ttui in tooltipList)
            {
                bool fadeIn = (ttui.AnchorEntity.Equals(mouseOverUI));

                if (fadeIn)
                {
                    if (ttui.FadeValue < 1f)
                    {
                        ttui.FadeValue += GameTimeManager.ElapsedGameTime / FADE_TIME;
                        if (ttui.FadeValue > 1f)
                            ttui.FadeValue = 1f;
                    }
                }
                else
                {
                    ttui.FadeValue -= GameTimeManager.ElapsedGameTime / FADE_TIME;
                    if (ttui.FadeValue <= 0f)
                    {
                        ttui.FadeValue = 0f;
                        ttui.Destroy();
                    }
                }

                ttui.transparency = ttui.FadeValue;
            }
        }

        private static void CreateTooltipForMouseOverUI()
        {
            // Check if already have a tooltip for that entity
            foreach (TooltipUI ttui in tooltipList)
                if (ttui.AnchorEntity.Equals(mouseOverUI))
                    return;

            TooltipUI tooltip = new TooltipUI(mouseOverUI, mouseOverUI.TooltipInfo.Text, mouseOverUI.TooltipInfo.PortAnchor);
            tooltip.Initialize();
            tooltipList.Add(tooltip);
        }
    }
}
