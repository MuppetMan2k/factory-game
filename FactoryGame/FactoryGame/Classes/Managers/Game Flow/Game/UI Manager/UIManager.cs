﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    static class UIManager
    {
        // -------- Public Methods --------
        public static void Initialize()
        {
            UIStyleManager.Initialize();
            HudManager.Initialize();
            TooltipManager.Initialize();
            BubbleManager.Initialize();
            WindowUIManager.Initialize();
            ScrollWindowManager.Initialize();
            DraggableUIManager.Initialize();
            ElementCarryingUIManager.Initialize();
        }

        public static void Denitialize()
        {
            HudManager.Denitialize();
            TooltipManager.Denitialize();
            BubbleManager.Denitialize();
            WindowUIManager.Denitialize();
            ScrollWindowManager.Denitialize();
            DraggableUIManager.Denitialize();
            ElementCarryingUIManager.Initialize();
        }

        public static void Update()
        {
            HudManager.Update();
            TooltipManager.Update();
            BubbleManager.Update();
            WindowUIManager.Update();
            ElementCarryingUIManager.Update();
        }

        public static void SBDraw()
        {
            ElementCarryingUIManager.SBDraw();
            WindowUIManager.SBDraw();
            BubbleManager.SBDraw();
            HudManager.SBDraw();
            TooltipManager.SBDraw();
        }

        public static void SBDrawToRenderTarget()
        {
            ScrollWindowManager.SBDrawToRenderTarget();
        }

        public static void OnMouseOverEntity(Entity entity)
        {
            BubbleManager.OnMouseOverEntity(entity);

            UIEntity ui = entity as UIEntity;
            if (ui != null)
                TooltipManager.OnMouseOverUI(ui);
        }
        public static void OnMouseOutEntity(Entity entity)
        {
            BubbleManager.OnMouseOutEntity();

            if (entity is UIEntity)
                TooltipManager.OnMouseOutUI();
        }
        public static void OnMouseClickEntity(Entity entity)
        {
            WindowUIManager.OnMouseClickEntity(entity);
        }

        public static List<UIEntity> GetAllUIEntities()
        {
            List<UIEntity> ui = new List<UIEntity>();

            ui.AddRange(HudManager.GetAllHuds());
            ui.AddRange(TooltipManager.GetAllTooltips());
            if (WindowUIManager.WindowExists())
                ui.Add(WindowUIManager.GetWindowUI());
            ui.AddRange(ElementCarryingUIManager.GetAllElementcarryingUI());

            return ui;
        }

        public static void OnElementMoved()
        {
            ElementCarryingUIManager.OnElementMoved();
        }

        public static void OnStaffEnterFactory(Staff staff)
        {
            ElementCarryingUIManager.OnStaffEnterFactory(staff);
        }
        public static void OnStaffExitFactory(Staff staff)
        {
            ElementCarryingUIManager.OnStaffExitFactory(staff);
        }
    }
}
