﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    static class UIStyleManager
    {
        // -------- Properties --------
        public static UIStyle Style { get; private set; }

        // -------- Public Methods --------
        public static void Initialize()
        {
            Style = UIStyle.Default;
        }
    }
}
