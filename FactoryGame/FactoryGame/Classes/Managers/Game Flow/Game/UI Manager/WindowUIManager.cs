﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    static class WindowUIManager
    {
        // -------- Properties --------
        public static int WindowWidth { get { return 400; } }
        public static int WindowHeight { get { return 550; } }
        public static int WindowLimitFrameWidth { get { return 25; } }
        public static int WindowLimitFrameHeight { get { return 50; } }
        public static int WindowHeaderHeight { get { return 35; } }
        public static int IconsPerRow { get { return 6; } }

        // -------- Private Fields --------
        private static WindowUI window;

        // -------- Public Methods --------
        public static void Initialize()
        {

        }

        public static void Denitialize()
        {
            window.Denitialize();
            window = null;
        }

        public static void Update()
        {
            if (window != null)
                window.Update();
        }

        public static void SBDraw()
        {
            if (window != null)
                window.SBDraw();
        }

        public static void OnMouseClickEntity(Entity entity)
        {
            if (window != null)
            {
                Entity currentEntity = window.GetWindowUIEntity();
                if (currentEntity != null && currentEntity.Equals(entity))
                    return;
            }

            WindowUI newWindow = CreateWindowForEntity(entity);
            if (newWindow == null)
                return;

            SetWindow(newWindow);
        }

        public static void RemoveWindow()
        {
            window.Denitialize();
            window = null;
        }

        public static void SetWindow(WindowUI newWindow)
        {
            if (newWindow.Equals(window))
                return;

            if (window != null)
                RemoveWindow();

            window = newWindow;
        }

        public static bool WindowExists()
        {
            return (window != null);
        }

        public static WindowUI GetWindowUI()
        {
            return window;
        }

        // -------- Private Methods ---------
        // Window creation methods
        private static WindowUI CreateWindowForEntity(Entity entity)
        {
            Staff staff = entity as Staff;
            if (staff != null)
            {
                StaffWindowUI staffWindow = new StaffWindowUI(GetCreatedWindowPosition(staff), staff);
                staffWindow.Initialize();
                return staffWindow;
            }

            // Add more window creation cases here...

            return null;
        }        

        private static Point GetCreatedWindowPosition(Entity entity)
        {
            // TODO
            return new Point(100, 100);
        }
    }
}
