﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    // -------- Enumerations --------
    public enum eGameFlowState
    {
        SPLASH,
        LOADING,
        MENU,
        GAME
    }

    static class GameFlowManager
    {
        // -------- Private Fields --------
        private static eGameFlowState gameFlowState;
        private static eGameFlowState queuedGameFlowState;
        private static bool gameFlowStateWillChange;

        // -------- Public Methods --------
        public static void Initialize()
        {
            gameFlowState = eGameFlowState.SPLASH;
            gameFlowStateWillChange = false;
            GameFlowInitialize();
        }

        public static void Update()
        {
            if (gameFlowStateWillChange)
            {
                CarryOutGameFlowStateChange();
                return;
            }

            GameFlowUpdate();
        }

        public static void Draw()
        {
            GameFlowDraw();
        }

        public static void DrawTransparent()
        {
            GameFlowDrawTransparent();
        }

        public static void SBDraw()
        {
            GameFlowSBDraw();
        }

        public static void SBDrawToRenderTarget()
        {
            GameFlowSBDrawToRenderTarget();
        }

        public static void ChangeGameFlowState(eGameFlowState newState)
        {
            if (gameFlowState == newState)
                return;

            queuedGameFlowState = newState;
            gameFlowStateWillChange = true;
        }

        // -------- Private Methods ---------
        private static void CarryOutGameFlowStateChange()
        {
            GameFlowDenitialize();
            gameFlowState = queuedGameFlowState;
            GameFlowInitialize();

            gameFlowStateWillChange = false;
        }

        // Initialize
        private static void GameFlowInitialize()
        {
            switch (gameFlowState)
            {
                case eGameFlowState.SPLASH:
                    SplashManager.Initialize();
                    break;
                case eGameFlowState.LOADING:
                    LoadingManager.Initialize();
                    break;
                case eGameFlowState.MENU:
                    MenuManager.Initialize();
                    break;
                case eGameFlowState.GAME:
                    GameManager.Initialize();
                    break;
            }
        }

        // Denitialize
        private static void GameFlowDenitialize()
        {
            switch (gameFlowState)
            {
                case eGameFlowState.SPLASH:
                    SplashManager.Denitialize();
                    break;
                case eGameFlowState.LOADING:
                    LoadingManager.Denitialize();
                    break;
                case eGameFlowState.MENU:
                    MenuManager.Denitialize();
                    break;
                case eGameFlowState.GAME:
                    GameManager.Denitialize();
                    break;
            }
        }

        // Update
        private static void GameFlowUpdate()
        {
            switch (gameFlowState)
            {
                case eGameFlowState.SPLASH:
                    SplashManager.Update();
                    break;
                case eGameFlowState.LOADING:
                    LoadingManager.Update();
                    break;
                case eGameFlowState.MENU:
                    MenuManager.Update();
                    break;
                case eGameFlowState.GAME:
                    GameManager.Update();
                    break;
            }
        }

        // Draw
        private static void GameFlowDraw()
        {
            switch (gameFlowState)
            {
                case eGameFlowState.SPLASH:
                    SplashManager.Draw();
                    break;
                case eGameFlowState.LOADING:
                    LoadingManager.Draw();
                    break;
                case eGameFlowState.MENU:
                    MenuManager.Draw();
                    break;
                case eGameFlowState.GAME:
                    GameManager.Draw();
                    break;
            }
        }

        // DrawTransparent
        private static void GameFlowDrawTransparent()
        {
            switch (gameFlowState)
            {
                case eGameFlowState.SPLASH:
                    SplashManager.DrawTransparent();
                    break;
                case eGameFlowState.LOADING:
                    LoadingManager.DrawTransparent();
                    break;
                case eGameFlowState.MENU:
                    MenuManager.DrawTransparent();
                    break;
                case eGameFlowState.GAME:
                    GameManager.DrawTransparent();
                    break;
            }
        }

        // SB Draw
        private static void GameFlowSBDraw()
        {
            switch (gameFlowState)
            {
                case eGameFlowState.SPLASH:
                    SplashManager.SBDraw();
                    break;
                case eGameFlowState.LOADING:
                    LoadingManager.SBDraw();
                    break;
                case eGameFlowState.MENU:
                    MenuManager.SBDraw();
                    break;
                case eGameFlowState.GAME:
                    GameManager.SBDraw();
                    break;
            }
        }

        // SB Draw To Render Target
        private static void GameFlowSBDrawToRenderTarget()
        {
            switch (gameFlowState)
            {
                case eGameFlowState.SPLASH:
                    SplashManager.SBDrawToRenderTarget();
                    break;
                case eGameFlowState.LOADING:
                    LoadingManager.SBDrawToRenderTarget();
                    break;
                case eGameFlowState.MENU:
                    MenuManager.SBDrawToRenderTarget();
                    break;
                case eGameFlowState.GAME:
                    GameManager.SBDrawToRenderTarget();
                    break;
            }
        }
    }
}
