﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    static class LoadingInformationManager
    {
        // -------- Private Fields --------
        private static eLoadingTargetFlowState loadingTargetFlowState;

        // -------- Public Methods --------
        public static LoadingInfo GetLoadingInfo()
        {
            var info = new LoadingInfo();

            info.TargetFlowState = loadingTargetFlowState;

            return info;
        }

        public static void SetLoadingTargetFlowState(eLoadingTargetFlowState targetFlowState)
        {
            loadingTargetFlowState = targetFlowState;
        }
    }
}
