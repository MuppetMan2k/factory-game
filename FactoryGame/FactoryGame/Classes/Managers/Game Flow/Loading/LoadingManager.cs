﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    static class LoadingManager
    {
        // -------- Private Fields --------
        private static LoadingInfo loadingInfo;

        // -------- Public Methods --------
        public static void Initialize()
        {
            CursorManager.SetVisiblity(false);
            loadingInfo = LoadingInformationManager.GetLoadingInfo();
        }

        public static void Denitialize()
        {

        }

        public static void Update()
        {
            switch (loadingInfo.TargetFlowState)
            {
                case eLoadingTargetFlowState.MENU:
                    GameFlowManager.ChangeGameFlowState(eGameFlowState.MENU);
                    break;
                case eLoadingTargetFlowState.GAME:
                    GameFlowManager.ChangeGameFlowState(eGameFlowState.GAME);
                    break;
                default:
                    break;
            }
        }

        public static void Draw()
        {

        }

        public static void DrawTransparent()
        {

        }

        public static void SBDraw()
        {

        }

        public static void SBDrawToRenderTarget()
        {

        }
    }
}
