﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    static class MenuManager
    {
        // -------- Public Methods --------
        public static void Initialize()
        {
            CursorManager.SetCursorMode(eCursorMode.POINTER);
        }

        public static void Denitialize()
        {

        }

        public static void Update()
        {
            LoadingInformationManager.SetLoadingTargetFlowState(eLoadingTargetFlowState.GAME);
            GameFlowManager.ChangeGameFlowState(eGameFlowState.LOADING);
        }

        public static void Draw()
        {

        }

        public static void DrawTransparent()
        {

        }

        public static void SBDraw()
        {

        }

        public static void SBDrawToRenderTarget()
        {

        }
    }
}
