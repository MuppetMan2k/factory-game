﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    static class SplashManager
    {
        // -------- Public Methods --------
        public static void Initialize()
        {
            CursorManager.SetVisiblity(false);
        }

        public static void Denitialize()
        {

        }

        public static void Update()
        {
            LoadingInformationManager.SetLoadingTargetFlowState(eLoadingTargetFlowState.MENU);
            GameFlowManager.ChangeGameFlowState(eGameFlowState.LOADING);
        }

        public static void Draw()
        {

        }

        public static void DrawTransparent()
        {

        }

        public static void SBDraw()
        {

        }

        public static void SBDrawToRenderTarget()
        {

        }
    }
}
