﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    static class ConfigFileManager
    {
        // -------- Properties --------
        public static Settings SettingsConfig { get; private set; }

        // -------- Public Methods --------
        public static void LoadContent()
        {
            LoadSettingsConfig();
        }

        // -------- Private Methods --------
        private static void LoadSettingsConfig()
        {
            string fileName = "Settings.config";

            if (!FileManager.FileExistsInStore(eFileType.SETTINGS, fileName))
            {
                SettingsConfig = Settings.GetDefaultSettings();
                return;
            }

            string path = FileManager.GetFilePath(eFileType.SETTINGS, fileName);
            SettingsConfig = XMLReader.ReadFromXml<Settings>(path);

            if (SettingsConfig == null)
                SettingsConfig = Settings.GetDefaultSettings();
        }
    }
}
