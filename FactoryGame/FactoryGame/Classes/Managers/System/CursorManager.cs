﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FactoryGame
{
    // -------- Enumerations --------
    public enum eCursorMode
    {
        POINTER,
        SELECT
    }

    static class CursorManager
    {
        // -------- Private Fields --------
        private static Point position;
        private static eCursorMode cursorMode;
        private static bool visible;

        // -------- Public Methods --------
        public static void Initialize()
        {
            visible = true;
            cursorMode = eCursorMode.POINTER;
        }

        public static void Update()
        {
            position = InputManager.GetMousePosition();
        }

        public static void SBDraw()
        {
            if (!visible)
                return;

            Texture2D texture;
            Rectangle rectangle;
            GetCursorTextureAndRectangle(out texture, out rectangle);

            GraphicsManager.GetSpriteBatch().Draw(texture, rectangle, Color.White);
        }

        public static void SetVisiblity(bool visibility)
        {
            visible = visibility;
        }

        public static void SetCursorMode(eCursorMode mode)
        {
            cursorMode = mode;
            SetVisiblity(true);
        }

        // -------- Private Methods ---------
        // Get cursor texture and rectangle
        private static void GetCursorTextureAndRectangle(out Texture2D texture, out Rectangle rectangle)
        {
            switch (cursorMode)
            {
                case eCursorMode.POINTER:
                    GetCursorTextureAndRectangle_Pointer(out texture, out rectangle);
                    break;
                case eCursorMode.SELECT:
                    GetCursorTextureAndRectangle_Select(out texture, out rectangle);
                    break;
                default:
                    GetCursorTextureAndRectangle_Pointer(out texture, out rectangle);
                    break;
            }
        }
        private static void GetCursorTextureAndRectangle_Pointer(out Texture2D texture, out Rectangle rectangle)
        {
            texture = ContentStorage.GetTexture("pointer-cursor");
            rectangle = new Rectangle(position.X, position.Y, 16, 16);
        }
        private static void GetCursorTextureAndRectangle_Select(out Texture2D texture, out Rectangle rectangle)
        {
            texture = ContentStorage.GetTexture("select-cursor");
            rectangle = new Rectangle(position.X - 6, position.Y, texture.Width, texture.Height);
        }
    }
}
