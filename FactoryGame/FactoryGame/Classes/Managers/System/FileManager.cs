﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace FactoryGame
{
    // -------- Enumerations --------
    public enum eFileType
    {
        SETTINGS
    }

    static class FileManager
    {
        // -------- Private Fields --------
        private static string fileStoreRootPath;

        // -------- Public Methods --------
        public static void Initialize()
        {
            fileStoreRootPath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
        }

        public static string GetFileStorePath()
        {
            string fileStorePath = System.IO.Path.Combine(fileStoreRootPath, ProgramInfo.CompanyName, ProgramInfo.ProgramName);

            return fileStorePath;
        }

        public static string GetFileTypeStorePath(eFileType fileType)
        {
            string folder = null;
            switch (fileType)
            {
                case eFileType.SETTINGS:
                    folder = "Settings";
                    break;
                default:
                    break;
            }

            if (string.IsNullOrEmpty(folder))
                return null;

            string fileTypeStorePath = System.IO.Path.Combine(GetFileStorePath(), folder);
            return fileTypeStorePath;
        }

        public static string GetFilePath(eFileType fileType, string fileName)
        {
            string filePath = System.IO.Path.Combine(GetFileTypeStorePath(fileType), fileName);
            return filePath;
        }

        public static bool FileExistsInStore(eFileType type, string fileName)
        {
            return File.Exists(GetFilePath(type, fileName));
        }
    }
}
