﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Keys = Microsoft.Xna.Framework.Input.Keys;

namespace FactoryGame
{
    static class GameExitManager
    {
        // -------- Private Fields --------
        private static FactoryGame game;

        // -------- Public Methods --------
        public static void Initialize(FactoryGame inGame)
        {
            game = inGame;
        }

        public static void Update()
        {
            if (InputManager.KeyIsDown(Keys.Escape))
                game.Exit();
        }
    }
}
