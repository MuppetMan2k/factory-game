﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    static class GameTimeManager
    {
        // -------- Properties --------
        public static float ElapsedGameTime { get; private set; }

        // -------- Public Methods --------
        public static void SetGameTime(GameTime gameTime)
        {
            ElapsedGameTime = (float)gameTime.ElapsedGameTime.TotalSeconds;
        }
    }
}
