﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FactoryGame
{
    static class GraphicsManager
    {
        // -------- Properties --------
        public static int ScreenWidth { get; private set; }
        public static int ScreenHeight { get; private set; }
        public static float AspectRatio { get { return (float)ScreenWidth / (float)ScreenHeight; } }

        // -------- Private Fields --------
        private static GraphicsDeviceManager graphics;
        private static GraphicsDevice device;
        private static SpriteBatch spriteBatch;

        private static int initialScreenWidth = 1024;
        private static int initialScreenHeight = 768;
        private static bool initialFullScreen = false;
        
        // -------- Public Methods --------
        // Initialization methods
        public static void SetUpGraphicsDeviceManager(Game parentGame)
        {
            graphics = new GraphicsDeviceManager(parentGame);

            graphics.PreferredBackBufferWidth = initialScreenWidth;
            graphics.PreferredBackBufferHeight = initialScreenHeight;
            graphics.IsFullScreen = initialFullScreen;
            graphics.ApplyChanges();
        }
        public static void SetUpGraphicsDevice(GraphicsDevice inDevice)
        {
            device = inDevice;
        }
        public static void SetUpSpriteBatch()
        {
            spriteBatch = new SpriteBatch(device);
        }

        // Get accessor methods
        public static GraphicsDevice GetGraphicsDevice()
        {
            return device;
        }
        public static SpriteBatch GetSpriteBatch()
        {
            return spriteBatch;
        }

        // Draw methods
        public static void Draw()
        {
            // Draw stage 0 - Render Target draw calls
            // =======================================
            GameFlowManager.SBDrawToRenderTarget();
            device.SetRenderTarget(null);
            
            device.Clear(Color.Black);

            // Draw stage 1 - 3D draw calls
            // ============================
            SetOpaqueDeviceOptions();
            GameFlowManager.Draw();

            // Draw stage 2 - Transparent 3D draw calls
            // ========================================
            SetTransparentDeviceOptions();
            GameFlowManager.DrawTransparent();

            // Draw stage 3 - Sprite batch draw calls
            // ======================================
            BeginSpriteBatch();
            GameFlowManager.SBDraw();
            CursorManager.SBDraw();
            Debug.SBDraw();
            EndSpriteBatch();
        }

        public static void BeginSpriteBatch()
        {
            spriteBatch.Begin(GetSpriteSortMode(), GetSpriteBlendState());
        }
        public static void EndSpriteBatch()
        {
            spriteBatch.End();
        }

        public static VertexBuffer GetFullScreenQuadVertexBuffer()
        {
            VertexBuffer buffer = new VertexBuffer(device, typeof(VertexPositionTexture), 4, BufferUsage.WriteOnly);

            VertexPositionTexture[] vertices = new VertexPositionTexture[4];
            vertices[0] = new VertexPositionTexture(new Vector3(-1f, 1f, 0f), new Vector2(0f, 0f));
            vertices[1] = new VertexPositionTexture(new Vector3(1f, 1f, 0f), new Vector2(1f, 0f));
            vertices[2] = new VertexPositionTexture(new Vector3(-1f, -1f, 0f), new Vector2(0f, 1f));
            vertices[3] = new VertexPositionTexture(new Vector3(1f, -1f, 0f), new Vector2(1f, 1f));

            buffer.SetData<VertexPositionTexture>(vertices);

            return buffer;
        }
        public static IndexBuffer GetFullScreenQuadIndexBuffer()
        {
            IndexBuffer buffer = new IndexBuffer(device, IndexElementSize.SixteenBits, 6, BufferUsage.WriteOnly);

            short[] indices = new short[6];
            indices[0] = 0;
            indices[1] = 1;
            indices[2] = 2;
            indices[3] = 2;
            indices[4] = 1;
            indices[5] = 3;

            buffer.SetData<short>(indices);

            return buffer;
        }

        public static Rectangle GetFullScreenRectangle()
        {
            return new Rectangle(0, 0, ScreenWidth, ScreenHeight);
        }

        public static Resolution GetDefaultResolution()
        {
            int width = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            int height = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
            return new Resolution(width, height);
        }

        public static void SetResolutionFromSettings()
        {
            Settings settings = ConfigFileManager.SettingsConfig;

            ScreenWidth = settings.Resolution.ScreenWidth;
            graphics.PreferredBackBufferWidth = settings.Resolution.ScreenWidth;
            ScreenHeight = settings.Resolution.ScreenHeight;
            graphics.PreferredBackBufferHeight = settings.Resolution.ScreenHeight;
            graphics.IsFullScreen = !settings.Windowed;
            graphics.ApplyChanges();
        }

        // -------- Private Methods ---------
        // Opaque device options
        private static void SetOpaqueDeviceOptions()
        {
            device.BlendState = BlendState.Opaque;
            device.RasterizerState = GetRasterizerState();
            device.DepthStencilState = GetOpaqueDepthStencilState();
        }
        private static DepthStencilState GetOpaqueDepthStencilState()
        {
            var dss = new DepthStencilState();

            dss.DepthBufferEnable = true;
            dss.DepthBufferFunction = CompareFunction.LessEqual;

            return dss;
        }

        // Transparent device options
        private static void SetTransparentDeviceOptions()
        {
            device.BlendState = GetTransparentBlendState();
            device.RasterizerState = GetRasterizerState();
            device.DepthStencilState = GetTransparentDepthStencilState();
        }
        private static BlendState GetTransparentBlendState()
        {
            var bs = new BlendState();

            bs.ColorBlendFunction = BlendFunction.Add;
            bs.ColorSourceBlend = Blend.SourceAlpha;
            bs.ColorDestinationBlend = Blend.InverseSourceAlpha;

            return bs;
        }
        private static DepthStencilState GetTransparentDepthStencilState()
        {
            var dss = new DepthStencilState();

            dss.DepthBufferEnable = true;
            dss.DepthBufferFunction = CompareFunction.LessEqual;

            return dss;
        }

        // Sprite Batch device options
        private static SpriteSortMode GetSpriteSortMode()
        {
            return SpriteSortMode.Deferred;
        }
        private static BlendState GetSpriteBlendState()
        {
            return BlendState.NonPremultiplied;
        }

        private static RasterizerState GetRasterizerState()
        {
            var rs = new RasterizerState();

            rs.CullMode = CullMode.CullCounterClockwiseFace;
            rs.MultiSampleAntiAlias = true;

            return rs;
        }
        
    }
}
