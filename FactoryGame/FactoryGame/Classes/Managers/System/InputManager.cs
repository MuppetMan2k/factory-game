﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace FactoryGame
{
    // -------- Enumerations --------
    public enum ePressState
    {
        READY,
        PRESSING,
        PRESSED,
        RELEASING
    }

    public enum eMouseButton
    {
        LEFT,
        RIGHT,
        MIDDLE,
        X1,
        X2
    }

    static class InputManager
    {
        // -------- Private Fields --------
        private static KeyboardState keyboardState;
        private static Dictionary<Keys, ePressState> keyPressStateDict;
        private static MouseState mouseState;
        private static Dictionary<eMouseButton, ePressState> buttonPressStateDict;
        private static Vector2 ssMousePosition;
        private static int currentScrollValue;
        private static int lastScrollValue;

        // -------- Public Methods --------
        public static void Initialize()
        {
            InitializeKeyboard();
            InitializeMouse();
        }

        public static void Update()
        {
            UpdateKeyboard();
            UpdateMouse();
        }

        // Keyboard methods
        public static bool KeyIsDown(Keys key, bool onlyPressing = true)
        {
            ePressState pressState = keyPressStateDict[key];

            if (onlyPressing)
                return (pressState == ePressState.PRESSING);
            else
                return (pressState == ePressState.PRESSING || pressState == ePressState.PRESSED);
        }
        public static bool KeyPairIsPressed(Keys holdKey, Keys triggerKey)
        {
            return (KeyIsDown(holdKey, false) && KeyIsDown(triggerKey));
        }

        // Mouse methods
        public static bool ButtonIsDown(eMouseButton button, bool onlyPressing = true)
        {
            ePressState pressState = buttonPressStateDict[button];

            if (onlyPressing)
                return (pressState == ePressState.PRESSING);
            else
                return (pressState == ePressState.PRESSING || pressState == ePressState.PRESSED);
        }
        public static ePressState GetButtonPressState(eMouseButton button)
        {
            return buttonPressStateDict[button];
        }
        public static int GetTotalMouseScroll()
        {
            return mouseState.ScrollWheelValue;
        }
        public static int GetElapsedMouseScroll()
        {
            return (currentScrollValue - lastScrollValue);
        }
        public static Point GetMousePosition()
        {
            return new Point(mouseState.X, mouseState.Y);
        }
        public static Vector2 GetMouseSSPosition()
        {
            return ssMousePosition;
        }

        public static bool MouseIsAtTopOfScreen()
        {
            return GetMousePosition().Y == 0;
        }
        public static bool MouseIsAtRightOfScreen()
        {
            return GetMousePosition().X == GraphicsManager.ScreenWidth - 1;
        }
        public static bool MouseIsAtBottomOfScreen()
        {
            return GetMousePosition().Y == GraphicsManager.ScreenHeight - 1;
        }
        public static bool MouseIsAtLeftOfScreen()
        {
            return GetMousePosition().X == 0;
        }

        // -------- Private Methods --------
        private static void InitializeKeyboard()
        {
            keyPressStateDict = new Dictionary<Keys, ePressState>();

            foreach (Keys key in (Keys[])Enum.GetValues(typeof(Keys)))
                keyPressStateDict.Add(key, ePressState.READY);
        }
        private static void InitializeMouse()
        {
            buttonPressStateDict = new Dictionary<eMouseButton, ePressState>();

            foreach (eMouseButton button in (eMouseButton[])Enum.GetValues(typeof(eMouseButton)))
                buttonPressStateDict.Add(button, ePressState.READY);

            ssMousePosition = SpaceUtilities.ConvertPsToSs(GetMousePosition());
            currentScrollValue = 0;
            lastScrollValue = 0;
        }

        private static void UpdateKeyboard()
        {
            keyboardState = Keyboard.GetState();

            foreach (Keys key in (Keys[])Enum.GetValues(typeof(Keys)))
            {
                ePressState pressState = keyPressStateDict[key];

                if (keyboardState.IsKeyDown(key))
                {
                    if (pressState == ePressState.READY)
                        keyPressStateDict[key] = ePressState.PRESSING;

                    if (pressState == ePressState.PRESSING)
                        keyPressStateDict[key] = ePressState.PRESSED;
                }
                else
                {
                    if (pressState == ePressState.RELEASING)
                        keyPressStateDict[key] = ePressState.READY;
                    else if (pressState != ePressState.READY)
                        keyPressStateDict[key] = ePressState.RELEASING;
                }
            }
        }
        private static void UpdateMouse()
        {
            mouseState = Mouse.GetState();

            foreach (eMouseButton button in (eMouseButton[])Enum.GetValues(typeof(eMouseButton)))
            {
                ePressState pressState = buttonPressStateDict[button];

                if (MouseButtonIsDown(button))
                {
                    if (pressState == ePressState.READY)
                        buttonPressStateDict[button] = ePressState.PRESSING;

                    if (pressState == ePressState.PRESSING)
                        buttonPressStateDict[button] = ePressState.PRESSED;
                }
                else
                {
                    if (pressState == ePressState.RELEASING)
                        buttonPressStateDict[button] = ePressState.READY;
                    else if (pressState != ePressState.READY)
                        buttonPressStateDict[button] = ePressState.RELEASING;
                }
            }

            ssMousePosition = SpaceUtilities.ConvertPsToSs(GetMousePosition());
            lastScrollValue = currentScrollValue;
            currentScrollValue = mouseState.ScrollWheelValue;
        }

        private static bool MouseButtonIsDown(eMouseButton button)
        {
            switch (button)
            {
                case eMouseButton.LEFT:
                    return mouseState.LeftButton == ButtonState.Pressed;
                case eMouseButton.MIDDLE:
                    return mouseState.MiddleButton == ButtonState.Pressed;
                case eMouseButton.RIGHT:
                    return mouseState.RightButton == ButtonState.Pressed;
                case eMouseButton.X1:
                    return mouseState.XButton1 == ButtonState.Pressed;
                case eMouseButton.X2:
                    return mouseState.XButton2 == ButtonState.Pressed;
                default:
                    return false;
            }
        }
    }
}
