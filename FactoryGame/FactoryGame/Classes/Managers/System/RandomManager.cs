﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    static class RandomManager
    {
        // -------- Private Fields --------
        private static Random random;

        // -------- Public Methods --------
        public static void Initialize()
        {
            random = new Random(CreateSeed());
        }

        public static float RandomNumber(float min, float max)
        {
            return (min + (max - min) * (float)random.NextDouble());
        }
        public static int RandomNumber(int min, int max)
        {
            return random.Next(min, max + 1);
        }

        // -------- Private Methods ---------
        private static int CreateSeed()
        {
            DateTime now = DateTime.UtcNow;

            int seed = now.Hour * 3600000 + now.Minute * 60000 + now.Second * 1000 + now.Millisecond;

            return seed;
        }
    }
}
