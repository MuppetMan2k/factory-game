﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class Resolution
    {
        // -------- Properties --------
        public int ScreenWidth { get; set; }
        public int ScreenHeight { get; set; }

        // -------- Constructors --------
        public Resolution(int inWidth, int inHeight)
        {
            ScreenWidth = inWidth;
            ScreenHeight = inHeight;
        }

        // -------- Public Methods --------
        public override string ToString()
        {
            return (ScreenWidth + "x" + ScreenHeight);
        }
        public static bool ParseFromString(string resString, out Resolution res)
        {
            string[] parts = resString.Split('x');

            if (parts.Length != 2)
            {
                res = null;
                return false;
            }

            int width;
            int height;
            if (!int.TryParse(parts[0], out width) || !int.TryParse(parts[1], out height))
            {
                res = null;
                return false;
            }

            res = new Resolution(width, height);
            return true;
        }
    }
}
