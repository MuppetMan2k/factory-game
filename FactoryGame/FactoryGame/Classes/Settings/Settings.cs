﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    class Settings
    {
        // -------- Public Fields --------
        public Resolution Resolution { get; set; }
        public bool Windowed { get; set; }

        // -------- Public Methods --------
        public static Settings GetDefaultSettings()
        {
            Settings settings = new Settings();

            settings.Resolution = GraphicsManager.GetDefaultResolution();
            settings.Windowed = true;
            // TODO - Add other default settings

            return settings;
        }
    }
}
