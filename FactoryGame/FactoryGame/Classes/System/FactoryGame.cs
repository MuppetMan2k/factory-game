using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace FactoryGame
{
    public class FactoryGame : Game
    {
        // -------- Constructors --------
        public FactoryGame()
        {
            GraphicsManager.SetUpGraphicsDeviceManager(this);
            ContentLoader.SetUpContentManager(Content);
        }

        // -------- Protected Methods --------
        protected override void Initialize()
        {
            GameExitManager.Initialize(this);
            InputManager.Initialize();
            FileManager.Initialize();
            ContentStorage.Initialize();
            Localization.Initialize();
            Debug.Initialize();
            CursorManager.Initialize();
            RandomManager.Initialize();

            GameFlowManager.Initialize();
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        protected override void LoadContent()
        {
            GraphicsManager.SetUpGraphicsDevice(GraphicsDevice);
            GraphicsManager.SetUpSpriteBatch();

            ConfigFileManager.LoadContent();
            GraphicsManager.SetResolutionFromSettings();

            ContentList.LoadContent();
            Localization.LoadContent();
            ContentStorage.LoadInitialContent();

            // TODO: use this.Content to load your game content here
        }

        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        protected override void Update(GameTime gameTime)
        {
            GameTimeManager.SetGameTime(gameTime);
            InputManager.Update();
            GameExitManager.Update();
            CursorManager.Update();
            Debug.Update();

            GameFlowManager.Update();
            // TODO: Add your update logic here

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsManager.Draw();

            base.Draw(gameTime);
        }
    }
}
