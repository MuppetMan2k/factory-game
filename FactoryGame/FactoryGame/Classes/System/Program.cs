using System;

namespace FactoryGame
{
#if WINDOWS || XBOX
    static class Program
    {
        static void Main(string[] args)
        {
            using (FactoryGame game = new FactoryGame())
            {
                game.Run();
            }
        }
    }
#endif
}

