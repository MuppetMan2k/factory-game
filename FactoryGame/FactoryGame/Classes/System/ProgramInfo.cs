﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    static class ProgramInfo
    {
        // -------- Properties --------
        public static string ProgramName { get { return "Factory Game"; } }
        public static string CompanyName { get { return "Jasmine Software"; } }
    }
}
