﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    static class AreaUtilities
    {
        // -------- Public Methods --------
        public static AreaCoordinate GetSingleTileArea(TileCoordinate tileCoord)
        {
            return new AreaCoordinate(tileCoord, 1, 1, eSquareRotation.ROTATION_0);
        }

        public static TileCoordinate ConvertAreaLocalCoordToAbsolute(AreaCoordinate area, TileCoordinate localCoord)
        {
            TileCoordinate absCoord = area.Coordinate;

            switch (area.Rotation)
            {
                case eSquareRotation.ROTATION_0:
                    absCoord.Row += localCoord.Row;
                    absCoord.Col += localCoord.Col;
                    break;
                case eSquareRotation.ROTATION_90:
                    absCoord.Row += localCoord.Col;
                    absCoord.Col -= localCoord.Row;
                    break;
                case eSquareRotation.ROTATION_180:
                    absCoord.Row -= localCoord.Row;
                    absCoord.Col -= localCoord.Col;
                    break;
                case eSquareRotation.ROTATION_270:
                    absCoord.Row -= localCoord.Col;
                    absCoord.Col += localCoord.Row;
                    break;
                default:
                    break;
            }

            return absCoord;
        }

        public static Vector2 GetAreaCentralTs(AreaCoordinate area)
        {
            Vector2 ts1 = TileUtilities.GetTileCornerTsPosition(area.Coordinate, area.Rotation);
            Vector2 center = new Vector2();

            switch (area.Rotation)
            {
                case eSquareRotation.ROTATION_0:
                    center = new Vector2(ts1.X + (float)area.Width / 2f, ts1.Y + (float)area.Height / 2f);
                    break;
                case eSquareRotation.ROTATION_90:
                    center = new Vector2(ts1.X - (float)area.Height / 2f, ts1.Y + (float)area.Width / 2f);
                    break;
                case eSquareRotation.ROTATION_180:
                    center = new Vector2(ts1.X - (float)area.Width / 2f, ts1.Y - (float)area.Height / 2f);
                    break;
                case eSquareRotation.ROTATION_270:
                    center = new Vector2(ts1.X + (float)area.Height / 2f, ts1.Y - (float)area.Width / 2f);
                    break;
                default:
                    break;
            }

            return center;
        }
    }
}
