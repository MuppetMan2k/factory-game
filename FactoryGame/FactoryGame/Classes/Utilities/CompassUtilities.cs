﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    // -------- Enumerations --------
    // Compass directions
    public enum eCompassDirection
    {
        NORTH,
        NORTHEAST,
        EAST,
        SOUTHEAST,
        SOUTH,
        SOUTHWEST,
        WEST,
        NORTHWEST
    }
    public enum eSquareCompassDirection
    {
        NORTH,
        EAST,
        SOUTH,
        WEST
    }
    public enum eDiagonalCompassDirection
    {
        NORTHEAST,
        SOUTHEAST,
        SOUTHWEST,
        NORTHWEST
    }

    // Rotations
    public enum eRotation
    {
        ROTATION_0,
        ROTATION_45,
        ROTATION_90,
        ROTATION_135,
        ROTATION_180,
        ROTATION_225,
        ROTATION_270,
        ROTATION_315
    }
    public enum eSquareRotation
    {
        ROTATION_0,
        ROTATION_90,
        ROTATION_180,
        ROTATION_270
    }
    public enum eRotationDirection
    {
        CLOCKWISE,
        ANTICLOCKWISE
    }

    static class CompassUtilities
    {
        // -------- Public Methods --------
        public static eCompassDirection ToAbsCompassDirection(eSquareCompassDirection direction)
        {
            switch (direction)
            {
                case eSquareCompassDirection.NORTH:
                    return eCompassDirection.NORTH;
                case eSquareCompassDirection.EAST:
                    return eCompassDirection.EAST;
                case eSquareCompassDirection.SOUTH:
                    return eCompassDirection.SOUTH;
                case eSquareCompassDirection.WEST:
                    return eCompassDirection.WEST;
            }

            return eCompassDirection.NORTH;
        }
        public static eCompassDirection ToAbsCompassDirection(eDiagonalCompassDirection direction)
        {
            switch (direction)
            {
                case eDiagonalCompassDirection.NORTHEAST:
                    return eCompassDirection.NORTHEAST;
                case eDiagonalCompassDirection.SOUTHEAST:
                    return eCompassDirection.SOUTHEAST;
                case eDiagonalCompassDirection.SOUTHWEST:
                    return eCompassDirection.SOUTHWEST;
                case eDiagonalCompassDirection.NORTHWEST:
                    return eCompassDirection.NORTHWEST;
            }
            return eCompassDirection.NORTHEAST;
        }

        public static eDiagonalCompassDirection RotateDiagonalCompassDirection(eDiagonalCompassDirection direction, eSquareRotation rotation, eRotationDirection rotDir)
        {
            int intRotation = (int)rotation * (rotDir == eRotationDirection.CLOCKWISE ? 1 : -1);
            int intDirection = (int)direction + intRotation;
            intDirection = EnumUtilities.NormalizeEnumIndex<eDiagonalCompassDirection>(intDirection);
            eDiagonalCompassDirection newDirection = (eDiagonalCompassDirection)intDirection;

            return newDirection;
        }
        public static eSquareCompassDirection RotateSquareCompassDirection(eSquareCompassDirection direction, eSquareRotation rotation, eRotationDirection rotDir)
        {
            int intRotation = (int)rotation * (rotDir == eRotationDirection.CLOCKWISE ? 1 : -1);
            int intDirection = (int)direction + intRotation;
            intDirection = EnumUtilities.NormalizeEnumIndex<eSquareCompassDirection>(intDirection);
            eSquareCompassDirection newDirection = (eSquareCompassDirection)intDirection;

            return newDirection;
        }

        public static float ConvertDirectionToCompassRotation(eCompassDirection direction)
        {
            switch (direction)
            {
                case eCompassDirection.NORTH:
                    return 0f;
                case eCompassDirection.NORTHEAST:
                    return 45f;
                case eCompassDirection.EAST:
                    return 90f;
                case eCompassDirection.SOUTHEAST:
                    return 135f;
                case eCompassDirection.SOUTH:
                    return 180f;
                case eCompassDirection.SOUTHWEST:
                    return 225f;
                case eCompassDirection.WEST:
                    return 270f;
                case eCompassDirection.NORTHWEST:
                    return 315f;
                default:
                    return 0f;
            }
        }
        public static float ConvertDirectionToCompassRotation(eSquareCompassDirection direction)
        {
            return ConvertDirectionToCompassRotation(ToAbsCompassDirection(direction));
        }
        public static float ConvertDirectionToCompassRotation(eDiagonalCompassDirection direction)
        {
            return ConvertDirectionToCompassRotation(ToAbsCompassDirection(direction));
        }

        public static eSquareRotation ConvertSquareDirectionToRotation(eSquareCompassDirection direction)
        {
            switch (direction)
            {
                case eSquareCompassDirection.NORTH:
                    return eSquareRotation.ROTATION_0;
                case eSquareCompassDirection.EAST:
                    return eSquareRotation.ROTATION_90;
                case eSquareCompassDirection.SOUTH:
                    return eSquareRotation.ROTATION_180;
                case eSquareCompassDirection.WEST:
                    return eSquareRotation.ROTATION_270;
                default:
                    return eSquareRotation.ROTATION_0;
            }
        }

        public static eSquareCompassDirection GetSquareDirectionBetweenTiles(TileCoordinate tile1, TileCoordinate tile2)
        {
            if (tile2.Row < tile1.Row)
                return eSquareCompassDirection.NORTH;

            if (tile2.Col > tile1.Col)
                return eSquareCompassDirection.EAST;

            if (tile2.Row > tile1.Row)
                return eSquareCompassDirection.SOUTH;

            if (tile2.Col < tile1.Col)
                return eSquareCompassDirection.WEST;

            return eSquareCompassDirection.NORTH;
        }
    }
}
