﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    static class EnumUtilities
    {
        // -------- Public Methods --------
        public static T[] GetEnumValues<T>()
        {
            return (T[])Enum.GetValues(typeof(T));
        }

        public static int NormalizeEnumIndex<T>(int index)
        {
            int enumCount = GetEnumValues<T>().Count();

            int normalizedIndex = index % enumCount;

            while (normalizedIndex < 0)
                normalizedIndex += enumCount;

            return normalizedIndex;
        }
    }
}
