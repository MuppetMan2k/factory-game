﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    static class GameEntityUtilities
    {
        // -------- Public Methods --------
        public static TileCoordinate GetGameEntityTileCoordinate(GameEntity entity)
        {
            AreaEntity areaEntity = entity as AreaEntity;
            if (areaEntity != null)
                return areaEntity.AreaCoordinate.Coordinate;

            CharacterEntity charEntity = entity as CharacterEntity;
            if (charEntity != null)
                return charEntity.CharacterCoordinate.GetApproxTileCoordinate();

            TileEntity tileEntity = entity as TileEntity;
            if (tileEntity != null)
                return tileEntity.TileCoordinate;

            WallEntity wallEntity = entity as WallEntity;
            if (wallEntity != null)
                return WallUtilities.GetTileCoordAdjacentToWallFace(new WallFaceCoordinate(wallEntity.WallCoordinate, eWallFace.INSIDE));

            WallMountEntity wallMountEntity = entity as WallMountEntity;
            if (wallMountEntity != null)
                return WallUtilities.GetTileCoordAdjacentToWallFace(WallUtilities.GetEffectiveWallFaceCoordinateForWallMount(wallMountEntity.WallMountCoordinate));

            MiniTileEntity miniTileEntity = entity as MiniTileEntity;
            if (miniTileEntity != null)
                return miniTileEntity.MiniTileCoordinate.TileCoordinate;

            return new TileCoordinate();
        }

        public static Vector3 GetGameEntityBubbleAnchorPointGS(GameEntity entity)
        {
            Vector2 ts = GetGameEntityCentralTs(entity);
            return new Vector3(ts.X, 1.5f, ts.Y);
        }
        public static Vector2 GetGameEntityCentralTs(GameEntity entity)
        {
            AreaEntity areaEntity = entity as AreaEntity;
            if (areaEntity != null)
                return AreaUtilities.GetAreaCentralTs(areaEntity.AreaCoordinate);

            CharacterEntity charEntity = entity as CharacterEntity;
            if (charEntity != null)
                return charEntity.CharacterCoordinate.GetCentralTileSpacePosition();

            TileEntity tileEntity = entity as TileEntity;
            if (tileEntity != null)
                return TileUtilities.GetCentralTsPosForTile(tileEntity.TileCoordinate);

            WallEntity wallEntity = entity as WallEntity;
            if (wallEntity != null)
                return WallUtilities.GetWallCentralTsPos(wallEntity.WallCoordinate);

            WallMountEntity wallMountEntity = entity as WallMountEntity;
            if (wallMountEntity != null)
                return WallUtilities.GetWallCentralTsPos(wallMountEntity.WallMountCoordinate.WallCoordinate);

            MiniTileEntity miniTileEntity = entity as MiniTileEntity;
            if (miniTileEntity != null)
                return TileUtilities.GetMiniTileTsPos(miniTileEntity.MiniTileCoordinate);

            return new Vector2();
        }

        public static eSquareRotation GetGameEntityRotation(GameEntity entity)
        {
            AreaEntity areaEntity = entity as AreaEntity;
            if (areaEntity != null)
                return areaEntity.AreaCoordinate.Rotation;

            CharacterEntity charEntity = entity as CharacterEntity;
            if (charEntity != null)
                return charEntity.CharacterCoordinate.Rotation;

            TileEntity tileEntity = entity as TileEntity;
            if (tileEntity != null)
                return eSquareRotation.ROTATION_0;

            WallEntity wallEntity = entity as WallEntity;
            if (wallEntity != null)
            {
                if (wallEntity.WallCoordinate.Direction == eWallDirection.NORTH)
                    return eSquareRotation.ROTATION_0;
                else
                    return eSquareRotation.ROTATION_270;
            }

            WallMountEntity wallMountEntity = entity as WallMountEntity;
            if (wallMountEntity != null)
            {
                if (wallMountEntity.WallMountCoordinate.WallCoordinate.Direction == eWallDirection.NORTH)
                {
                    if (wallMountEntity.WallMountCoordinate.Location == eWallMountLocation.OUTSIDE)
                        return eSquareRotation.ROTATION_180;
                    else
                        return eSquareRotation.ROTATION_0;
                }
                else
                {
                    if (wallMountEntity.WallMountCoordinate.Location == eWallMountLocation.OUTSIDE)
                        return eSquareRotation.ROTATION_90;
                    else
                        return eSquareRotation.ROTATION_270;
                }
            }

            return eSquareRotation.ROTATION_0;
        }

        public static string GetGameEntityNameLocId(GameEntity entity)
        {
            Machine machine = entity as Machine;
            if (machine != null)
                return machine.NameLocId;

            return "";
        }

        public static List<ElementCarrying> GetGameEntityElementCarryings(GameEntity entity)
        {
            var carryings = new List<ElementCarrying>();

            Staff staff = entity as Staff;
            if (staff != null && StaffManager.StaffCanLoadElements(staff.Type))
            {
                var c = new ElementCarrying();
                c.LocId = "text_elemcarry_carry";
                c.NumElementHolders = 1;
                c.ElementHolders = new List<ElementHolder>();
                c.ElementHolders.Add(staff.CarryElement);
                c.Limitless = false;
                carryings.Add(c);
            }

            Machine machine = entity as Machine;
            if (machine != null)
            {
                var cIn = new ElementCarrying();
                cIn.LocId = "text_elemcarry_machine_in";
                cIn.NumElementHolders = machine.InElementStore.GetVisibleElementHolderNumber();
                cIn.ElementHolders = machine.InElementStore.ElementHolders;
                cIn.Limitless = false;
                carryings.Add(cIn);

                var cOut = new ElementCarrying();
                cOut.LocId = "text_elemcarry_machine_out";
                cOut.NumElementHolders = machine.OutElementStore.GetVisibleElementHolderNumber();
                cOut.ElementHolders = machine.OutElementStore.ElementHolders;
                cOut.Limitless = false;
                carryings.Add(cOut);
            }

            FactoryPort port = entity as FactoryPort;
            if (port != null)
            {
                var c = new ElementCarrying();
                c.LocId = port.IsInPort ? "text_elemcarry_factory_in" : "text_elemcarry_factory_out";
                c.NumElementHolders = port.ElementStore.GetVisibleElementHolderNumber();
                c.ElementHolders = port.ElementStore.ElementHolders;
                c.Limitless = true;
                carryings.Add(c);
            }

            // TODO - Add further element-carrying entity cases here, e.g. shelves

            return carryings;
        }
    }
}
