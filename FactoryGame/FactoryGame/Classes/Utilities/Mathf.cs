﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    static class Mathf
    {
        // -------- Properties --------
        public static float Pi
        {
            get
            {
                return (float)Math.PI;
            }
        }

        // -------- Public Methods --------
        public static float ToRads(float deg)
        {
            return MathHelper.ToRadians(deg);
        }
        public static float ToDeg(float rads)
        {
            return MathHelper.ToDegrees(rads);
        }

        public static float Cos(float val)
        {
            return (float)Math.Cos((float)val);
        }
        public static float Sin(float val)
        {
            return (float)Math.Sin((float)val);
        }
        public static float Tan(float val)
        {
            return (float)Math.Tan((float)val);
        }
        public static float Atan2(float y, float x)
        {
            return (float)Math.Atan2((float)y, (float)x);
        }

        public static float Sqrt(float val)
        {
            return (float)Math.Sqrt((double)val);
        }
        public static float SafeSqrt(float val)
        {
            if (val <= 0)
                return 0f;

            return Sqrt(val);
        }

        public static float Square(float val)
        {
            return val * val;
        }

        public static float Clamp(float val, float min, float max)
        {
            if (val > max)
                return max;
            if (val < min)
                return min;

            return val;
        }
        public static int Clamp(int val, int min, int max)
        {
            if (val > max)
                return max;
            if (val < min)
                return min;

            return val;
        }
        public static float Clamp01(float val)
        {
            return Clamp(val, 0f, 1f);
        }
        public static int Clamp01(int val)
        {
            return Clamp(val, 0, 1);
        }
        public static float ClampUpper(float val, float max)
        {
            if (val > max)
                return max;

            return val;
        }
        public static int ClampUpper(int val, int max)
        {
            if (val > max)
                return max;

            return val;
        }
        public static float ClampLower(float val, float min)
        {
            if (val < min)
                return min;

            return val;
        }
        public static int ClampLower(int val, int min)
        {
            if (val < min)
                return min;

            return val;
        }

        public static float Max(params float[] vals)
        {
            if (vals.Length == 0)
                return 0f;

            float max = vals[0];

            for (int i = 1; i < vals.Length; i++)
                if (vals[i] > max)
                    max = vals[i];

            return max;
        }
        public static int Max(params int[] vals)
        {
            if (vals.Length == 0)
                return 0;

            int max = vals[0];

            for (int i = 1; i < vals.Length; i++)
                if (vals[i] > max)
                    max = vals[i];

            return max;
        }
        public static float Min(params float[] vals)
        {
            if (vals.Length == 0)
                return 0f;

            float min = vals[0];

            for (int i = 1; i < vals.Length; i++)
                if (vals[i] < min)
                    min = vals[i];

            return min;
        }
        public static int Min(params int[] vals)
        {
            if (vals.Length == 0)
                return 0;

            int min = vals[0];

            for (int i = 1; i < vals.Length; i++)
                if (vals[i] < min)
                    min = vals[i];

            return min;
        }

        public static float Average(params float[] vals)
        {
            if (vals.Length == 0)
                return 0f;

            float total = 0;
            for (int i = 0; i < vals.Length; i++)
                total += vals[i];

            return total / vals.Length;
        }

        public static int Round(float val)
        {
            float remainder = val % 1f;

            if (remainder >= 0.5f)
                return (int)val + 1;
            else
                return (int)val;
        }
        public static int Floor(float val)
        {
            return (int)val;
        }
        public static int Ceiling(float val)
        {
            int valInt = (int)val;

            if (val % 1 == 0)
                return valInt;

            return valInt + 1;
        }

        public static bool IsEven(int val)
        {
            return (val % 2 == 0);
        }
        public static bool IsOdd(int val)
        {
            return (val % 2 != 0);
        }

        public static float Lerp(float val1, float val2, float interp)
        {
            return (val1 + interp * (val2 - val1));
        }

        public static float Abs(float val)
        {
            return Math.Abs(val);
        }
        public static int Abs(int val)
        {
            return Math.Abs(val);
        }

        public static float Barycentric(float val1, float val2, float val3, float amount1, float amount2)
        {
            return MathHelper.Barycentric(val1, val2, val3, amount1, amount2);
        }
        public static void GetBarycentricAmounts(Vector2 vert1, Vector2 vert2, Vector2 vert3, Vector2 point, out float amount1, out float amount2, out float amount3)
        {
            amount1 = ((vert2.Y - vert3.Y) * (point.X - vert3.X) + (vert3.X - vert2.X) * (point.Y - vert3.Y)) / ((vert2.Y - vert3.Y) * (vert1.X - vert3.X) + (vert3.X - vert2.X) * (vert1.Y - vert3.Y));
            amount2 = ((vert3.Y - vert1.Y) * (point.X - vert3.X) + (vert1.X - vert3.X) * (point.Y - vert3.Y)) / ((vert2.Y - vert3.Y) * (vert1.X - vert3.X) + (vert3.X - vert2.X) * (vert1.Y - vert3.Y));
            amount3 = 1 - amount1 - amount2;
        }
    }
}
