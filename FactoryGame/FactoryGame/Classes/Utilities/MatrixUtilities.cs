﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    static class MatrixUtilities
    {
        // -------- Public Methods --------
        public static Matrix CreateRotationX(float degrees)
        {
            return Matrix.CreateRotationX(Mathf.ToRads(degrees));
        }
        public static Matrix CreateRotationY(float degrees)
        {
            return Matrix.CreateRotationY(Mathf.ToRads(degrees));
        }
        public static Matrix CreateRotationZ(float degrees)
        {
            return Matrix.CreateRotationZ(Mathf.ToRads(degrees));
        }
        public static Matrix CreateRotationAxis(Vector3 axis, float degrees)
        {
            return Matrix.CreateFromAxisAngle(axis, Mathf.ToRads(degrees));
        }

        public static Matrix CreateTranslationMatrix(Vector3 vec)
        {
            return Matrix.CreateTranslation(vec);
        }
        public static Matrix CreateTranslationMatrix(Vector2 tsPos)
        {
            return CreateTranslationMatrix(tsPos.X, tsPos.Y);
        }
        public static Matrix CreateTranslationMatrix(float x, float y)
        {
            return Matrix.CreateTranslation(x, 0f, y);
        }
        public static Matrix CreateTranslationMatrix(TileCoordinate coord)
        {
            return CreateTranslationMatrix((float)coord.Col, (float)coord.Row);
        }

        public static Matrix CreateRotationMatrix(eSquareRotation rotation)
        {
            switch (rotation)
            {
                case eSquareRotation.ROTATION_0:
                    return Matrix.Identity;
                case eSquareRotation.ROTATION_90:
                    return CreateCompassSpaceRotation(90f);
                case eSquareRotation.ROTATION_180:
                    return CreateCompassSpaceRotation(180f);
                case eSquareRotation.ROTATION_270:
                    return CreateCompassSpaceRotation(-90f);
                default:
                    return Matrix.Identity;
            }
        }

        public static Matrix CreateCompassSpaceRotation(float degrees)
        {
            return CreateRotationY(-degrees);
        }

        public static Matrix CreateScale(float x, float y, float z)
        {
            return Matrix.CreateScale(x, y, z);
        }
    }
}
