﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    static class SpaceUtilities
    {
        // -------- Public Methods --------
        public static Vector2 ConvertGsToTs(Vector3 gsVec)
        {
            return new Vector2(gsVec.X, gsVec.Z);
        }
        public static Vector3 ConvertTsToGs(Vector2 tsVec)
        {
            return new Vector3(tsVec.X, 0f, tsVec.Y);
        }

        public static Vector2 ConvertPsToSs(Vector2 psVec)
        {
            float x = (2f * psVec.X / (float)GraphicsManager.ScreenWidth) - 1f;
            float y = 1f - (2f * psVec.Y / (float)GraphicsManager.ScreenHeight);

            return new Vector2(x, y);
        }
        public static Vector2 ConvertPsToSs(Point psPoint)
        {
            return ConvertPsToSs(new Vector2((float)psPoint.X, (float)psPoint.Y));
        }
        public static Vector2 ConvertSsToPs(Vector2 ssVec)
        {
            float x = (float)GraphicsManager.ScreenWidth * 0.5f * (ssVec.X + 1f);
            float y = (float)GraphicsManager.ScreenHeight * 0.5f * (1f - ssVec.Y);

            return new Vector2(x, y);
        }
        public static Point ConvertSsToPsPoint(Vector2 ssVec)
        {
            Vector2 psVec = ConvertSsToPs(ssVec);
            Point psPoint = new Point((int)psVec.X, (int)psVec.Y);
            return psPoint;
        }

        public static Vector2 Convert3DSsVecTo2D(Vector3 ssVec)
        {
            return new Vector2(ssVec.X, ssVec.Y);
        }
        public static Vector3 Convert2DSsVecTo3D(Vector2 ssVec)
        {
            return new Vector3(ssVec.X, ssVec.Y, 0f);
        }

        public static Vector2 ConvertTileCoordToTs(TileCoordinate coord)
        {
            return new Vector2(coord.Col, coord.Row);
        }

        public static Vector2 ConvertGsToSs(Vector3 gsVec)
        {
            Matrix transform = CameraManager.ViewMatrix * CameraManager.ProjectionMatrix;
            Vector3 ssVec = Vector3.Transform(gsVec, transform);

            return new Vector2(ssVec.X, ssVec.Y);
        }
        public static Point ConvertGsToPsPoint(Vector3 gsVec)
        {
            return ConvertSsToPsPoint(ConvertGsToSs(gsVec));
        }
    }
}
