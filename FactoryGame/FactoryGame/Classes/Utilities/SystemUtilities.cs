﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    static class SystemUtilities
    {
        // -------- Public Methods --------
        public static string GetApplicationPath()
        {
            string path = System.Reflection.Assembly.GetExecutingAssembly().Location;
            return System.IO.Path.GetDirectoryName(path);
        }
    }
}
