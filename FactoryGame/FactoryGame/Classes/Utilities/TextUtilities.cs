﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    static class TextUtilities
    {
        // -------- Public Methods --------
        public static string GetPercentageString(float fraction)
        {
            int percentage = (int)(fraction * 100f);
            string percString = string.Format(Localization.LocalizeString("text_percentage"), percentage.ToString());
            return percString;
        }
    }
}
