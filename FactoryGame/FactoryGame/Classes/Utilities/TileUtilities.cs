﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    static class TileUtilities
    {
        // -------- Public Methods --------
        public static TileCoordinate GetOffsetTile(TileCoordinate tile, eCompassDirection direction, int offset = 1)
        {
            TileCoordinate offsetTile = tile;

            switch (direction)
            {
                case eCompassDirection.NORTH:
                    offsetTile.Row -= offset;
                    break;
                case eCompassDirection.NORTHEAST:
                    offsetTile.Row -= offset;
                    offsetTile.Col += offset;
                    break;
                case eCompassDirection.EAST:
                    offsetTile.Col += offset;
                    break;
                case eCompassDirection.SOUTHEAST:
                    offsetTile.Row += offset;
                    offsetTile.Col += offset;
                    break;
                case eCompassDirection.SOUTH:
                    offsetTile.Row += offset;
                    break;
                case eCompassDirection.SOUTHWEST:
                    offsetTile.Row += offset;
                    offsetTile.Col -= offset;
                    break;
                case eCompassDirection.WEST:
                    offsetTile.Col -= offset;
                    break;
                case eCompassDirection.NORTHWEST:
                    offsetTile.Row -= offset;
                    offsetTile.Col -= offset;
                    break;
            }

            return offsetTile;
        }
        public static TileCoordinate GetOffsetTile(TileCoordinate tile, eSquareCompassDirection direction, int offset = 1)
        {
            return GetOffsetTile(tile, CompassUtilities.ToAbsCompassDirection(direction), offset);
        }
        public static TileCoordinate GetOffsetTile(TileCoordinate tile, eDiagonalCompassDirection direction, int offset = 1)
        {
            return GetOffsetTile(tile, CompassUtilities.ToAbsCompassDirection(direction), offset);
        }

        public static Vector2 AddRotatedTsPositionToTsOrigin(Vector2 origin, Vector2 addition, eSquareRotation rotation)
        {
            switch (rotation)
            {
                case eSquareRotation.ROTATION_0:
                    origin += addition;
                    break;
                case eSquareRotation.ROTATION_90:
                    origin.X -= addition.Y;
                    origin.Y += addition.X;
                    break;
                case eSquareRotation.ROTATION_180:
                    origin.X -= addition.X;
                    origin.Y -= addition.Y;
                    break;
                case eSquareRotation.ROTATION_270:
                    origin.X += addition.Y;
                    origin.Y -= addition.X;
                    break;
                default:
                    break;
            }

            return origin;
        }

        public static TileCoordinate AddRotatedTileToTileOrigin(TileCoordinate origin, TileCoordinate addition, eSquareRotation rotation)
        {
            switch (rotation)
            {
                case eSquareRotation.ROTATION_0:
                    origin.Row += addition.Row;
                    origin.Col += addition.Col;
                    break;
                case eSquareRotation.ROTATION_90:
                    origin.Col -= addition.Row;
                    origin.Row += addition.Col;
                    break;
                case eSquareRotation.ROTATION_180:
                    origin.Col -= addition.Col;
                    origin.Row -= addition.Row;
                    break;
                case eSquareRotation.ROTATION_270:
                    origin.Col += addition.Row;
                    origin.Row -= addition.Col;
                    break;
                default:
                    break;
            }

            return origin;
        }

        public static Vector2 GetInteractionTsPosition(TileCoordinate entityCoord, eSquareRotation entityRotation, Vector2 relativeInteractionPosition)
        {
            Vector2 coord = GetTileCornerTsPosition(entityCoord, entityRotation);
            return AddRotatedTsPositionToTsOrigin(coord, relativeInteractionPosition, entityRotation);
        }
        public static eSquareRotation GetInteractionRotation(eSquareRotation entityRotation, eSquareRotation relativeInteractionRotation)
        {
            int index = (int)entityRotation + (int)relativeInteractionRotation;
            index = EnumUtilities.NormalizeEnumIndex<eSquareRotation>(index);

            return (eSquareRotation)index;
        }

        public static Vector2 GetTileCornerTsPosition(TileCoordinate coord, eSquareRotation tileRotation)
        {
            Vector2 tsPos = SpaceUtilities.ConvertTileCoordToTs(coord);

            switch (tileRotation)
            {
                case eSquareRotation.ROTATION_0:
                    break;
                case eSquareRotation.ROTATION_90:
                    tsPos.X++;
                    break;
                case eSquareRotation.ROTATION_180:
                    tsPos.X++;
                    tsPos.Y++;
                    break;
                case eSquareRotation.ROTATION_270:
                    tsPos.Y++;
                    break;
                default:
                    break;
            }

            return tsPos;
        }

        public static Vector2 GetCentralTsPosForTile(TileCoordinate coord)
        {
            return new Vector2(coord.Col + 0.5f, coord.Row + 0.5f);
        }
        public static Vector2 GetCentralTsPosForTile(Vector2 tileTsPos)
        {
            return (tileTsPos + new Vector2(0.5f));
        }

        public static Vector2 GetMiniTileTsPos(MiniTileCoordinate coord)
        {
            Vector2 ts = SpaceUtilities.ConvertTileCoordToTs(coord.TileCoordinate);
            ts += coord.Position;

            return ts;
        }
    }
}
