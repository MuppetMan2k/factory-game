﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryGame
{
    static class TimeUtilities
    {
        // -------- Public Methods --------
        public static float ConvertSecondsToHours(float seconds)
        {
            return seconds / 3600f;
        }

        public static TimeSpan ConvertSecondsToTimeSpan(float seconds)
        {
            return new TimeSpan(0, 0, 0, 0, (int)(seconds * 1000f));
        }

        public static string GetShortDayName(eDay day)
        {
            switch (day)
            {
                case eDay.MONDAY:
                    return Localization.LocalizeString("text_day_short_monday");
                case eDay.TUESDAY:
                    return Localization.LocalizeString("text_day_short_tuesday");
                case eDay.WEDNESDAY:
                    return Localization.LocalizeString("text_day_short_wednesday");
                case eDay.THURSDAY:
                    return Localization.LocalizeString("text_day_short_thursday");
                case eDay.FRIDAY:
                    return Localization.LocalizeString("text_day_short_friday");
                case eDay.SATURDAY:
                    return Localization.LocalizeString("text_day_short_saturday");
                case eDay.SUNDAY:
                    return Localization.LocalizeString("text_day_short_sunday");
                default:
                    return "DAY NOT RECOGNIZED!";
            }
        }
    }
}
