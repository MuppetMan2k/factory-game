﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    static class TriangleUtilities
    {
        // -------- Public Methods --------
        public static void GetSsTriangleBarycentricAmounts(Triangle triangle, Vector2 pos, out float amount1, out float amount2, out float amount3)
        {
            Vector2 vert1 = SpaceUtilities.Convert3DSsVecTo2D(triangle.Vertex1);
            Vector2 vert2 = SpaceUtilities.Convert3DSsVecTo2D(triangle.Vertex2);
            Vector2 vert3 = SpaceUtilities.Convert3DSsVecTo2D(triangle.Vertex3);
            Mathf.GetBarycentricAmounts(vert1, vert2, vert3, pos, out amount1, out amount2, out amount3);
        }

        public static bool PositionIsWithinSsTriangle(Triangle triangle, Vector2 pos)
        {
            float amount1;
            float amount2;
            float amount3;

            GetSsTriangleBarycentricAmounts(triangle, pos, out amount1, out amount2, out amount3);

            if (amount1 < 0f || amount1 > 1f)
                return false;

            if (amount2 < 0f || amount2 > 1f)
                return false;

            if (amount3 < 0f || amount3 > 1f)
                return false;

            return true;
        }
    }
}
