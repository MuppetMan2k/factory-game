﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FactoryGame
{
    static class UIUtilities
    {
        // -------- Public Methods --------
        public static Point GetRectangleAnchorPosition(Rectangle rectangle, eUIAnchor anchor)
        {
            switch (anchor)
            {
                case eUIAnchor.TOP_LEFT:
                    return new Point(rectangle.Left, rectangle.Top);
                case eUIAnchor.TOP_CENTER:
                    return new Point(rectangle.Center.X, rectangle.Top);
                case eUIAnchor.TOP_RIGHT:
                    return new Point(rectangle.Right, rectangle.Top);
                case eUIAnchor.CENTER_LEFT:
                    return new Point(rectangle.Left, rectangle.Center.Y);
                case eUIAnchor.CENTER_CENTER:
                    return rectangle.Center;
                case eUIAnchor.CENTER_RIGHT:
                    return new Point(rectangle.Right, rectangle.Center.Y);
                case eUIAnchor.BOTTOM_LEFT:
                    return new Point(rectangle.Left, rectangle.Bottom);
                case eUIAnchor.BOTTOM_CENTER:
                    return new Point(rectangle.Center.X, rectangle.Bottom);
                case eUIAnchor.BOTTOM_RIGHT:
                    return new Point(rectangle.Right, rectangle.Bottom);
                default:
                    return new Point(rectangle.Left, rectangle.Top);
            }
        }

        public static bool PointIsInRectangle(Point point, Rectangle rectangle)
        {
            if (point.X < rectangle.Left)
                return false;

            if (point.X > rectangle.Right)
                return false;

            if (point.Y < rectangle.Top)
                return false;

            if (point.Y > rectangle.Bottom)
                return false;

            return true;
        }

        public static bool PointIsOverTexturedRectangle(Point point, Rectangle rectangle, Texture2D texture)
        {
            if (!PointIsInRectangle(point, rectangle))
                return false;

            Point relativePoint = new Point(point.X - rectangle.Left, point.Y - rectangle.Top);

            return (!TexturePixelIsTransparent(texture, relativePoint));
        }

        public static bool TexturePixelIsTransparent(Texture2D texture, Point pixelUV)
        {
            if (pixelUV.X < 0)
                return true;
            else if (pixelUV.X >= texture.Width)
                return true;
            if (pixelUV.Y < 0)
                return true;
            else if (pixelUV.Y >= texture.Height)
                return true;
            
            Color[] pixelData = new Color[texture.Width * texture.Height];
            texture.GetData<Color>(pixelData);

            int index = texture.Width * pixelUV.Y + pixelUV.X;

            return (pixelData[index].A == 0);
        }

        public static List<string> ConvertTextToLines(string text, int maxWidth, int maxLines, SpriteFont font)
        {
            List<string> lines = new List<string>();

            if (string.IsNullOrEmpty(text))
                return lines;

            string[] words = text.Split(new char[] { ' ' });

            string line = "";
            int wordsInLine = 0;
            for (int w = 0; w < words.Length; w++)
            {
                string lineWithNextWord = line;
                if (wordsInLine > 0)
                    lineWithNextWord += ' ';
                lineWithNextWord += words[w];

                if (font.MeasureString(lineWithNextWord).X <= maxWidth)
                {
                    // Can add the word
                    line = lineWithNextWord;
                    wordsInLine++;
                    continue;
                }

                if (wordsInLine == 0)
                {
                    // Single word went over max width, start new line
                    lines.Add(lineWithNextWord);
                    line = "";
                    if (lines.Count == maxLines && maxLines > 0)
                        return lines;
                }

                // Need to finish line without word and shift word to next line
                lines.Add(line);
                w--;
                line = "";
                wordsInLine = 0;
                if (lines.Count == maxLines && maxLines > 0)
                    return lines;
            }

            // Check for final unfinished line
            if (wordsInLine > 0)
                lines.Add(line);

            return lines;
        }

        public static int CalculateBoundedAlignedTextLeftEdge(int boundLeft, int boundWidth, int textWidth, eTextAlignment alignment)
        {
            switch (alignment)
            {
                case eTextAlignment.LEFT:
                    return boundLeft;
                case eTextAlignment.RIGHT:
                    return boundLeft + (boundWidth - textWidth);
                case eTextAlignment.CENTER:
                    return boundLeft + (boundWidth - textWidth) / 2;
                default:
                    return boundLeft;
            }
        }

        public static int GetCornerRoundSize(eCornerRoundSize roundSize)
        {
            switch (roundSize)
            {
                case eCornerRoundSize.RADIUS_10:
                    return 10;
                case eCornerRoundSize.RADIUS_20:
                    return 20;
                default:
                    return 0;
            }
        }
        public static string GetCornerRoundTextureId(eCornerRoundSize roundSize)
        {
            switch (roundSize)
            {
                case eCornerRoundSize.RADIUS_10:
                    return "port-corner-10";
                case eCornerRoundSize.RADIUS_20:
                    return "port-corner-20";
                default:
                    return "port-corner-10";
            }
        }

        public static int GetIconSize(eIconSize iconSize)
        {
            switch (iconSize)
            {
                case eIconSize.ICON_20:
                    return 20;
                case eIconSize.ICON_25:
                    return 25;
                case eIconSize.ICON_30:
                    return 30;
                case eIconSize.ICON_40:
                    return 40;
                case eIconSize.ICON_50:
                    return 50;
                default:
                    return 20;
            }
        }
        public static string GetIconTextureId(eIconSize iconSize)
        {
            switch (iconSize)
            {
                case eIconSize.ICON_20:
                    return "icon-20";
                case eIconSize.ICON_25:
                    return "icon-25";
                case eIconSize.ICON_30:
                    return "icon-30";
                case eIconSize.ICON_40:
                    return "icon-40";
                case eIconSize.ICON_50:
                    return "icon-50";
                default:
                    return "icon-20";
            }
        }
    }
}
