﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    static class VectorUtilities
    {
        // -------- Public Methods --------
        public static Vector3 RotateCompassRotation(Vector3 vec, float degrees)
        {
            Matrix transform = MatrixUtilities.CreateCompassSpaceRotation(degrees);
            return Vector3.Transform(vec, transform);
        }
        public static Vector3 RotateVectorAboutX(Vector3 vec, float degrees)
        {
            Matrix transform = MatrixUtilities.CreateRotationX(degrees);
            return Vector3.Transform(vec, transform);
        }
        public static Vector3 RotateVectorAboutY(Vector3 vec, float degrees)
        {
            Matrix transform = MatrixUtilities.CreateRotationY(degrees);
            return Vector3.Transform(vec, transform);
        }
        public static Vector3 RotateVectorAboutZ(Vector3 vec, float degrees)
        {
            Matrix transform = MatrixUtilities.CreateRotationZ(degrees);
            return Vector3.Transform(vec, transform);
        }
        public static Vector3 RotateVectorAboutAxis(Vector3 vec, Vector3 axis, float degrees)
        {
            Matrix transform = MatrixUtilities.CreateRotationAxis(axis, degrees);
            return Vector3.Transform(vec, transform);
        }
    }
}
