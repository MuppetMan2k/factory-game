﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FactoryGame
{
    static class WallUtilities
    {
        // -------- Public Methods --------
        public static TileCoordinate GetTileCoordAdjacentToWallFace(WallFaceCoordinate coord)
        {
            TileCoordinate tile = coord.WallCoordinate.Coordinate;

            if (coord.Face == eWallFace.OUTSIDE)
            {
                if (coord.WallCoordinate.Direction == eWallDirection.NORTH)
                    tile.Row--;
                else
                    tile.Col--;
            }

            return tile;
        }
        
        public static WallCoordinate GetWallCoordAdjacentToTile(TileCoordinate coord, eSquareCompassDirection direction)
        {
            switch (direction)
            {
                case eSquareCompassDirection.NORTH:
                    return new WallCoordinate(coord, eWallDirection.NORTH);
                case eSquareCompassDirection.EAST:
                    coord.Col++;
                    return new WallCoordinate(coord, eWallDirection.WEST);
                case eSquareCompassDirection.SOUTH:
                    coord.Row++;
                    return new WallCoordinate(coord, eWallDirection.NORTH);
                case eSquareCompassDirection.WEST:
                    return new WallCoordinate(coord, eWallDirection.WEST);
                default:
                    return new WallCoordinate(coord, eWallDirection.NORTH);
            }
        }

        public static Vector2 GetWallCentralTsPos(WallCoordinate coord)
        {
            Vector2 ts = SpaceUtilities.ConvertTileCoordToTs(coord.Coordinate);
            if (coord.Direction == eWallDirection.NORTH)
                ts.X += 0.5f;
            else
                ts.Y += 0.5f;

            return ts;
        }

        public static int ConvertWallDirectionToMapIndex(eWallDirection direction)
        {
            return (int)direction;
        }

        public static eWallFace GetEffectiveWallFaceForWallMountLocation(eWallMountLocation location)
        {
            if (location == eWallMountLocation.OUTSIDE)
                return eWallFace.OUTSIDE;

            return eWallFace.INSIDE;
        }

        public static WallFaceCoordinate GetEffectiveWallFaceCoordinateForWallMount(WallMountCoordinate coord)
        {
            eWallFace face = GetEffectiveWallFaceForWallMountLocation(coord.Location);
            return new WallFaceCoordinate(coord.WallCoordinate, face);
        }
    }
}
