﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;

namespace FactoryGame
{
    static class XMLReader
    {
        // -------- Public Methods --------
        public static T ReadFromXml<T>(string path)
        {
            var reader = new XmlSerializer(typeof(T));
            var file = new StreamReader(path);

            T newObj;
            try
            {
                newObj = (T)reader.Deserialize(file);
            }
            catch
            {
                newObj = default(T);
            }

            return newObj;
        }
    }
}
