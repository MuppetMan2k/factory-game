﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;

namespace FactoryGame
{
    static class XMLWriter
    {
        // -------- Public Methods --------
        public static void WriteToXml<T>(string path, T objToWrite)
        {
            var writer = new XmlSerializer(typeof(T));
            var file = new StreamWriter(path);

            writer.Serialize(file, objToWrite);
            file.Close();
        }
    }
}

