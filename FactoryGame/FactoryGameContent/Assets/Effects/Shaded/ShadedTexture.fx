float4x4 xWorld;
float4x4 xView;
float4x4 xProjection;

Texture xTexture;
sampler TextureSampler = sampler_state { texture = < xTexture > ; magfilter = LINEAR; minfilter = LINEAR; mipfilter = LINEAR; AddressU = mirror; AddressV = mirror; };

float3 xDirLightColor;
float xDirLightIntensity;
float3 xDirLightDir;

float3 xAmbLightColor;
float xAmbLightIntensity;

float xAlpha;
float xWhiteLerp;
float3 xForcedColor;

// -------- Technique-Agnostic --------
struct VertexShaderInput
{
	float4 Position : POSITION0;
	float4 Normal : NORMAL0;
	float2 TexCoord : TEXCOORD0;
};

struct VertexShaderOutput
{
	float4 Position : POSITION0;
	float4 Normal : NORMAL0;
	float2 TexCoord : TEXCOORD0;
};

VertexShaderOutput VertexFunction(VertexShaderInput input)
{
	VertexShaderOutput output;

	float4 worldPosition = mul(input.Position, xWorld);
	float4 viewPosition = mul(worldPosition, xView);
	output.Position = mul(viewPosition, xProjection);

	output.Normal = normalize(mul(input.Normal, xWorld));

	output.TexCoord = input.TexCoord;

	return output;
}

// -------- Technique: 'shaded' --------
float4 ShadedPixelFunction(VertexShaderOutput input) : COLOR0
{
	float dirIncidence = clamp(dot(-xDirLightDir, normalize(input.Normal)), 0, 1);
	float dirTntensity = xDirLightIntensity * dirIncidence;

	float3 multiplierColor = xDirLightColor * dirTntensity + xAmbLightColor * xAmbLightIntensity;
	multiplierColor.rgb = clamp(multiplierColor.rgb, 0, 1);

	float4 outColor = tex2D(TextureSampler, input.TexCoord);
	outColor.rgb *= multiplierColor.rgb;
	outColor.rgb = lerp(outColor.rgb, 1, xWhiteLerp);
	outColor.a = xAlpha;

	return outColor;
}

technique shaded
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 VertexFunction();
		PixelShader = compile ps_3_0 ShadedPixelFunction();
	}
}

// -------- Technique: 'silhouette' --------
float4 SilhouettePixelFunction(VertexShaderOutput input) : COLOR0
{
	return float4(0, 0, 0, 1);
}

technique silhouette
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 VertexFunction();
		PixelShader = compile ps_3_0 SilhouettePixelFunction();
	}
};

// -------- Technique: 'monochrome' --------
float4 MonochromePixelFunction(VertexShaderOutput input) : COLOR0
{
	float4 outColor = ShadedPixelFunction(input);
	outColor.r = (outColor.r + outColor.g + outColor.b) / 3;
	outColor.gb = outColor.r;

	return outColor;
}

technique monochrome
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 VertexFunction();
		PixelShader = compile ps_3_0 MonochromePixelFunction();
	}
}

// -------- Technique: 'forcedColor' --------
const float colorInfluence = 0.2;

float4 ForcedColorPixelFunction(VertexShaderOutput input) : COLOR0
{
	float4 outColor = ShadedPixelFunction(input);
	outColor.rgb = (1 - colorInfluence) * xForcedColor.rgb + colorInfluence * outColor.rgb;

	return outColor;

	/*float dirIncidence = clamp(dot(-xDirLightDir, input.Normal), 0, 1);
	float dirTntensity = xDirLightIntensity * dirIncidence;

	float3 multiplierColor = xDirLightColor * dirTntensity + xAmbLightColor * xAmbLightIntensity;
	multiplierColor.rgb = clamp(multiplierColor.rgb, 0, 1);
	multiplierColor.rgb = lerp(multiplierColor.rgb, 1, 0.5);

	float4 outColor;
	outColor.rgb = xForcedColor.rgb * multiplierColor.rgb;
	outColor.rgb = lerp(outColor.rgb, 1, xWhiteLerp);
	outColor.a = xAlpha;

	return outColor;*/
}

technique forcedColor
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 VertexFunction();
		PixelShader = compile ps_3_0 ForcedColorPixelFunction();
	}
}